# Body21

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**action** | **string** | Toogle is deprecated, use visibility instead | 
**selection** | **string** | Choose between: show/lock all routes, toggle all routes or hide/unlock all routes | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

