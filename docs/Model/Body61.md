# Body61

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**planning_id** | **int** | Planning ID | 
**type** | **string** | Action Name | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

