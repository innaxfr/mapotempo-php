# Body35

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ref** | **string** | A free reference, like an external ID from other information system (forbidden characters are ./\\) | [optional] 
**label** | **string** |  | [optional] 
**color** | **string** | Color code with #. Default: #000000. | [optional] 
**icon** | **string** | Icon name from font-awesome. Default: fa-circle. | [optional] 
**icon_size** | **string** | Icon size. Default: medium. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

