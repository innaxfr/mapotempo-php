# Body25

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**automatic_insert** | **bool** | If &#x60;true&#x60;, the best index in the route is automatically computed to have minimum impact on total route distance (without taking into account constraints like time_window_start/time_window_end, you have to start a new optimization if needed). | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

