# Body23

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ref** | **string** |  | [optional] 
**hidden** | **bool** |  | [optional] 
**locked** | **bool** |  | [optional] 
**color** | **string** | Color code with #. For instance: #FF0000. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

