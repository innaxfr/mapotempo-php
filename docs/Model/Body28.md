# Body28

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**replace** | **bool** |  | [optional] 
**file** | **string** | CSV file, encoding, separator and line return automatically detected, with localized CSV header according to HTTP header Accept-Language. | [optional] 
**stores** | [**\Mapotempo\Model\V01Store[]**](V01Store.md) | In mutual exclusion with CSV file upload. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

