# Body41

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**replace_vehicles** | **bool** |  | [optional] 
**file** | **string** | CSV file, encoding, separator and line return automatically detected, with localized CSV header according to HTTP header Accept-Language. | [optional] 
**vehicle_usage_sets** | [**\Mapotempo\Model\V01VehicleUsageSet[]**](V01VehicleUsageSet.md) | In mutual exclusion with CSV file upload. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

