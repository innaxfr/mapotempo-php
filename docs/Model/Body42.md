# Body42

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | 
**store_start_id** | **int** |  | [optional] 
**store_stop_id** | **int** |  | [optional] 
**store_rest_id** | **int** |  | [optional] 
**max_distance** | **int** | Maximum achievable distance in meters | [optional] 
**cost** | **double** | Cost of the vehicle plus the cost of the structure | [optional] 
**cost_time** | **double** | Cost of service per hour | [optional] 
**cost_distance** | **double** | Kilometre/Miles cost | [optional] 
**time_window_start** | **string** | Schedule time (HH:MM) or number of seconds since midnight | 
**time_window_end** | **string** | Schedule time (HH:MM) or number of seconds since midnight | 
**service_time_start** | **string** | Schedule time (HH:MM) or number of seconds since midnight | [optional] 
**service_time_end** | **string** | Schedule time (HH:MM) or number of seconds since midnight | [optional] 
**work_time** | **string** | Work time (HH:MM:SS) or number of seconds | [optional] 
**rest_start** | **string** | Schedule time (HH:MM) or number of seconds since midnight | [optional] 
**rest_stop** | **string** | Schedule time (HH:MM) or number of seconds since midnight | [optional] 
**rest_duration** | **string** | Rest duration (HH:MM:SS) or number of seconds | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

