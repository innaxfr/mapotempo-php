# Body24

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**with_geojson** | **string** | Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. | [optional] [default to 'false']

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

