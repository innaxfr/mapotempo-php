# Body26

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**details** | **bool** | Output Route Details | [optional] 
**synchronous** | **bool** | Synchronous processing or not. If not, operation will return nothing and the optimum result will be set on planning as soon &#x60;Customer.job_optimizer_id&#x60; will be null. | [optional] [default to true]
**active_only** | **bool** | If true only active stops are taken into account by optimization, else inactive stops are also taken into account but are not activated in result route. | [optional] [default to true]
**ignore_overload_multipliers** | **string** | Deliverable Unit id and whether or not it should be ignored : {&#x27;0&#x27;&#x3D;&gt;{&#x27;unit_id&#x27;&#x3D;&gt;&#x27;7&#x27;, &#x27;ignore&#x27;&#x3D;&gt;&#x27;true&#x27;}} | [optional] 
**with_details** | **bool** | Output route details | [optional] 
**with_geojson** | **string** | Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. | [optional] [default to 'false']

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

