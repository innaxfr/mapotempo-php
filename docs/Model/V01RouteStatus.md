# V01RouteStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**vehicle_usage_id** | **int** |  | [optional] 
**last_sent_to** | **string** | Type GPS Device of Last Sent. | [optional] 
**last_sent_at** | [**\DateTime**](\DateTime.md) | Last Time Sent To External GPS Device. | [optional] 
**quantities** | [**\Mapotempo\Model\V01DeliverableUnitQuantity[]**](V01DeliverableUnitQuantity.md) |  | [optional] 
**departure_status** | **string** | Departure status of start store. | [optional] 
**departure_status_code** | **string** | Status code of start store. | [optional] 
**departure_eta** | [**\DateTime**](\DateTime.md) | Estimated time of departure from remote device. | [optional] 
**departure_eta_formated** | [**\DateTime**](\DateTime.md) | Estimated time of departure from remote device. | [optional] 
**arrival_status** | **string** | Arrival status of stop store. | [optional] 
**arrival_status_code** | **string** | Status code of stop store. | [optional] 
**arrival_eta** | [**\DateTime**](\DateTime.md) | Estimated time of arrival from remote device. | [optional] 
**arrival_eta_formated** | [**\DateTime**](\DateTime.md) | Estimated time of arrival from remote device. | [optional] 
**stops** | [**\Mapotempo\Model\V01StopStatus[]**](V01StopStatus.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

