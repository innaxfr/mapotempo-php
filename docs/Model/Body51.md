# Body51

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lat** | **float** | Decimal value between -90 and 90. | 
**lng** | **float** | Decimal value between -180 and 180. | 
**size** | **int** | Area accessible from the start point by this travel time in seconds. | 
**vehicle_usage_id** | **int** | If not provided, use default router from customer. | [optional] 
**departure** | [**\DateTime**](\DateTime.md) | Departure time (only used if router supports traffic) | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

