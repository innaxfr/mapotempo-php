# V01Layer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **string** |  | [optional] 
**url** | **string** |  | [optional] 
**attribution** | **string** |  | [optional] 
**urlssl** | **string** |  | [optional] 
**source** | **string** |  | [optional] 
**overlay** | **bool** | False if it is a base layer, true if it is just an overlay. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

