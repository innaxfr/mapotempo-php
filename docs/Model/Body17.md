# Body17

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**route_id** | **int** | Route id to switch associated vehicle. | 
**vehicle_usage_id** | **int** | New vehicle id to associate to the route. | 
**with_details** | **bool** | Output complete planning. | [optional] 
**with_geojson** | **string** | Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. | [optional] [default to 'false']

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

