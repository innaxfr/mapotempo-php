# V01RouteProperties

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**vehicle_usage_id** | **int** |  | [optional] 
**hidden** | **bool** |  | [optional] 
**locked** | **bool** |  | [optional] 
**color** | **string** | Color code with #. For instance: #FF0000. | [optional] 
**geojson** | **string** | Geojson string of track and stops of the route. Default empty, set parameter &#x60;with_geojson&#x3D;true|point|polyline&#x60; to get this extra content. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

