# V01Status304

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**detail** | **string** | Server rendered details | [optional] 
**status** | **int** | HTTP status code | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

