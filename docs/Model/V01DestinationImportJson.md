# V01DestinationImportJson

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**ref** | **string** | A free reference, like an external ID from other information system (forbidden characters are ./\\) | [optional] 
**name** | **string** |  | [optional] 
**street** | **string** | Content from address only useful for geocoding. | [optional] 
**detail** | **string** | Content from address useless for geocoding (flat number). | [optional] 
**postalcode** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**state** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**lat** | **float** | Decimal value between -90 and 90. If no geolocation is provided, point will be automatically geocoded. | [optional] 
**lng** | **float** | Decimal value between -180 and 180. If no geolocation is provided, point will be automatically geocoded. | [optional] 
**comment** | **string** |  | [optional] 
**phone_number** | **string** |  | [optional] 
**tag_ids** | **int[]** |  | [optional] 
**setup_duration** | **string** | Unique preparation duration however number of visits (In case of multiple visits at same time for one destination, this duration will be taken into account only one time). Format is HH:MM:SS or number of seconds. | [optional] 
**visits** | [**\Mapotempo\Model\V01VisitImportJson[]**](V01VisitImportJson.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

