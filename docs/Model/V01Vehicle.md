# V01Vehicle

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**ref** | **string** | A free reference, like an external ID from other information system (forbidden characters are ./\\) | [optional] 
**name** | **string** |  | [optional] 
**capacity_unit** | **string** | Deprecated, use capacities and deliverable_unit entity instead. | [optional] 
**speed_multiplier** | **float** |  | [optional] 
**contact_email** | **string** |  | [optional] 
**phone_number** | **string** |  | [optional] 
**emission** | **float** |  | [optional] 
**consumption** | **int** |  | [optional] 
**color** | **string** | Color code with #. For instance: #FF0000 | [optional] 
**fuel_type** | **string** |  | [optional] 
**router_id** | **int** |  | [optional] 
**router_dimension** | **string** |  | [optional] 
**router_options** | [**\Mapotempo\Model\V01RouterOptions**](V01RouterOptions.md) |  | [optional] 
**max_distance** | **int** | Maximum achievable distance in meters | [optional] 
**tag_ids** | **int[]** |  | [optional] 
**devices** | **object** |  | [optional] 
**vehicle_usages** | [**\Mapotempo\Model\V01VehicleUsage[]**](V01VehicleUsage.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

