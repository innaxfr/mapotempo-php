# Body

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ref** | **string** | Only available in admin. | [optional] 
**name** | **string** | Only available in admin. | 
**end_subscription** | [**\DateTime**](\DateTime.md) | Only available in admin. | [optional] 
**max_vehicles** | **int** | Only available in admin. | 
**visit_duration** | **string** | Default visit duration (HH:MM:SS) or number of seconds (can be overidded on each Visit entity) | [optional] 
**setup_duration** | **string** | Default setup duration (HH:MM:SS) or number of seconds (can be overidded on each Destination entity) | [optional] 
**default_country** | **string** | Default country (can be overidded on each Destination entity) | 
**router_id** | **int** |  | 
**router_dimension** | **string** |  | [optional] 
**speed_multiplier** | **float** | Default router speed multiplier (can be overidded on each Vehicle entity) | [optional] 
**optimization_max_split_size** | **int** | Maximum number of visits to split problem (default is 500) | [optional] 
**optimization_cluster_size** | **int** | Time in seconds to group near visits (default is 0) | [optional] 
**optimization_time** | **float** | Maximum optimization time by vehicle (default is 60) | [optional] 
**optimization_minimal_time** | **float** | Minimum optimization time by vehicle (default is 3) | [optional] 
**optimization_stop_soft_upper_bound** | **float** | Stops delay coefficient, 0 to avoid delay (default is 0.3) | [optional] 
**optimization_vehicle_soft_upper_bound** | **float** | Vehicles delay coefficient, 0 to avoid delay (default is 0.3) | [optional] 
**optimization_cost_waiting_time** | **float** | Coefficient to manage waiting time (default is 1) | [optional] 
**optimization_force_start** | **bool** | Force time for departure (default is false) | [optional] 
**print_planning_annotating** | **bool** |  | [optional] 
**print_header** | **string** |  | [optional] 
**print_stop_time** | **bool** |  | [optional] 
**print_map** | **bool** |  | [optional] 
**print_barcode** | **string** | Print the Reference as Barcode | [optional] 
**advanced_options** | **string** | Advanced options in a serialized json format | [optional] 
**devices** | **string** | Only available in admin. | [optional] 
**profile_id** | **string** |  | 
**router_options_traffic** | **bool** |  | [optional] 
**router_options_track** | **bool** |  | [optional] 
**router_options_motorway** | **bool** |  | [optional] 
**router_options_toll** | **bool** |  | [optional] 
**router_options_trailers** | **int** |  | [optional] 
**router_options_weight** | **float** |  | [optional] 
**router_options_weight_per_axle** | **float** |  | [optional] 
**router_options_height** | **float** |  | [optional] 
**router_options_width** | **float** |  | [optional] 
**router_options_length** | **float** |  | [optional] 
**router_options_hazardous_goods** | **string** |  | [optional] 
**router_options_max_walk_distance** | **float** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

