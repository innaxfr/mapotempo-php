# V01VehicleTemperature

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vehicle_id** | **int** |  | [optional] 
**vehicle_name** | **string** |  | [optional] 
**route_id** | **int** |  | [optional] 
**device_infos** | [**\Mapotempo\Model\V01TemperatureInfos**](V01TemperatureInfos.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

