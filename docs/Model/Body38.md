# Body38

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer_id** | **int** |  | 
**ref** | **string** | A free reference, like an external ID from other information system (forbidden characters are ./\\) | [optional] 
**name** | **string** |  | 
**contact_email** | **string** |  | [optional] 
**phone_number** | **string** |  | [optional] 
**emission** | **float** |  | [optional] 
**consumption** | **int** |  | [optional] 
**color** | **string** | Color code with #. For instance: #FF0000 | [optional] 
**fuel_type** | **string** |  | [optional] 
**router_id** | **int** |  | [optional] 
**router_dimension** | **string** |  | [optional] 
**speed_multiplier** | **float** |  | [optional] 
**max_distance** | **int** | Maximum achievable distance in meters | [optional] 
**time_window_start** | **int** | Schedule time (HH:MM:SS) or number of seconds | [optional] 
**time_window_end** | **int** | Schedule time (HH:MM:SS) or number of seconds | [optional] 
**store_start_id** | **int** |  | [optional] 
**store_stop_id** | **int** |  | [optional] 
**service_time_start** | **int** | Schedule time (HH:MM:SS) or number of seconds | [optional] 
**service_time_end** | **int** | Schedule time (HH:MM:SS) or number of seconds | [optional] 
**work_time** | **int** | Work time duration (HH:MM:SS) or number of seconds | [optional] 
**rest_start** | **int** | Schedule time (HH:MM:SS) or number of seconds | [optional] 
**rest_stop** | **int** | Schedule time (HH:MM:SS) or number of seconds | [optional] 
**rest_duration** | **int** | Rest duration (HH:MM:SS) or number of seconds | [optional] 
**store_rest_id** | **int** |  | [optional] 
**active** | **bool** |  | [optional] 
**cost** | **double** | Cost of the vehicle plus the cost of the structure | [optional] 
**cost_time** | **double** | Cost of service per hour | [optional] 
**cost_distance** | **double** | Kilometre/Miles cost | [optional] 
**capacities_deliverable_unit_id** | **int** |  | [optional] 
**capacities_quantity** | **float** |  | [optional] 
**router_options_track** | **bool** |  | [optional] 
**router_options_motorway** | **bool** |  | [optional] 
**router_options_toll** | **bool** |  | [optional] 
**router_options_trailers** | **int** |  | [optional] 
**router_options_weight** | **float** |  | [optional] 
**router_options_weight_per_axle** | **float** |  | [optional] 
**router_options_height** | **float** |  | [optional] 
**router_options_width** | **float** |  | [optional] 
**router_options_length** | **float** |  | [optional] 
**router_options_hazardous_goods** | **string** |  | [optional] 
**router_options_max_walk_distance** | **float** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

