# V01Stop

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**index** | **int** | Stop&#x27;s Index | [optional] 
**status** | **string** | Status of stop. | [optional] 
**status_code** | **string** | Status code of stop. | [optional] 
**eta** | [**\DateTime**](\DateTime.md) | Estimated time of arrival from remote device. | [optional] 
**eta_formated** | [**\DateTime**](\DateTime.md) | Estimated time of arrival from remote device. | [optional] 
**visit_ref** | **string** |  | [optional] 
**destination_ref** | **string** |  | [optional] 
**active** | **bool** | Stop taken into account or not in the route. | [optional] 
**distance** | **float** | Distance between the stop and previous one. | [optional] 
**drive_time** | **int** | Time in seconds between the stop and previous one. | [optional] 
**visit_id** | **int** |  | [optional] 
**destination_id** | **int** |  | [optional] 
**wait_time** | [**\DateTime**](\DateTime.md) | Time before delivery. | [optional] 
**time** | [**\DateTime**](\DateTime.md) | Arrival planned at. | [optional] 
**no_path** | **bool** |  | [optional] 
**out_of_window** | **bool** |  | [optional] 
**out_of_capacity** | **bool** |  | [optional] 
**unmanageable_capacity** | **bool** |  | [optional] 
**out_of_drive_time** | **bool** |  | [optional] 
**out_of_work_time** | **bool** |  | [optional] 
**out_of_max_distance** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

