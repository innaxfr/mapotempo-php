# Body48

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**size** | **int** | Area accessible from the start store by this travel time in seconds. | 
**departure_date** | [**\DateTime**](\DateTime.md) | Departure date (only used if router supports traffic). Time Window Start time of the vehicle is used in addition of this date. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

