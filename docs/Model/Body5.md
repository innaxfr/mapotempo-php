# Body5

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**replace** | **bool** |  | [optional] 
**planning_name** | **string** |  | [optional] 
**planning_ref** | **string** |  | [optional] 
**planning_date** | **string** |  | [optional] 
**planning_vehicle_usage_set_id** | **int** |  | [optional] 
**remote** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

