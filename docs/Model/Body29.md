# Body29

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ref** | **string** | A free reference, like an external ID from other information system (forbidden characters are ./\\) | [optional] 
**name** | **string** |  | 
**street** | **string** | Content from address only useful for geocoding. | [optional] 
**postalcode** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**state** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**lat** | **float** | Decimal value between -90 and 90. If no geolocation is provided, point will be automatically geocoded. | [optional] 
**lng** | **float** | Decimal value between -180 and 180. If no geolocation is provided, point will be automatically geocoded. | [optional] 
**color** | **string** | Color code with #. Default: #000000. | [optional] 
**icon** | **string** | Icon name from font-awesome. Default: fa-home. | [optional] 
**icon_size** | **string** | Icon size. Default: large. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

