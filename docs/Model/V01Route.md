# V01Route

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**vehicle_usage_id** | **int** |  | [optional] 
**hidden** | **bool** |  | [optional] 
**locked** | **bool** |  | [optional] 
**color** | **string** | Color code with #. For instance: #FF0000. | [optional] 
**geojson** | **string** | Geojson string of track and stops of the route. Default empty, set parameter &#x60;with_geojson&#x3D;true|point|polyline&#x60; to get this extra content. | [optional] 
**ref** | **string** |  | [optional] 
**outdated** | **bool** |  | [optional] 
**distance** | **float** | Total route&#x27;s distance. | [optional] 
**drive_time** | **int** | Total drive time, in seconds | [optional] 
**visits_duration** | **int** | Total visits duration, in seconds | [optional] 
**wait_time** | **int** | Total wait time, in seconds | [optional] 
**emission** | **float** |  | [optional] 
**start** | [**\DateTime**](\DateTime.md) |  | [optional] 
**end** | [**\DateTime**](\DateTime.md) |  | [optional] 
**departure_status** | **string** | Departure status of start store. | [optional] 
**departure_eta** | [**\DateTime**](\DateTime.md) | Estimated time of departure from remote device for start store. | [optional] 
**arrival_status** | **string** | Arrival status of stop store. | [optional] 
**arrival_eta** | [**\DateTime**](\DateTime.md) | Estimated time of arrival from remote device for stop store. | [optional] 
**stops** | [**\Mapotempo\Model\V01Stop[]**](V01Stop.md) |  | [optional] 
**stop_out_of_drive_time** | **bool** | Flag is true when returning to vehicle&#x27;s store_stop makes route time more than vehicle drive time. See also Stop.out_of_drive_time for previous stops in route. | [optional] 
**stop_out_of_work_time** | **bool** | Flag is true when returning to vehicle&#x27;s store_stop makes route time outside vehicle time window. See also Stop.out_of_work_time for previous stops in route. | [optional] 
**stop_out_of_max_distance** | **bool** | Flag is true when returning to vehicle&#x27;s store_stop makes route distance more than vehicle max distance. See also Stop.out_of_max_distance for previous stops in route. | [optional] 
**stop_no_path** | **bool** | Flag is true when no route has been found to return to vehicle&#x27;s store_stop. See also Stop.no_path for previous stops in route. | [optional] 
**stop_distance** | **float** | Distance between the vehicle&#x27;s store_stop and last stop. | [optional] 
**stop_drive_time** | **int** | Time in seconds between the vehicle&#x27;s store_stop and last stop. | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Last Updated At. | [optional] 
**last_sent_to** | **string** | Type GPS Device of Last Sent. | [optional] 
**last_sent_at** | [**\DateTime**](\DateTime.md) | Last Time Sent To External GPS Device. | [optional] 
**optimized_at** | [**\DateTime**](\DateTime.md) | Last optimized at. | [optional] 
**quantities** | [**\Mapotempo\Model\V01DeliverableUnitQuantity[]**](V01DeliverableUnitQuantity.md) |  | [optional] 
**total_cost** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

