# Body12

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ref** | **string** | A free reference, like an external ID from other information system (forbidden characters are ./\\) | [optional] 
**priority** | **int** | Insertion priority used during optimization in case all visits cannot be planned (-4 to 4, 0 if not defined). | [optional] 
**quantities_deliverable_unit_id** | **int** |  | [optional] 
**quantities_quantity** | **float** |  | [optional] 
**time_window_start_1** | **string** | Schedule time (HH:MM) or number of seconds since midnight | [optional] 
**time_window_end_1** | **string** | Schedule time (HH:MM) or number of seconds since midnight | [optional] 
**time_window_start_2** | **string** | Schedule time (HH:MM) or number of seconds since midnight | [optional] 
**time_window_end_2** | **string** | Schedule time (HH:MM) or number of seconds since midnight | [optional] 
**duration** | **string** | Visit duration (HH:MM:SS) or number of seconds | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

