# V01Zone

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **string** |  | [optional] 
**speed_multiplier** | **float** | Speed multiplier for this area. Taken into accound only for routers which support avoid_zones or speed_zones. | [optional] 
**vehicle_id** | **int** |  | [optional] 
**polygon** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

