# Body9

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lat** | **float** | Decimal value between -90 and 90. If no geolocation is provided, point will be automatically geocoded. | [optional] 
**lng** | **float** | Decimal value between -180 and 180. If no geolocation is provided, point will be automatically geocoded. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

