# V01DeliverableUnitQuantity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deliverable_unit_id** | **int** |  | [optional] 
**quantity** | **float** |  | [optional] 
**operation** | **float** |  | [optional] 
**codes** | **string** | Barcodes for this deliverable unit | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

