# Body18

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_time** | **float** | Maximum time for best routes (in seconds). | [optional] 
**max_distance** | **float** | Maximum distance for best routes (in meters). | [optional] 
**active_only** | **bool** | Use only active stops. | [optional] [default to true]
**out_of_zone** | **bool** | Take into account points out of zones. | [optional] [default to true]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

