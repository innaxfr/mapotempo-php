# Body53

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**route_id** | **int** | Route ID | 
**type** | **string** | Action Name | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

