# Body4

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ref** | **string** | A free reference, like an external ID from other information system (forbidden characters are ./\\) | [optional] 
**label** | **string** |  | [optional] 
**icon** | **string** | Icon name from font-awesome. Default: fa-archive. | [optional] 
**default_quantity** | **float** | Default quantity value (can be overidded on each Visit entity) | [optional] 
**default_capacity** | **float** | Default capacity value (can be overidded on each Vehicle entity) | [optional] 
**default_codes** | **string[]** | Default barcodes for this deliverable unit | [optional] 
**optimization_overload_multiplier** | **int** | Multiplier used during optimization in case of overload. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

