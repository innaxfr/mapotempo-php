# V01Order

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**visit_id** | **int** |  | [optional] 
**destination_id** | **int** |  | [optional] 
**shift** | **int** |  | [optional] 
**product_ids** | **int[]** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

