# Body22

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**with_details** | **bool** | Output route details | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

