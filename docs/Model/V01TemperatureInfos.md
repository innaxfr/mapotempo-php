# V01TemperatureInfos

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**device_name** | **string** |  | [optional] 
**device_id** | **string** |  | [optional] 
**temperature** | **float** |  | [optional] 
**time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**time_formatted** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

