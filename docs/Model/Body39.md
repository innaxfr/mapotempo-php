# Body39

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ref** | **string** | A free reference, like an external ID from other information system (forbidden characters are ./\\) | [optional] 
**name** | **string** |  | [optional] 
**contact_email** | **string** |  | [optional] 
**phone_number** | **string** |  | [optional] 
**emission** | **float** |  | [optional] 
**consumption** | **int** |  | [optional] 
**color** | **string** | Color code with #. For instance: #FF0000 | [optional] 
**fuel_type** | **string** |  | [optional] 
**router_id** | **int** |  | [optional] 
**router_dimension** | **string** |  | [optional] 
**speed_multiplier** | **float** |  | [optional] 
**max_distance** | **int** | Maximum achievable distance in meters | [optional] 
**tag_ids** | **int[]** |  | [optional] 
**vehicle_usages** | [**\Mapotempo\Model\V01VehicleUsage[]**](V01VehicleUsage.md) |  | [optional] 
**capacities_deliverable_unit_id** | **int** |  | [optional] 
**capacities_quantity** | **float** |  | [optional] 
**router_options_track** | **bool** |  | [optional] 
**router_options_motorway** | **bool** |  | [optional] 
**router_options_toll** | **bool** |  | [optional] 
**router_options_trailers** | **int** |  | [optional] 
**router_options_weight** | **float** |  | [optional] 
**router_options_weight_per_axle** | **float** |  | [optional] 
**router_options_height** | **float** |  | [optional] 
**router_options_width** | **float** |  | [optional] 
**router_options_length** | **float** |  | [optional] 
**router_options_hazardous_goods** | **string** |  | [optional] 
**router_options_max_walk_distance** | **float** |  | [optional] 
**router_options_approach** | **string** |  | [optional] 
**router_options_snap** | **float** |  | [optional] 
**router_options_strict_restriction** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

