# Body56

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**external_refs_fleet_user** | **string[]** |  | 
**external_refs_external_ref** | **string[]** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

