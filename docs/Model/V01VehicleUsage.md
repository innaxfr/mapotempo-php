# V01VehicleUsage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**vehicle_usage_set_id** | **int** |  | [optional] 
**time_window_start** | **string** | Schedule time (HH:MM:SS) or number of seconds | [optional] 
**time_window_end** | **string** | Schedule time (HH:MM:SS) or number of seconds | [optional] 
**store_start_id** | **int** |  | [optional] 
**store_stop_id** | **int** |  | [optional] 
**service_time_start** | **string** | Schedule time (HH:MM:SS) or number of seconds | [optional] 
**service_time_end** | **string** | Schedule time (HH:MM:SS) or number of seconds | [optional] 
**work_time** | **string** | Work time duration (HH:MM:SS) or number of seconds | [optional] 
**rest_start** | **string** | Schedule time (HH:MM:SS) or number of seconds | [optional] 
**rest_stop** | **string** | Schedule time (HH:MM:SS) or number of seconds | [optional] 
**rest_duration** | **string** | Rest duration (HH:MM:SS) or number of seconds | [optional] 
**store_rest_id** | **int** |  | [optional] 
**active** | **bool** |  | [optional] 
**tag_ids** | **int[]** |  | [optional] 
**cost** | **double** | Cost of the vehicle plus the cost of the structure | [optional] 
**cost_time** | **double** | Cost of service per hour | [optional] 
**cost_distance** | **double** | Kilometre/Miles cost | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

