# V01StopStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**index** | **int** | Stop&#x27;s Index | [optional] 
**status** | **string** | Status of stop. | [optional] 
**status_code** | **string** | Status code of stop. | [optional] 
**eta** | [**\DateTime**](\DateTime.md) | Estimated time of arrival from remote device. | [optional] 
**eta_formated** | [**\DateTime**](\DateTime.md) | Estimated time of arrival from remote device. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

