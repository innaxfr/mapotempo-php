# Body16

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ref** | **string** | A free reference, like an external ID from other information system (forbidden characters are ./\\) | [optional] 
**name** | **string** |  | [optional] 
**date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**begin_date** | [**\DateTime**](\DateTime.md) | Begin validity period | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) | End validity period | [optional] 
**active** | **bool** |  | [optional] [default to true]
**vehicle_usage_set_id** | **int** |  | [optional] 
**zoning_ids** | **int[]** | If a new zoning is specified before planning save, all visits will be affected to vehicles specified in zones. | [optional] 
**routes** | [**\Mapotempo\Model\V01RouteProperties[]**](V01RouteProperties.md) |  | [optional] 
**with_geojson** | **string** | Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. | [optional] [default to 'false']

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

