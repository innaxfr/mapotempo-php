# V01OrderArray

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **string** |  | [optional] 
**base_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**length** | **string** |  | [optional] 
**orders** | [**\Mapotempo\Model\V01Order[]**](V01Order.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

