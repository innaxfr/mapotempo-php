# V01VisitImportJson

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**ref** | **string** | A free reference, like an external ID from other information system (forbidden characters are ./\\) | [optional] 
**quantity_default** | **int** | Deprecated, use quantities instead. | [optional] 
**time_window_start_1** | **string** | Schedule time (HH:MM:SS) or number of seconds | [optional] 
**time_window_end_1** | **string** | Schedule time (HH:MM:SS) or number of seconds | [optional] 
**time_window_start_2** | **string** | Schedule time (HH:MM:SS) or number of seconds | [optional] 
**time_window_end_2** | **string** | Schedule time (HH:MM:SS) or number of seconds | [optional] 
**duration** | **string** | Visit duration (HH:MM:SS) or number of seconds | [optional] 
**tag_ids** | **int[]** |  | [optional] 
**priority** | **int** | Insertion priority used during optimization in case all visits cannot be planned (-4 to 4, 0 if not defined). | [optional] 
**route** | **string** | Route reference. If route reference is specified, a new planning will be created with a route using the specified reference | [optional] 
**ref_vehicle** | **string** | Vehicle reference. If vehicle reference is specified, a new planning will be created with a route using the vehicle with specified reference | [optional] 
**active** | **bool** | In order to specify is stop is active in planning or not | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

