# Body37

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ref** | **string** | Only available in admin. A free reference, like an external ID from other information system (forbidden characters are ./\\) | [optional] 
**email** | **string** |  | [optional] 
**customer_id** | **int** |  | [optional] 
**layer_id** | **int** |  | [optional] 
**api_key** | **string** |  | [optional] 
**url_click2call** | **string** |  | [optional] 
**prefered_unit** | **string** |  | [optional] 
**locale** | **string** | Currently used in mailing | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

