# V01Planning

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**ref** | **string** | A free reference, like an external ID from other information system (forbidden characters are ./\\) | [optional] 
**name** | **string** |  | [optional] 
**outdated** | **bool** |  | [optional] 
**zoning_id** | **int** | DEPRECATED. Use zoning_ids instead. | [optional] 
**zoning_outdated** | **bool** |  | [optional] 
**date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**begin_date** | [**\DateTime**](\DateTime.md) | Begin validity period | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) | End validity period | [optional] 
**active** | **bool** |  | [optional] [default to true]
**vehicle_usage_set_id** | **int** |  | [optional] 
**zoning_ids** | **int[]** | If a new zoning is specified before planning save, all visits will be affected to vehicles specified in zones. | [optional] 
**route_ids** | **int[]** |  | [optional] 
**tag_ids** | **int[]** | Restrict visits/destinations in the plan (visits/destinations should have all of these tags to be present in the plan). | [optional] 
**tag_operation** | **string** | Choose how to use selected tags: &#x60;and&#x60; (for visits with all tags, by default) / &#x60;or&#x60; (for visits with at least one tag). | [optional] [default to 'and']
**updated_at** | [**\DateTime**](\DateTime.md) | Last Updated At | [optional] 
**geojson** | **string** | Geojson string of track and stops of the route. Default empty, set parameter &#x60;with_geojson&#x3D;true|point|polyline&#x60; to get this extra content. | [optional] 
**routes** | [**\Mapotempo\Model\V01RouteProperties[]**](V01RouteProperties.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

