# Body8

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**street** | **string** | Content from address only useful for geocoding. | [optional] 
**postalcode** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**state** | **string** |  | [optional] 
**country** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

