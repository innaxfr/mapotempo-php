# Body36

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ref** | **string** | Only available in admin. A free reference, like an external ID from other information system (forbidden characters are ./\\) | [optional] 
**email** | **string** |  | 
**customer_id** | **int** |  | 
**layer_id** | **int** |  | 
**api_key** | **string** |  | [optional] 
**url_click2call** | **string** |  | [optional] 
**prefered_unit** | **string** |  | [optional] 
**locale** | **string** | Currently used in mailing | [optional] 
**password** | **string** |  | [optional] 
**send_password_email** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

