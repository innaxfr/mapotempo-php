# V01VehiclePosition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vehicle_id** | **int** |  | [optional] 
**device_name** | **string** |  | [optional] 
**lat** | **float** |  | [optional] 
**lng** | **float** |  | [optional] 
**direction** | **float** |  | [optional] 
**speed** | **float** |  | [optional] 
**time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**time_formatted** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

