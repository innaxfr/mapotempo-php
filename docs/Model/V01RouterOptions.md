# V01RouterOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**track** | **bool** |  | [optional] 
**motorway** | **bool** |  | [optional] 
**toll** | **bool** |  | [optional] 
**trailers** | **int** |  | [optional] 
**weight** | **float** | Total weight with trailers and shipping goods, in tons | [optional] 
**weight_per_axle** | **float** |  | [optional] 
**height** | **float** |  | [optional] 
**width** | **float** |  | [optional] 
**length** | **float** |  | [optional] 
**hazardous_goods** | **string** |  | [optional] 
**max_walk_distance** | **float** |  | [optional] 
**approach** | **string** |  | [optional] 
**snap** | **float** |  | [optional] 
**strict_restriction** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

