# Mapotempo\VisitsApi

All URIs are relative to *//app.mapotempo.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteVisits**](VisitsApi.md#deletevisits) | **DELETE** /0.1/visits | Delete multiple visits.
[**getVisits**](VisitsApi.md#getvisits) | **GET** /0.1/visits | Fetch customer&#x27;s visits.

# **deleteVisits**
> deleteVisits()

Delete multiple visits.

Delete multiple visits.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\VisitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->deleteVisits();
} catch (Exception $e) {
    echo 'Exception when calling VisitsApi->deleteVisits: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVisits**
> \Mapotempo\Model\V01Visit[] getVisits($ids, $with_quantities)

Fetch customer's visits.

Fetch customer's visits.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\VisitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ids = array("ids_example"); // string[] | Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add `ref:` before each ref, e.g. `ref:ref1,ref:ref2,ref:ref3`.
$with_quantities = true; // bool | Include the quantities when using geojson output.

try {
    $result = $apiInstance->getVisits($ids, $with_quantities);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VisitsApi->getVisits: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ids** | [**string[]**](../Model/string.md)| Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add &#x60;ref:&#x60; before each ref, e.g. &#x60;ref:ref1,ref:ref2,ref:ref3&#x60;. | [optional]
 **with_quantities** | **bool**| Include the quantities when using geojson output. | [optional]

### Return type

[**\Mapotempo\Model\V01Visit[]**](../Model/V01Visit.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

