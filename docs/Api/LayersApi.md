# Mapotempo\LayersApi

All URIs are relative to *//app.mapotempo.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getLayers**](LayersApi.md#getlayers) | **GET** /0.1/layers | Fetch layers.

# **getLayers**
> \Mapotempo\Model\V01Layer[] getLayers()

Fetch layers.

Get the list of available layers which can be used for maps.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\LayersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getLayers();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LayersApi->getLayers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Mapotempo\Model\V01Layer[]**](../Model/V01Layer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

