# Mapotempo\VehicleUsageSetsApi

All URIs are relative to *//app.mapotempo.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createVehicleUsageSet**](VehicleUsageSetsApi.md#createvehicleusageset) | **POST** /0.1/vehicle_usage_sets | Create vehicle_usage_set.
[**deleteVehicleUsageSet**](VehicleUsageSetsApi.md#deletevehicleusageset) | **DELETE** /0.1/vehicle_usage_sets/{id} | Delete vehicle_usage_set.
[**deleteVehicleUsageSets**](VehicleUsageSetsApi.md#deletevehicleusagesets) | **DELETE** /0.1/vehicle_usage_sets | Delete multiple vehicle_usage_sets.
[**getVehicleUsage**](VehicleUsageSetsApi.md#getvehicleusage) | **GET** /0.1/vehicle_usage_sets/{vehicle_usage_set_id}/vehicle_usages/{id} | Fetch vehicle_usage.
[**getVehicleUsageSet**](VehicleUsageSetsApi.md#getvehicleusageset) | **GET** /0.1/vehicle_usage_sets/{id} | Fetch vehicle_usage_set.
[**getVehicleUsageSets**](VehicleUsageSetsApi.md#getvehicleusagesets) | **GET** /0.1/vehicle_usage_sets | Fetch customer&#x27;s vehicle_usage_sets. At least one vehicle_usage_set exists per customer.
[**getVehicleUsages**](VehicleUsageSetsApi.md#getvehicleusages) | **GET** /0.1/vehicle_usage_sets/{vehicle_usage_set_id}/vehicle_usages | Fetch customer&#x27;s vehicle_usages.
[**importVehicleUsageSets**](VehicleUsageSetsApi.md#importvehicleusagesets) | **PUT** /0.1/vehicle_usage_sets | Import vehicle usage set by upload a CSV file or by JSON.
[**updateVehicleUsage**](VehicleUsageSetsApi.md#updatevehicleusage) | **PUT** /0.1/vehicle_usage_sets/{vehicle_usage_set_id}/vehicle_usages/{id} | Update vehicle_usage.
[**updateVehicleUsageSet**](VehicleUsageSetsApi.md#updatevehicleusageset) | **PUT** /0.1/vehicle_usage_sets/{id} | Update vehicle_usage_set.

# **createVehicleUsageSet**
> \Mapotempo\Model\V01VehicleUsageSet createVehicleUsageSet($body)

Create vehicle_usage_set.

(Note a `vehicle_usage_set` is already automatically created when a customer account is initialized.)         For instance, if the same customer account needs to use one vehicle 2 times per day (morning and evening), he needs 2 `VehicleUsageSet` called \"Morning\" and \"Evening\". A `VehicleUsage` per `Vehicle` is automatically created. The new `VehicleUsageSet` allows to define new default values for `VehicleUsage.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\VehicleUsageSetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Mapotempo\Model\Body42(); // \Mapotempo\Model\Body42 | 

try {
    $result = $apiInstance->createVehicleUsageSet($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehicleUsageSetsApi->createVehicleUsageSet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Mapotempo\Model\Body42**](../Model/Body42.md)|  |

### Return type

[**\Mapotempo\Model\V01VehicleUsageSet**](../Model/V01VehicleUsageSet.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteVehicleUsageSet**
> deleteVehicleUsageSet($id)

Delete vehicle_usage_set.

Delete vehicle_usage_set.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\VehicleUsageSetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->deleteVehicleUsageSet($id);
} catch (Exception $e) {
    echo 'Exception when calling VehicleUsageSetsApi->deleteVehicleUsageSet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteVehicleUsageSets**
> deleteVehicleUsageSets()

Delete multiple vehicle_usage_sets.

Delete multiple vehicle_usage_sets.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\VehicleUsageSetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->deleteVehicleUsageSets();
} catch (Exception $e) {
    echo 'Exception when calling VehicleUsageSetsApi->deleteVehicleUsageSets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVehicleUsage**
> \Mapotempo\Model\V01VehicleUsageWithVehicle getVehicleUsage($vehicle_usage_set_id, $id)

Fetch vehicle_usage.

Fetch vehicle_usage.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\VehicleUsageSetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$vehicle_usage_set_id = 56; // int | 
$id = 56; // int | 

try {
    $result = $apiInstance->getVehicleUsage($vehicle_usage_set_id, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehicleUsageSetsApi->getVehicleUsage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_usage_set_id** | **int**|  |
 **id** | **int**|  |

### Return type

[**\Mapotempo\Model\V01VehicleUsageWithVehicle**](../Model/V01VehicleUsageWithVehicle.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVehicleUsageSet**
> \Mapotempo\Model\V01VehicleUsageSet getVehicleUsageSet($id)

Fetch vehicle_usage_set.

Fetch vehicle_usage_set.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\VehicleUsageSetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $result = $apiInstance->getVehicleUsageSet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehicleUsageSetsApi->getVehicleUsageSet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\Mapotempo\Model\V01VehicleUsageSet**](../Model/V01VehicleUsageSet.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVehicleUsageSets**
> \Mapotempo\Model\V01VehicleUsageSet[] getVehicleUsageSets($ids)

Fetch customer's vehicle_usage_sets. At least one vehicle_usage_set exists per customer.

Fetch customer's vehicle_usage_sets. At least one vehicle_usage_set exists per customer.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\VehicleUsageSetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ids = array(56); // int[] | Select returned vehicle_usage_sets by id.

try {
    $result = $apiInstance->getVehicleUsageSets($ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehicleUsageSetsApi->getVehicleUsageSets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ids** | [**int[]**](../Model/int.md)| Select returned vehicle_usage_sets by id. | [optional]

### Return type

[**\Mapotempo\Model\V01VehicleUsageSet[]**](../Model/V01VehicleUsageSet.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVehicleUsages**
> \Mapotempo\Model\V01VehicleUsageWithVehicle[] getVehicleUsages($vehicle_usage_set_id, $ids)

Fetch customer's vehicle_usages.

Fetch customer's vehicle_usages.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\VehicleUsageSetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$vehicle_usage_set_id = 56; // int | 
$ids = array(56); // int[] | Select returned vehicle_usages by id.

try {
    $result = $apiInstance->getVehicleUsages($vehicle_usage_set_id, $ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehicleUsageSetsApi->getVehicleUsages: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_usage_set_id** | **int**|  |
 **ids** | [**int[]**](../Model/int.md)| Select returned vehicle_usages by id. | [optional]

### Return type

[**\Mapotempo\Model\V01VehicleUsageWithVehicle[]**](../Model/V01VehicleUsageWithVehicle.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **importVehicleUsageSets**
> \Mapotempo\Model\V01VehicleUsageSet[] importVehicleUsageSets($body)

Import vehicle usage set by upload a CSV file or by JSON.

Import vehicle usage set by upload a CSV file or by JSON.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\VehicleUsageSetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Mapotempo\Model\Body41(); // \Mapotempo\Model\Body41 | 

try {
    $result = $apiInstance->importVehicleUsageSets($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehicleUsageSetsApi->importVehicleUsageSets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Mapotempo\Model\Body41**](../Model/Body41.md)|  | [optional]

### Return type

[**\Mapotempo\Model\V01VehicleUsageSet[]**](../Model/V01VehicleUsageSet.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateVehicleUsage**
> \Mapotempo\Model\V01VehicleUsageWithVehicle updateVehicleUsage($vehicle_usage_set_id, $id, $body)

Update vehicle_usage.

Update vehicle_usage.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\VehicleUsageSetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$vehicle_usage_set_id = 56; // int | 
$id = 56; // int | 
$body = new \Mapotempo\Model\Body40(); // \Mapotempo\Model\Body40 | 

try {
    $result = $apiInstance->updateVehicleUsage($vehicle_usage_set_id, $id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehicleUsageSetsApi->updateVehicleUsage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_usage_set_id** | **int**|  |
 **id** | **int**|  |
 **body** | [**\Mapotempo\Model\Body40**](../Model/Body40.md)|  | [optional]

### Return type

[**\Mapotempo\Model\V01VehicleUsageWithVehicle**](../Model/V01VehicleUsageWithVehicle.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateVehicleUsageSet**
> \Mapotempo\Model\V01VehicleUsageSet updateVehicleUsageSet($id, $body)

Update vehicle_usage_set.

Update vehicle_usage_set.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\VehicleUsageSetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 
$body = new \Mapotempo\Model\Body43(); // \Mapotempo\Model\Body43 | 

try {
    $result = $apiInstance->updateVehicleUsageSet($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehicleUsageSetsApi->updateVehicleUsageSet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **body** | [**\Mapotempo\Model\Body43**](../Model/Body43.md)|  | [optional]

### Return type

[**\Mapotempo\Model\V01VehicleUsageSet**](../Model/V01VehicleUsageSet.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

