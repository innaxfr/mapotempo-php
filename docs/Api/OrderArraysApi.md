# Mapotempo\OrderArraysApi

All URIs are relative to *//app.mapotempo.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**massAssignmentOrder**](OrderArraysApi.md#massassignmentorder) | **PATCH** /0.1/order_arrays/{id} | Orders mass assignment.
[**updateOrder**](OrderArraysApi.md#updateorder) | **PUT** /0.1/order_arrays/{order_array_id}/orders/{id} | Update order.

# **massAssignmentOrder**
> \Mapotempo\Model\V01OrderArray massAssignmentOrder($id)

Orders mass assignment.

Only available if \"enable orders\" option is active for current customer.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\OrderArraysApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $result = $apiInstance->massAssignmentOrder($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderArraysApi->massAssignmentOrder: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\Mapotempo\Model\V01OrderArray**](../Model/V01OrderArray.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateOrder**
> \Mapotempo\Model\V01Order updateOrder($order_array_id, $id, $body)

Update order.

Only available if \"enable orders\" option is active for current customer.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\OrderArraysApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$order_array_id = 56; // int | 
$id = 56; // int | 
$body = new \Mapotempo\Model\Body13(); // \Mapotempo\Model\Body13 | 

try {
    $result = $apiInstance->updateOrder($order_array_id, $id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderArraysApi->updateOrder: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_array_id** | **int**|  |
 **id** | **int**|  |
 **body** | [**\Mapotempo\Model\Body13**](../Model/Body13.md)|  | [optional]

### Return type

[**\Mapotempo\Model\V01Order**](../Model/V01Order.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

