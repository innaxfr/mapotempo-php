# Mapotempo\StoresApi

All URIs are relative to *//app.mapotempo.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**autocompleteStore**](StoresApi.md#autocompletestore) | **PATCH** /0.1/stores/geocode_complete | Auto completion on store.
[**createStore**](StoresApi.md#createstore) | **POST** /0.1/stores | Create store.
[**deleteStore**](StoresApi.md#deletestore) | **DELETE** /0.1/stores/{id} | Delete store.
[**deleteStores**](StoresApi.md#deletestores) | **DELETE** /0.1/stores | Delete multiple stores.
[**geocodeStore**](StoresApi.md#geocodestore) | **PATCH** /0.1/stores/geocode | Geocode store.
[**getStore**](StoresApi.md#getstore) | **GET** /0.1/stores/{id} | Fetch store.
[**getStores**](StoresApi.md#getstores) | **GET** /0.1/stores | Fetch customer&#x27;s stores. At least one store exists per customer.
[**importStores**](StoresApi.md#importstores) | **PUT** /0.1/stores | Import stores by upload a CSV file or by JSON.
[**reverseGeocodingStore**](StoresApi.md#reversegeocodingstore) | **PATCH** /0.1/stores/reverse | Reverse geocoding.
[**updateStore**](StoresApi.md#updatestore) | **PUT** /0.1/stores/{id} | Update store.

# **autocompleteStore**
> autocompleteStore($street, $postalcode, $city, $state, $country)

Auto completion on store.

Auto completion on address of store. Results are priorized from last geolocated store of the customer account.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$street = "street_example"; // string | 
$postalcode = "postalcode_example"; // string | 
$city = "city_example"; // string | 
$state = "state_example"; // string | 
$country = "country_example"; // string | 

try {
    $apiInstance->autocompleteStore($street, $postalcode, $city, $state, $country);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->autocompleteStore: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **street** | **string**|  | [optional]
 **postalcode** | **string**|  | [optional]
 **city** | **string**|  | [optional]
 **state** | **string**|  | [optional]
 **country** | **string**|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createStore**
> \Mapotempo\Model\V01Store createStore($body)

Create store.

(Note a default store is already automatically created when a customer account is initialized.)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Mapotempo\Model\Body29(); // \Mapotempo\Model\Body29 | 

try {
    $result = $apiInstance->createStore($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->createStore: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Mapotempo\Model\Body29**](../Model/Body29.md)|  |

### Return type

[**\Mapotempo\Model\V01Store**](../Model/V01Store.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteStore**
> deleteStore($id)

Delete store.

At least one remaining store is required after deletion.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.

try {
    $apiInstance->deleteStore($id);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->deleteStore: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteStores**
> deleteStores()

Delete multiple stores.

At least one remaining store is required after deletion.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->deleteStores();
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->deleteStores: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **geocodeStore**
> \Mapotempo\Model\V01Store geocodeStore($street, $postalcode, $city, $state, $country)

Geocode store.

Result of geocoding is not saved with this operation. You can use update operation to save the result of geocoding.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$street = "street_example"; // string | 
$postalcode = "postalcode_example"; // string | 
$city = "city_example"; // string | 
$state = "state_example"; // string | 
$country = "country_example"; // string | 

try {
    $result = $apiInstance->geocodeStore($street, $postalcode, $city, $state, $country);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->geocodeStore: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **street** | **string**|  | [optional]
 **postalcode** | **string**|  | [optional]
 **city** | **string**|  | [optional]
 **state** | **string**|  | [optional]
 **country** | **string**|  | [optional]

### Return type

[**\Mapotempo\Model\V01Store**](../Model/V01Store.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getStore**
> \Mapotempo\Model\V01Store getStore($id)

Fetch store.

Fetch store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.

try {
    $result = $apiInstance->getStore($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->getStore: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |

### Return type

[**\Mapotempo\Model\V01Store**](../Model/V01Store.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getStores**
> \Mapotempo\Model\V01Store[] getStores($ids, $name_like, $ref_like, $words_mask)

Fetch customer's stores. At least one store exists per customer.

Fetch customer's stores. At least one store exists per customer.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ids = array("ids_example"); // string[] | Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add `ref:` before each ref, e.g. `ref:ref1,ref:ref2,ref:ref3`.
$name_like = "name_like_example"; // string | Find stores with similarity on the name (case insensitive).
$ref_like = "ref_like_example"; // string | Find stores with similarity on the reference (case insensitive).
$words_mask = "words_mask_example"; // string | Words separated by commas that will skip the similarity check (case insensitive)

try {
    $result = $apiInstance->getStores($ids, $name_like, $ref_like, $words_mask);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->getStores: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ids** | [**string[]**](../Model/string.md)| Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add &#x60;ref:&#x60; before each ref, e.g. &#x60;ref:ref1,ref:ref2,ref:ref3&#x60;. | [optional]
 **name_like** | **string**| Find stores with similarity on the name (case insensitive). | [optional]
 **ref_like** | **string**| Find stores with similarity on the reference (case insensitive). | [optional]
 **words_mask** | **string**| Words separated by commas that will skip the similarity check (case insensitive) | [optional]

### Return type

[**\Mapotempo\Model\V01Store[]**](../Model/V01Store.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **importStores**
> \Mapotempo\Model\V01Store[] importStores($body)

Import stores by upload a CSV file or by JSON.

Import stores by upload a CSV file or by JSON.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Mapotempo\Model\Body28(); // \Mapotempo\Model\Body28 | 

try {
    $result = $apiInstance->importStores($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->importStores: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Mapotempo\Model\Body28**](../Model/Body28.md)|  | [optional]

### Return type

[**\Mapotempo\Model\V01Store[]**](../Model/V01Store.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **reverseGeocodingStore**
> \Mapotempo\Model\V01Store reverseGeocodingStore($lat, $lng)

Reverse geocoding.

Result of reverse geocoding is not saved with this operation.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$lat = 3.4; // float | 
$lng = 3.4; // float | 

try {
    $result = $apiInstance->reverseGeocodingStore($lat, $lng);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->reverseGeocodingStore: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lat** | **float**|  | [optional]
 **lng** | **float**|  | [optional]

### Return type

[**\Mapotempo\Model\V01Store**](../Model/V01Store.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateStore**
> \Mapotempo\Model\V01Store updateStore($id, $body)

Update store.

If want to force geocoding for a new address, you have to send empty lat/lng with new address.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$body = new \Mapotempo\Model\Body30(); // \Mapotempo\Model\Body30 | 

try {
    $result = $apiInstance->updateStore($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->updateStore: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **body** | [**\Mapotempo\Model\Body30**](../Model/Body30.md)|  | [optional]

### Return type

[**\Mapotempo\Model\V01Store**](../Model/V01Store.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

