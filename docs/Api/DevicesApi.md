# Mapotempo\DevicesApi

All URIs are relative to *//app.mapotempo.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**checkAuth**](DevicesApi.md#checkauth) | **GET** /0.1/devices/{device}/auth/{id} | Validate device Credentials.
[**deviceAlyacomSendMultiple**](DevicesApi.md#devicealyacomsendmultiple) | **POST** /0.1/devices/alyacom/send_multiple | Send Planning Routes.
[**deviceFleetClear**](DevicesApi.md#devicefleetclear) | **DELETE** /0.1/devices/fleet/clear | Clear Route.
[**deviceFleetClearMultiple**](DevicesApi.md#devicefleetclearmultiple) | **DELETE** /0.1/devices/fleet/clear_multiple | Clear multiple routes.
[**deviceFleetCreateCompanyAndDrivers**](DevicesApi.md#devicefleetcreatecompanyanddrivers) | **PATCH** /0.1/devices/fleet/create_company | Create company with drivers.
[**deviceFleetCreateDrivers**](DevicesApi.md#devicefleetcreatedrivers) | **PATCH** /0.1/devices/fleet/create_or_update_drivers | Create drivers.
[**deviceFleetDemoClear**](DevicesApi.md#devicefleetdemoclear) | **DELETE** /0.1/devices/device_demo/clear | Clear Route.
[**deviceFleetDemoClearMultiple**](DevicesApi.md#devicefleetdemoclearmultiple) | **DELETE** /0.1/devices/device_demo/clear_multiple | Clear Planning Routes.
[**deviceFleetDemoSendMultiple**](DevicesApi.md#devicefleetdemosendmultiple) | **POST** /0.1/devices/device_demo/send_multiple | Send Planning Routes.
[**deviceFleetList**](DevicesApi.md#devicefleetlist) | **GET** /0.1/devices/fleet/devices | List Devices.
[**deviceFleetSendMultiple**](DevicesApi.md#devicefleetsendmultiple) | **POST** /0.1/devices/fleet/send_multiple | Send Planning Routes.
[**deviceFleetSync**](DevicesApi.md#devicefleetsync) | **POST** /0.1/devices/fleet/sync | Synchronise Vehicles.
[**deviceFleetTrackingList**](DevicesApi.md#devicefleettrackinglist) | **GET** /0.1/devices/suivi_de_flotte/devices | List Devices.
[**deviceMasternautSendMultiple**](DevicesApi.md#devicemasternautsendmultiple) | **POST** /0.1/devices/masternaut/send_multiple | Send Planning Routes.
[**deviceNoticoClear**](DevicesApi.md#devicenoticoclear) | **DELETE** /0.1/devices/notico/clear | Clear Route.
[**deviceNoticoClearMultiple**](DevicesApi.md#devicenoticoclearmultiple) | **DELETE** /0.1/devices/notico/clear_multiple | Clear Planning Routes.
[**deviceNoticoSendMultiple**](DevicesApi.md#devicenoticosendmultiple) | **POST** /0.1/devices/notico/send_multiple | Send Planning Routes.
[**deviceOrangeClear**](DevicesApi.md#deviceorangeclear) | **DELETE** /0.1/devices/orange/clear | Clear Route.
[**deviceOrangeClearMultiple**](DevicesApi.md#deviceorangeclearmultiple) | **DELETE** /0.1/devices/orange/clear_multiple | Clear Planning Routes.
[**deviceOrangeClearSync**](DevicesApi.md#deviceorangeclearsync) | **POST** /0.1/devices/orange/sync | Synchronise Vehicles.
[**deviceOrangeList**](DevicesApi.md#deviceorangelist) | **GET** /0.1/devices/orange/devices | List Devices.
[**deviceOrangeSendMultiple**](DevicesApi.md#deviceorangesendmultiple) | **POST** /0.1/devices/orange/send_multiple | Send Planning Routes.
[**devicePraxedoClear**](DevicesApi.md#devicepraxedoclear) | **DELETE** /0.1/devices/praxedo/clear | Clear route from Praxedo.
[**devicePraxedoClear_0**](DevicesApi.md#devicepraxedoclear_0) | **DELETE** /0.1/devices/praxedo/clear_multiple | Clear routes from Praxedo.
[**devicePraxedoSendMultiple**](DevicesApi.md#devicepraxedosendmultiple) | **POST** /0.1/devices/praxedo/send_multiple | Send Planning Routes.
[**deviceSopacList**](DevicesApi.md#devicesopaclist) | **GET** /0.1/devices/sopac/devices | List Devices.
[**deviceTeksatClear**](DevicesApi.md#deviceteksatclear) | **DELETE** /0.1/devices/teksat/clear | Clear Route.
[**deviceTeksatClearMultiple**](DevicesApi.md#deviceteksatclearmultiple) | **DELETE** /0.1/devices/teksat/clear_multiple | Clear Planning Routes.
[**deviceTeksatList**](DevicesApi.md#deviceteksatlist) | **GET** /0.1/devices/teksat/devices | List Devices.
[**deviceTeksatSendMultiple**](DevicesApi.md#deviceteksatsendmultiple) | **POST** /0.1/devices/teksat/send_multiple | Send Planning Routes.
[**deviceTeksatSync**](DevicesApi.md#deviceteksatsync) | **POST** /0.1/devices/teksat/sync | Synchronise Vehicles.
[**deviceTomtomClear**](DevicesApi.md#devicetomtomclear) | **DELETE** /0.1/devices/tomtom/clear | Clear Route.
[**deviceTomtomClearMultiple**](DevicesApi.md#devicetomtomclearmultiple) | **DELETE** /0.1/devices/tomtom/clear_multiple | Clear Planning Routes.
[**deviceTomtomList**](DevicesApi.md#devicetomtomlist) | **GET** /0.1/devices/tomtom/devices | List Devices.
[**deviceTomtomSendMultiple**](DevicesApi.md#devicetomtomsendmultiple) | **POST** /0.1/devices/tomtom/send_multiple | Send Planning Routes.
[**deviceTomtomSync**](DevicesApi.md#devicetomtomsync) | **POST** /0.1/devices/tomtom/sync | Synchronise Vehicles.
[**getFleetRoutes**](DevicesApi.md#getfleetroutes) | **GET** /0.1/devices/fleet/fetch_routes | Get Fleet routes.
[**reporting_**](DevicesApi.md#reporting_) | **GET** /0.1/devices/fleet/reporting | Get reporting.
[**sendRoute**](DevicesApi.md#sendroute) | **POST** /0.1/devices/{device}/send | Send Route.

# **checkAuth**
> checkAuth($device, $id)

Validate device Credentials.

Generic device.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$device = "device_example"; // string | 
$id = 56; // int | Customer ID as we need to get customer devices

try {
    $apiInstance->checkAuth($device, $id);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->checkAuth: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **device** | **string**|  |
 **id** | **int**| Customer ID as we need to get customer devices |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceAlyacomSendMultiple**
> deviceAlyacomSendMultiple($body)

Send Planning Routes.

For Alyacom device.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Mapotempo\Model\Body57(); // \Mapotempo\Model\Body57 | 

try {
    $apiInstance->deviceAlyacomSendMultiple($body);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceAlyacomSendMultiple: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Mapotempo\Model\Body57**](../Model/Body57.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceFleetClear**
> deviceFleetClear($route_id)

Clear Route.

In Mapotempo Live (Fleet).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$route_id = 56; // int | Route ID

try {
    $apiInstance->deviceFleetClear($route_id);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceFleetClear: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **route_id** | **int**| Route ID |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceFleetClearMultiple**
> deviceFleetClearMultiple($external_refs_fleet_user, $external_refs_external_ref)

Clear multiple routes.

In Mapotempo Live (Fleet).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$external_refs_fleet_user = array("external_refs_fleet_user_example"); // string[] | 
$external_refs_external_ref = array("external_refs_external_ref_example"); // string[] | 

try {
    $apiInstance->deviceFleetClearMultiple($external_refs_fleet_user, $external_refs_external_ref);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceFleetClearMultiple: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **external_refs_fleet_user** | [**string[]**](../Model/string.md)|  |
 **external_refs_external_ref** | [**string[]**](../Model/string.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceFleetCreateCompanyAndDrivers**
> deviceFleetCreateCompanyAndDrivers()

Create company with drivers.

In Mapotempo Live (Fleet).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->deviceFleetCreateCompanyAndDrivers();
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceFleetCreateCompanyAndDrivers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceFleetCreateDrivers**
> deviceFleetCreateDrivers()

Create drivers.

In Mapotempo Live (Fleet).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->deviceFleetCreateDrivers();
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceFleetCreateDrivers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceFleetDemoClear**
> deviceFleetDemoClear($route_id)

Clear Route.

In Mapotempo Live demo version (Fleet Demo).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$route_id = 56; // int | Route ID

try {
    $apiInstance->deviceFleetDemoClear($route_id);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceFleetDemoClear: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **route_id** | **int**| Route ID |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceFleetDemoClearMultiple**
> deviceFleetDemoClearMultiple($planning_id)

Clear Planning Routes.

In Mapotempo Live demo version (Fleet Demo).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$planning_id = 56; // int | Planning ID

try {
    $apiInstance->deviceFleetDemoClearMultiple($planning_id);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceFleetDemoClearMultiple: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planning_id** | **int**| Planning ID |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceFleetDemoSendMultiple**
> deviceFleetDemoSendMultiple($body)

Send Planning Routes.

In Mapotempo Live demo version (Fleet Demo).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Mapotempo\Model\Body54(); // \Mapotempo\Model\Body54 | 

try {
    $apiInstance->deviceFleetDemoSendMultiple($body);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceFleetDemoSendMultiple: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Mapotempo\Model\Body54**](../Model/Body54.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceFleetList**
> \Mapotempo\Model\V01DevicesDeviceItem[] deviceFleetList()

List Devices.

List Mapotempo Live devices (Fleet).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->deviceFleetList();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceFleetList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Mapotempo\Model\V01DevicesDeviceItem[]**](../Model/V01DevicesDeviceItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceFleetSendMultiple**
> deviceFleetSendMultiple($body)

Send Planning Routes.

In Mapotempo Live (Fleet).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Mapotempo\Model\Body55(); // \Mapotempo\Model\Body55 | 

try {
    $apiInstance->deviceFleetSendMultiple($body);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceFleetSendMultiple: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Mapotempo\Model\Body55**](../Model/Body55.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceFleetSync**
> deviceFleetSync()

Synchronise Vehicles.

In Mapotempo Live (Fleet).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->deviceFleetSync();
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceFleetSync: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceFleetTrackingList**
> \Mapotempo\Model\V01DevicesDeviceItem[] deviceFleetTrackingList()

List Devices.

For fleet tracking devices.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->deviceFleetTrackingList();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceFleetTrackingList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Mapotempo\Model\V01DevicesDeviceItem[]**](../Model/V01DevicesDeviceItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceMasternautSendMultiple**
> deviceMasternautSendMultiple($body)

Send Planning Routes.

For Masternaut device.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Mapotempo\Model\Body58(); // \Mapotempo\Model\Body58 | 

try {
    $apiInstance->deviceMasternautSendMultiple($body);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceMasternautSendMultiple: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Mapotempo\Model\Body58**](../Model/Body58.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceNoticoClear**
> deviceNoticoClear($route_id)

Clear Route.

For Notico device.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$route_id = 56; // int | Route ID

try {
    $apiInstance->deviceNoticoClear($route_id);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceNoticoClear: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **route_id** | **int**| Route ID |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceNoticoClearMultiple**
> deviceNoticoClearMultiple($planning_id)

Clear Planning Routes.

For Notico device.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$planning_id = 56; // int | Planning ID

try {
    $apiInstance->deviceNoticoClearMultiple($planning_id);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceNoticoClearMultiple: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planning_id** | **int**| Planning ID |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceNoticoSendMultiple**
> deviceNoticoSendMultiple($body)

Send Planning Routes.

For Notico device.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Mapotempo\Model\Body62(); // \Mapotempo\Model\Body62 | 

try {
    $apiInstance->deviceNoticoSendMultiple($body);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceNoticoSendMultiple: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Mapotempo\Model\Body62**](../Model/Body62.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceOrangeClear**
> deviceOrangeClear($route_id)

Clear Route.

For Orange device.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$route_id = 56; // int | Route ID

try {
    $apiInstance->deviceOrangeClear($route_id);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceOrangeClear: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **route_id** | **int**| Route ID |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceOrangeClearMultiple**
> deviceOrangeClearMultiple($planning_id)

Clear Planning Routes.

For Orange device.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$planning_id = 56; // int | Planning ID

try {
    $apiInstance->deviceOrangeClearMultiple($planning_id);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceOrangeClearMultiple: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planning_id** | **int**| Planning ID |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceOrangeClearSync**
> deviceOrangeClearSync()

Synchronise Vehicles.

For Orange device.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->deviceOrangeClearSync();
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceOrangeClearSync: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceOrangeList**
> \Mapotempo\Model\V01DevicesDeviceItem[] deviceOrangeList()

List Devices.

For Orange device.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->deviceOrangeList();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceOrangeList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Mapotempo\Model\V01DevicesDeviceItem[]**](../Model/V01DevicesDeviceItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceOrangeSendMultiple**
> deviceOrangeSendMultiple($body)

Send Planning Routes.

For Orange device.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Mapotempo\Model\Body59(); // \Mapotempo\Model\Body59 | 

try {
    $apiInstance->deviceOrangeSendMultiple($body);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceOrangeSendMultiple: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Mapotempo\Model\Body59**](../Model/Body59.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **devicePraxedoClear**
> devicePraxedoClear($route_id)

Clear route from Praxedo.

Clear route from Praxedo.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$route_id = 56; // int | Planning ID

try {
    $apiInstance->devicePraxedoClear($route_id);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->devicePraxedoClear: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **route_id** | **int**| Planning ID |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **devicePraxedoClear_0**
> devicePraxedoClear_0($planning_id)

Clear routes from Praxedo.

Clear routes from Praxedo.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$planning_id = 56; // int | Planning ID

try {
    $apiInstance->devicePraxedoClear_0($planning_id);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->devicePraxedoClear_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planning_id** | **int**| Planning ID |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **devicePraxedoSendMultiple**
> devicePraxedoSendMultiple($body)

Send Planning Routes.

On Praxedo.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Mapotempo\Model\Body63(); // \Mapotempo\Model\Body63 | 

try {
    $apiInstance->devicePraxedoSendMultiple($body);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->devicePraxedoSendMultiple: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Mapotempo\Model\Body63**](../Model/Body63.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceSopacList**
> \Mapotempo\Model\V01DevicesDeviceItem[] deviceSopacList()

List Devices.

List Sopac devices.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->deviceSopacList();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceSopacList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Mapotempo\Model\V01DevicesDeviceItem[]**](../Model/V01DevicesDeviceItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceTeksatClear**
> deviceTeksatClear($route_id)

Clear Route.

For Teksat devices.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$route_id = 56; // int | Route ID

try {
    $apiInstance->deviceTeksatClear($route_id);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceTeksatClear: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **route_id** | **int**| Route ID |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceTeksatClearMultiple**
> deviceTeksatClearMultiple($planning_id)

Clear Planning Routes.

For Teksat devices.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$planning_id = 56; // int | Planning ID

try {
    $apiInstance->deviceTeksatClearMultiple($planning_id);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceTeksatClearMultiple: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planning_id** | **int**| Planning ID |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceTeksatList**
> \Mapotempo\Model\V01DevicesDeviceItem[] deviceTeksatList()

List Devices.

For Teksat devices.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->deviceTeksatList();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceTeksatList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Mapotempo\Model\V01DevicesDeviceItem[]**](../Model/V01DevicesDeviceItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceTeksatSendMultiple**
> deviceTeksatSendMultiple($body)

Send Planning Routes.

For Teksat devices.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Mapotempo\Model\Body60(); // \Mapotempo\Model\Body60 | 

try {
    $apiInstance->deviceTeksatSendMultiple($body);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceTeksatSendMultiple: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Mapotempo\Model\Body60**](../Model/Body60.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceTeksatSync**
> deviceTeksatSync()

Synchronise Vehicles.

For Teksat devices.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->deviceTeksatSync();
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceTeksatSync: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceTomtomClear**
> deviceTomtomClear($route_id)

Clear Route.

For TomTom device.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$route_id = 56; // int | Route ID

try {
    $apiInstance->deviceTomtomClear($route_id);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceTomtomClear: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **route_id** | **int**| Route ID |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceTomtomClearMultiple**
> deviceTomtomClearMultiple($planning_id)

Clear Planning Routes.

For TomTom device.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$planning_id = 56; // int | Planning ID

try {
    $apiInstance->deviceTomtomClearMultiple($planning_id);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceTomtomClearMultiple: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planning_id** | **int**| Planning ID |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceTomtomList**
> \Mapotempo\Model\V01DevicesDeviceItem[] deviceTomtomList()

List Devices.

For TomTom device.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->deviceTomtomList();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceTomtomList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Mapotempo\Model\V01DevicesDeviceItem[]**](../Model/V01DevicesDeviceItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceTomtomSendMultiple**
> deviceTomtomSendMultiple($body)

Send Planning Routes.

For TomTom device.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Mapotempo\Model\Body61(); // \Mapotempo\Model\Body61 | 

try {
    $apiInstance->deviceTomtomSendMultiple($body);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceTomtomSendMultiple: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Mapotempo\Model\Body61**](../Model/Body61.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deviceTomtomSync**
> deviceTomtomSync()

Synchronise Vehicles.

For TomTom device.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->deviceTomtomSync();
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->deviceTomtomSync: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getFleetRoutes**
> getFleetRoutes($from, $to)

Get Fleet routes.

In Mapotempo Live (Fleet).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$from = new \DateTime("2013-10-20"); // \DateTime | 
$to = new \DateTime("2013-10-20"); // \DateTime | 

try {
    $apiInstance->getFleetRoutes($from, $to);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->getFleetRoutes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **from** | **\DateTime**|  | [optional]
 **to** | **\DateTime**|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **reporting_**
> reporting_($begin_date, $end_date, $with_actions)

Get reporting.

Get reporting from Mapotempo Live missions. Range between begin_date and end_date must be inferior to 31 days.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$begin_date = new \DateTime("2013-10-20"); // \DateTime | Select only plannings after this date.Local format depends of the locale sent in http header. Default locale send is english (en)    ex:    en: mm-dd-yyyy    fr: dd-mm-yyyy
$end_date = new \DateTime("2013-10-20"); // \DateTime | Select only plannings before this date.Local format depends of the locale sent in http header. Default locale send is english (en)    ex:    en: mm-dd-yyyy    fr: dd-mm-yyyy
$with_actions = true; // bool | Get history of actions

try {
    $apiInstance->reporting_($begin_date, $end_date, $with_actions);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->reporting_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **begin_date** | **\DateTime**| Select only plannings after this date.Local format depends of the locale sent in http header. Default locale send is english (en)    ex:    en: mm-dd-yyyy    fr: dd-mm-yyyy |
 **end_date** | **\DateTime**| Select only plannings before this date.Local format depends of the locale sent in http header. Default locale send is english (en)    ex:    en: mm-dd-yyyy    fr: dd-mm-yyyy |
 **with_actions** | **bool**| Get history of actions | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sendRoute**
> sendRoute($body, $device)

Send Route.

Generic device.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DevicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Mapotempo\Model\Body53(); // \Mapotempo\Model\Body53 | 
$device = "device_example"; // string | 

try {
    $apiInstance->sendRoute($body, $device);
} catch (Exception $e) {
    echo 'Exception when calling DevicesApi->sendRoute: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Mapotempo\Model\Body53**](../Model/Body53.md)|  |
 **device** | **string**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

