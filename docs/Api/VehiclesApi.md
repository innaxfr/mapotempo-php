# Mapotempo\VehiclesApi

All URIs are relative to *//app.mapotempo.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createVehicle**](VehiclesApi.md#createvehicle) | **POST** /0.1/vehicles | Create vehicle (admin).
[**currentPosition**](VehiclesApi.md#currentposition) | **GET** /0.1/vehicles/current_position | Get vehicle&#x27;s positions.
[**deleteVehicle**](VehiclesApi.md#deletevehicle) | **DELETE** /0.1/vehicles/{id} | Delete vehicle (admin).
[**deleteVehicles**](VehiclesApi.md#deletevehicles) | **DELETE** /0.1/vehicles | Delete multiple vehicles (admin).
[**getDeliverablesByVehicles**](VehiclesApi.md#getdeliverablesbyvehicles) | **GET** /0.1/vehicles/{id}/deliverable_units | Fetch deliverables by vehicle for select plans
[**getTemperature**](VehiclesApi.md#gettemperature) | **GET** /0.1/vehicles/temperature | Get vehicle&#x27;s temperatures.
[**getVehicle**](VehiclesApi.md#getvehicle) | **GET** /0.1/vehicles/{id} | Fetch vehicle.
[**getVehicles**](VehiclesApi.md#getvehicles) | **GET** /0.1/vehicles | Fetch customer&#x27;s vehicles.
[**updateVehicle**](VehiclesApi.md#updatevehicle) | **PUT** /0.1/vehicles/{id} | Update vehicle.

# **createVehicle**
> \Mapotempo\Model\V01Vehicle createVehicle($body)

Create vehicle (admin).

Only available with an admin api_key. For each new created Vehicle and VehicleUsageSet a new VehicleUsage will be created at the same time (i.e. customer has 2 VehicleUsageSets 'Morning' and 'Evening', a new Vehicle is created: 2 new VehicleUsages will be automatically created with the new vehicle).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\VehiclesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Mapotempo\Model\Body38(); // \Mapotempo\Model\Body38 | 

try {
    $result = $apiInstance->createVehicle($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehiclesApi->createVehicle: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Mapotempo\Model\Body38**](../Model/Body38.md)|  |

### Return type

[**\Mapotempo\Model\V01Vehicle**](../Model/V01Vehicle.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **currentPosition**
> \Mapotempo\Model\V01VehiclePosition[] currentPosition($ids)

Get vehicle's positions.

Only available if enable_vehicle_position is true for customer.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\VehiclesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ids = array(56); // int[] | 

try {
    $result = $apiInstance->currentPosition($ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehiclesApi->currentPosition: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ids** | [**int[]**](../Model/int.md)|  | [optional]

### Return type

[**\Mapotempo\Model\V01VehiclePosition[]**](../Model/V01VehiclePosition.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteVehicle**
> deleteVehicle($id)

Delete vehicle (admin).

Only available with an admin api_key.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\VehiclesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.

try {
    $apiInstance->deleteVehicle($id);
} catch (Exception $e) {
    echo 'Exception when calling VehiclesApi->deleteVehicle: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteVehicles**
> deleteVehicles()

Delete multiple vehicles (admin).

Only available with an admin api_key.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\VehiclesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->deleteVehicles();
} catch (Exception $e) {
    echo 'Exception when calling VehiclesApi->deleteVehicles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDeliverablesByVehicles**
> \Mapotempo\Model\V01Layer getDeliverablesByVehicles($id, $planning_ids)

Fetch deliverables by vehicle for select plans

Get list of deliverable for a vehicle on each selected plans

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\VehiclesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Vehicle ID
$planning_ids = "planning_ids_example"; // string | Plannings ids

try {
    $result = $apiInstance->getDeliverablesByVehicles($id, $planning_ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehiclesApi->getDeliverablesByVehicles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Vehicle ID |
 **planning_ids** | **string**| Plannings ids |

### Return type

[**\Mapotempo\Model\V01Layer**](../Model/V01Layer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTemperature**
> \Mapotempo\Model\V01VehicleTemperature[] getTemperature($route_ids)

Get vehicle's temperatures.

return vehicle temperature

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\VehiclesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$route_ids = array(56); // int[] | 

try {
    $result = $apiInstance->getTemperature($route_ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehiclesApi->getTemperature: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **route_ids** | [**int[]**](../Model/int.md)|  | [optional]

### Return type

[**\Mapotempo\Model\V01VehicleTemperature[]**](../Model/V01VehicleTemperature.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVehicle**
> \Mapotempo\Model\V01Vehicle getVehicle($id)

Fetch vehicle.

Fetch vehicle.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\VehiclesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.

try {
    $result = $apiInstance->getVehicle($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehiclesApi->getVehicle: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |

### Return type

[**\Mapotempo\Model\V01Vehicle**](../Model/V01Vehicle.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVehicles**
> \Mapotempo\Model\V01Vehicle[] getVehicles($ids, $name_like, $ref_like, $words_mask)

Fetch customer's vehicles.

Fetch customer's vehicles.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\VehiclesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ids = array("ids_example"); // string[] | Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add `ref:` before each ref, e.g. `ref:ref1,ref:ref2,ref:ref3`.
$name_like = "name_like_example"; // string | Find vehicles with similarity on the name (case insensitive).
$ref_like = "ref_like_example"; // string | Find vehicles with similarity on the reference (case insensitive).
$words_mask = "words_mask_example"; // string | Words separated by commas that will skip the similarity check (case insensitive)

try {
    $result = $apiInstance->getVehicles($ids, $name_like, $ref_like, $words_mask);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehiclesApi->getVehicles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ids** | [**string[]**](../Model/string.md)| Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add &#x60;ref:&#x60; before each ref, e.g. &#x60;ref:ref1,ref:ref2,ref:ref3&#x60;. | [optional]
 **name_like** | **string**| Find vehicles with similarity on the name (case insensitive). | [optional]
 **ref_like** | **string**| Find vehicles with similarity on the reference (case insensitive). | [optional]
 **words_mask** | **string**| Words separated by commas that will skip the similarity check (case insensitive) | [optional]

### Return type

[**\Mapotempo\Model\V01Vehicle[]**](../Model/V01Vehicle.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateVehicle**
> \Mapotempo\Model\V01Vehicle updateVehicle($id, $body)

Update vehicle.

Update vehicle.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\VehiclesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$body = new \Mapotempo\Model\Body39(); // \Mapotempo\Model\Body39 | 

try {
    $result = $apiInstance->updateVehicle($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehiclesApi->updateVehicle: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **body** | [**\Mapotempo\Model\Body39**](../Model/Body39.md)|  | [optional]

### Return type

[**\Mapotempo\Model\V01Vehicle**](../Model/V01Vehicle.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

