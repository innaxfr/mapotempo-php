# Mapotempo\ZoningsApi

All URIs are relative to *//app.mapotempo.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**buildIsochrone**](ZoningsApi.md#buildisochrone) | **PATCH** /0.1/zonings/isochrone | Build isochrone for a point.
[**buildIsodistance**](ZoningsApi.md#buildisodistance) | **PATCH** /0.1/zonings/isodistance | Build isodistance for a point.
[**createZoning**](ZoningsApi.md#createzoning) | **POST** /0.1/zonings | Create zoning.
[**deleteZoning**](ZoningsApi.md#deletezoning) | **DELETE** /0.1/zonings/{id} | Delete zoning.
[**deleteZonings**](ZoningsApi.md#deletezonings) | **DELETE** /0.1/zonings | Delete multiple zonings.
[**generateAutomatic**](ZoningsApi.md#generateautomatic) | **PATCH** /0.1/zonings/{id}/automatic/{planning_id} | Generate zoning automatically.
[**generateFromPlanning**](ZoningsApi.md#generatefromplanning) | **PATCH** /0.1/zonings/{id}/from_planning/{planning_id} | Generate zoning from planning.
[**generateIsochrone**](ZoningsApi.md#generateisochrone) | **PATCH** /0.1/zonings/{id}/isochrone | Generate isochrones.
[**generateIsochroneVehicleUsage**](ZoningsApi.md#generateisochronevehicleusage) | **PATCH** /0.1/zonings/{id}/vehicle_usage/{vehicle_usage_id}/isochrone | Generate isochrone for only one vehicle usage.
[**generateIsodistance**](ZoningsApi.md#generateisodistance) | **PATCH** /0.1/zonings/{id}/isodistance | Generate isodistances.
[**generateIsodistanceVehicleUsage**](ZoningsApi.md#generateisodistancevehicleusage) | **PATCH** /0.1/zonings/{id}/vehicle_usage/{vehicle_usage_id}/isodistance | Generate isodistance for only one vehicle usage.
[**getZoning**](ZoningsApi.md#getzoning) | **GET** /0.1/zonings/{id} | Fetch zoning.
[**getZonings**](ZoningsApi.md#getzonings) | **GET** /0.1/zonings | Fetch customer&#x27;s zonings.
[**polygonByPoint**](ZoningsApi.md#polygonbypoint) | **GET** /0.1/zonings/{id}/polygon_by_point | Find the zone where a point belongs to.
[**updateZoning**](ZoningsApi.md#updatezoning) | **PUT** /0.1/zonings/{id} | Update zoning.

# **buildIsochrone**
> \Mapotempo\Model\V01Zoning buildIsochrone($lat, $lng, $size, $vehicle_usage_id, $departure)

Build isochrone for a point.

Build isochrone polygon from a specific point. No zoning is saved in database.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\ZoningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$lat = 3.4; // float | 
$lng = 3.4; // float | 
$size = 56; // int | 
$vehicle_usage_id = 56; // int | 
$departure = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 

try {
    $result = $apiInstance->buildIsochrone($lat, $lng, $size, $vehicle_usage_id, $departure);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ZoningsApi->buildIsochrone: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lat** | **float**|  |
 **lng** | **float**|  |
 **size** | **int**|  |
 **vehicle_usage_id** | **int**|  |
 **departure** | **\DateTime**|  |

### Return type

[**\Mapotempo\Model\V01Zoning**](../Model/V01Zoning.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **buildIsodistance**
> \Mapotempo\Model\V01Zoning buildIsodistance($lat, $lng, $size, $vehicle_usage_id, $departure)

Build isodistance for a point.

Build isodistance polygon from a specific point. No zoning is saved in database.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\ZoningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$lat = 3.4; // float | 
$lng = 3.4; // float | 
$size = 56; // int | 
$vehicle_usage_id = 56; // int | 
$departure = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 

try {
    $result = $apiInstance->buildIsodistance($lat, $lng, $size, $vehicle_usage_id, $departure);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ZoningsApi->buildIsodistance: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lat** | **float**|  |
 **lng** | **float**|  |
 **size** | **int**|  |
 **vehicle_usage_id** | **int**|  |
 **departure** | **\DateTime**|  |

### Return type

[**\Mapotempo\Model\V01Zoning**](../Model/V01Zoning.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createZoning**
> \Mapotempo\Model\V01Zoning createZoning($body)

Create zoning.

Create a new empty zoning. Zones will can be created for this zoning thereafter.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\ZoningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Mapotempo\Model\Body44(); // \Mapotempo\Model\Body44 | 

try {
    $result = $apiInstance->createZoning($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ZoningsApi->createZoning: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Mapotempo\Model\Body44**](../Model/Body44.md)|  |

### Return type

[**\Mapotempo\Model\V01Zoning**](../Model/V01Zoning.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteZoning**
> deleteZoning($id)

Delete zoning.

Delete zoning.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\ZoningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->deleteZoning($id);
} catch (Exception $e) {
    echo 'Exception when calling ZoningsApi->deleteZoning: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteZonings**
> deleteZonings()

Delete multiple zonings.

Delete multiple zonings.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\ZoningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->deleteZonings();
} catch (Exception $e) {
    echo 'Exception when calling ZoningsApi->deleteZonings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **generateAutomatic**
> \Mapotempo\Model\V01Zoning generateAutomatic($id, $planning_id, $n)

Generate zoning automatically.

Create #N new automatic zones in current zoning for a dedicated planning (#N should be less or egal your vehicle's fleet size). All planning's stops (or all visits from destinations if there is no planning linked to this zoning) are taken into account, even if they are out of route. All previous existing zones are cleared. Each generated zone is linked to a dedicated vehicle.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\ZoningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 
$planning_id = 56; // int | 
$n = 56; // int | 

try {
    $result = $apiInstance->generateAutomatic($id, $planning_id, $n);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ZoningsApi->generateAutomatic: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **planning_id** | **int**|  |
 **n** | **int**|  | [optional]

### Return type

[**\Mapotempo\Model\V01Zoning**](../Model/V01Zoning.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **generateFromPlanning**
> \Mapotempo\Model\V01Zoning generateFromPlanning($id, $planning_id)

Generate zoning from planning.

Create new automatic zones in current zoning for a dedicated planning. Only stops in a route with vehicle are taken into account. All previous existing zones are cleared. Each generated zone is linked to a dedicated vehicle.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\ZoningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 
$planning_id = 56; // int | 

try {
    $result = $apiInstance->generateFromPlanning($id, $planning_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ZoningsApi->generateFromPlanning: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **planning_id** | **int**|  |

### Return type

[**\Mapotempo\Model\V01Zoning**](../Model/V01Zoning.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **generateIsochrone**
> \Mapotempo\Model\V01Zoning generateIsochrone($size, $vehicle_usage_set_id, $departure_date, $id)

Generate isochrones.

Generate zoning with isochrone polygons for all vehicles. All previous existing zones are cleared.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\ZoningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$size = 56; // int | 
$vehicle_usage_set_id = 56; // int | 
$departure_date = new \DateTime("2013-10-20"); // \DateTime | 
$id = 56; // int | 

try {
    $result = $apiInstance->generateIsochrone($size, $vehicle_usage_set_id, $departure_date, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ZoningsApi->generateIsochrone: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**|  |
 **vehicle_usage_set_id** | **int**|  |
 **departure_date** | **\DateTime**|  |
 **id** | **int**|  |

### Return type

[**\Mapotempo\Model\V01Zoning**](../Model/V01Zoning.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **generateIsochroneVehicleUsage**
> \Mapotempo\Model\V01Zone generateIsochroneVehicleUsage($size, $departure_date, $id, $vehicle_usage_id)

Generate isochrone for only one vehicle usage.

Generate zoning with isochrone polygon from specified vehicle's start.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\ZoningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$size = 56; // int | 
$departure_date = new \DateTime("2013-10-20"); // \DateTime | 
$id = 56; // int | 
$vehicle_usage_id = 56; // int | 

try {
    $result = $apiInstance->generateIsochroneVehicleUsage($size, $departure_date, $id, $vehicle_usage_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ZoningsApi->generateIsochroneVehicleUsage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**|  |
 **departure_date** | **\DateTime**|  |
 **id** | **int**|  |
 **vehicle_usage_id** | **int**|  |

### Return type

[**\Mapotempo\Model\V01Zone**](../Model/V01Zone.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **generateIsodistance**
> \Mapotempo\Model\V01Zoning generateIsodistance($size, $vehicle_usage_set_id, $departure_date, $id)

Generate isodistances.

Generate zoning with isodistance polygons for all vehicles. All previous existing zones are cleared.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\ZoningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$size = 56; // int | 
$vehicle_usage_set_id = 56; // int | 
$departure_date = new \DateTime("2013-10-20"); // \DateTime | 
$id = 56; // int | 

try {
    $result = $apiInstance->generateIsodistance($size, $vehicle_usage_set_id, $departure_date, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ZoningsApi->generateIsodistance: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**|  |
 **vehicle_usage_set_id** | **int**|  |
 **departure_date** | **\DateTime**|  |
 **id** | **int**|  |

### Return type

[**\Mapotempo\Model\V01Zoning**](../Model/V01Zoning.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **generateIsodistanceVehicleUsage**
> \Mapotempo\Model\V01Zone generateIsodistanceVehicleUsage($size, $departure_date, $id, $vehicle_usage_id)

Generate isodistance for only one vehicle usage.

Generate zoning with isodistance polygon from specified vehicle's start.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\ZoningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$size = 56; // int | 
$departure_date = new \DateTime("2013-10-20"); // \DateTime | 
$id = 56; // int | 
$vehicle_usage_id = 56; // int | 

try {
    $result = $apiInstance->generateIsodistanceVehicleUsage($size, $departure_date, $id, $vehicle_usage_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ZoningsApi->generateIsodistanceVehicleUsage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**|  |
 **departure_date** | **\DateTime**|  |
 **id** | **int**|  |
 **vehicle_usage_id** | **int**|  |

### Return type

[**\Mapotempo\Model\V01Zone**](../Model/V01Zone.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getZoning**
> \Mapotempo\Model\V01Zoning getZoning($id)

Fetch zoning.

Fetch zoning.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\ZoningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $result = $apiInstance->getZoning($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ZoningsApi->getZoning: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\Mapotempo\Model\V01Zoning**](../Model/V01Zoning.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getZonings**
> \Mapotempo\Model\V01Zoning[] getZonings($ids)

Fetch customer's zonings.

Fetch customer's zonings.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\ZoningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ids = array(56); // int[] | Ids separated by comma

try {
    $result = $apiInstance->getZonings($ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ZoningsApi->getZonings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ids** | [**int[]**](../Model/int.md)| Ids separated by comma | [optional]

### Return type

[**\Mapotempo\Model\V01Zoning[]**](../Model/V01Zoning.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **polygonByPoint**
> \Mapotempo\Model\V01Zone polygonByPoint($lat, $lng, $id)

Find the zone where a point belongs to.

Find the closest zone for the specified coordinates.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\ZoningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$lat = 3.4; // float | Decimal value between -90 and 90.
$lng = 3.4; // float | Decimal value between -180 and 180.
$id = 56; // int | 

try {
    $result = $apiInstance->polygonByPoint($lat, $lng, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ZoningsApi->polygonByPoint: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lat** | **float**| Decimal value between -90 and 90. |
 **lng** | **float**| Decimal value between -180 and 180. |
 **id** | **int**|  |

### Return type

[**\Mapotempo\Model\V01Zone**](../Model/V01Zone.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateZoning**
> \Mapotempo\Model\V01Zoning updateZoning($id, $body)

Update zoning.

Update zoning.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\ZoningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 
$body = new \Mapotempo\Model\Body45(); // \Mapotempo\Model\Body45 | 

try {
    $result = $apiInstance->updateZoning($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ZoningsApi->updateZoning: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **body** | [**\Mapotempo\Model\Body45**](../Model/Body45.md)|  | [optional]

### Return type

[**\Mapotempo\Model\V01Zoning**](../Model/V01Zoning.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

