# Mapotempo\RoutesApi

All URIs are relative to *//app.mapotempo.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getRoutes**](RoutesApi.md#getroutes) | **GET** /0.1/routes | Fetch customer&#x27;s routes.

# **getRoutes**
> \Mapotempo\Model\V01Route[] getRoutes($ids, $with_geojson, $with_stores, $with_quantities)

Fetch customer's routes.

Fetch customer's routes.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\RoutesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ids = array("ids_example"); // string[] | Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add `ref:` before each ref, e.g. `ref:ref1,ref:ref2,ref:ref3`.
$with_geojson = "false"; // string | Fill the geojson field with route geometry, when using json output. For geojson output, param can be only set to `point` to return only points, `polyline` to return with encoded linestring.
$with_stores = true; // bool | Include the stores when using geojson output.
$with_quantities = true; // bool | Include the quantities when using geojson output.

try {
    $result = $apiInstance->getRoutes($ids, $with_geojson, $with_stores, $with_quantities);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoutesApi->getRoutes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ids** | [**string[]**](../Model/string.md)| Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add &#x60;ref:&#x60; before each ref, e.g. &#x60;ref:ref1,ref:ref2,ref:ref3&#x60;. | [optional]
 **with_geojson** | **string**| Fill the geojson field with route geometry, when using json output. For geojson output, param can be only set to &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring. | [optional] [default to false]
 **with_stores** | **bool**| Include the stores when using geojson output. | [optional]
 **with_quantities** | **bool**| Include the quantities when using geojson output. | [optional]

### Return type

[**\Mapotempo\Model\V01Route[]**](../Model/V01Route.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

