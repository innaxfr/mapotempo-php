# Mapotempo\RoutersApi

All URIs are relative to *//app.mapotempo.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getRouters**](RoutersApi.md#getrouters) | **GET** /0.1/routers | Fetch customer&#x27;s routers.

# **getRouters**
> \Mapotempo\Model\V01Router[] getRouters()

Fetch customer's routers.

Get the list of available routers which can be used for finding route.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\RoutersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getRouters();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoutersApi->getRouters: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Mapotempo\Model\V01Router[]**](../Model/V01Router.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

