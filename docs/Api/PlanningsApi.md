# Mapotempo\PlanningsApi

All URIs are relative to *//app.mapotempo.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activationStops**](PlanningsApi.md#activationstops) | **PATCH** /0.1/plannings/{planning_id}/routes/{id}/active/{active} | Change stops activation.
[**applyZonings**](PlanningsApi.md#applyzonings) | **GET** /0.1/plannings/{id}/apply_zonings | Apply zonings.
[**automaticInsertStop**](PlanningsApi.md#automaticinsertstop) | **PATCH** /0.1/plannings/{id}/automatic_insert | Insert one or more stop into planning routes.
[**clonePlanning**](PlanningsApi.md#cloneplanning) | **PATCH** /0.1/plannings/{id}/duplicate | Clone the planning.
[**createPlanning**](PlanningsApi.md#createplanning) | **POST** /0.1/plannings | Create planning.
[**deletePlanning**](PlanningsApi.md#deleteplanning) | **DELETE** /0.1/plannings/{id} | Delete planning.
[**deletePlannings**](PlanningsApi.md#deleteplannings) | **DELETE** /0.1/plannings | Delete multiple plannings.
[**getPlanning**](PlanningsApi.md#getplanning) | **GET** /0.1/plannings/{id} | Fetch planning.
[**getPlannings**](PlanningsApi.md#getplannings) | **GET** /0.1/plannings | Fetch customer&#x27;s plannings.
[**getQuantityS**](PlanningsApi.md#getquantitys) | **GET** /0.1/plannings/{id}/quantities | Fetch quantities from a planning.
[**getRoute**](PlanningsApi.md#getroute) | **GET** /0.1/plannings/{planning_id}/routes/{id} | Fetch route.
[**getRouteByVehicle**](PlanningsApi.md#getroutebyvehicle) | **GET** /0.1/plannings/{planning_id}/routes_by_vehicle/{id} | Fetch route from vehicle.
[**getRoutes**](PlanningsApi.md#getroutes) | **GET** /0.1/plannings/{planning_id}/routes | Fetch planning&#x27;s routes.
[**getStop**](PlanningsApi.md#getstop) | **GET** /0.1/plannings/{planning_id}/routes/{route_id}/stops/{id} | Fetch stop.
[**getVehicleUsageS**](PlanningsApi.md#getvehicleusages) | **GET** /0.1/plannings/{id}/vehicle_usages | Fetch vehicle usage(s) from a planning.
[**moveStop**](PlanningsApi.md#movestop) | **PATCH** /0.1/plannings/{planning_id}/routes/{route_id}/stops/{id}/move/{index} | Move stop position in routes.
[**moveVisits**](PlanningsApi.md#movevisits) | **PATCH** /0.1/plannings/{planning_id}/routes/{id}/visits/moves | Move visit(s) to route. Append in order at end (if automatic_insert is false).
[**optimizeRoute**](PlanningsApi.md#optimizeroute) | **PATCH** /0.1/plannings/{planning_id}/routes/{id}/optimize | Optimize a single route.
[**optimizeRoutes**](PlanningsApi.md#optimizeroutes) | **GET** /0.1/plannings/{id}/optimize | Optimize routes.
[**refreshPlanning**](PlanningsApi.md#refreshplanning) | **GET** /0.1/plannings/{id}/refresh | Recompute the planning after parameter update.
[**reverseStopsOrder**](PlanningsApi.md#reversestopsorder) | **PATCH** /0.1/plannings/{planning_id}/routes/{id}/reverse_order | Reverse stops order.
[**sendSMS**](PlanningsApi.md#sendsms) | **GET** /0.1/plannings/{id}/send_sms | Send SMS for each stop visit.
[**sendSMS_0**](PlanningsApi.md#sendsms_0) | **GET** /0.1/plannings/{planning_id}/routes/{id}/send_sms | Send SMS for each stop visit.
[**switchVehicles**](PlanningsApi.md#switchvehicles) | **PATCH** /0.1/plannings/{id}/switch | Switch two vehicles.
[**updatePlanning**](PlanningsApi.md#updateplanning) | **PUT** /0.1/plannings/{id} | Update planning.
[**updateRoute**](PlanningsApi.md#updateroute) | **PUT** /0.1/plannings/{planning_id}/routes/{id} | Update route attributes.
[**updateRoutes**](PlanningsApi.md#updateroutes) | **PATCH** /0.1/plannings/{id}/update_routes | Update routes visibility and lock. (DEPRECATED. For more information, please use Update Planning operation instead)
[**updateStop**](PlanningsApi.md#updatestop) | **PUT** /0.1/plannings/{planning_id}/routes/{route_id}/stops/{id} | Update stop.
[**updateStopsStatus**](PlanningsApi.md#updatestopsstatus) | **PATCH** /0.1/plannings/{id}/update_stops_status | Update stops live status.
[**useOrderArray**](PlanningsApi.md#useorderarray) | **PATCH** /0.1/plannings/{id}/order_array | Use order_array in the planning.

# **activationStops**
> \Mapotempo\Model\V01Route activationStops($planning_id, $id, $active, $with_geojson)

Change stops activation.

Allow to activate/deactivate all stops in a planning's route.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$planning_id = "planning_id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$active = "active_example"; // string | 
$with_geojson = "with_geojson_example"; // string | 

try {
    $result = $apiInstance->activationStops($planning_id, $id, $active, $with_geojson);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->activationStops: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planning_id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **active** | **string**|  |
 **with_geojson** | **string**|  | [optional]

### Return type

[**\Mapotempo\Model\V01Route**](../Model/V01Route.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **applyZonings**
> applyZonings($id, $with_details, $with_geojson)

Apply zonings.

Apply zoning by assign stops to vehicles using the corresponding zones.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$with_details = true; // bool | Output route details
$with_geojson = "false"; // string | Fill in response the geojson field with route geometry: `point` to return only points, `polyline` to return with encoded linestring, `true` to return both.

try {
    $apiInstance->applyZonings($id, $with_details, $with_geojson);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->applyZonings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **with_details** | **bool**| Output route details | [optional]
 **with_geojson** | **string**| Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **automaticInsertStop**
> automaticInsertStop($id, $max_time, $max_distance, $active_only, $out_of_zone)

Insert one or more stop into planning routes.

Insert automatically one or more stops in best routes and on best positions to have minimal influence on route's total time (this operation doesn't take into account time windows if they exist...). You should use this operation with existing stops in current planning's routes. In addition, you should not use this operation with many stops. You should use instead zoning (with automatic clustering creation for instance) to set multiple stops in each available route.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$max_time = 3.4; // float | 
$max_distance = 3.4; // float | 
$active_only = true; // bool | 
$out_of_zone = true; // bool | 

try {
    $apiInstance->automaticInsertStop($id, $max_time, $max_distance, $active_only, $out_of_zone);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->automaticInsertStop: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **max_time** | **float**|  | [optional]
 **max_distance** | **float**|  | [optional]
 **active_only** | **bool**|  | [optional]
 **out_of_zone** | **bool**|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **clonePlanning**
> \Mapotempo\Model\V01Planning clonePlanning($id, $with_geojson)

Clone the planning.

Clone the planning.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$with_geojson = "with_geojson_example"; // string | 

try {
    $result = $apiInstance->clonePlanning($id, $with_geojson);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->clonePlanning: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **with_geojson** | **string**|  | [optional]

### Return type

[**\Mapotempo\Model\V01Planning**](../Model/V01Planning.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createPlanning**
> \Mapotempo\Model\V01Planning createPlanning($body)

Create planning.

Create a planning. An out-of-route (unplanned) route and a route for each vehicle are automatically created. If some visits exist (or fetch if you use tags), as many stops as fetching visits will be created (ie: there is no specific operation to create routes and stops, the application create them for you).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Mapotempo\Model\Body14(); // \Mapotempo\Model\Body14 | 

try {
    $result = $apiInstance->createPlanning($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->createPlanning: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Mapotempo\Model\Body14**](../Model/Body14.md)|  |

### Return type

[**\Mapotempo\Model\V01Planning**](../Model/V01Planning.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletePlanning**
> deletePlanning($id)

Delete planning.

Delete planning.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.

try {
    $apiInstance->deletePlanning($id);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->deletePlanning: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletePlannings**
> deletePlannings($ids)

Delete multiple plannings.

Delete multiple plannings.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ids = array("ids_example"); // string[] | 

try {
    $apiInstance->deletePlannings($ids);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->deletePlannings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ids** | [**string[]**](../Model/string.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPlanning**
> \Mapotempo\Model\V01Planning getPlanning($id, $with_geojson, $with_quantities, $with_stores)

Fetch planning.

Fetch planning.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$with_geojson = "false"; // string | Fill the geojson field with route geometry, when using json output. For geojson output, param can be only set to `point` to return only points, `polyline` to return with encoded linestring.
$with_quantities = true; // bool | Include the quantities when using geojson output.
$with_stores = true; // bool | Include the stores in geojson output.

try {
    $result = $apiInstance->getPlanning($id, $with_geojson, $with_quantities, $with_stores);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->getPlanning: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **with_geojson** | **string**| Fill the geojson field with route geometry, when using json output. For geojson output, param can be only set to &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring. | [optional] [default to false]
 **with_quantities** | **bool**| Include the quantities when using geojson output. | [optional]
 **with_stores** | **bool**| Include the stores in geojson output. | [optional]

### Return type

[**\Mapotempo\Model\V01Planning**](../Model/V01Planning.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPlannings**
> \Mapotempo\Model\V01Planning[] getPlannings($ids, $begin_date, $end_date, $active, $tags, $with_geojson, $name_like, $ref_like, $words_mask)

Fetch customer's plannings.

Fetch customer's plannings.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ids = array("ids_example"); // string[] | Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add `ref:` before each ref, e.g. `ref:ref1,ref:ref2,ref:ref3`.
$begin_date = new \DateTime("2013-10-20"); // \DateTime | Select only plannings after this date.
$end_date = new \DateTime("2013-10-20"); // \DateTime | Select only plannings before this date.
$active = true; // bool | Select only active plannings.
$tags = array("tags_example"); // string[] | Select plannings which contains at least one of these tags label
$with_geojson = "false"; // string | Fill in response the geojson field with route geometry: `point` to return only points, `polyline` to return with encoded linestring, `true` to return both.
$name_like = "name_like_example"; // string | Find plannings with similarity on the name (case insensitive).
$ref_like = "ref_like_example"; // string | Find plannings with similarity on the reference (case insensitive).
$words_mask = "words_mask_example"; // string | Words separated by commas that will skip the similarity check (case insensitive)

try {
    $result = $apiInstance->getPlannings($ids, $begin_date, $end_date, $active, $tags, $with_geojson, $name_like, $ref_like, $words_mask);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->getPlannings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ids** | [**string[]**](../Model/string.md)| Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add &#x60;ref:&#x60; before each ref, e.g. &#x60;ref:ref1,ref:ref2,ref:ref3&#x60;. | [optional]
 **begin_date** | **\DateTime**| Select only plannings after this date. | [optional]
 **end_date** | **\DateTime**| Select only plannings before this date. | [optional]
 **active** | **bool**| Select only active plannings. | [optional]
 **tags** | [**string[]**](../Model/string.md)| Select plannings which contains at least one of these tags label | [optional]
 **with_geojson** | **string**| Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. | [optional] [default to false]
 **name_like** | **string**| Find plannings with similarity on the name (case insensitive). | [optional]
 **ref_like** | **string**| Find plannings with similarity on the reference (case insensitive). | [optional]
 **words_mask** | **string**| Words separated by commas that will skip the similarity check (case insensitive) | [optional]

### Return type

[**\Mapotempo\Model\V01Planning[]**](../Model/V01Planning.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getQuantityS**
> getQuantityS($id)

Fetch quantities from a planning.

For internal usage

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | The planning id

try {
    $apiInstance->getQuantityS($id);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->getQuantityS: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The planning id |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getRoute**
> \Mapotempo\Model\V01Route getRoute($planning_id, $id, $with_geojson)

Fetch route.

Fetch route.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$planning_id = "planning_id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$with_geojson = "false"; // string | Fill the geojson field with route geometry, when using json output. For geojson output, param can be only set to `point` to return only points, `polyline` to return with encoded linestring.

try {
    $result = $apiInstance->getRoute($planning_id, $id, $with_geojson);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->getRoute: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planning_id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **with_geojson** | **string**| Fill the geojson field with route geometry, when using json output. For geojson output, param can be only set to &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring. | [optional] [default to false]

### Return type

[**\Mapotempo\Model\V01Route**](../Model/V01Route.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getRouteByVehicle**
> \Mapotempo\Model\V01Route getRouteByVehicle($planning_id, $id, $with_geojson)

Fetch route from vehicle.

Fetch route from vehicle.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$planning_id = "planning_id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$with_geojson = "false"; // string | Fill in response the geojson field with route geometry: `point` to return only points, `polyline` to return with encoded linestring, `true` to return both.

try {
    $result = $apiInstance->getRouteByVehicle($planning_id, $id, $with_geojson);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->getRouteByVehicle: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planning_id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **with_geojson** | **string**| Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. | [optional] [default to false]

### Return type

[**\Mapotempo\Model\V01Route**](../Model/V01Route.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getRoutes**
> \Mapotempo\Model\V01Route[] getRoutes($planning_id, $ids, $with_geojson, $with_stores, $with_quantities)

Fetch planning's routes.

Fetch planning's routes.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$planning_id = "planning_id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$ids = array("ids_example"); // string[] | Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add `ref:` before each ref, e.g. `ref:ref1,ref:ref2,ref:ref3`.
$with_geojson = "false"; // string | Fill the geojson field with route geometry, when using json output. For geojson output, param can be only set to `point` to return only points, `polyline` to return with encoded linestring.
$with_stores = true; // bool | Include the stores when using geojson output.
$with_quantities = true; // bool | Include the quantities when using geojson output.

try {
    $result = $apiInstance->getRoutes($planning_id, $ids, $with_geojson, $with_stores, $with_quantities);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->getRoutes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planning_id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **ids** | [**string[]**](../Model/string.md)| Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add &#x60;ref:&#x60; before each ref, e.g. &#x60;ref:ref1,ref:ref2,ref:ref3&#x60;. | [optional]
 **with_geojson** | **string**| Fill the geojson field with route geometry, when using json output. For geojson output, param can be only set to &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring. | [optional] [default to false]
 **with_stores** | **bool**| Include the stores when using geojson output. | [optional]
 **with_quantities** | **bool**| Include the quantities when using geojson output. | [optional]

### Return type

[**\Mapotempo\Model\V01Route[]**](../Model/V01Route.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getStop**
> \Mapotempo\Model\V01Stop getStop($planning_id, $route_id, $id)

Fetch stop.

Fetch stop.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$planning_id = "planning_id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$route_id = "route_id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$id = 56; // int | 

try {
    $result = $apiInstance->getStop($planning_id, $route_id, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->getStop: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planning_id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **route_id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **id** | **int**|  |

### Return type

[**\Mapotempo\Model\V01Stop**](../Model/V01Stop.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVehicleUsageS**
> getVehicleUsageS($id)

Fetch vehicle usage(s) from a planning.

For internal usage

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | The planning id

try {
    $apiInstance->getVehicleUsageS($id);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->getVehicleUsageS: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The planning id |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **moveStop**
> moveStop($planning_id, $route_id, $id, $index)

Move stop position in routes.

Set a new #N position for a stop in route which was in a previous #M position in the same route or another.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$planning_id = "planning_id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$route_id = "route_id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$id = 56; // int | Stop id to move
$index = 56; // int | New position in the route

try {
    $apiInstance->moveStop($planning_id, $route_id, $id, $index);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->moveStop: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planning_id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **route_id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **id** | **int**| Stop id to move |
 **index** | **int**| New position in the route |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **moveVisits**
> moveVisits($planning_id, $id, $automatic_insert)

Move visit(s) to route. Append in order at end (if automatic_insert is false).

Set a new A route (or vehicle) for a visit which was in a previous B route in the same planning. `automatic_insert` parameter allows to compute or not the best index of the stops created for visits.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$planning_id = "planning_id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$automatic_insert = true; // bool | 

try {
    $apiInstance->moveVisits($planning_id, $id, $automatic_insert);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->moveVisits: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planning_id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **automatic_insert** | **bool**|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **optimizeRoute**
> optimizeRoute($planning_id, $id, $details, $synchronous, $active_only, $ignore_overload_multipliers, $with_details, $with_geojson)

Optimize a single route.

Get the shortest route in time or distance.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$planning_id = "planning_id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$details = true; // bool | 
$synchronous = true; // bool | 
$active_only = true; // bool | 
$ignore_overload_multipliers = "ignore_overload_multipliers_example"; // string | 
$with_details = true; // bool | 
$with_geojson = "with_geojson_example"; // string | 

try {
    $apiInstance->optimizeRoute($planning_id, $id, $details, $synchronous, $active_only, $ignore_overload_multipliers, $with_details, $with_geojson);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->optimizeRoute: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planning_id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **details** | **bool**|  | [optional]
 **synchronous** | **bool**|  | [optional]
 **active_only** | **bool**|  | [optional]
 **ignore_overload_multipliers** | **string**|  | [optional]
 **with_details** | **bool**|  | [optional]
 **with_geojson** | **string**|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **optimizeRoutes**
> optimizeRoutes($id, $global, $synchronous, $active_only, $ignore_overload_multipliers, $with_details, $with_geojson)

Optimize routes.

Optimize all unlocked routes by keeping visits in same route or not.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$global = true; // bool | Use global optimization and move visits between routes if needed
$synchronous = true; // bool | Synchronous processing or not. If not, operation will return nothing and the optimum result will be set on planning as soon Customer.job_optimizer_id will be null.
$active_only = true; // bool | If `true` only active stops are taken into account by optimization, else inactive stops are also taken into account but are not activated in result route.
$ignore_overload_multipliers = "ignore_overload_multipliers_example"; // string | Deliverable Unit id and whether or not it should be ignored : {'0'=>{'unit_id'=>'7', 'ignore'=>'true'}}
$with_details = true; // bool | Output route details
$with_geojson = "false"; // string | Fill in response the geojson field with route geometry: `point` to return only points, `polyline` to return with encoded linestring, `true` to return both.

try {
    $apiInstance->optimizeRoutes($id, $global, $synchronous, $active_only, $ignore_overload_multipliers, $with_details, $with_geojson);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->optimizeRoutes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **global** | **bool**| Use global optimization and move visits between routes if needed | [optional]
 **synchronous** | **bool**| Synchronous processing or not. If not, operation will return nothing and the optimum result will be set on planning as soon Customer.job_optimizer_id will be null. | [optional] [default to true]
 **active_only** | **bool**| If &#x60;true&#x60; only active stops are taken into account by optimization, else inactive stops are also taken into account but are not activated in result route. | [optional] [default to true]
 **ignore_overload_multipliers** | **string**| Deliverable Unit id and whether or not it should be ignored : {&#x27;0&#x27;&#x3D;&gt;{&#x27;unit_id&#x27;&#x3D;&gt;&#x27;7&#x27;, &#x27;ignore&#x27;&#x3D;&gt;&#x27;true&#x27;}} | [optional]
 **with_details** | **bool**| Output route details | [optional]
 **with_geojson** | **string**| Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **refreshPlanning**
> \Mapotempo\Model\V01Planning refreshPlanning($id, $with_geojson)

Recompute the planning after parameter update.

Refresh planning and outdated routes infos if inputs have been changed (for instance stores, destinations, visits, etc...)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$with_geojson = "false"; // string | Fill in response the geojson field with route geometry: `point` to return only points, `polyline` to return with encoded linestring, `true` to return both.

try {
    $result = $apiInstance->refreshPlanning($id, $with_geojson);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->refreshPlanning: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **with_geojson** | **string**| Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. | [optional] [default to false]

### Return type

[**\Mapotempo\Model\V01Planning**](../Model/V01Planning.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **reverseStopsOrder**
> \Mapotempo\Model\V01Route reverseStopsOrder($planning_id, $id)

Reverse stops order.

Reverse all the stops in a route.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$planning_id = "planning_id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.

try {
    $result = $apiInstance->reverseStopsOrder($planning_id, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->reverseStopsOrder: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planning_id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |

### Return type

[**\Mapotempo\Model\V01Route**](../Model/V01Route.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sendSMS**
> sendSMS($id)

Send SMS for each stop visit.

Send SMS for each stop visit of each routes. Only available if `enable_sms` option is true for customer account.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.

try {
    $apiInstance->sendSMS($id);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->sendSMS: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sendSMS_0**
> sendSMS_0($planning_id, $id)

Send SMS for each stop visit.

Send SMS for each stop visit of the specified route. Only available if `enable_sms` option is true for customer account.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$planning_id = "planning_id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.

try {
    $apiInstance->sendSMS_0($planning_id, $id);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->sendSMS_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planning_id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **switchVehicles**
> switchVehicles($route_id, $vehicle_usage_id, $with_details, $with_geojson, $id)

Switch two vehicles.

Switch vehicle associated to one route with another existing vehicle.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$route_id = 56; // int | 
$vehicle_usage_id = 56; // int | 
$with_details = true; // bool | 
$with_geojson = "with_geojson_example"; // string | 
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.

try {
    $apiInstance->switchVehicles($route_id, $vehicle_usage_id, $with_details, $with_geojson, $id);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->switchVehicles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **route_id** | **int**|  |
 **vehicle_usage_id** | **int**|  |
 **with_details** | **bool**|  |
 **with_geojson** | **string**|  |
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatePlanning**
> \Mapotempo\Model\V01Planning updatePlanning($id, $body)

Update planning.

Update planning.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$body = new \Mapotempo\Model\Body16(); // \Mapotempo\Model\Body16 | 

try {
    $result = $apiInstance->updatePlanning($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->updatePlanning: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **body** | [**\Mapotempo\Model\Body16**](../Model/Body16.md)|  | [optional]

### Return type

[**\Mapotempo\Model\V01Planning**](../Model/V01Planning.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateRoute**
> \Mapotempo\Model\V01RouteProperties updateRoute($planning_id, $id, $body)

Update route attributes.

Update route attributes.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$planning_id = "planning_id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$body = new \Mapotempo\Model\Body23(); // \Mapotempo\Model\Body23 | 

try {
    $result = $apiInstance->updateRoute($planning_id, $id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->updateRoute: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planning_id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **body** | [**\Mapotempo\Model\Body23**](../Model/Body23.md)|  | [optional]

### Return type

[**\Mapotempo\Model\V01RouteProperties**](../Model/V01RouteProperties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateRoutes**
> updateRoutes($action, $selection, $id)

Update routes visibility and lock. (DEPRECATED. For more information, please use Update Planning operation instead)

Update routes visibility and lock. (DEPRECATED. For more information, please use Update Planning operation instead)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$action = "action_example"; // string | 
$selection = "selection_example"; // string | 
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.

try {
    $apiInstance->updateRoutes($action, $selection, $id);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->updateRoutes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **action** | **string**|  |
 **selection** | **string**|  |
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateStop**
> updateStop($planning_id, $route_id, $id, $body)

Update stop.

Update stop.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$planning_id = "planning_id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$route_id = "route_id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$id = 56; // int | Stop id to update
$body = new \Mapotempo\Model\Body27(); // \Mapotempo\Model\Body27 | 

try {
    $apiInstance->updateStop($planning_id, $route_id, $id, $body);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->updateStop: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planning_id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **route_id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **id** | **int**| Stop id to update |
 **body** | [**\Mapotempo\Model\Body27**](../Model/Body27.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateStopsStatus**
> updateStopsStatus($id, $with_details)

Update stops live status.

Update live stops status from remote devices. Only available if `enable_stop_status` option is true for customer account.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$with_details = true; // bool | 

try {
    $apiInstance->updateStopsStatus($id, $with_details);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->updateStopsStatus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **with_details** | **bool**|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **useOrderArray**
> \Mapotempo\Model\V01Planning useOrderArray($order_array_id, $shift, $with_geojson, $id)

Use order_array in the planning.

Only available if `enable_orders` option is active for current customer account.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\PlanningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$order_array_id = 56; // int | 
$shift = 56; // int | 
$with_geojson = "with_geojson_example"; // string | 
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.

try {
    $result = $apiInstance->useOrderArray($order_array_id, $shift, $with_geojson, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlanningsApi->useOrderArray: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_array_id** | **int**|  |
 **shift** | **int**|  |
 **with_geojson** | **string**|  |
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |

### Return type

[**\Mapotempo\Model\V01Planning**](../Model/V01Planning.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

