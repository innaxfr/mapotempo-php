# Mapotempo\DeliverableUnitsApi

All URIs are relative to *//app.mapotempo.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createDeliverableUnit**](DeliverableUnitsApi.md#createdeliverableunit) | **POST** /0.1/deliverable_units | Create deliverable unit.
[**deleteDeliverableUnit**](DeliverableUnitsApi.md#deletedeliverableunit) | **DELETE** /0.1/deliverable_units/{id} | Delete deliverable unit.
[**deleteDeliverableUnits**](DeliverableUnitsApi.md#deletedeliverableunits) | **DELETE** /0.1/deliverable_units | Delete multiple deliverable units.
[**getDeliverableUnit**](DeliverableUnitsApi.md#getdeliverableunit) | **GET** /0.1/deliverable_units/{id} | Fetch deliverable unit.
[**getDeliverableUnits**](DeliverableUnitsApi.md#getdeliverableunits) | **GET** /0.1/deliverable_units | Fetch customer&#x27;s deliverable units. At least one deliverable unit exists per customer.
[**updateDeliverableUnit**](DeliverableUnitsApi.md#updatedeliverableunit) | **PUT** /0.1/deliverable_units/{id} | Update deliverable unit.

# **createDeliverableUnit**
> \Mapotempo\Model\V01DeliverableUnit createDeliverableUnit($body)

Create deliverable unit.

(Note a default deliverable unit is already automatically created when a customer account is initialized.) By creating a new deliverable unit, it will be possible to specify quantities and capacities for this another unit.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DeliverableUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Mapotempo\Model\Body3(); // \Mapotempo\Model\Body3 | 

try {
    $result = $apiInstance->createDeliverableUnit($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliverableUnitsApi->createDeliverableUnit: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Mapotempo\Model\Body3**](../Model/Body3.md)|  | [optional]

### Return type

[**\Mapotempo\Model\V01DeliverableUnit**](../Model/V01DeliverableUnit.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteDeliverableUnit**
> deleteDeliverableUnit($id)

Delete deliverable unit.

Delete deliverable unit.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DeliverableUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.

try {
    $apiInstance->deleteDeliverableUnit($id);
} catch (Exception $e) {
    echo 'Exception when calling DeliverableUnitsApi->deleteDeliverableUnit: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteDeliverableUnits**
> deleteDeliverableUnits()

Delete multiple deliverable units.

Delete multiple deliverable units.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DeliverableUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->deleteDeliverableUnits();
} catch (Exception $e) {
    echo 'Exception when calling DeliverableUnitsApi->deleteDeliverableUnits: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDeliverableUnit**
> \Mapotempo\Model\V01DeliverableUnit getDeliverableUnit($id)

Fetch deliverable unit.

Fetch deliverable unit.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DeliverableUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $result = $apiInstance->getDeliverableUnit($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliverableUnitsApi->getDeliverableUnit: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\Mapotempo\Model\V01DeliverableUnit**](../Model/V01DeliverableUnit.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDeliverableUnits**
> \Mapotempo\Model\V01DeliverableUnit[] getDeliverableUnits($ids, $label_like, $ref_like, $words_mask)

Fetch customer's deliverable units. At least one deliverable unit exists per customer.

Fetch customer's deliverable units. At least one deliverable unit exists per customer.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DeliverableUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ids = array("ids_example"); // string[] | Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add `ref:` before each ref, e.g. `ref:ref1,ref:ref2,ref:ref3`.
$label_like = "label_like_example"; // string | Find deliverable units with similarity on the label (case insensitive).
$ref_like = "ref_like_example"; // string | Find deliverable units with similarity on the reference (case insensitive).
$words_mask = "words_mask_example"; // string | Words separated by commas that will skip the similarity check (case insensitive)

try {
    $result = $apiInstance->getDeliverableUnits($ids, $label_like, $ref_like, $words_mask);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliverableUnitsApi->getDeliverableUnits: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ids** | [**string[]**](../Model/string.md)| Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add &#x60;ref:&#x60; before each ref, e.g. &#x60;ref:ref1,ref:ref2,ref:ref3&#x60;. | [optional]
 **label_like** | **string**| Find deliverable units with similarity on the label (case insensitive). | [optional]
 **ref_like** | **string**| Find deliverable units with similarity on the reference (case insensitive). | [optional]
 **words_mask** | **string**| Words separated by commas that will skip the similarity check (case insensitive) | [optional]

### Return type

[**\Mapotempo\Model\V01DeliverableUnit[]**](../Model/V01DeliverableUnit.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateDeliverableUnit**
> \Mapotempo\Model\V01DeliverableUnit updateDeliverableUnit($id, $body)

Update deliverable unit.

Update deliverable unit.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DeliverableUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$body = new \Mapotempo\Model\Body4(); // \Mapotempo\Model\Body4 | 

try {
    $result = $apiInstance->updateDeliverableUnit($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliverableUnitsApi->updateDeliverableUnit: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **body** | [**\Mapotempo\Model\Body4**](../Model/Body4.md)|  | [optional]

### Return type

[**\Mapotempo\Model\V01DeliverableUnit**](../Model/V01DeliverableUnit.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

