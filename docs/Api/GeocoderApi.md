# Mapotempo\GeocoderApi

All URIs are relative to *//app.mapotempo.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**geocode**](GeocoderApi.md#geocode) | **GET** /0.1/geocoder/search | Geocode.

# **geocode**
> geocode($q, $lat, $lng, $limit)

Geocode.

Return a list of address which match with input query.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\GeocoderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$q = "q_example"; // string | Free query string.
$lat = 3.4; // float | Prioritize results around this latitude.
$lng = 3.4; // float | Prioritize results around this longitude.
$limit = 56; // int | Max results numbers. (default and upper max 10)

try {
    $apiInstance->geocode($q, $lat, $lng, $limit);
} catch (Exception $e) {
    echo 'Exception when calling GeocoderApi->geocode: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **string**| Free query string. |
 **lat** | **float**| Prioritize results around this latitude. | [optional]
 **lng** | **float**| Prioritize results around this longitude. | [optional]
 **limit** | **int**| Max results numbers. (default and upper max 10) | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

