# Mapotempo\DestinationsApi

All URIs are relative to *//app.mapotempo.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**autocompleteDestination**](DestinationsApi.md#autocompletedestination) | **PATCH** /0.1/destinations/geocode_complete | Auto completion on destination.
[**createDestination**](DestinationsApi.md#createdestination) | **POST** /0.1/destinations | Create destination.
[**createVisit**](DestinationsApi.md#createvisit) | **POST** /0.1/destinations/{destination_id}/visits | Create visit.
[**deleteDestination**](DestinationsApi.md#deletedestination) | **DELETE** /0.1/destinations/{id} | Delete destination.
[**deleteDestinations**](DestinationsApi.md#deletedestinations) | **DELETE** /0.1/destinations | Delete multiple destinations.
[**deleteVisit**](DestinationsApi.md#deletevisit) | **DELETE** /0.1/destinations/{destination_id}/visits/{id} | Delete visit.
[**geocodeDestination**](DestinationsApi.md#geocodedestination) | **PATCH** /0.1/destinations/geocode | Geocode destination.
[**getDestination**](DestinationsApi.md#getdestination) | **GET** /0.1/destinations/{id} | Fetch destination.
[**getDestinations**](DestinationsApi.md#getdestinations) | **GET** /0.1/destinations | Fetch customer&#x27;s destinations.
[**getVisit**](DestinationsApi.md#getvisit) | **GET** /0.1/destinations/{destination_id}/visits/{id} | Fetch visit.
[**getVisits**](DestinationsApi.md#getvisits) | **GET** /0.1/destinations/{destination_id}/visits | Fetch destination&#x27;s visits.
[**importDestinations**](DestinationsApi.md#importdestinations) | **PUT** /0.1/destinations | Import destinations by upload a CSV file, by JSON or from TomTom.
[**reverseGeocodingDestination**](DestinationsApi.md#reversegeocodingdestination) | **PATCH** /0.1/destinations/reverse | Reverse geocoding.
[**updateDestination**](DestinationsApi.md#updatedestination) | **PUT** /0.1/destinations/{id} | Update destination.
[**updateVisit**](DestinationsApi.md#updatevisit) | **PUT** /0.1/destinations/{destination_id}/visits/{id} | Update visit.

# **autocompleteDestination**
> \Mapotempo\Model\V01Destination autocompleteDestination($street, $postalcode, $city, $state, $country)

Auto completion on destination.

Auto completion on address of destination. Results are priorized from last geolocated store of the customer account.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DestinationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$street = "street_example"; // string | 
$postalcode = "postalcode_example"; // string | 
$city = "city_example"; // string | 
$state = "state_example"; // string | 
$country = "country_example"; // string | 

try {
    $result = $apiInstance->autocompleteDestination($street, $postalcode, $city, $state, $country);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinationsApi->autocompleteDestination: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **street** | **string**|  | [optional]
 **postalcode** | **string**|  | [optional]
 **city** | **string**|  | [optional]
 **state** | **string**|  | [optional]
 **country** | **string**|  | [optional]

### Return type

[**\Mapotempo\Model\V01Destination**](../Model/V01Destination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createDestination**
> \Mapotempo\Model\V01Destination createDestination($body)

Create destination.

Create destination (point to visit). A same destination/point can be visited multiple times.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DestinationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Mapotempo\Model\Body6(); // \Mapotempo\Model\Body6 | 

try {
    $result = $apiInstance->createDestination($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinationsApi->createDestination: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Mapotempo\Model\Body6**](../Model/Body6.md)|  |

### Return type

[**\Mapotempo\Model\V01Destination**](../Model/V01Destination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createVisit**
> \Mapotempo\Model\V01Visit createVisit($destination_id, $body)

Create visit.

Create visit.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DestinationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$destination_id = "destination_id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$body = new \Mapotempo\Model\Body11(); // \Mapotempo\Model\Body11 | 

try {
    $result = $apiInstance->createVisit($destination_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinationsApi->createVisit: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **destination_id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **body** | [**\Mapotempo\Model\Body11**](../Model/Body11.md)|  | [optional]

### Return type

[**\Mapotempo\Model\V01Visit**](../Model/V01Visit.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteDestination**
> deleteDestination($id)

Delete destination.

Delete destination.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DestinationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.

try {
    $apiInstance->deleteDestination($id);
} catch (Exception $e) {
    echo 'Exception when calling DestinationsApi->deleteDestination: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteDestinations**
> deleteDestinations()

Delete multiple destinations.

Delete multiple destinations.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DestinationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->deleteDestinations();
} catch (Exception $e) {
    echo 'Exception when calling DestinationsApi->deleteDestinations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteVisit**
> deleteVisit($destination_id, $id)

Delete visit.

Delete visit.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DestinationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$destination_id = "destination_id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.

try {
    $apiInstance->deleteVisit($destination_id, $id);
} catch (Exception $e) {
    echo 'Exception when calling DestinationsApi->deleteVisit: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **destination_id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **geocodeDestination**
> \Mapotempo\Model\V01Destination geocodeDestination($street, $postalcode, $city, $state, $country)

Geocode destination.

Result of geocoding is not saved with this operation. You can use update operation to save the result of geocoding.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DestinationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$street = "street_example"; // string | 
$postalcode = "postalcode_example"; // string | 
$city = "city_example"; // string | 
$state = "state_example"; // string | 
$country = "country_example"; // string | 

try {
    $result = $apiInstance->geocodeDestination($street, $postalcode, $city, $state, $country);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinationsApi->geocodeDestination: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **street** | **string**|  | [optional]
 **postalcode** | **string**|  | [optional]
 **city** | **string**|  | [optional]
 **state** | **string**|  | [optional]
 **country** | **string**|  | [optional]

### Return type

[**\Mapotempo\Model\V01Destination**](../Model/V01Destination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDestination**
> \Mapotempo\Model\V01Destination getDestination($id)

Fetch destination.

Fetch destination.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DestinationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.

try {
    $result = $apiInstance->getDestination($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinationsApi->getDestination: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |

### Return type

[**\Mapotempo\Model\V01Destination**](../Model/V01Destination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDestinations**
> \Mapotempo\Model\V01Destination[] getDestinations($ids, $with_quantities, $name_like, $ref_like, $words_mask)

Fetch customer's destinations.

Fetch all customer's destinations (points to visit).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DestinationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ids = array("ids_example"); // string[] | Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add `ref:` before each ref, e.g. `ref:ref1,ref:ref2,ref:ref3`.
$with_quantities = true; // bool | Include the quantities when using geojson output.
$name_like = "name_like_example"; // string | Find destinations with similarity on the name (case insensitive).
$ref_like = "ref_like_example"; // string | Find destinations with similarity on the reference (case insensitive).
$words_mask = "words_mask_example"; // string | Words separated by commas that will skip the similarity check (case insensitive)

try {
    $result = $apiInstance->getDestinations($ids, $with_quantities, $name_like, $ref_like, $words_mask);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinationsApi->getDestinations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ids** | [**string[]**](../Model/string.md)| Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add &#x60;ref:&#x60; before each ref, e.g. &#x60;ref:ref1,ref:ref2,ref:ref3&#x60;. | [optional]
 **with_quantities** | **bool**| Include the quantities when using geojson output. | [optional]
 **name_like** | **string**| Find destinations with similarity on the name (case insensitive). | [optional]
 **ref_like** | **string**| Find destinations with similarity on the reference (case insensitive). | [optional]
 **words_mask** | **string**| Words separated by commas that will skip the similarity check (case insensitive) | [optional]

### Return type

[**\Mapotempo\Model\V01Destination[]**](../Model/V01Destination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVisit**
> \Mapotempo\Model\V01Visit getVisit($destination_id, $id)

Fetch visit.

Fetch visit.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DestinationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$destination_id = "destination_id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.

try {
    $result = $apiInstance->getVisit($destination_id, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinationsApi->getVisit: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **destination_id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |

### Return type

[**\Mapotempo\Model\V01Visit**](../Model/V01Visit.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVisits**
> \Mapotempo\Model\V01Visit[] getVisits($destination_id, $ids)

Fetch destination's visits.

Fetch destination's visits.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DestinationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$destination_id = "destination_id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$ids = array("ids_example"); // string[] | Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add `ref:` before each ref, e.g. `ref:ref1,ref:ref2,ref:ref3`.

try {
    $result = $apiInstance->getVisits($destination_id, $ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinationsApi->getVisits: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **destination_id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **ids** | [**string[]**](../Model/string.md)| Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add &#x60;ref:&#x60; before each ref, e.g. &#x60;ref:ref1,ref:ref2,ref:ref3&#x60;. | [optional]

### Return type

[**\Mapotempo\Model\V01Visit[]**](../Model/V01Visit.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **importDestinations**
> \Mapotempo\Model\V01Destination[] importDestinations($body, $accept_language)

Import destinations by upload a CSV file, by JSON or from TomTom.

Import multiple destinations and visits. Use your internal and unique ids as a \"reference\" to automatically retrieve and update objects. If \"route\" key is provided for a visit or if a planning attribute is sent, a planning will be automatically created at the same time. If all \"route\" attibutes are blank or none attribute for planning is sent, only destinations and visits will be created/updated.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DestinationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Mapotempo\Model\Body5(); // \Mapotempo\Model\Body5 | 
$accept_language = "accept_language_example"; // string | Language used in CSV file. Default value: en. French language: fr.

try {
    $result = $apiInstance->importDestinations($body, $accept_language);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinationsApi->importDestinations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Mapotempo\Model\Body5**](../Model/Body5.md)|  | [optional]
 **accept_language** | **string**| Language used in CSV file. Default value: en. French language: fr. | [optional]

### Return type

[**\Mapotempo\Model\V01Destination[]**](../Model/V01Destination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **reverseGeocodingDestination**
> \Mapotempo\Model\V01Destination reverseGeocodingDestination($lat, $lng)

Reverse geocoding.

Result of reverse geocoding is not saved with this operation.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DestinationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$lat = 3.4; // float | 
$lng = 3.4; // float | 

try {
    $result = $apiInstance->reverseGeocodingDestination($lat, $lng);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinationsApi->reverseGeocodingDestination: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lat** | **float**|  | [optional]
 **lng** | **float**|  | [optional]

### Return type

[**\Mapotempo\Model\V01Destination**](../Model/V01Destination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateDestination**
> \Mapotempo\Model\V01Destination updateDestination($id, $body)

Update destination.

Update destination (point to visit). If want to force geocoding for a new address, you have to send empty lat/lng with new address.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DestinationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$body = new \Mapotempo\Model\Body7(); // \Mapotempo\Model\Body7 | 

try {
    $result = $apiInstance->updateDestination($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinationsApi->updateDestination: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **body** | [**\Mapotempo\Model\Body7**](../Model/Body7.md)|  | [optional]

### Return type

[**\Mapotempo\Model\V01Destination**](../Model/V01Destination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateVisit**
> \Mapotempo\Model\V01Visit updateVisit($destination_id, $id, $body)

Update visit.

If want to force geocoding for a new address, you have to send empty lat/lng with new address.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\DestinationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$destination_id = "destination_id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$body = new \Mapotempo\Model\Body12(); // \Mapotempo\Model\Body12 | 

try {
    $result = $apiInstance->updateVisit($destination_id, $id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinationsApi->updateVisit: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **destination_id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **id** | **string**| Id or the ref field value, then use &#x60;ref:[value]&#x60;. |
 **body** | [**\Mapotempo\Model\Body12**](../Model/Body12.md)|  | [optional]

### Return type

[**\Mapotempo\Model\V01Visit**](../Model/V01Visit.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json; charset=UTF-8, application/vnd.geo+json; charset=UTF-8, application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

