# SwaggerClient-php
[Simplified view of domain model](https://app.mapotempo.com//api/0.1/Model-simpel.svg). ## Model Model is structured around four majors concepts: the Customer account, Destinations, Vehicles and Plannings. * `Customers`: many of objects are linked to a customer account (relating to the user calling API). The customer has many `Users`, each user has his own `api_key`. Be carefull not to confuse with following model `Destination`, `Customer`can only be created by a admin `User`. * `Destinations`: location points to visit with constraints. The same `Destination` can be visited several times : in this case several `Visits` are associated to one `Destination`. * `Vehicles`: vehicles definition are splited in two parts:  * the structural definition named `Vehicle` (car, truck, bike, consumption, etc.)  * and the vehicle usage `VehicleUsage`, a specific usage of a physical vehicle in a specific context. Vehicles can be used in many contexts called `VehicleUsageSet` (set of all vehicles usages under a context). Multiple values are only available if dedicated option for customer is active. For instance, if customer needs to use its vehicle 2 times per day (morning and evening), he needs 2 `VehicleUsageSet` called \"Morning\" and \"Evening\" : each can have different values defined for stores, rest, etc... `VehicleUsageSet` defines default values for vehicle usage. * `Plannings`: `Planning` is a set of `Routes` to `Visit` `Destinations` with `Vehicle` within a `VehicleUsageSet` context. A route is a track between all destinations reached by a vehicle (a new route is created for each customer's vehicle and a route without vehicle is created for all out-of-route destinations). By default all customer's visites are used in a planning.  ## Technical access ### Swagger descriptor This REST API is described with Swagger. The Swagger descriptor defines the request end-points, the parameters and the return values. The API can be addressed by HTTP request or with a generated client using the Swagger descriptor. ### API key All access to the API are subject to an `api_key` parameter in order to authenticate the user. This parameter can be sent with a query string on each available operation: `https://app.mapotempo.com/api/0.1/{objects}?api_key={your_personal_api_key}` ### Return The API supports several return formats: `json` and `xml` which depend of the requested extension used in url. ### I18n Functionnal textual returns are subject to translation and depend of HTTP header `Accept-Language`. HTTP error codes are not translated. ## Admin acces Using an admin `api_key` switches to advanced opperations (on `Customer`, `User`, `Vehicle`, `Profile`). Most of operations from the current api are usable either for normal user `api_key` or admin user `api_key` (not both). ## More concepts When a customer is created some objects are created by default with this new customer: * `Vehicle`: multiple, depending of the `max_vehicles` defined for customer * `DeliverableUnit`: one default * `VehicleUsageSet`: one default * `VehicleUsage`: multiple, depending of the vehicles number * `Store`: one default  ### Profiles, Layers, Routers `Profile` is a concept which allows to set several parameters for the customer: * `Layer`: which allows to choose the background map * `Router`: which allows to build route's information.  Several default profiles are available and can be listed with an admin `api_key`.  ### Tags `Tag` is a concept to filter visits and create planning only for a subset of visits. For instance, if some visits are tagged \"Monday\", it allows to create a new planning for \"Monday\" tag and use only dedicated visits. ### Zonings `Zoning` is a concept which allows to define multiple `Zones` (areas) around destinatons. A `Zone` can be affected to a `Vehicle` and if it is used into a `Planning`, all `Destinations` inside areas will be affected to the zone's vehicle (or `Route`). A polygon defining a `Zone` can be created outside the application or can be automatically generated from a planning.  ## Code samples * Create and display destinations or visits. Here some samples for these operations: [using PHP](https://app.mapotempo.com//api/0.1/examples/php/example.php), [using Ruby](https://app.mapotempo.com//api/0.1/examples/ruby/example.rb). Note you can import destinations/visits and create a planning at the same time if you know beforehand the route for each destination/visit. See the details of importDestinations operation to import your data and create a planning in only one call. * Same operations are available for stores (note you have an existing default store). * With created destinations/visits, you can create a planning (routes and stops are automatically created depending of yours vehicles and destinations/visits) * In existing planning, you have availability to move stops (which represent visits) on a dedicated route (which represent a dedicated vehicle). * With many unaffected (out-of-route) stops in a planning, you may create a zoning to move many stops in several routes. Create a zoning (you can generate zones in this zoning automatically from automatic clustering), if you apply zoning (containing zones linked to a vehicle) on your planning, all stops contained in different zones will be moved in dedicated routes.

This PHP package is automatically generated by the [Swagger Codegen](https://github.com/swagger-api/swagger-codegen) project:

- API version: 1.0.0
- Package version: 0.1
- Build package: io.swagger.codegen.v3.generators.php.PhpClientCodegen
For more information, please visit [https://github.com/Mapotempo/mapotempo-web](https://github.com/Mapotempo/mapotempo-web)

## Requirements

PHP 5.5 and later

## Installation & Usage
### Composer

To install the bindings via [Composer](http://getcomposer.org/), add the following to `composer.json`:

```
{
  "repositories": [
    {
      "type": "git",
      "url": "https://github.com/GIT_USER_ID/GIT_REPO_ID.git"
    }
  ],
  "require": {
    "GIT_USER_ID/GIT_REPO_ID": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
    require_once('/path/to/SwaggerClient-php/vendor/autoload.php');
```

## Tests

To run the unit tests:

```
composer install
./vendor/bin/phpunit
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Mapotempo\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Mapotempo\Model\Body(); // \Mapotempo\Model\Body | 

try {
    $result = $apiInstance->createCustomer($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->createCustomer: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Mapotempo\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.

try {
    $apiInstance->deleteCustomer($id);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->deleteCustomer: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Mapotempo\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$job_id = 56; // int | 

try {
    $apiInstance->deleteJob($id, $job_id);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->deleteJob: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Mapotempo\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$exclude_users = true; // bool | 

try {
    $result = $apiInstance->duplicateCustomer($id, $exclude_users);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->duplicateCustomer: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Mapotempo\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.

try {
    $result = $apiInstance->getCustomer($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->getCustomer: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Mapotempo\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $result = $apiInstance->getCustomerUsers($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->getCustomerUsers: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Mapotempo\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$name_like = "name_like_example"; // string | Find accounts with similarity on the name (case insensitive).
$ref_like = "ref_like_example"; // string | Find accounts with similarity on the reference (case insensitive).
$words_mask = "words_mask_example"; // string | Words separated by commas that will skip the similarity check (case insensitive)

try {
    $result = $apiInstance->getCustomers($name_like, $ref_like, $words_mask);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->getCustomers: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Mapotempo\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$job_id = 56; // int | 

try {
    $apiInstance->getJob($id, $job_id);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->getJob: ', $e->getMessage(), PHP_EOL;
}

$apiInstance = new Mapotempo\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id or the ref field value, then use `ref:[value]`.
$body = new \Mapotempo\Model\Body1(); // \Mapotempo\Model\Body1 | 

try {
    $result = $apiInstance->updateCustomer($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->updateCustomer: ', $e->getMessage(), PHP_EOL;
}
?>
```

## Documentation for API Endpoints

All URIs are relative to *//app.mapotempo.com/api*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*CustomersApi* | [**createCustomer**](docs/Api/CustomersApi.md#createcustomer) | **POST** /0.1/customers | Create customer account (admin).
*CustomersApi* | [**deleteCustomer**](docs/Api/CustomersApi.md#deletecustomer) | **DELETE** /0.1/customers/{id} | Delete customer account (admin).
*CustomersApi* | [**deleteJob**](docs/Api/CustomersApi.md#deletejob) | **DELETE** /0.1/customers/{id}/job/{job_id} | Cancel job.
*CustomersApi* | [**duplicateCustomer**](docs/Api/CustomersApi.md#duplicatecustomer) | **PATCH** /0.1/customers/{id}/duplicate | Duplicate customer account (admin).
*CustomersApi* | [**getCustomer**](docs/Api/CustomersApi.md#getcustomer) | **GET** /0.1/customers/{id} | Fetch customer account.
*CustomersApi* | [**getCustomerUsers**](docs/Api/CustomersApi.md#getcustomerusers) | **GET** /0.1/customers/{id}/users | Fetch users for customer account id.
*CustomersApi* | [**getCustomers**](docs/Api/CustomersApi.md#getcustomers) | **GET** /0.1/customers | Fetch customer accounts (admin).
*CustomersApi* | [**getJob**](docs/Api/CustomersApi.md#getjob) | **GET** /0.1/customers/{id}/job/{job_id} | Return a job.
*CustomersApi* | [**updateCustomer**](docs/Api/CustomersApi.md#updatecustomer) | **PUT** /0.1/customers/{id} | Update customer account.
*DeliverableUnitsApi* | [**createDeliverableUnit**](docs/Api/DeliverableUnitsApi.md#createdeliverableunit) | **POST** /0.1/deliverable_units | Create deliverable unit.
*DeliverableUnitsApi* | [**deleteDeliverableUnit**](docs/Api/DeliverableUnitsApi.md#deletedeliverableunit) | **DELETE** /0.1/deliverable_units/{id} | Delete deliverable unit.
*DeliverableUnitsApi* | [**deleteDeliverableUnits**](docs/Api/DeliverableUnitsApi.md#deletedeliverableunits) | **DELETE** /0.1/deliverable_units | Delete multiple deliverable units.
*DeliverableUnitsApi* | [**getDeliverableUnit**](docs/Api/DeliverableUnitsApi.md#getdeliverableunit) | **GET** /0.1/deliverable_units/{id} | Fetch deliverable unit.
*DeliverableUnitsApi* | [**getDeliverableUnits**](docs/Api/DeliverableUnitsApi.md#getdeliverableunits) | **GET** /0.1/deliverable_units | Fetch customer&#x27;s deliverable units. At least one deliverable unit exists per customer.
*DeliverableUnitsApi* | [**updateDeliverableUnit**](docs/Api/DeliverableUnitsApi.md#updatedeliverableunit) | **PUT** /0.1/deliverable_units/{id} | Update deliverable unit.
*DestinationsApi* | [**autocompleteDestination**](docs/Api/DestinationsApi.md#autocompletedestination) | **PATCH** /0.1/destinations/geocode_complete | Auto completion on destination.
*DestinationsApi* | [**createDestination**](docs/Api/DestinationsApi.md#createdestination) | **POST** /0.1/destinations | Create destination.
*DestinationsApi* | [**createVisit**](docs/Api/DestinationsApi.md#createvisit) | **POST** /0.1/destinations/{destination_id}/visits | Create visit.
*DestinationsApi* | [**deleteDestination**](docs/Api/DestinationsApi.md#deletedestination) | **DELETE** /0.1/destinations/{id} | Delete destination.
*DestinationsApi* | [**deleteDestinations**](docs/Api/DestinationsApi.md#deletedestinations) | **DELETE** /0.1/destinations | Delete multiple destinations.
*DestinationsApi* | [**deleteVisit**](docs/Api/DestinationsApi.md#deletevisit) | **DELETE** /0.1/destinations/{destination_id}/visits/{id} | Delete visit.
*DestinationsApi* | [**geocodeDestination**](docs/Api/DestinationsApi.md#geocodedestination) | **PATCH** /0.1/destinations/geocode | Geocode destination.
*DestinationsApi* | [**getDestination**](docs/Api/DestinationsApi.md#getdestination) | **GET** /0.1/destinations/{id} | Fetch destination.
*DestinationsApi* | [**getDestinations**](docs/Api/DestinationsApi.md#getdestinations) | **GET** /0.1/destinations | Fetch customer&#x27;s destinations.
*DestinationsApi* | [**getVisit**](docs/Api/DestinationsApi.md#getvisit) | **GET** /0.1/destinations/{destination_id}/visits/{id} | Fetch visit.
*DestinationsApi* | [**getVisits**](docs/Api/DestinationsApi.md#getvisits) | **GET** /0.1/destinations/{destination_id}/visits | Fetch destination&#x27;s visits.
*DestinationsApi* | [**importDestinations**](docs/Api/DestinationsApi.md#importdestinations) | **PUT** /0.1/destinations | Import destinations by upload a CSV file, by JSON or from TomTom.
*DestinationsApi* | [**reverseGeocodingDestination**](docs/Api/DestinationsApi.md#reversegeocodingdestination) | **PATCH** /0.1/destinations/reverse | Reverse geocoding.
*DestinationsApi* | [**updateDestination**](docs/Api/DestinationsApi.md#updatedestination) | **PUT** /0.1/destinations/{id} | Update destination.
*DestinationsApi* | [**updateVisit**](docs/Api/DestinationsApi.md#updatevisit) | **PUT** /0.1/destinations/{destination_id}/visits/{id} | Update visit.
*DevicesApi* | [**checkAuth**](docs/Api/DevicesApi.md#checkauth) | **GET** /0.1/devices/{device}/auth/{id} | Validate device Credentials.
*DevicesApi* | [**deviceAlyacomSendMultiple**](docs/Api/DevicesApi.md#devicealyacomsendmultiple) | **POST** /0.1/devices/alyacom/send_multiple | Send Planning Routes.
*DevicesApi* | [**deviceFleetClear**](docs/Api/DevicesApi.md#devicefleetclear) | **DELETE** /0.1/devices/fleet/clear | Clear Route.
*DevicesApi* | [**deviceFleetClearMultiple**](docs/Api/DevicesApi.md#devicefleetclearmultiple) | **DELETE** /0.1/devices/fleet/clear_multiple | Clear multiple routes.
*DevicesApi* | [**deviceFleetCreateCompanyAndDrivers**](docs/Api/DevicesApi.md#devicefleetcreatecompanyanddrivers) | **PATCH** /0.1/devices/fleet/create_company | Create company with drivers.
*DevicesApi* | [**deviceFleetCreateDrivers**](docs/Api/DevicesApi.md#devicefleetcreatedrivers) | **PATCH** /0.1/devices/fleet/create_or_update_drivers | Create drivers.
*DevicesApi* | [**deviceFleetDemoClear**](docs/Api/DevicesApi.md#devicefleetdemoclear) | **DELETE** /0.1/devices/device_demo/clear | Clear Route.
*DevicesApi* | [**deviceFleetDemoClearMultiple**](docs/Api/DevicesApi.md#devicefleetdemoclearmultiple) | **DELETE** /0.1/devices/device_demo/clear_multiple | Clear Planning Routes.
*DevicesApi* | [**deviceFleetDemoSendMultiple**](docs/Api/DevicesApi.md#devicefleetdemosendmultiple) | **POST** /0.1/devices/device_demo/send_multiple | Send Planning Routes.
*DevicesApi* | [**deviceFleetList**](docs/Api/DevicesApi.md#devicefleetlist) | **GET** /0.1/devices/fleet/devices | List Devices.
*DevicesApi* | [**deviceFleetSendMultiple**](docs/Api/DevicesApi.md#devicefleetsendmultiple) | **POST** /0.1/devices/fleet/send_multiple | Send Planning Routes.
*DevicesApi* | [**deviceFleetSync**](docs/Api/DevicesApi.md#devicefleetsync) | **POST** /0.1/devices/fleet/sync | Synchronise Vehicles.
*DevicesApi* | [**deviceFleetTrackingList**](docs/Api/DevicesApi.md#devicefleettrackinglist) | **GET** /0.1/devices/suivi_de_flotte/devices | List Devices.
*DevicesApi* | [**deviceMasternautSendMultiple**](docs/Api/DevicesApi.md#devicemasternautsendmultiple) | **POST** /0.1/devices/masternaut/send_multiple | Send Planning Routes.
*DevicesApi* | [**deviceNoticoClear**](docs/Api/DevicesApi.md#devicenoticoclear) | **DELETE** /0.1/devices/notico/clear | Clear Route.
*DevicesApi* | [**deviceNoticoClearMultiple**](docs/Api/DevicesApi.md#devicenoticoclearmultiple) | **DELETE** /0.1/devices/notico/clear_multiple | Clear Planning Routes.
*DevicesApi* | [**deviceNoticoSendMultiple**](docs/Api/DevicesApi.md#devicenoticosendmultiple) | **POST** /0.1/devices/notico/send_multiple | Send Planning Routes.
*DevicesApi* | [**deviceOrangeClear**](docs/Api/DevicesApi.md#deviceorangeclear) | **DELETE** /0.1/devices/orange/clear | Clear Route.
*DevicesApi* | [**deviceOrangeClearMultiple**](docs/Api/DevicesApi.md#deviceorangeclearmultiple) | **DELETE** /0.1/devices/orange/clear_multiple | Clear Planning Routes.
*DevicesApi* | [**deviceOrangeClearSync**](docs/Api/DevicesApi.md#deviceorangeclearsync) | **POST** /0.1/devices/orange/sync | Synchronise Vehicles.
*DevicesApi* | [**deviceOrangeList**](docs/Api/DevicesApi.md#deviceorangelist) | **GET** /0.1/devices/orange/devices | List Devices.
*DevicesApi* | [**deviceOrangeSendMultiple**](docs/Api/DevicesApi.md#deviceorangesendmultiple) | **POST** /0.1/devices/orange/send_multiple | Send Planning Routes.
*DevicesApi* | [**devicePraxedoClear**](docs/Api/DevicesApi.md#devicepraxedoclear) | **DELETE** /0.1/devices/praxedo/clear | Clear route from Praxedo.
*DevicesApi* | [**devicePraxedoClear_0**](docs/Api/DevicesApi.md#devicepraxedoclear_0) | **DELETE** /0.1/devices/praxedo/clear_multiple | Clear routes from Praxedo.
*DevicesApi* | [**devicePraxedoSendMultiple**](docs/Api/DevicesApi.md#devicepraxedosendmultiple) | **POST** /0.1/devices/praxedo/send_multiple | Send Planning Routes.
*DevicesApi* | [**deviceSopacList**](docs/Api/DevicesApi.md#devicesopaclist) | **GET** /0.1/devices/sopac/devices | List Devices.
*DevicesApi* | [**deviceTeksatClear**](docs/Api/DevicesApi.md#deviceteksatclear) | **DELETE** /0.1/devices/teksat/clear | Clear Route.
*DevicesApi* | [**deviceTeksatClearMultiple**](docs/Api/DevicesApi.md#deviceteksatclearmultiple) | **DELETE** /0.1/devices/teksat/clear_multiple | Clear Planning Routes.
*DevicesApi* | [**deviceTeksatList**](docs/Api/DevicesApi.md#deviceteksatlist) | **GET** /0.1/devices/teksat/devices | List Devices.
*DevicesApi* | [**deviceTeksatSendMultiple**](docs/Api/DevicesApi.md#deviceteksatsendmultiple) | **POST** /0.1/devices/teksat/send_multiple | Send Planning Routes.
*DevicesApi* | [**deviceTeksatSync**](docs/Api/DevicesApi.md#deviceteksatsync) | **POST** /0.1/devices/teksat/sync | Synchronise Vehicles.
*DevicesApi* | [**deviceTomtomClear**](docs/Api/DevicesApi.md#devicetomtomclear) | **DELETE** /0.1/devices/tomtom/clear | Clear Route.
*DevicesApi* | [**deviceTomtomClearMultiple**](docs/Api/DevicesApi.md#devicetomtomclearmultiple) | **DELETE** /0.1/devices/tomtom/clear_multiple | Clear Planning Routes.
*DevicesApi* | [**deviceTomtomList**](docs/Api/DevicesApi.md#devicetomtomlist) | **GET** /0.1/devices/tomtom/devices | List Devices.
*DevicesApi* | [**deviceTomtomSendMultiple**](docs/Api/DevicesApi.md#devicetomtomsendmultiple) | **POST** /0.1/devices/tomtom/send_multiple | Send Planning Routes.
*DevicesApi* | [**deviceTomtomSync**](docs/Api/DevicesApi.md#devicetomtomsync) | **POST** /0.1/devices/tomtom/sync | Synchronise Vehicles.
*DevicesApi* | [**getFleetRoutes**](docs/Api/DevicesApi.md#getfleetroutes) | **GET** /0.1/devices/fleet/fetch_routes | Get Fleet routes.
*DevicesApi* | [**reporting_**](docs/Api/DevicesApi.md#reporting_) | **GET** /0.1/devices/fleet/reporting | Get reporting.
*DevicesApi* | [**sendRoute**](docs/Api/DevicesApi.md#sendroute) | **POST** /0.1/devices/{device}/send | Send Route.
*GeocoderApi* | [**geocode**](docs/Api/GeocoderApi.md#geocode) | **GET** /0.1/geocoder/search | Geocode.
*LayersApi* | [**getLayers**](docs/Api/LayersApi.md#getlayers) | **GET** /0.1/layers | Fetch layers.
*OrderArraysApi* | [**massAssignmentOrder**](docs/Api/OrderArraysApi.md#massassignmentorder) | **PATCH** /0.1/order_arrays/{id} | Orders mass assignment.
*OrderArraysApi* | [**updateOrder**](docs/Api/OrderArraysApi.md#updateorder) | **PUT** /0.1/order_arrays/{order_array_id}/orders/{id} | Update order.
*PlanningsApi* | [**activationStops**](docs/Api/PlanningsApi.md#activationstops) | **PATCH** /0.1/plannings/{planning_id}/routes/{id}/active/{active} | Change stops activation.
*PlanningsApi* | [**applyZonings**](docs/Api/PlanningsApi.md#applyzonings) | **GET** /0.1/plannings/{id}/apply_zonings | Apply zonings.
*PlanningsApi* | [**automaticInsertStop**](docs/Api/PlanningsApi.md#automaticinsertstop) | **PATCH** /0.1/plannings/{id}/automatic_insert | Insert one or more stop into planning routes.
*PlanningsApi* | [**clonePlanning**](docs/Api/PlanningsApi.md#cloneplanning) | **PATCH** /0.1/plannings/{id}/duplicate | Clone the planning.
*PlanningsApi* | [**createPlanning**](docs/Api/PlanningsApi.md#createplanning) | **POST** /0.1/plannings | Create planning.
*PlanningsApi* | [**deletePlanning**](docs/Api/PlanningsApi.md#deleteplanning) | **DELETE** /0.1/plannings/{id} | Delete planning.
*PlanningsApi* | [**deletePlannings**](docs/Api/PlanningsApi.md#deleteplannings) | **DELETE** /0.1/plannings | Delete multiple plannings.
*PlanningsApi* | [**getPlanning**](docs/Api/PlanningsApi.md#getplanning) | **GET** /0.1/plannings/{id} | Fetch planning.
*PlanningsApi* | [**getPlannings**](docs/Api/PlanningsApi.md#getplannings) | **GET** /0.1/plannings | Fetch customer&#x27;s plannings.
*PlanningsApi* | [**getQuantityS**](docs/Api/PlanningsApi.md#getquantitys) | **GET** /0.1/plannings/{id}/quantities | Fetch quantities from a planning.
*PlanningsApi* | [**getRoute**](docs/Api/PlanningsApi.md#getroute) | **GET** /0.1/plannings/{planning_id}/routes/{id} | Fetch route.
*PlanningsApi* | [**getRouteByVehicle**](docs/Api/PlanningsApi.md#getroutebyvehicle) | **GET** /0.1/plannings/{planning_id}/routes_by_vehicle/{id} | Fetch route from vehicle.
*PlanningsApi* | [**getRoutes**](docs/Api/PlanningsApi.md#getroutes) | **GET** /0.1/plannings/{planning_id}/routes | Fetch planning&#x27;s routes.
*PlanningsApi* | [**getStop**](docs/Api/PlanningsApi.md#getstop) | **GET** /0.1/plannings/{planning_id}/routes/{route_id}/stops/{id} | Fetch stop.
*PlanningsApi* | [**getVehicleUsageS**](docs/Api/PlanningsApi.md#getvehicleusages) | **GET** /0.1/plannings/{id}/vehicle_usages | Fetch vehicle usage(s) from a planning.
*PlanningsApi* | [**moveStop**](docs/Api/PlanningsApi.md#movestop) | **PATCH** /0.1/plannings/{planning_id}/routes/{route_id}/stops/{id}/move/{index} | Move stop position in routes.
*PlanningsApi* | [**moveVisits**](docs/Api/PlanningsApi.md#movevisits) | **PATCH** /0.1/plannings/{planning_id}/routes/{id}/visits/moves | Move visit(s) to route. Append in order at end (if automatic_insert is false).
*PlanningsApi* | [**optimizeRoute**](docs/Api/PlanningsApi.md#optimizeroute) | **PATCH** /0.1/plannings/{planning_id}/routes/{id}/optimize | Optimize a single route.
*PlanningsApi* | [**optimizeRoutes**](docs/Api/PlanningsApi.md#optimizeroutes) | **GET** /0.1/plannings/{id}/optimize | Optimize routes.
*PlanningsApi* | [**refreshPlanning**](docs/Api/PlanningsApi.md#refreshplanning) | **GET** /0.1/plannings/{id}/refresh | Recompute the planning after parameter update.
*PlanningsApi* | [**reverseStopsOrder**](docs/Api/PlanningsApi.md#reversestopsorder) | **PATCH** /0.1/plannings/{planning_id}/routes/{id}/reverse_order | Reverse stops order.
*PlanningsApi* | [**sendSMS**](docs/Api/PlanningsApi.md#sendsms) | **GET** /0.1/plannings/{id}/send_sms | Send SMS for each stop visit.
*PlanningsApi* | [**sendSMS_0**](docs/Api/PlanningsApi.md#sendsms_0) | **GET** /0.1/plannings/{planning_id}/routes/{id}/send_sms | Send SMS for each stop visit.
*PlanningsApi* | [**switchVehicles**](docs/Api/PlanningsApi.md#switchvehicles) | **PATCH** /0.1/plannings/{id}/switch | Switch two vehicles.
*PlanningsApi* | [**updatePlanning**](docs/Api/PlanningsApi.md#updateplanning) | **PUT** /0.1/plannings/{id} | Update planning.
*PlanningsApi* | [**updateRoute**](docs/Api/PlanningsApi.md#updateroute) | **PUT** /0.1/plannings/{planning_id}/routes/{id} | Update route attributes.
*PlanningsApi* | [**updateRoutes**](docs/Api/PlanningsApi.md#updateroutes) | **PATCH** /0.1/plannings/{id}/update_routes | Update routes visibility and lock. (DEPRECATED. For more information, please use Update Planning operation instead)
*PlanningsApi* | [**updateStop**](docs/Api/PlanningsApi.md#updatestop) | **PUT** /0.1/plannings/{planning_id}/routes/{route_id}/stops/{id} | Update stop.
*PlanningsApi* | [**updateStopsStatus**](docs/Api/PlanningsApi.md#updatestopsstatus) | **PATCH** /0.1/plannings/{id}/update_stops_status | Update stops live status.
*PlanningsApi* | [**useOrderArray**](docs/Api/PlanningsApi.md#useorderarray) | **PATCH** /0.1/plannings/{id}/order_array | Use order_array in the planning.
*ProfilesApi* | [**getProfileLayers**](docs/Api/ProfilesApi.md#getprofilelayers) | **GET** /0.1/profiles/{id}/layers | Fetch layers in the profile (admin).
*ProfilesApi* | [**getProfileRouters**](docs/Api/ProfilesApi.md#getprofilerouters) | **GET** /0.1/profiles/{id}/routers | Fetch routers in the profile (admin).
*ProfilesApi* | [**getProfiles**](docs/Api/ProfilesApi.md#getprofiles) | **GET** /0.1/profiles | Fetch profiles (admin).
*ReleaseApi* | [**release**](docs/Api/ReleaseApi.md#release) | **GET** /0.1/release | Return release hash.
*RoutersApi* | [**getRouters**](docs/Api/RoutersApi.md#getrouters) | **GET** /0.1/routers | Fetch customer&#x27;s routers.
*RoutesApi* | [**getRoutes**](docs/Api/RoutesApi.md#getroutes) | **GET** /0.1/routes | Fetch customer&#x27;s routes.
*StoresApi* | [**autocompleteStore**](docs/Api/StoresApi.md#autocompletestore) | **PATCH** /0.1/stores/geocode_complete | Auto completion on store.
*StoresApi* | [**createStore**](docs/Api/StoresApi.md#createstore) | **POST** /0.1/stores | Create store.
*StoresApi* | [**deleteStore**](docs/Api/StoresApi.md#deletestore) | **DELETE** /0.1/stores/{id} | Delete store.
*StoresApi* | [**deleteStores**](docs/Api/StoresApi.md#deletestores) | **DELETE** /0.1/stores | Delete multiple stores.
*StoresApi* | [**geocodeStore**](docs/Api/StoresApi.md#geocodestore) | **PATCH** /0.1/stores/geocode | Geocode store.
*StoresApi* | [**getStore**](docs/Api/StoresApi.md#getstore) | **GET** /0.1/stores/{id} | Fetch store.
*StoresApi* | [**getStores**](docs/Api/StoresApi.md#getstores) | **GET** /0.1/stores | Fetch customer&#x27;s stores. At least one store exists per customer.
*StoresApi* | [**importStores**](docs/Api/StoresApi.md#importstores) | **PUT** /0.1/stores | Import stores by upload a CSV file or by JSON.
*StoresApi* | [**reverseGeocodingStore**](docs/Api/StoresApi.md#reversegeocodingstore) | **PATCH** /0.1/stores/reverse | Reverse geocoding.
*StoresApi* | [**updateStore**](docs/Api/StoresApi.md#updatestore) | **PUT** /0.1/stores/{id} | Update store.
*TagsApi* | [**createTag**](docs/Api/TagsApi.md#createtag) | **POST** /0.1/tags | Create tag.
*TagsApi* | [**deleteTag**](docs/Api/TagsApi.md#deletetag) | **DELETE** /0.1/tags/{id} | Delete tag.
*TagsApi* | [**deleteTags**](docs/Api/TagsApi.md#deletetags) | **DELETE** /0.1/tags | Delete multiple tags.
*TagsApi* | [**getTag**](docs/Api/TagsApi.md#gettag) | **GET** /0.1/tags/{id} | Fetch tag.
*TagsApi* | [**getTags**](docs/Api/TagsApi.md#gettags) | **GET** /0.1/tags | Fetch customer&#x27;s tags.
*TagsApi* | [**updateTag**](docs/Api/TagsApi.md#updatetag) | **PUT** /0.1/tags/{id} | Update tag.
*TagsApi* | [**visitsByTags**](docs/Api/TagsApi.md#visitsbytags) | **GET** /0.1/tags/{id}/visits | Get visit with corresponding tag id or tag ref
*UsersApi* | [**createUser**](docs/Api/UsersApi.md#createuser) | **POST** /0.1/users | Create user (admin).
*UsersApi* | [**deleteUser**](docs/Api/UsersApi.md#deleteuser) | **DELETE** /0.1/users/{id} | Delete user (admin).
*UsersApi* | [**deleteUsers**](docs/Api/UsersApi.md#deleteusers) | **DELETE** /0.1/users | Delete multiple users (admin).
*UsersApi* | [**getUser**](docs/Api/UsersApi.md#getuser) | **GET** /0.1/users/{id} | Fetch user.
*UsersApi* | [**getUsers**](docs/Api/UsersApi.md#getusers) | **GET** /0.1/users | Fetch customer&#x27;s users (or all users with an admin key).
*UsersApi* | [**updateUser**](docs/Api/UsersApi.md#updateuser) | **PUT** /0.1/users/{id} | Update user.
*VehicleUsageSetsApi* | [**createVehicleUsageSet**](docs/Api/VehicleUsageSetsApi.md#createvehicleusageset) | **POST** /0.1/vehicle_usage_sets | Create vehicle_usage_set.
*VehicleUsageSetsApi* | [**deleteVehicleUsageSet**](docs/Api/VehicleUsageSetsApi.md#deletevehicleusageset) | **DELETE** /0.1/vehicle_usage_sets/{id} | Delete vehicle_usage_set.
*VehicleUsageSetsApi* | [**deleteVehicleUsageSets**](docs/Api/VehicleUsageSetsApi.md#deletevehicleusagesets) | **DELETE** /0.1/vehicle_usage_sets | Delete multiple vehicle_usage_sets.
*VehicleUsageSetsApi* | [**getVehicleUsage**](docs/Api/VehicleUsageSetsApi.md#getvehicleusage) | **GET** /0.1/vehicle_usage_sets/{vehicle_usage_set_id}/vehicle_usages/{id} | Fetch vehicle_usage.
*VehicleUsageSetsApi* | [**getVehicleUsageSet**](docs/Api/VehicleUsageSetsApi.md#getvehicleusageset) | **GET** /0.1/vehicle_usage_sets/{id} | Fetch vehicle_usage_set.
*VehicleUsageSetsApi* | [**getVehicleUsageSets**](docs/Api/VehicleUsageSetsApi.md#getvehicleusagesets) | **GET** /0.1/vehicle_usage_sets | Fetch customer&#x27;s vehicle_usage_sets. At least one vehicle_usage_set exists per customer.
*VehicleUsageSetsApi* | [**getVehicleUsages**](docs/Api/VehicleUsageSetsApi.md#getvehicleusages) | **GET** /0.1/vehicle_usage_sets/{vehicle_usage_set_id}/vehicle_usages | Fetch customer&#x27;s vehicle_usages.
*VehicleUsageSetsApi* | [**importVehicleUsageSets**](docs/Api/VehicleUsageSetsApi.md#importvehicleusagesets) | **PUT** /0.1/vehicle_usage_sets | Import vehicle usage set by upload a CSV file or by JSON.
*VehicleUsageSetsApi* | [**updateVehicleUsage**](docs/Api/VehicleUsageSetsApi.md#updatevehicleusage) | **PUT** /0.1/vehicle_usage_sets/{vehicle_usage_set_id}/vehicle_usages/{id} | Update vehicle_usage.
*VehicleUsageSetsApi* | [**updateVehicleUsageSet**](docs/Api/VehicleUsageSetsApi.md#updatevehicleusageset) | **PUT** /0.1/vehicle_usage_sets/{id} | Update vehicle_usage_set.
*VehiclesApi* | [**createVehicle**](docs/Api/VehiclesApi.md#createvehicle) | **POST** /0.1/vehicles | Create vehicle (admin).
*VehiclesApi* | [**currentPosition**](docs/Api/VehiclesApi.md#currentposition) | **GET** /0.1/vehicles/current_position | Get vehicle&#x27;s positions.
*VehiclesApi* | [**deleteVehicle**](docs/Api/VehiclesApi.md#deletevehicle) | **DELETE** /0.1/vehicles/{id} | Delete vehicle (admin).
*VehiclesApi* | [**deleteVehicles**](docs/Api/VehiclesApi.md#deletevehicles) | **DELETE** /0.1/vehicles | Delete multiple vehicles (admin).
*VehiclesApi* | [**getDeliverablesByVehicles**](docs/Api/VehiclesApi.md#getdeliverablesbyvehicles) | **GET** /0.1/vehicles/{id}/deliverable_units | Fetch deliverables by vehicle for select plans
*VehiclesApi* | [**getTemperature**](docs/Api/VehiclesApi.md#gettemperature) | **GET** /0.1/vehicles/temperature | Get vehicle&#x27;s temperatures.
*VehiclesApi* | [**getVehicle**](docs/Api/VehiclesApi.md#getvehicle) | **GET** /0.1/vehicles/{id} | Fetch vehicle.
*VehiclesApi* | [**getVehicles**](docs/Api/VehiclesApi.md#getvehicles) | **GET** /0.1/vehicles | Fetch customer&#x27;s vehicles.
*VehiclesApi* | [**updateVehicle**](docs/Api/VehiclesApi.md#updatevehicle) | **PUT** /0.1/vehicles/{id} | Update vehicle.
*VisitsApi* | [**deleteVisits**](docs/Api/VisitsApi.md#deletevisits) | **DELETE** /0.1/visits | Delete multiple visits.
*VisitsApi* | [**getVisits**](docs/Api/VisitsApi.md#getvisits) | **GET** /0.1/visits | Fetch customer&#x27;s visits.
*ZoningsApi* | [**buildIsochrone**](docs/Api/ZoningsApi.md#buildisochrone) | **PATCH** /0.1/zonings/isochrone | Build isochrone for a point.
*ZoningsApi* | [**buildIsodistance**](docs/Api/ZoningsApi.md#buildisodistance) | **PATCH** /0.1/zonings/isodistance | Build isodistance for a point.
*ZoningsApi* | [**createZoning**](docs/Api/ZoningsApi.md#createzoning) | **POST** /0.1/zonings | Create zoning.
*ZoningsApi* | [**deleteZoning**](docs/Api/ZoningsApi.md#deletezoning) | **DELETE** /0.1/zonings/{id} | Delete zoning.
*ZoningsApi* | [**deleteZonings**](docs/Api/ZoningsApi.md#deletezonings) | **DELETE** /0.1/zonings | Delete multiple zonings.
*ZoningsApi* | [**generateAutomatic**](docs/Api/ZoningsApi.md#generateautomatic) | **PATCH** /0.1/zonings/{id}/automatic/{planning_id} | Generate zoning automatically.
*ZoningsApi* | [**generateFromPlanning**](docs/Api/ZoningsApi.md#generatefromplanning) | **PATCH** /0.1/zonings/{id}/from_planning/{planning_id} | Generate zoning from planning.
*ZoningsApi* | [**generateIsochrone**](docs/Api/ZoningsApi.md#generateisochrone) | **PATCH** /0.1/zonings/{id}/isochrone | Generate isochrones.
*ZoningsApi* | [**generateIsochroneVehicleUsage**](docs/Api/ZoningsApi.md#generateisochronevehicleusage) | **PATCH** /0.1/zonings/{id}/vehicle_usage/{vehicle_usage_id}/isochrone | Generate isochrone for only one vehicle usage.
*ZoningsApi* | [**generateIsodistance**](docs/Api/ZoningsApi.md#generateisodistance) | **PATCH** /0.1/zonings/{id}/isodistance | Generate isodistances.
*ZoningsApi* | [**generateIsodistanceVehicleUsage**](docs/Api/ZoningsApi.md#generateisodistancevehicleusage) | **PATCH** /0.1/zonings/{id}/vehicle_usage/{vehicle_usage_id}/isodistance | Generate isodistance for only one vehicle usage.
*ZoningsApi* | [**getZoning**](docs/Api/ZoningsApi.md#getzoning) | **GET** /0.1/zonings/{id} | Fetch zoning.
*ZoningsApi* | [**getZonings**](docs/Api/ZoningsApi.md#getzonings) | **GET** /0.1/zonings | Fetch customer&#x27;s zonings.
*ZoningsApi* | [**polygonByPoint**](docs/Api/ZoningsApi.md#polygonbypoint) | **GET** /0.1/zonings/{id}/polygon_by_point | Find the zone where a point belongs to.
*ZoningsApi* | [**updateZoning**](docs/Api/ZoningsApi.md#updatezoning) | **PUT** /0.1/zonings/{id} | Update zoning.

## Documentation For Models

 - [Body](docs/Model/Body.md)
 - [Body1](docs/Model/Body1.md)
 - [Body10](docs/Model/Body10.md)
 - [Body11](docs/Model/Body11.md)
 - [Body12](docs/Model/Body12.md)
 - [Body13](docs/Model/Body13.md)
 - [Body14](docs/Model/Body14.md)
 - [Body15](docs/Model/Body15.md)
 - [Body16](docs/Model/Body16.md)
 - [Body17](docs/Model/Body17.md)
 - [Body18](docs/Model/Body18.md)
 - [Body19](docs/Model/Body19.md)
 - [Body2](docs/Model/Body2.md)
 - [Body20](docs/Model/Body20.md)
 - [Body21](docs/Model/Body21.md)
 - [Body22](docs/Model/Body22.md)
 - [Body23](docs/Model/Body23.md)
 - [Body24](docs/Model/Body24.md)
 - [Body25](docs/Model/Body25.md)
 - [Body26](docs/Model/Body26.md)
 - [Body27](docs/Model/Body27.md)
 - [Body28](docs/Model/Body28.md)
 - [Body29](docs/Model/Body29.md)
 - [Body3](docs/Model/Body3.md)
 - [Body30](docs/Model/Body30.md)
 - [Body31](docs/Model/Body31.md)
 - [Body32](docs/Model/Body32.md)
 - [Body33](docs/Model/Body33.md)
 - [Body34](docs/Model/Body34.md)
 - [Body35](docs/Model/Body35.md)
 - [Body36](docs/Model/Body36.md)
 - [Body37](docs/Model/Body37.md)
 - [Body38](docs/Model/Body38.md)
 - [Body39](docs/Model/Body39.md)
 - [Body4](docs/Model/Body4.md)
 - [Body40](docs/Model/Body40.md)
 - [Body41](docs/Model/Body41.md)
 - [Body42](docs/Model/Body42.md)
 - [Body43](docs/Model/Body43.md)
 - [Body44](docs/Model/Body44.md)
 - [Body45](docs/Model/Body45.md)
 - [Body46](docs/Model/Body46.md)
 - [Body47](docs/Model/Body47.md)
 - [Body48](docs/Model/Body48.md)
 - [Body49](docs/Model/Body49.md)
 - [Body5](docs/Model/Body5.md)
 - [Body50](docs/Model/Body50.md)
 - [Body51](docs/Model/Body51.md)
 - [Body52](docs/Model/Body52.md)
 - [Body53](docs/Model/Body53.md)
 - [Body54](docs/Model/Body54.md)
 - [Body55](docs/Model/Body55.md)
 - [Body56](docs/Model/Body56.md)
 - [Body57](docs/Model/Body57.md)
 - [Body58](docs/Model/Body58.md)
 - [Body59](docs/Model/Body59.md)
 - [Body6](docs/Model/Body6.md)
 - [Body60](docs/Model/Body60.md)
 - [Body61](docs/Model/Body61.md)
 - [Body62](docs/Model/Body62.md)
 - [Body63](docs/Model/Body63.md)
 - [Body7](docs/Model/Body7.md)
 - [Body8](docs/Model/Body8.md)
 - [Body9](docs/Model/Body9.md)
 - [V01CustomerAdmin](docs/Model/V01CustomerAdmin.md)
 - [V01DeliverableUnit](docs/Model/V01DeliverableUnit.md)
 - [V01DeliverableUnitQuantity](docs/Model/V01DeliverableUnitQuantity.md)
 - [V01Destination](docs/Model/V01Destination.md)
 - [V01DestinationImportJson](docs/Model/V01DestinationImportJson.md)
 - [V01DevicesDeviceItem](docs/Model/V01DevicesDeviceItem.md)
 - [V01Layer](docs/Model/V01Layer.md)
 - [V01Order](docs/Model/V01Order.md)
 - [V01OrderArray](docs/Model/V01OrderArray.md)
 - [V01Planning](docs/Model/V01Planning.md)
 - [V01Profile](docs/Model/V01Profile.md)
 - [V01Route](docs/Model/V01Route.md)
 - [V01RouteProperties](docs/Model/V01RouteProperties.md)
 - [V01RouteStatus](docs/Model/V01RouteStatus.md)
 - [V01Router](docs/Model/V01Router.md)
 - [V01RouterOptions](docs/Model/V01RouterOptions.md)
 - [V01Status304](docs/Model/V01Status304.md)
 - [V01Status400](docs/Model/V01Status400.md)
 - [V01Status401](docs/Model/V01Status401.md)
 - [V01Status402](docs/Model/V01Status402.md)
 - [V01Status403](docs/Model/V01Status403.md)
 - [V01Status404](docs/Model/V01Status404.md)
 - [V01Status405](docs/Model/V01Status405.md)
 - [V01Status409](docs/Model/V01Status409.md)
 - [V01Status500](docs/Model/V01Status500.md)
 - [V01Stop](docs/Model/V01Stop.md)
 - [V01StopStatus](docs/Model/V01StopStatus.md)
 - [V01Store](docs/Model/V01Store.md)
 - [V01Tag](docs/Model/V01Tag.md)
 - [V01TemperatureInfos](docs/Model/V01TemperatureInfos.md)
 - [V01User](docs/Model/V01User.md)
 - [V01Vehicle](docs/Model/V01Vehicle.md)
 - [V01VehiclePosition](docs/Model/V01VehiclePosition.md)
 - [V01VehicleTemperature](docs/Model/V01VehicleTemperature.md)
 - [V01VehicleUsage](docs/Model/V01VehicleUsage.md)
 - [V01VehicleUsageSet](docs/Model/V01VehicleUsageSet.md)
 - [V01VehicleUsageWithVehicle](docs/Model/V01VehicleUsageWithVehicle.md)
 - [V01VehicleWithoutVehicleUsage](docs/Model/V01VehicleWithoutVehicleUsage.md)
 - [V01Visit](docs/Model/V01Visit.md)
 - [V01VisitImportJson](docs/Model/V01VisitImportJson.md)
 - [V01Zone](docs/Model/V01Zone.md)
 - [V01Zoning](docs/Model/V01Zoning.md)

## Documentation For Authorization

 All endpoints do not require authorization.


## Author

tech@mapotempo.com

