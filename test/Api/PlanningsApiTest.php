<?php
/**
 * PlanningsApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * API
 *
 * [Simplified view of domain model](https://app.mapotempo.com//api/0.1/Model-simpel.svg). ## Model Model is structured around four majors concepts: the Customer account, Destinations, Vehicles and Plannings. * `Customers`: many of objects are linked to a customer account (relating to the user calling API). The customer has many `Users`, each user has his own `api_key`. Be carefull not to confuse with following model `Destination`, `Customer`can only be created by a admin `User`. * `Destinations`: location points to visit with constraints. The same `Destination` can be visited several times : in this case several `Visits` are associated to one `Destination`. * `Vehicles`: vehicles definition are splited in two parts:  * the structural definition named `Vehicle` (car, truck, bike, consumption, etc.)  * and the vehicle usage `VehicleUsage`, a specific usage of a physical vehicle in a specific context. Vehicles can be used in many contexts called `VehicleUsageSet` (set of all vehicles usages under a context). Multiple values are only available if dedicated option for customer is active. For instance, if customer needs to use its vehicle 2 times per day (morning and evening), he needs 2 `VehicleUsageSet` called \"Morning\" and \"Evening\" : each can have different values defined for stores, rest, etc... `VehicleUsageSet` defines default values for vehicle usage. * `Plannings`: `Planning` is a set of `Routes` to `Visit` `Destinations` with `Vehicle` within a `VehicleUsageSet` context. A route is a track between all destinations reached by a vehicle (a new route is created for each customer's vehicle and a route without vehicle is created for all out-of-route destinations). By default all customer's visites are used in a planning.  ## Technical access ### Swagger descriptor This REST API is described with Swagger. The Swagger descriptor defines the request end-points, the parameters and the return values. The API can be addressed by HTTP request or with a generated client using the Swagger descriptor. ### API key All access to the API are subject to an `api_key` parameter in order to authenticate the user. This parameter can be sent with a query string on each available operation: `https://app.mapotempo.com/api/0.1/{objects}?api_key={your_personal_api_key}` ### Return The API supports several return formats: `json` and `xml` which depend of the requested extension used in url. ### I18n Functionnal textual returns are subject to translation and depend of HTTP header `Accept-Language`. HTTP error codes are not translated. ## Admin acces Using an admin `api_key` switches to advanced opperations (on `Customer`, `User`, `Vehicle`, `Profile`). Most of operations from the current api are usable either for normal user `api_key` or admin user `api_key` (not both). ## More concepts When a customer is created some objects are created by default with this new customer: * `Vehicle`: multiple, depending of the `max_vehicles` defined for customer * `DeliverableUnit`: one default * `VehicleUsageSet`: one default * `VehicleUsage`: multiple, depending of the vehicles number * `Store`: one default  ### Profiles, Layers, Routers `Profile` is a concept which allows to set several parameters for the customer: * `Layer`: which allows to choose the background map * `Router`: which allows to build route's information.  Several default profiles are available and can be listed with an admin `api_key`.  ### Tags `Tag` is a concept to filter visits and create planning only for a subset of visits. For instance, if some visits are tagged \"Monday\", it allows to create a new planning for \"Monday\" tag and use only dedicated visits. ### Zonings `Zoning` is a concept which allows to define multiple `Zones` (areas) around destinatons. A `Zone` can be affected to a `Vehicle` and if it is used into a `Planning`, all `Destinations` inside areas will be affected to the zone's vehicle (or `Route`). A polygon defining a `Zone` can be created outside the application or can be automatically generated from a planning.  ## Code samples * Create and display destinations or visits. Here some samples for these operations: [using PHP](https://app.mapotempo.com//api/0.1/examples/php/example.php), [using Ruby](https://app.mapotempo.com//api/0.1/examples/ruby/example.rb). Note you can import destinations/visits and create a planning at the same time if you know beforehand the route for each destination/visit. See the details of importDestinations operation to import your data and create a planning in only one call. * Same operations are available for stores (note you have an existing default store). * With created destinations/visits, you can create a planning (routes and stops are automatically created depending of yours vehicles and destinations/visits) * In existing planning, you have availability to move stops (which represent visits) on a dedicated route (which represent a dedicated vehicle). * With many unaffected (out-of-route) stops in a planning, you may create a zoning to move many stops in several routes. Create a zoning (you can generate zones in this zoning automatically from automatic clustering), if you apply zoning (containing zones linked to a vehicle) on your planning, all stops contained in different zones will be moved in dedicated routes.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: tech@mapotempo.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.20
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace Mapotempo;

use Mapotempo\Configuration;
use Mapotempo\ApiException;
use Mapotempo\ObjectSerializer;

/**
 * PlanningsApiTest Class Doc Comment
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class PlanningsApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for activationStops
     *
     * Change stops activation..
     *
     */
    public function testActivationStops()
    {
    }

    /**
     * Test case for applyZonings
     *
     * Apply zonings..
     *
     */
    public function testApplyZonings()
    {
    }

    /**
     * Test case for automaticInsertStop
     *
     * Insert one or more stop into planning routes..
     *
     */
    public function testAutomaticInsertStop()
    {
    }

    /**
     * Test case for clonePlanning
     *
     * Clone the planning..
     *
     */
    public function testClonePlanning()
    {
    }

    /**
     * Test case for createPlanning
     *
     * Create planning..
     *
     */
    public function testCreatePlanning()
    {
    }

    /**
     * Test case for deletePlanning
     *
     * Delete planning..
     *
     */
    public function testDeletePlanning()
    {
    }

    /**
     * Test case for deletePlannings
     *
     * Delete multiple plannings..
     *
     */
    public function testDeletePlannings()
    {
    }

    /**
     * Test case for getPlanning
     *
     * Fetch planning..
     *
     */
    public function testGetPlanning()
    {
    }

    /**
     * Test case for getPlannings
     *
     * Fetch customer's plannings..
     *
     */
    public function testGetPlannings()
    {
    }

    /**
     * Test case for getQuantityS
     *
     * Fetch quantities from a planning..
     *
     */
    public function testGetQuantityS()
    {
    }

    /**
     * Test case for getRoute
     *
     * Fetch route..
     *
     */
    public function testGetRoute()
    {
    }

    /**
     * Test case for getRouteByVehicle
     *
     * Fetch route from vehicle..
     *
     */
    public function testGetRouteByVehicle()
    {
    }

    /**
     * Test case for getRoutes
     *
     * Fetch planning's routes..
     *
     */
    public function testGetRoutes()
    {
    }

    /**
     * Test case for getStop
     *
     * Fetch stop..
     *
     */
    public function testGetStop()
    {
    }

    /**
     * Test case for getVehicleUsageS
     *
     * Fetch vehicle usage(s) from a planning..
     *
     */
    public function testGetVehicleUsageS()
    {
    }

    /**
     * Test case for moveStop
     *
     * Move stop position in routes..
     *
     */
    public function testMoveStop()
    {
    }

    /**
     * Test case for moveVisits
     *
     * Move visit(s) to route. Append in order at end (if automatic_insert is false)..
     *
     */
    public function testMoveVisits()
    {
    }

    /**
     * Test case for optimizeRoute
     *
     * Optimize a single route..
     *
     */
    public function testOptimizeRoute()
    {
    }

    /**
     * Test case for optimizeRoutes
     *
     * Optimize routes..
     *
     */
    public function testOptimizeRoutes()
    {
    }

    /**
     * Test case for refreshPlanning
     *
     * Recompute the planning after parameter update..
     *
     */
    public function testRefreshPlanning()
    {
    }

    /**
     * Test case for reverseStopsOrder
     *
     * Reverse stops order..
     *
     */
    public function testReverseStopsOrder()
    {
    }

    /**
     * Test case for sendSMS
     *
     * Send SMS for each stop visit..
     *
     */
    public function testSendSMS()
    {
    }

    /**
     * Test case for sendSMS_0
     *
     * Send SMS for each stop visit..
     *
     */
    public function testSendSMS0()
    {
    }

    /**
     * Test case for switchVehicles
     *
     * Switch two vehicles..
     *
     */
    public function testSwitchVehicles()
    {
    }

    /**
     * Test case for updatePlanning
     *
     * Update planning..
     *
     */
    public function testUpdatePlanning()
    {
    }

    /**
     * Test case for updateRoute
     *
     * Update route attributes..
     *
     */
    public function testUpdateRoute()
    {
    }

    /**
     * Test case for updateRoutes
     *
     * Update routes visibility and lock. (DEPRECATED. For more information, please use Update Planning operation instead).
     *
     */
    public function testUpdateRoutes()
    {
    }

    /**
     * Test case for updateStop
     *
     * Update stop..
     *
     */
    public function testUpdateStop()
    {
    }

    /**
     * Test case for updateStopsStatus
     *
     * Update stops live status..
     *
     */
    public function testUpdateStopsStatus()
    {
    }

    /**
     * Test case for useOrderArray
     *
     * Use order_array in the planning..
     *
     */
    public function testUseOrderArray()
    {
    }
}
