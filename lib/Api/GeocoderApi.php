<?php
/**
 * GeocoderApi
 * PHP version 5
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * API
 *
 * [Simplified view of domain model](https://app.mapotempo.com//api/0.1/Model-simpel.svg). ## Model Model is structured around four majors concepts: the Customer account, Destinations, Vehicles and Plannings. * `Customers`: many of objects are linked to a customer account (relating to the user calling API). The customer has many `Users`, each user has his own `api_key`. Be carefull not to confuse with following model `Destination`, `Customer`can only be created by a admin `User`. * `Destinations`: location points to visit with constraints. The same `Destination` can be visited several times : in this case several `Visits` are associated to one `Destination`. * `Vehicles`: vehicles definition are splited in two parts:  * the structural definition named `Vehicle` (car, truck, bike, consumption, etc.)  * and the vehicle usage `VehicleUsage`, a specific usage of a physical vehicle in a specific context. Vehicles can be used in many contexts called `VehicleUsageSet` (set of all vehicles usages under a context). Multiple values are only available if dedicated option for customer is active. For instance, if customer needs to use its vehicle 2 times per day (morning and evening), he needs 2 `VehicleUsageSet` called \"Morning\" and \"Evening\" : each can have different values defined for stores, rest, etc... `VehicleUsageSet` defines default values for vehicle usage. * `Plannings`: `Planning` is a set of `Routes` to `Visit` `Destinations` with `Vehicle` within a `VehicleUsageSet` context. A route is a track between all destinations reached by a vehicle (a new route is created for each customer's vehicle and a route without vehicle is created for all out-of-route destinations). By default all customer's visites are used in a planning.  ## Technical access ### Swagger descriptor This REST API is described with Swagger. The Swagger descriptor defines the request end-points, the parameters and the return values. The API can be addressed by HTTP request or with a generated client using the Swagger descriptor. ### API key All access to the API are subject to an `api_key` parameter in order to authenticate the user. This parameter can be sent with a query string on each available operation: `https://app.mapotempo.com/api/0.1/{objects}?api_key={your_personal_api_key}` ### Return The API supports several return formats: `json` and `xml` which depend of the requested extension used in url. ### I18n Functionnal textual returns are subject to translation and depend of HTTP header `Accept-Language`. HTTP error codes are not translated. ## Admin acces Using an admin `api_key` switches to advanced opperations (on `Customer`, `User`, `Vehicle`, `Profile`). Most of operations from the current api are usable either for normal user `api_key` or admin user `api_key` (not both). ## More concepts When a customer is created some objects are created by default with this new customer: * `Vehicle`: multiple, depending of the `max_vehicles` defined for customer * `DeliverableUnit`: one default * `VehicleUsageSet`: one default * `VehicleUsage`: multiple, depending of the vehicles number * `Store`: one default  ### Profiles, Layers, Routers `Profile` is a concept which allows to set several parameters for the customer: * `Layer`: which allows to choose the background map * `Router`: which allows to build route's information.  Several default profiles are available and can be listed with an admin `api_key`.  ### Tags `Tag` is a concept to filter visits and create planning only for a subset of visits. For instance, if some visits are tagged \"Monday\", it allows to create a new planning for \"Monday\" tag and use only dedicated visits. ### Zonings `Zoning` is a concept which allows to define multiple `Zones` (areas) around destinatons. A `Zone` can be affected to a `Vehicle` and if it is used into a `Planning`, all `Destinations` inside areas will be affected to the zone's vehicle (or `Route`). A polygon defining a `Zone` can be created outside the application or can be automatically generated from a planning.  ## Code samples * Create and display destinations or visits. Here some samples for these operations: [using PHP](https://app.mapotempo.com//api/0.1/examples/php/example.php), [using Ruby](https://app.mapotempo.com//api/0.1/examples/ruby/example.rb). Note you can import destinations/visits and create a planning at the same time if you know beforehand the route for each destination/visit. See the details of importDestinations operation to import your data and create a planning in only one call. * Same operations are available for stores (note you have an existing default store). * With created destinations/visits, you can create a planning (routes and stops are automatically created depending of yours vehicles and destinations/visits) * In existing planning, you have availability to move stops (which represent visits) on a dedicated route (which represent a dedicated vehicle). * With many unaffected (out-of-route) stops in a planning, you may create a zoning to move many stops in several routes. Create a zoning (you can generate zones in this zoning automatically from automatic clustering), if you apply zoning (containing zones linked to a vehicle) on your planning, all stops contained in different zones will be moved in dedicated routes.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: tech@mapotempo.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.20
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Mapotempo\Api;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\MultipartStream;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;
use Mapotempo\ApiException;
use Mapotempo\Configuration;
use Mapotempo\HeaderSelector;
use Mapotempo\ObjectSerializer;

/**
 * GeocoderApi Class Doc Comment
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class GeocoderApi
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var Configuration
     */
    protected $config;

    /**
     * @var HeaderSelector
     */
    protected $headerSelector;

    /**
     * @param ClientInterface $client
     * @param Configuration   $config
     * @param HeaderSelector  $selector
     */
    public function __construct(
        ClientInterface $client = null,
        Configuration $config = null,
        HeaderSelector $selector = null
    ) {
        $this->client = $client ?: new Client();
        $this->config = $config ?: new Configuration();
        $this->headerSelector = $selector ?: new HeaderSelector();
    }

    /**
     * @return Configuration
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Operation geocode
     *
     * Geocode.
     *
     * @param  string $q Free query string. (required)
     * @param  float $lat Prioritize results around this latitude. (optional)
     * @param  float $lng Prioritize results around this longitude. (optional)
     * @param  int $limit Max results numbers. (default and upper max 10) (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return void
     */
    public function geocode($q, $lat = null, $lng = null, $limit = null)
    {
        $this->geocodeWithHttpInfo($q, $lat, $lng, $limit);
    }

    /**
     * Operation geocodeWithHttpInfo
     *
     * Geocode.
     *
     * @param  string $q Free query string. (required)
     * @param  float $lat Prioritize results around this latitude. (optional)
     * @param  float $lng Prioritize results around this longitude. (optional)
     * @param  int $limit Max results numbers. (default and upper max 10) (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of null, HTTP status code, HTTP response headers (array of strings)
     */
    public function geocodeWithHttpInfo($q, $lat = null, $lng = null, $limit = null)
    {
        $returnType = '';
        $request = $this->geocodeRequest($q, $lat, $lng, $limit);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            return [null, $statusCode, $response->getHeaders()];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
            }
            throw $e;
        }
    }

    /**
     * Operation geocodeAsync
     *
     * Geocode.
     *
     * @param  string $q Free query string. (required)
     * @param  float $lat Prioritize results around this latitude. (optional)
     * @param  float $lng Prioritize results around this longitude. (optional)
     * @param  int $limit Max results numbers. (default and upper max 10) (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function geocodeAsync($q, $lat = null, $lng = null, $limit = null)
    {
        return $this->geocodeAsyncWithHttpInfo($q, $lat, $lng, $limit)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation geocodeAsyncWithHttpInfo
     *
     * Geocode.
     *
     * @param  string $q Free query string. (required)
     * @param  float $lat Prioritize results around this latitude. (optional)
     * @param  float $lng Prioritize results around this longitude. (optional)
     * @param  int $limit Max results numbers. (default and upper max 10) (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function geocodeAsyncWithHttpInfo($q, $lat = null, $lng = null, $limit = null)
    {
        $returnType = '';
        $request = $this->geocodeRequest($q, $lat, $lng, $limit);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    return [null, $response->getStatusCode(), $response->getHeaders()];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'geocode'
     *
     * @param  string $q Free query string. (required)
     * @param  float $lat Prioritize results around this latitude. (optional)
     * @param  float $lng Prioritize results around this longitude. (optional)
     * @param  int $limit Max results numbers. (default and upper max 10) (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function geocodeRequest($q, $lat = null, $lng = null, $limit = null)
    {
        // verify the required parameter 'q' is set
        if ($q === null || (is_array($q) && count($q) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $q when calling geocode'
            );
        }

        $resourcePath = '/0.1/geocoder/search';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // query params
        if ($q !== null) {
            $queryParams['q'] = ObjectSerializer::toQueryValue($q);
        }
        // query params
        if ($lat !== null) {
            $queryParams['lat'] = ObjectSerializer::toQueryValue($lat);
        }
        // query params
        if ($lng !== null) {
            $queryParams['lng'] = ObjectSerializer::toQueryValue($lng);
        }
        // query params
        if ($limit !== null) {
            $queryParams['limit'] = ObjectSerializer::toQueryValue($limit);
        }


        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                []
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                [],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Create http client option
     *
     * @throws \RuntimeException on file opening failure
     * @return array of http client options
     */
    protected function createHttpClientOption()
    {
        $options = [];
        if ($this->config->getDebug()) {
            $options[RequestOptions::DEBUG] = fopen($this->config->getDebugFile(), 'a');
            if (!$options[RequestOptions::DEBUG]) {
                throw new \RuntimeException('Failed to open the debug file: ' . $this->config->getDebugFile());
            }
        }

        return $options;
    }
}
