<?php
/**
 * PlanningsApi
 * PHP version 5
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * API
 *
 * [Simplified view of domain model](https://app.mapotempo.com//api/0.1/Model-simpel.svg). ## Model Model is structured around four majors concepts: the Customer account, Destinations, Vehicles and Plannings. * `Customers`: many of objects are linked to a customer account (relating to the user calling API). The customer has many `Users`, each user has his own `api_key`. Be carefull not to confuse with following model `Destination`, `Customer`can only be created by a admin `User`. * `Destinations`: location points to visit with constraints. The same `Destination` can be visited several times : in this case several `Visits` are associated to one `Destination`. * `Vehicles`: vehicles definition are splited in two parts:  * the structural definition named `Vehicle` (car, truck, bike, consumption, etc.)  * and the vehicle usage `VehicleUsage`, a specific usage of a physical vehicle in a specific context. Vehicles can be used in many contexts called `VehicleUsageSet` (set of all vehicles usages under a context). Multiple values are only available if dedicated option for customer is active. For instance, if customer needs to use its vehicle 2 times per day (morning and evening), he needs 2 `VehicleUsageSet` called \"Morning\" and \"Evening\" : each can have different values defined for stores, rest, etc... `VehicleUsageSet` defines default values for vehicle usage. * `Plannings`: `Planning` is a set of `Routes` to `Visit` `Destinations` with `Vehicle` within a `VehicleUsageSet` context. A route is a track between all destinations reached by a vehicle (a new route is created for each customer's vehicle and a route without vehicle is created for all out-of-route destinations). By default all customer's visites are used in a planning.  ## Technical access ### Swagger descriptor This REST API is described with Swagger. The Swagger descriptor defines the request end-points, the parameters and the return values. The API can be addressed by HTTP request or with a generated client using the Swagger descriptor. ### API key All access to the API are subject to an `api_key` parameter in order to authenticate the user. This parameter can be sent with a query string on each available operation: `https://app.mapotempo.com/api/0.1/{objects}?api_key={your_personal_api_key}` ### Return The API supports several return formats: `json` and `xml` which depend of the requested extension used in url. ### I18n Functionnal textual returns are subject to translation and depend of HTTP header `Accept-Language`. HTTP error codes are not translated. ## Admin acces Using an admin `api_key` switches to advanced opperations (on `Customer`, `User`, `Vehicle`, `Profile`). Most of operations from the current api are usable either for normal user `api_key` or admin user `api_key` (not both). ## More concepts When a customer is created some objects are created by default with this new customer: * `Vehicle`: multiple, depending of the `max_vehicles` defined for customer * `DeliverableUnit`: one default * `VehicleUsageSet`: one default * `VehicleUsage`: multiple, depending of the vehicles number * `Store`: one default  ### Profiles, Layers, Routers `Profile` is a concept which allows to set several parameters for the customer: * `Layer`: which allows to choose the background map * `Router`: which allows to build route's information.  Several default profiles are available and can be listed with an admin `api_key`.  ### Tags `Tag` is a concept to filter visits and create planning only for a subset of visits. For instance, if some visits are tagged \"Monday\", it allows to create a new planning for \"Monday\" tag and use only dedicated visits. ### Zonings `Zoning` is a concept which allows to define multiple `Zones` (areas) around destinatons. A `Zone` can be affected to a `Vehicle` and if it is used into a `Planning`, all `Destinations` inside areas will be affected to the zone's vehicle (or `Route`). A polygon defining a `Zone` can be created outside the application or can be automatically generated from a planning.  ## Code samples * Create and display destinations or visits. Here some samples for these operations: [using PHP](https://app.mapotempo.com//api/0.1/examples/php/example.php), [using Ruby](https://app.mapotempo.com//api/0.1/examples/ruby/example.rb). Note you can import destinations/visits and create a planning at the same time if you know beforehand the route for each destination/visit. See the details of importDestinations operation to import your data and create a planning in only one call. * Same operations are available for stores (note you have an existing default store). * With created destinations/visits, you can create a planning (routes and stops are automatically created depending of yours vehicles and destinations/visits) * In existing planning, you have availability to move stops (which represent visits) on a dedicated route (which represent a dedicated vehicle). * With many unaffected (out-of-route) stops in a planning, you may create a zoning to move many stops in several routes. Create a zoning (you can generate zones in this zoning automatically from automatic clustering), if you apply zoning (containing zones linked to a vehicle) on your planning, all stops contained in different zones will be moved in dedicated routes.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: tech@mapotempo.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.20
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Mapotempo\Api;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\MultipartStream;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;
use Mapotempo\ApiException;
use Mapotempo\Configuration;
use Mapotempo\HeaderSelector;
use Mapotempo\ObjectSerializer;

/**
 * PlanningsApi Class Doc Comment
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class PlanningsApi
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var Configuration
     */
    protected $config;

    /**
     * @var HeaderSelector
     */
    protected $headerSelector;

    /**
     * @param ClientInterface $client
     * @param Configuration   $config
     * @param HeaderSelector  $selector
     */
    public function __construct(
        ClientInterface $client = null,
        Configuration $config = null,
        HeaderSelector $selector = null
    ) {
        $this->client = $client ?: new Client();
        $this->config = $config ?: new Configuration();
        $this->headerSelector = $selector ?: new HeaderSelector();
    }

    /**
     * @return Configuration
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Operation activationStops
     *
     * Change stops activation.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $active active (required)
     * @param  string $with_geojson with_geojson (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \Mapotempo\Model\V01Route
     */
    public function activationStops($planning_id, $id, $active, $with_geojson = null)
    {
        list($response) = $this->activationStopsWithHttpInfo($planning_id, $id, $active, $with_geojson);
        return $response;
    }

    /**
     * Operation activationStopsWithHttpInfo
     *
     * Change stops activation.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $active (required)
     * @param  string $with_geojson (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \Mapotempo\Model\V01Route, HTTP status code, HTTP response headers (array of strings)
     */
    public function activationStopsWithHttpInfo($planning_id, $id, $active, $with_geojson = null)
    {
        $returnType = '\Mapotempo\Model\V01Route';
        $request = $this->activationStopsRequest($planning_id, $id, $active, $with_geojson);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string','integer','bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Route',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status400',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 401:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status401',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 402:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status402',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status403',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 404:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status404',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 405:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status405',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 409:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status409',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 500:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status500',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation activationStopsAsync
     *
     * Change stops activation.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $active (required)
     * @param  string $with_geojson (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function activationStopsAsync($planning_id, $id, $active, $with_geojson = null)
    {
        return $this->activationStopsAsyncWithHttpInfo($planning_id, $id, $active, $with_geojson)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation activationStopsAsyncWithHttpInfo
     *
     * Change stops activation.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $active (required)
     * @param  string $with_geojson (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function activationStopsAsyncWithHttpInfo($planning_id, $id, $active, $with_geojson = null)
    {
        $returnType = '\Mapotempo\Model\V01Route';
        $request = $this->activationStopsRequest($planning_id, $id, $active, $with_geojson);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'activationStops'
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $active (required)
     * @param  string $with_geojson (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function activationStopsRequest($planning_id, $id, $active, $with_geojson = null)
    {
        // verify the required parameter 'planning_id' is set
        if ($planning_id === null || (is_array($planning_id) && count($planning_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $planning_id when calling activationStops'
            );
        }
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling activationStops'
            );
        }
        // verify the required parameter 'active' is set
        if ($active === null || (is_array($active) && count($active) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $active when calling activationStops'
            );
        }

        $resourcePath = '/0.1/plannings/{planning_id}/routes/{id}/active/{active}';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;


        // path params
        if ($planning_id !== null) {
            $resourcePath = str_replace(
                '{' . 'planning_id' . '}',
                ObjectSerializer::toPathValue($planning_id),
                $resourcePath
            );
        }
        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }
        // path params
        if ($active !== null) {
            $resourcePath = str_replace(
                '{' . 'active' . '}',
                ObjectSerializer::toPathValue($active),
                $resourcePath
            );
        }

        // form params
        if ($with_geojson !== null) {
            $formParams['with_geojson'] = ObjectSerializer::toFormValue($with_geojson);
        }
        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml'],
                ['multipart/form-data']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'PATCH',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation applyZonings
     *
     * Apply zonings.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $with_details Output route details (optional)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return void
     */
    public function applyZonings($id, $with_details = null, $with_geojson = 'false')
    {
        $this->applyZoningsWithHttpInfo($id, $with_details, $with_geojson);
    }

    /**
     * Operation applyZoningsWithHttpInfo
     *
     * Apply zonings.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $with_details Output route details (optional)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of null, HTTP status code, HTTP response headers (array of strings)
     */
    public function applyZoningsWithHttpInfo($id, $with_details = null, $with_geojson = 'false')
    {
        $returnType = '';
        $request = $this->applyZoningsRequest($id, $with_details, $with_geojson);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            return [null, $statusCode, $response->getHeaders()];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
            }
            throw $e;
        }
    }

    /**
     * Operation applyZoningsAsync
     *
     * Apply zonings.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $with_details Output route details (optional)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function applyZoningsAsync($id, $with_details = null, $with_geojson = 'false')
    {
        return $this->applyZoningsAsyncWithHttpInfo($id, $with_details, $with_geojson)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation applyZoningsAsyncWithHttpInfo
     *
     * Apply zonings.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $with_details Output route details (optional)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function applyZoningsAsyncWithHttpInfo($id, $with_details = null, $with_geojson = 'false')
    {
        $returnType = '';
        $request = $this->applyZoningsRequest($id, $with_details, $with_geojson);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    return [null, $response->getStatusCode(), $response->getHeaders()];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'applyZonings'
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $with_details Output route details (optional)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function applyZoningsRequest($id, $with_details = null, $with_geojson = 'false')
    {
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling applyZonings'
            );
        }

        $resourcePath = '/0.1/plannings/{id}/apply_zonings';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // query params
        if ($with_details !== null) {
            $queryParams['with_details'] = ObjectSerializer::toQueryValue($with_details);
        }
        // query params
        if ($with_geojson !== null) {
            $queryParams['with_geojson'] = ObjectSerializer::toQueryValue($with_geojson);
        }

        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                []
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                [],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation automaticInsertStop
     *
     * Insert one or more stop into planning routes.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  float $max_time max_time (optional)
     * @param  float $max_distance max_distance (optional)
     * @param  bool $active_only active_only (optional)
     * @param  bool $out_of_zone out_of_zone (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return void
     */
    public function automaticInsertStop($id, $max_time = null, $max_distance = null, $active_only = null, $out_of_zone = null)
    {
        $this->automaticInsertStopWithHttpInfo($id, $max_time, $max_distance, $active_only, $out_of_zone);
    }

    /**
     * Operation automaticInsertStopWithHttpInfo
     *
     * Insert one or more stop into planning routes.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  float $max_time (optional)
     * @param  float $max_distance (optional)
     * @param  bool $active_only (optional)
     * @param  bool $out_of_zone (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of null, HTTP status code, HTTP response headers (array of strings)
     */
    public function automaticInsertStopWithHttpInfo($id, $max_time = null, $max_distance = null, $active_only = null, $out_of_zone = null)
    {
        $returnType = '';
        $request = $this->automaticInsertStopRequest($id, $max_time, $max_distance, $active_only, $out_of_zone);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            return [null, $statusCode, $response->getHeaders()];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
            }
            throw $e;
        }
    }

    /**
     * Operation automaticInsertStopAsync
     *
     * Insert one or more stop into planning routes.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  float $max_time (optional)
     * @param  float $max_distance (optional)
     * @param  bool $active_only (optional)
     * @param  bool $out_of_zone (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function automaticInsertStopAsync($id, $max_time = null, $max_distance = null, $active_only = null, $out_of_zone = null)
    {
        return $this->automaticInsertStopAsyncWithHttpInfo($id, $max_time, $max_distance, $active_only, $out_of_zone)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation automaticInsertStopAsyncWithHttpInfo
     *
     * Insert one or more stop into planning routes.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  float $max_time (optional)
     * @param  float $max_distance (optional)
     * @param  bool $active_only (optional)
     * @param  bool $out_of_zone (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function automaticInsertStopAsyncWithHttpInfo($id, $max_time = null, $max_distance = null, $active_only = null, $out_of_zone = null)
    {
        $returnType = '';
        $request = $this->automaticInsertStopRequest($id, $max_time, $max_distance, $active_only, $out_of_zone);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    return [null, $response->getStatusCode(), $response->getHeaders()];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'automaticInsertStop'
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  float $max_time (optional)
     * @param  float $max_distance (optional)
     * @param  bool $active_only (optional)
     * @param  bool $out_of_zone (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function automaticInsertStopRequest($id, $max_time = null, $max_distance = null, $active_only = null, $out_of_zone = null)
    {
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling automaticInsertStop'
            );
        }

        $resourcePath = '/0.1/plannings/{id}/automatic_insert';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;


        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // form params
        if ($max_time !== null) {
            $formParams['max_time'] = ObjectSerializer::toFormValue($max_time);
        }
        // form params
        if ($max_distance !== null) {
            $formParams['max_distance'] = ObjectSerializer::toFormValue($max_distance);
        }
        // form params
        if ($active_only !== null) {
            $formParams['active_only'] = ObjectSerializer::toFormValue($active_only);
        }
        // form params
        if ($out_of_zone !== null) {
            $formParams['out_of_zone'] = ObjectSerializer::toFormValue($out_of_zone);
        }
        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                []
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                [],
                ['multipart/form-data']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'PATCH',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation clonePlanning
     *
     * Clone the planning.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson with_geojson (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \Mapotempo\Model\V01Planning
     */
    public function clonePlanning($id, $with_geojson = null)
    {
        list($response) = $this->clonePlanningWithHttpInfo($id, $with_geojson);
        return $response;
    }

    /**
     * Operation clonePlanningWithHttpInfo
     *
     * Clone the planning.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \Mapotempo\Model\V01Planning, HTTP status code, HTTP response headers (array of strings)
     */
    public function clonePlanningWithHttpInfo($id, $with_geojson = null)
    {
        $returnType = '\Mapotempo\Model\V01Planning';
        $request = $this->clonePlanningRequest($id, $with_geojson);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string','integer','bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Planning',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status400',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 401:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status401',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 402:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status402',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status403',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 404:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status404',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 405:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status405',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 409:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status409',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 500:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status500',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation clonePlanningAsync
     *
     * Clone the planning.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function clonePlanningAsync($id, $with_geojson = null)
    {
        return $this->clonePlanningAsyncWithHttpInfo($id, $with_geojson)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation clonePlanningAsyncWithHttpInfo
     *
     * Clone the planning.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function clonePlanningAsyncWithHttpInfo($id, $with_geojson = null)
    {
        $returnType = '\Mapotempo\Model\V01Planning';
        $request = $this->clonePlanningRequest($id, $with_geojson);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'clonePlanning'
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function clonePlanningRequest($id, $with_geojson = null)
    {
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling clonePlanning'
            );
        }

        $resourcePath = '/0.1/plannings/{id}/duplicate';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;


        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // form params
        if ($with_geojson !== null) {
            $formParams['with_geojson'] = ObjectSerializer::toFormValue($with_geojson);
        }
        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml'],
                ['multipart/form-data']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'PATCH',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation createPlanning
     *
     * Create planning.
     *
     * @param  \Mapotempo\Model\Body14 $body body (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \Mapotempo\Model\V01Planning
     */
    public function createPlanning($body)
    {
        list($response) = $this->createPlanningWithHttpInfo($body);
        return $response;
    }

    /**
     * Operation createPlanningWithHttpInfo
     *
     * Create planning.
     *
     * @param  \Mapotempo\Model\Body14 $body (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \Mapotempo\Model\V01Planning, HTTP status code, HTTP response headers (array of strings)
     */
    public function createPlanningWithHttpInfo($body)
    {
        $returnType = '\Mapotempo\Model\V01Planning';
        $request = $this->createPlanningRequest($body);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string','integer','bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Planning',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status400',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 401:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status401',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 402:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status402',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status403',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 404:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status404',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 405:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status405',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 409:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status409',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 500:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status500',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation createPlanningAsync
     *
     * Create planning.
     *
     * @param  \Mapotempo\Model\Body14 $body (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function createPlanningAsync($body)
    {
        return $this->createPlanningAsyncWithHttpInfo($body)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation createPlanningAsyncWithHttpInfo
     *
     * Create planning.
     *
     * @param  \Mapotempo\Model\Body14 $body (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function createPlanningAsyncWithHttpInfo($body)
    {
        $returnType = '\Mapotempo\Model\V01Planning';
        $request = $this->createPlanningRequest($body);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'createPlanning'
     *
     * @param  \Mapotempo\Model\Body14 $body (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function createPlanningRequest($body)
    {
        // verify the required parameter 'body' is set
        if ($body === null || (is_array($body) && count($body) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $body when calling createPlanning'
            );
        }

        $resourcePath = '/0.1/plannings';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;



        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml'],
                ['application/json']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'POST',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation deletePlanning
     *
     * Delete planning.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return void
     */
    public function deletePlanning($id)
    {
        $this->deletePlanningWithHttpInfo($id);
    }

    /**
     * Operation deletePlanningWithHttpInfo
     *
     * Delete planning.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of null, HTTP status code, HTTP response headers (array of strings)
     */
    public function deletePlanningWithHttpInfo($id)
    {
        $returnType = '';
        $request = $this->deletePlanningRequest($id);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            return [null, $statusCode, $response->getHeaders()];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
            }
            throw $e;
        }
    }

    /**
     * Operation deletePlanningAsync
     *
     * Delete planning.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function deletePlanningAsync($id)
    {
        return $this->deletePlanningAsyncWithHttpInfo($id)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation deletePlanningAsyncWithHttpInfo
     *
     * Delete planning.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function deletePlanningAsyncWithHttpInfo($id)
    {
        $returnType = '';
        $request = $this->deletePlanningRequest($id);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    return [null, $response->getStatusCode(), $response->getHeaders()];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'deletePlanning'
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function deletePlanningRequest($id)
    {
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling deletePlanning'
            );
        }

        $resourcePath = '/0.1/plannings/{id}';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;


        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                []
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                [],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'DELETE',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation deletePlannings
     *
     * Delete multiple plannings.
     *
     * @param  string[] $ids ids (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return void
     */
    public function deletePlannings($ids)
    {
        $this->deletePlanningsWithHttpInfo($ids);
    }

    /**
     * Operation deletePlanningsWithHttpInfo
     *
     * Delete multiple plannings.
     *
     * @param  string[] $ids (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of null, HTTP status code, HTTP response headers (array of strings)
     */
    public function deletePlanningsWithHttpInfo($ids)
    {
        $returnType = '';
        $request = $this->deletePlanningsRequest($ids);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            return [null, $statusCode, $response->getHeaders()];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
            }
            throw $e;
        }
    }

    /**
     * Operation deletePlanningsAsync
     *
     * Delete multiple plannings.
     *
     * @param  string[] $ids (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function deletePlanningsAsync($ids)
    {
        return $this->deletePlanningsAsyncWithHttpInfo($ids)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation deletePlanningsAsyncWithHttpInfo
     *
     * Delete multiple plannings.
     *
     * @param  string[] $ids (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function deletePlanningsAsyncWithHttpInfo($ids)
    {
        $returnType = '';
        $request = $this->deletePlanningsRequest($ids);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    return [null, $response->getStatusCode(), $response->getHeaders()];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'deletePlannings'
     *
     * @param  string[] $ids (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function deletePlanningsRequest($ids)
    {
        // verify the required parameter 'ids' is set
        if ($ids === null || (is_array($ids) && count($ids) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $ids when calling deletePlannings'
            );
        }

        $resourcePath = '/0.1/plannings';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;



        // form params
        if ($ids !== null) {
            $formParams['ids'] = ObjectSerializer::toFormValue($ids);
        }
        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                []
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                [],
                ['multipart/form-data']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'DELETE',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation getPlanning
     *
     * Fetch planning.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson Fill the geojson field with route geometry, when using json output. For geojson output, param can be only set to &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring. (optional, default to false)
     * @param  bool $with_quantities Include the quantities when using geojson output. (optional)
     * @param  bool $with_stores Include the stores in geojson output. (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \Mapotempo\Model\V01Planning
     */
    public function getPlanning($id, $with_geojson = 'false', $with_quantities = null, $with_stores = null)
    {
        list($response) = $this->getPlanningWithHttpInfo($id, $with_geojson, $with_quantities, $with_stores);
        return $response;
    }

    /**
     * Operation getPlanningWithHttpInfo
     *
     * Fetch planning.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson Fill the geojson field with route geometry, when using json output. For geojson output, param can be only set to &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring. (optional, default to false)
     * @param  bool $with_quantities Include the quantities when using geojson output. (optional)
     * @param  bool $with_stores Include the stores in geojson output. (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \Mapotempo\Model\V01Planning, HTTP status code, HTTP response headers (array of strings)
     */
    public function getPlanningWithHttpInfo($id, $with_geojson = 'false', $with_quantities = null, $with_stores = null)
    {
        $returnType = '\Mapotempo\Model\V01Planning';
        $request = $this->getPlanningRequest($id, $with_geojson, $with_quantities, $with_stores);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string','integer','bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Planning',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation getPlanningAsync
     *
     * Fetch planning.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson Fill the geojson field with route geometry, when using json output. For geojson output, param can be only set to &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring. (optional, default to false)
     * @param  bool $with_quantities Include the quantities when using geojson output. (optional)
     * @param  bool $with_stores Include the stores in geojson output. (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getPlanningAsync($id, $with_geojson = 'false', $with_quantities = null, $with_stores = null)
    {
        return $this->getPlanningAsyncWithHttpInfo($id, $with_geojson, $with_quantities, $with_stores)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation getPlanningAsyncWithHttpInfo
     *
     * Fetch planning.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson Fill the geojson field with route geometry, when using json output. For geojson output, param can be only set to &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring. (optional, default to false)
     * @param  bool $with_quantities Include the quantities when using geojson output. (optional)
     * @param  bool $with_stores Include the stores in geojson output. (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getPlanningAsyncWithHttpInfo($id, $with_geojson = 'false', $with_quantities = null, $with_stores = null)
    {
        $returnType = '\Mapotempo\Model\V01Planning';
        $request = $this->getPlanningRequest($id, $with_geojson, $with_quantities, $with_stores);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'getPlanning'
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson Fill the geojson field with route geometry, when using json output. For geojson output, param can be only set to &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring. (optional, default to false)
     * @param  bool $with_quantities Include the quantities when using geojson output. (optional)
     * @param  bool $with_stores Include the stores in geojson output. (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function getPlanningRequest($id, $with_geojson = 'false', $with_quantities = null, $with_stores = null)
    {
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling getPlanning'
            );
        }

        $resourcePath = '/0.1/plannings/{id}';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // query params
        if ($with_geojson !== null) {
            $queryParams['with_geojson'] = ObjectSerializer::toQueryValue($with_geojson);
        }
        // query params
        if ($with_quantities !== null) {
            $queryParams['with_quantities'] = ObjectSerializer::toQueryValue($with_quantities);
        }
        // query params
        if ($with_stores !== null) {
            $queryParams['with_stores'] = ObjectSerializer::toQueryValue($with_stores);
        }

        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml'],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation getPlannings
     *
     * Fetch customer's plannings.
     *
     * @param  string[] $ids Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add &#x60;ref:&#x60; before each ref, e.g. &#x60;ref:ref1,ref:ref2,ref:ref3&#x60;. (optional)
     * @param  \DateTime $begin_date Select only plannings after this date. (optional)
     * @param  \DateTime $end_date Select only plannings before this date. (optional)
     * @param  bool $active Select only active plannings. (optional)
     * @param  string[] $tags Select plannings which contains at least one of these tags label (optional)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     * @param  string $name_like Find plannings with similarity on the name (case insensitive). (optional)
     * @param  string $ref_like Find plannings with similarity on the reference (case insensitive). (optional)
     * @param  string $words_mask Words separated by commas that will skip the similarity check (case insensitive) (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \Mapotempo\Model\V01Planning[]
     */
    public function getPlannings($ids = null, $begin_date = null, $end_date = null, $active = null, $tags = null, $with_geojson = 'false', $name_like = null, $ref_like = null, $words_mask = null)
    {
        list($response) = $this->getPlanningsWithHttpInfo($ids, $begin_date, $end_date, $active, $tags, $with_geojson, $name_like, $ref_like, $words_mask);
        return $response;
    }

    /**
     * Operation getPlanningsWithHttpInfo
     *
     * Fetch customer's plannings.
     *
     * @param  string[] $ids Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add &#x60;ref:&#x60; before each ref, e.g. &#x60;ref:ref1,ref:ref2,ref:ref3&#x60;. (optional)
     * @param  \DateTime $begin_date Select only plannings after this date. (optional)
     * @param  \DateTime $end_date Select only plannings before this date. (optional)
     * @param  bool $active Select only active plannings. (optional)
     * @param  string[] $tags Select plannings which contains at least one of these tags label (optional)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     * @param  string $name_like Find plannings with similarity on the name (case insensitive). (optional)
     * @param  string $ref_like Find plannings with similarity on the reference (case insensitive). (optional)
     * @param  string $words_mask Words separated by commas that will skip the similarity check (case insensitive) (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \Mapotempo\Model\V01Planning[], HTTP status code, HTTP response headers (array of strings)
     */
    public function getPlanningsWithHttpInfo($ids = null, $begin_date = null, $end_date = null, $active = null, $tags = null, $with_geojson = 'false', $name_like = null, $ref_like = null, $words_mask = null)
    {
        $returnType = '\Mapotempo\Model\V01Planning[]';
        $request = $this->getPlanningsRequest($ids, $begin_date, $end_date, $active, $tags, $with_geojson, $name_like, $ref_like, $words_mask);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string','integer','bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Planning[]',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation getPlanningsAsync
     *
     * Fetch customer's plannings.
     *
     * @param  string[] $ids Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add &#x60;ref:&#x60; before each ref, e.g. &#x60;ref:ref1,ref:ref2,ref:ref3&#x60;. (optional)
     * @param  \DateTime $begin_date Select only plannings after this date. (optional)
     * @param  \DateTime $end_date Select only plannings before this date. (optional)
     * @param  bool $active Select only active plannings. (optional)
     * @param  string[] $tags Select plannings which contains at least one of these tags label (optional)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     * @param  string $name_like Find plannings with similarity on the name (case insensitive). (optional)
     * @param  string $ref_like Find plannings with similarity on the reference (case insensitive). (optional)
     * @param  string $words_mask Words separated by commas that will skip the similarity check (case insensitive) (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getPlanningsAsync($ids = null, $begin_date = null, $end_date = null, $active = null, $tags = null, $with_geojson = 'false', $name_like = null, $ref_like = null, $words_mask = null)
    {
        return $this->getPlanningsAsyncWithHttpInfo($ids, $begin_date, $end_date, $active, $tags, $with_geojson, $name_like, $ref_like, $words_mask)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation getPlanningsAsyncWithHttpInfo
     *
     * Fetch customer's plannings.
     *
     * @param  string[] $ids Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add &#x60;ref:&#x60; before each ref, e.g. &#x60;ref:ref1,ref:ref2,ref:ref3&#x60;. (optional)
     * @param  \DateTime $begin_date Select only plannings after this date. (optional)
     * @param  \DateTime $end_date Select only plannings before this date. (optional)
     * @param  bool $active Select only active plannings. (optional)
     * @param  string[] $tags Select plannings which contains at least one of these tags label (optional)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     * @param  string $name_like Find plannings with similarity on the name (case insensitive). (optional)
     * @param  string $ref_like Find plannings with similarity on the reference (case insensitive). (optional)
     * @param  string $words_mask Words separated by commas that will skip the similarity check (case insensitive) (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getPlanningsAsyncWithHttpInfo($ids = null, $begin_date = null, $end_date = null, $active = null, $tags = null, $with_geojson = 'false', $name_like = null, $ref_like = null, $words_mask = null)
    {
        $returnType = '\Mapotempo\Model\V01Planning[]';
        $request = $this->getPlanningsRequest($ids, $begin_date, $end_date, $active, $tags, $with_geojson, $name_like, $ref_like, $words_mask);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'getPlannings'
     *
     * @param  string[] $ids Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add &#x60;ref:&#x60; before each ref, e.g. &#x60;ref:ref1,ref:ref2,ref:ref3&#x60;. (optional)
     * @param  \DateTime $begin_date Select only plannings after this date. (optional)
     * @param  \DateTime $end_date Select only plannings before this date. (optional)
     * @param  bool $active Select only active plannings. (optional)
     * @param  string[] $tags Select plannings which contains at least one of these tags label (optional)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     * @param  string $name_like Find plannings with similarity on the name (case insensitive). (optional)
     * @param  string $ref_like Find plannings with similarity on the reference (case insensitive). (optional)
     * @param  string $words_mask Words separated by commas that will skip the similarity check (case insensitive) (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function getPlanningsRequest($ids = null, $begin_date = null, $end_date = null, $active = null, $tags = null, $with_geojson = 'false', $name_like = null, $ref_like = null, $words_mask = null)
    {

        $resourcePath = '/0.1/plannings';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // query params
        if (is_array($ids)) {
            $ids = ObjectSerializer::serializeCollection($ids, 'csv', true);
        }
        if ($ids !== null) {
            $queryParams['ids'] = ObjectSerializer::toQueryValue($ids);
        }
        // query params
        if ($begin_date !== null) {
            $queryParams['begin_date'] = ObjectSerializer::toQueryValue($begin_date);
        }
        // query params
        if ($end_date !== null) {
            $queryParams['end_date'] = ObjectSerializer::toQueryValue($end_date);
        }
        // query params
        if ($active !== null) {
            $queryParams['active'] = ObjectSerializer::toQueryValue($active);
        }
        // query params
        if (is_array($tags)) {
            $tags = ObjectSerializer::serializeCollection($tags, 'csv', true);
        }
        if ($tags !== null) {
            $queryParams['tags'] = ObjectSerializer::toQueryValue($tags);
        }
        // query params
        if ($with_geojson !== null) {
            $queryParams['with_geojson'] = ObjectSerializer::toQueryValue($with_geojson);
        }
        // query params
        if ($name_like !== null) {
            $queryParams['name_like'] = ObjectSerializer::toQueryValue($name_like);
        }
        // query params
        if ($ref_like !== null) {
            $queryParams['ref_like'] = ObjectSerializer::toQueryValue($ref_like);
        }
        // query params
        if ($words_mask !== null) {
            $queryParams['words_mask'] = ObjectSerializer::toQueryValue($words_mask);
        }


        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml'],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation getQuantityS
     *
     * Fetch quantities from a planning.
     *
     * @param  int $id The planning id (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return void
     */
    public function getQuantityS($id)
    {
        $this->getQuantitySWithHttpInfo($id);
    }

    /**
     * Operation getQuantitySWithHttpInfo
     *
     * Fetch quantities from a planning.
     *
     * @param  int $id The planning id (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of null, HTTP status code, HTTP response headers (array of strings)
     */
    public function getQuantitySWithHttpInfo($id)
    {
        $returnType = '';
        $request = $this->getQuantitySRequest($id);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            return [null, $statusCode, $response->getHeaders()];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status400',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 401:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status401',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 402:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status402',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status403',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 404:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status404',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 405:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status405',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 409:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status409',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 500:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status500',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation getQuantitySAsync
     *
     * Fetch quantities from a planning.
     *
     * @param  int $id The planning id (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getQuantitySAsync($id)
    {
        return $this->getQuantitySAsyncWithHttpInfo($id)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation getQuantitySAsyncWithHttpInfo
     *
     * Fetch quantities from a planning.
     *
     * @param  int $id The planning id (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getQuantitySAsyncWithHttpInfo($id)
    {
        $returnType = '';
        $request = $this->getQuantitySRequest($id);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    return [null, $response->getStatusCode(), $response->getHeaders()];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'getQuantityS'
     *
     * @param  int $id The planning id (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function getQuantitySRequest($id)
    {
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling getQuantityS'
            );
        }

        $resourcePath = '/0.1/plannings/{id}/quantities';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;


        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml'],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation getRoute
     *
     * Fetch route.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson Fill the geojson field with route geometry, when using json output. For geojson output, param can be only set to &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring. (optional, default to false)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \Mapotempo\Model\V01Route
     */
    public function getRoute($planning_id, $id, $with_geojson = 'false')
    {
        list($response) = $this->getRouteWithHttpInfo($planning_id, $id, $with_geojson);
        return $response;
    }

    /**
     * Operation getRouteWithHttpInfo
     *
     * Fetch route.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson Fill the geojson field with route geometry, when using json output. For geojson output, param can be only set to &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring. (optional, default to false)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \Mapotempo\Model\V01Route, HTTP status code, HTTP response headers (array of strings)
     */
    public function getRouteWithHttpInfo($planning_id, $id, $with_geojson = 'false')
    {
        $returnType = '\Mapotempo\Model\V01Route';
        $request = $this->getRouteRequest($planning_id, $id, $with_geojson);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string','integer','bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Route',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation getRouteAsync
     *
     * Fetch route.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson Fill the geojson field with route geometry, when using json output. For geojson output, param can be only set to &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring. (optional, default to false)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getRouteAsync($planning_id, $id, $with_geojson = 'false')
    {
        return $this->getRouteAsyncWithHttpInfo($planning_id, $id, $with_geojson)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation getRouteAsyncWithHttpInfo
     *
     * Fetch route.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson Fill the geojson field with route geometry, when using json output. For geojson output, param can be only set to &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring. (optional, default to false)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getRouteAsyncWithHttpInfo($planning_id, $id, $with_geojson = 'false')
    {
        $returnType = '\Mapotempo\Model\V01Route';
        $request = $this->getRouteRequest($planning_id, $id, $with_geojson);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'getRoute'
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson Fill the geojson field with route geometry, when using json output. For geojson output, param can be only set to &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring. (optional, default to false)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function getRouteRequest($planning_id, $id, $with_geojson = 'false')
    {
        // verify the required parameter 'planning_id' is set
        if ($planning_id === null || (is_array($planning_id) && count($planning_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $planning_id when calling getRoute'
            );
        }
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling getRoute'
            );
        }

        $resourcePath = '/0.1/plannings/{planning_id}/routes/{id}';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // query params
        if ($with_geojson !== null) {
            $queryParams['with_geojson'] = ObjectSerializer::toQueryValue($with_geojson);
        }

        // path params
        if ($planning_id !== null) {
            $resourcePath = str_replace(
                '{' . 'planning_id' . '}',
                ObjectSerializer::toPathValue($planning_id),
                $resourcePath
            );
        }
        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml'],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation getRouteByVehicle
     *
     * Fetch route from vehicle.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \Mapotempo\Model\V01Route
     */
    public function getRouteByVehicle($planning_id, $id, $with_geojson = 'false')
    {
        list($response) = $this->getRouteByVehicleWithHttpInfo($planning_id, $id, $with_geojson);
        return $response;
    }

    /**
     * Operation getRouteByVehicleWithHttpInfo
     *
     * Fetch route from vehicle.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \Mapotempo\Model\V01Route, HTTP status code, HTTP response headers (array of strings)
     */
    public function getRouteByVehicleWithHttpInfo($planning_id, $id, $with_geojson = 'false')
    {
        $returnType = '\Mapotempo\Model\V01Route';
        $request = $this->getRouteByVehicleRequest($planning_id, $id, $with_geojson);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string','integer','bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Route',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status400',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 401:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status401',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 402:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status402',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status403',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 404:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status404',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 405:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status405',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 409:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status409',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 500:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status500',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation getRouteByVehicleAsync
     *
     * Fetch route from vehicle.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getRouteByVehicleAsync($planning_id, $id, $with_geojson = 'false')
    {
        return $this->getRouteByVehicleAsyncWithHttpInfo($planning_id, $id, $with_geojson)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation getRouteByVehicleAsyncWithHttpInfo
     *
     * Fetch route from vehicle.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getRouteByVehicleAsyncWithHttpInfo($planning_id, $id, $with_geojson = 'false')
    {
        $returnType = '\Mapotempo\Model\V01Route';
        $request = $this->getRouteByVehicleRequest($planning_id, $id, $with_geojson);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'getRouteByVehicle'
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function getRouteByVehicleRequest($planning_id, $id, $with_geojson = 'false')
    {
        // verify the required parameter 'planning_id' is set
        if ($planning_id === null || (is_array($planning_id) && count($planning_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $planning_id when calling getRouteByVehicle'
            );
        }
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling getRouteByVehicle'
            );
        }

        $resourcePath = '/0.1/plannings/{planning_id}/routes_by_vehicle/{id}';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // query params
        if ($with_geojson !== null) {
            $queryParams['with_geojson'] = ObjectSerializer::toQueryValue($with_geojson);
        }

        // path params
        if ($planning_id !== null) {
            $resourcePath = str_replace(
                '{' . 'planning_id' . '}',
                ObjectSerializer::toPathValue($planning_id),
                $resourcePath
            );
        }
        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml'],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation getRoutes
     *
     * Fetch planning's routes.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string[] $ids Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add &#x60;ref:&#x60; before each ref, e.g. &#x60;ref:ref1,ref:ref2,ref:ref3&#x60;. (optional)
     * @param  string $with_geojson Fill the geojson field with route geometry, when using json output. For geojson output, param can be only set to &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring. (optional, default to false)
     * @param  bool $with_stores Include the stores when using geojson output. (optional)
     * @param  bool $with_quantities Include the quantities when using geojson output. (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \Mapotempo\Model\V01Route[]
     */
    public function getRoutes($planning_id, $ids = null, $with_geojson = 'false', $with_stores = null, $with_quantities = null)
    {
        list($response) = $this->getRoutesWithHttpInfo($planning_id, $ids, $with_geojson, $with_stores, $with_quantities);
        return $response;
    }

    /**
     * Operation getRoutesWithHttpInfo
     *
     * Fetch planning's routes.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string[] $ids Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add &#x60;ref:&#x60; before each ref, e.g. &#x60;ref:ref1,ref:ref2,ref:ref3&#x60;. (optional)
     * @param  string $with_geojson Fill the geojson field with route geometry, when using json output. For geojson output, param can be only set to &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring. (optional, default to false)
     * @param  bool $with_stores Include the stores when using geojson output. (optional)
     * @param  bool $with_quantities Include the quantities when using geojson output. (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \Mapotempo\Model\V01Route[], HTTP status code, HTTP response headers (array of strings)
     */
    public function getRoutesWithHttpInfo($planning_id, $ids = null, $with_geojson = 'false', $with_stores = null, $with_quantities = null)
    {
        $returnType = '\Mapotempo\Model\V01Route[]';
        $request = $this->getRoutesRequest($planning_id, $ids, $with_geojson, $with_stores, $with_quantities);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string','integer','bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Route[]',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation getRoutesAsync
     *
     * Fetch planning's routes.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string[] $ids Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add &#x60;ref:&#x60; before each ref, e.g. &#x60;ref:ref1,ref:ref2,ref:ref3&#x60;. (optional)
     * @param  string $with_geojson Fill the geojson field with route geometry, when using json output. For geojson output, param can be only set to &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring. (optional, default to false)
     * @param  bool $with_stores Include the stores when using geojson output. (optional)
     * @param  bool $with_quantities Include the quantities when using geojson output. (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getRoutesAsync($planning_id, $ids = null, $with_geojson = 'false', $with_stores = null, $with_quantities = null)
    {
        return $this->getRoutesAsyncWithHttpInfo($planning_id, $ids, $with_geojson, $with_stores, $with_quantities)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation getRoutesAsyncWithHttpInfo
     *
     * Fetch planning's routes.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string[] $ids Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add &#x60;ref:&#x60; before each ref, e.g. &#x60;ref:ref1,ref:ref2,ref:ref3&#x60;. (optional)
     * @param  string $with_geojson Fill the geojson field with route geometry, when using json output. For geojson output, param can be only set to &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring. (optional, default to false)
     * @param  bool $with_stores Include the stores when using geojson output. (optional)
     * @param  bool $with_quantities Include the quantities when using geojson output. (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getRoutesAsyncWithHttpInfo($planning_id, $ids = null, $with_geojson = 'false', $with_stores = null, $with_quantities = null)
    {
        $returnType = '\Mapotempo\Model\V01Route[]';
        $request = $this->getRoutesRequest($planning_id, $ids, $with_geojson, $with_stores, $with_quantities);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'getRoutes'
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string[] $ids Ids separated by comma. You can specify ref (not containing comma) instead of id, in this case you have to add &#x60;ref:&#x60; before each ref, e.g. &#x60;ref:ref1,ref:ref2,ref:ref3&#x60;. (optional)
     * @param  string $with_geojson Fill the geojson field with route geometry, when using json output. For geojson output, param can be only set to &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring. (optional, default to false)
     * @param  bool $with_stores Include the stores when using geojson output. (optional)
     * @param  bool $with_quantities Include the quantities when using geojson output. (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function getRoutesRequest($planning_id, $ids = null, $with_geojson = 'false', $with_stores = null, $with_quantities = null)
    {
        // verify the required parameter 'planning_id' is set
        if ($planning_id === null || (is_array($planning_id) && count($planning_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $planning_id when calling getRoutes'
            );
        }

        $resourcePath = '/0.1/plannings/{planning_id}/routes';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // query params
        if (is_array($ids)) {
            $ids = ObjectSerializer::serializeCollection($ids, 'csv', true);
        }
        if ($ids !== null) {
            $queryParams['ids'] = ObjectSerializer::toQueryValue($ids);
        }
        // query params
        if ($with_geojson !== null) {
            $queryParams['with_geojson'] = ObjectSerializer::toQueryValue($with_geojson);
        }
        // query params
        if ($with_stores !== null) {
            $queryParams['with_stores'] = ObjectSerializer::toQueryValue($with_stores);
        }
        // query params
        if ($with_quantities !== null) {
            $queryParams['with_quantities'] = ObjectSerializer::toQueryValue($with_quantities);
        }

        // path params
        if ($planning_id !== null) {
            $resourcePath = str_replace(
                '{' . 'planning_id' . '}',
                ObjectSerializer::toPathValue($planning_id),
                $resourcePath
            );
        }

        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml'],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation getStop
     *
     * Fetch stop.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $route_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  int $id id (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \Mapotempo\Model\V01Stop
     */
    public function getStop($planning_id, $route_id, $id)
    {
        list($response) = $this->getStopWithHttpInfo($planning_id, $route_id, $id);
        return $response;
    }

    /**
     * Operation getStopWithHttpInfo
     *
     * Fetch stop.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $route_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  int $id (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \Mapotempo\Model\V01Stop, HTTP status code, HTTP response headers (array of strings)
     */
    public function getStopWithHttpInfo($planning_id, $route_id, $id)
    {
        $returnType = '\Mapotempo\Model\V01Stop';
        $request = $this->getStopRequest($planning_id, $route_id, $id);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string','integer','bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Stop',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status400',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 401:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status401',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 402:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status402',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status403',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 404:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status404',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 405:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status405',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 409:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status409',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 500:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status500',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation getStopAsync
     *
     * Fetch stop.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $route_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  int $id (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getStopAsync($planning_id, $route_id, $id)
    {
        return $this->getStopAsyncWithHttpInfo($planning_id, $route_id, $id)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation getStopAsyncWithHttpInfo
     *
     * Fetch stop.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $route_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  int $id (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getStopAsyncWithHttpInfo($planning_id, $route_id, $id)
    {
        $returnType = '\Mapotempo\Model\V01Stop';
        $request = $this->getStopRequest($planning_id, $route_id, $id);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'getStop'
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $route_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  int $id (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function getStopRequest($planning_id, $route_id, $id)
    {
        // verify the required parameter 'planning_id' is set
        if ($planning_id === null || (is_array($planning_id) && count($planning_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $planning_id when calling getStop'
            );
        }
        // verify the required parameter 'route_id' is set
        if ($route_id === null || (is_array($route_id) && count($route_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $route_id when calling getStop'
            );
        }
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling getStop'
            );
        }

        $resourcePath = '/0.1/plannings/{planning_id}/routes/{route_id}/stops/{id}';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;


        // path params
        if ($planning_id !== null) {
            $resourcePath = str_replace(
                '{' . 'planning_id' . '}',
                ObjectSerializer::toPathValue($planning_id),
                $resourcePath
            );
        }
        // path params
        if ($route_id !== null) {
            $resourcePath = str_replace(
                '{' . 'route_id' . '}',
                ObjectSerializer::toPathValue($route_id),
                $resourcePath
            );
        }
        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml'],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation getVehicleUsageS
     *
     * Fetch vehicle usage(s) from a planning.
     *
     * @param  int $id The planning id (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return void
     */
    public function getVehicleUsageS($id)
    {
        $this->getVehicleUsageSWithHttpInfo($id);
    }

    /**
     * Operation getVehicleUsageSWithHttpInfo
     *
     * Fetch vehicle usage(s) from a planning.
     *
     * @param  int $id The planning id (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of null, HTTP status code, HTTP response headers (array of strings)
     */
    public function getVehicleUsageSWithHttpInfo($id)
    {
        $returnType = '';
        $request = $this->getVehicleUsageSRequest($id);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            return [null, $statusCode, $response->getHeaders()];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status400',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 401:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status401',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 402:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status402',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status403',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 404:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status404',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 405:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status405',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 409:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status409',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 500:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status500',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation getVehicleUsageSAsync
     *
     * Fetch vehicle usage(s) from a planning.
     *
     * @param  int $id The planning id (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getVehicleUsageSAsync($id)
    {
        return $this->getVehicleUsageSAsyncWithHttpInfo($id)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation getVehicleUsageSAsyncWithHttpInfo
     *
     * Fetch vehicle usage(s) from a planning.
     *
     * @param  int $id The planning id (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getVehicleUsageSAsyncWithHttpInfo($id)
    {
        $returnType = '';
        $request = $this->getVehicleUsageSRequest($id);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    return [null, $response->getStatusCode(), $response->getHeaders()];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'getVehicleUsageS'
     *
     * @param  int $id The planning id (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function getVehicleUsageSRequest($id)
    {
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling getVehicleUsageS'
            );
        }

        $resourcePath = '/0.1/plannings/{id}/vehicle_usages';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;


        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml'],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation moveStop
     *
     * Move stop position in routes.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $route_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  int $id Stop id to move (required)
     * @param  int $index New position in the route (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return void
     */
    public function moveStop($planning_id, $route_id, $id, $index)
    {
        $this->moveStopWithHttpInfo($planning_id, $route_id, $id, $index);
    }

    /**
     * Operation moveStopWithHttpInfo
     *
     * Move stop position in routes.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $route_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  int $id Stop id to move (required)
     * @param  int $index New position in the route (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of null, HTTP status code, HTTP response headers (array of strings)
     */
    public function moveStopWithHttpInfo($planning_id, $route_id, $id, $index)
    {
        $returnType = '';
        $request = $this->moveStopRequest($planning_id, $route_id, $id, $index);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            return [null, $statusCode, $response->getHeaders()];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
            }
            throw $e;
        }
    }

    /**
     * Operation moveStopAsync
     *
     * Move stop position in routes.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $route_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  int $id Stop id to move (required)
     * @param  int $index New position in the route (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function moveStopAsync($planning_id, $route_id, $id, $index)
    {
        return $this->moveStopAsyncWithHttpInfo($planning_id, $route_id, $id, $index)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation moveStopAsyncWithHttpInfo
     *
     * Move stop position in routes.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $route_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  int $id Stop id to move (required)
     * @param  int $index New position in the route (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function moveStopAsyncWithHttpInfo($planning_id, $route_id, $id, $index)
    {
        $returnType = '';
        $request = $this->moveStopRequest($planning_id, $route_id, $id, $index);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    return [null, $response->getStatusCode(), $response->getHeaders()];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'moveStop'
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $route_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  int $id Stop id to move (required)
     * @param  int $index New position in the route (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function moveStopRequest($planning_id, $route_id, $id, $index)
    {
        // verify the required parameter 'planning_id' is set
        if ($planning_id === null || (is_array($planning_id) && count($planning_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $planning_id when calling moveStop'
            );
        }
        // verify the required parameter 'route_id' is set
        if ($route_id === null || (is_array($route_id) && count($route_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $route_id when calling moveStop'
            );
        }
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling moveStop'
            );
        }
        // verify the required parameter 'index' is set
        if ($index === null || (is_array($index) && count($index) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $index when calling moveStop'
            );
        }

        $resourcePath = '/0.1/plannings/{planning_id}/routes/{route_id}/stops/{id}/move/{index}';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;


        // path params
        if ($planning_id !== null) {
            $resourcePath = str_replace(
                '{' . 'planning_id' . '}',
                ObjectSerializer::toPathValue($planning_id),
                $resourcePath
            );
        }
        // path params
        if ($route_id !== null) {
            $resourcePath = str_replace(
                '{' . 'route_id' . '}',
                ObjectSerializer::toPathValue($route_id),
                $resourcePath
            );
        }
        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }
        // path params
        if ($index !== null) {
            $resourcePath = str_replace(
                '{' . 'index' . '}',
                ObjectSerializer::toPathValue($index),
                $resourcePath
            );
        }

        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                []
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                [],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'PATCH',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation moveVisits
     *
     * Move visit(s) to route. Append in order at end (if automatic_insert is false).
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $automatic_insert automatic_insert (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return void
     */
    public function moveVisits($planning_id, $id, $automatic_insert = null)
    {
        $this->moveVisitsWithHttpInfo($planning_id, $id, $automatic_insert);
    }

    /**
     * Operation moveVisitsWithHttpInfo
     *
     * Move visit(s) to route. Append in order at end (if automatic_insert is false).
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $automatic_insert (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of null, HTTP status code, HTTP response headers (array of strings)
     */
    public function moveVisitsWithHttpInfo($planning_id, $id, $automatic_insert = null)
    {
        $returnType = '';
        $request = $this->moveVisitsRequest($planning_id, $id, $automatic_insert);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            return [null, $statusCode, $response->getHeaders()];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
            }
            throw $e;
        }
    }

    /**
     * Operation moveVisitsAsync
     *
     * Move visit(s) to route. Append in order at end (if automatic_insert is false).
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $automatic_insert (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function moveVisitsAsync($planning_id, $id, $automatic_insert = null)
    {
        return $this->moveVisitsAsyncWithHttpInfo($planning_id, $id, $automatic_insert)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation moveVisitsAsyncWithHttpInfo
     *
     * Move visit(s) to route. Append in order at end (if automatic_insert is false).
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $automatic_insert (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function moveVisitsAsyncWithHttpInfo($planning_id, $id, $automatic_insert = null)
    {
        $returnType = '';
        $request = $this->moveVisitsRequest($planning_id, $id, $automatic_insert);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    return [null, $response->getStatusCode(), $response->getHeaders()];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'moveVisits'
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $automatic_insert (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function moveVisitsRequest($planning_id, $id, $automatic_insert = null)
    {
        // verify the required parameter 'planning_id' is set
        if ($planning_id === null || (is_array($planning_id) && count($planning_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $planning_id when calling moveVisits'
            );
        }
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling moveVisits'
            );
        }

        $resourcePath = '/0.1/plannings/{planning_id}/routes/{id}/visits/moves';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;


        // path params
        if ($planning_id !== null) {
            $resourcePath = str_replace(
                '{' . 'planning_id' . '}',
                ObjectSerializer::toPathValue($planning_id),
                $resourcePath
            );
        }
        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // form params
        if ($automatic_insert !== null) {
            $formParams['automatic_insert'] = ObjectSerializer::toFormValue($automatic_insert);
        }
        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                []
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                [],
                ['multipart/form-data']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'PATCH',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation optimizeRoute
     *
     * Optimize a single route.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $details details (optional)
     * @param  bool $synchronous synchronous (optional)
     * @param  bool $active_only active_only (optional)
     * @param  string $ignore_overload_multipliers ignore_overload_multipliers (optional)
     * @param  bool $with_details with_details (optional)
     * @param  string $with_geojson with_geojson (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return void
     */
    public function optimizeRoute($planning_id, $id, $details = null, $synchronous = null, $active_only = null, $ignore_overload_multipliers = null, $with_details = null, $with_geojson = null)
    {
        $this->optimizeRouteWithHttpInfo($planning_id, $id, $details, $synchronous, $active_only, $ignore_overload_multipliers, $with_details, $with_geojson);
    }

    /**
     * Operation optimizeRouteWithHttpInfo
     *
     * Optimize a single route.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $details (optional)
     * @param  bool $synchronous (optional)
     * @param  bool $active_only (optional)
     * @param  string $ignore_overload_multipliers (optional)
     * @param  bool $with_details (optional)
     * @param  string $with_geojson (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of null, HTTP status code, HTTP response headers (array of strings)
     */
    public function optimizeRouteWithHttpInfo($planning_id, $id, $details = null, $synchronous = null, $active_only = null, $ignore_overload_multipliers = null, $with_details = null, $with_geojson = null)
    {
        $returnType = '';
        $request = $this->optimizeRouteRequest($planning_id, $id, $details, $synchronous, $active_only, $ignore_overload_multipliers, $with_details, $with_geojson);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            return [null, $statusCode, $response->getHeaders()];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
            }
            throw $e;
        }
    }

    /**
     * Operation optimizeRouteAsync
     *
     * Optimize a single route.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $details (optional)
     * @param  bool $synchronous (optional)
     * @param  bool $active_only (optional)
     * @param  string $ignore_overload_multipliers (optional)
     * @param  bool $with_details (optional)
     * @param  string $with_geojson (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function optimizeRouteAsync($planning_id, $id, $details = null, $synchronous = null, $active_only = null, $ignore_overload_multipliers = null, $with_details = null, $with_geojson = null)
    {
        return $this->optimizeRouteAsyncWithHttpInfo($planning_id, $id, $details, $synchronous, $active_only, $ignore_overload_multipliers, $with_details, $with_geojson)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation optimizeRouteAsyncWithHttpInfo
     *
     * Optimize a single route.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $details (optional)
     * @param  bool $synchronous (optional)
     * @param  bool $active_only (optional)
     * @param  string $ignore_overload_multipliers (optional)
     * @param  bool $with_details (optional)
     * @param  string $with_geojson (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function optimizeRouteAsyncWithHttpInfo($planning_id, $id, $details = null, $synchronous = null, $active_only = null, $ignore_overload_multipliers = null, $with_details = null, $with_geojson = null)
    {
        $returnType = '';
        $request = $this->optimizeRouteRequest($planning_id, $id, $details, $synchronous, $active_only, $ignore_overload_multipliers, $with_details, $with_geojson);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    return [null, $response->getStatusCode(), $response->getHeaders()];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'optimizeRoute'
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $details (optional)
     * @param  bool $synchronous (optional)
     * @param  bool $active_only (optional)
     * @param  string $ignore_overload_multipliers (optional)
     * @param  bool $with_details (optional)
     * @param  string $with_geojson (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function optimizeRouteRequest($planning_id, $id, $details = null, $synchronous = null, $active_only = null, $ignore_overload_multipliers = null, $with_details = null, $with_geojson = null)
    {
        // verify the required parameter 'planning_id' is set
        if ($planning_id === null || (is_array($planning_id) && count($planning_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $planning_id when calling optimizeRoute'
            );
        }
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling optimizeRoute'
            );
        }

        $resourcePath = '/0.1/plannings/{planning_id}/routes/{id}/optimize';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;


        // path params
        if ($planning_id !== null) {
            $resourcePath = str_replace(
                '{' . 'planning_id' . '}',
                ObjectSerializer::toPathValue($planning_id),
                $resourcePath
            );
        }
        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // form params
        if ($details !== null) {
            $formParams['details'] = ObjectSerializer::toFormValue($details);
        }
        // form params
        if ($synchronous !== null) {
            $formParams['synchronous'] = ObjectSerializer::toFormValue($synchronous);
        }
        // form params
        if ($active_only !== null) {
            $formParams['active_only'] = ObjectSerializer::toFormValue($active_only);
        }
        // form params
        if ($ignore_overload_multipliers !== null) {
            $formParams['ignore_overload_multipliers'] = ObjectSerializer::toFormValue($ignore_overload_multipliers);
        }
        // form params
        if ($with_details !== null) {
            $formParams['with_details'] = ObjectSerializer::toFormValue($with_details);
        }
        // form params
        if ($with_geojson !== null) {
            $formParams['with_geojson'] = ObjectSerializer::toFormValue($with_geojson);
        }
        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                []
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                [],
                ['multipart/form-data']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'PATCH',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation optimizeRoutes
     *
     * Optimize routes.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $global Use global optimization and move visits between routes if needed (optional)
     * @param  bool $synchronous Synchronous processing or not. If not, operation will return nothing and the optimum result will be set on planning as soon Customer.job_optimizer_id will be null. (optional, default to true)
     * @param  bool $active_only If &#x60;true&#x60; only active stops are taken into account by optimization, else inactive stops are also taken into account but are not activated in result route. (optional, default to true)
     * @param  string $ignore_overload_multipliers Deliverable Unit id and whether or not it should be ignored : {&#x27;0&#x27;&#x3D;&gt;{&#x27;unit_id&#x27;&#x3D;&gt;&#x27;7&#x27;, &#x27;ignore&#x27;&#x3D;&gt;&#x27;true&#x27;}} (optional)
     * @param  bool $with_details Output route details (optional)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return void
     */
    public function optimizeRoutes($id, $global = null, $synchronous = 'true', $active_only = 'true', $ignore_overload_multipliers = null, $with_details = null, $with_geojson = 'false')
    {
        $this->optimizeRoutesWithHttpInfo($id, $global, $synchronous, $active_only, $ignore_overload_multipliers, $with_details, $with_geojson);
    }

    /**
     * Operation optimizeRoutesWithHttpInfo
     *
     * Optimize routes.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $global Use global optimization and move visits between routes if needed (optional)
     * @param  bool $synchronous Synchronous processing or not. If not, operation will return nothing and the optimum result will be set on planning as soon Customer.job_optimizer_id will be null. (optional, default to true)
     * @param  bool $active_only If &#x60;true&#x60; only active stops are taken into account by optimization, else inactive stops are also taken into account but are not activated in result route. (optional, default to true)
     * @param  string $ignore_overload_multipliers Deliverable Unit id and whether or not it should be ignored : {&#x27;0&#x27;&#x3D;&gt;{&#x27;unit_id&#x27;&#x3D;&gt;&#x27;7&#x27;, &#x27;ignore&#x27;&#x3D;&gt;&#x27;true&#x27;}} (optional)
     * @param  bool $with_details Output route details (optional)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of null, HTTP status code, HTTP response headers (array of strings)
     */
    public function optimizeRoutesWithHttpInfo($id, $global = null, $synchronous = 'true', $active_only = 'true', $ignore_overload_multipliers = null, $with_details = null, $with_geojson = 'false')
    {
        $returnType = '';
        $request = $this->optimizeRoutesRequest($id, $global, $synchronous, $active_only, $ignore_overload_multipliers, $with_details, $with_geojson);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            return [null, $statusCode, $response->getHeaders()];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
            }
            throw $e;
        }
    }

    /**
     * Operation optimizeRoutesAsync
     *
     * Optimize routes.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $global Use global optimization and move visits between routes if needed (optional)
     * @param  bool $synchronous Synchronous processing or not. If not, operation will return nothing and the optimum result will be set on planning as soon Customer.job_optimizer_id will be null. (optional, default to true)
     * @param  bool $active_only If &#x60;true&#x60; only active stops are taken into account by optimization, else inactive stops are also taken into account but are not activated in result route. (optional, default to true)
     * @param  string $ignore_overload_multipliers Deliverable Unit id and whether or not it should be ignored : {&#x27;0&#x27;&#x3D;&gt;{&#x27;unit_id&#x27;&#x3D;&gt;&#x27;7&#x27;, &#x27;ignore&#x27;&#x3D;&gt;&#x27;true&#x27;}} (optional)
     * @param  bool $with_details Output route details (optional)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function optimizeRoutesAsync($id, $global = null, $synchronous = 'true', $active_only = 'true', $ignore_overload_multipliers = null, $with_details = null, $with_geojson = 'false')
    {
        return $this->optimizeRoutesAsyncWithHttpInfo($id, $global, $synchronous, $active_only, $ignore_overload_multipliers, $with_details, $with_geojson)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation optimizeRoutesAsyncWithHttpInfo
     *
     * Optimize routes.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $global Use global optimization and move visits between routes if needed (optional)
     * @param  bool $synchronous Synchronous processing or not. If not, operation will return nothing and the optimum result will be set on planning as soon Customer.job_optimizer_id will be null. (optional, default to true)
     * @param  bool $active_only If &#x60;true&#x60; only active stops are taken into account by optimization, else inactive stops are also taken into account but are not activated in result route. (optional, default to true)
     * @param  string $ignore_overload_multipliers Deliverable Unit id and whether or not it should be ignored : {&#x27;0&#x27;&#x3D;&gt;{&#x27;unit_id&#x27;&#x3D;&gt;&#x27;7&#x27;, &#x27;ignore&#x27;&#x3D;&gt;&#x27;true&#x27;}} (optional)
     * @param  bool $with_details Output route details (optional)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function optimizeRoutesAsyncWithHttpInfo($id, $global = null, $synchronous = 'true', $active_only = 'true', $ignore_overload_multipliers = null, $with_details = null, $with_geojson = 'false')
    {
        $returnType = '';
        $request = $this->optimizeRoutesRequest($id, $global, $synchronous, $active_only, $ignore_overload_multipliers, $with_details, $with_geojson);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    return [null, $response->getStatusCode(), $response->getHeaders()];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'optimizeRoutes'
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $global Use global optimization and move visits between routes if needed (optional)
     * @param  bool $synchronous Synchronous processing or not. If not, operation will return nothing and the optimum result will be set on planning as soon Customer.job_optimizer_id will be null. (optional, default to true)
     * @param  bool $active_only If &#x60;true&#x60; only active stops are taken into account by optimization, else inactive stops are also taken into account but are not activated in result route. (optional, default to true)
     * @param  string $ignore_overload_multipliers Deliverable Unit id and whether or not it should be ignored : {&#x27;0&#x27;&#x3D;&gt;{&#x27;unit_id&#x27;&#x3D;&gt;&#x27;7&#x27;, &#x27;ignore&#x27;&#x3D;&gt;&#x27;true&#x27;}} (optional)
     * @param  bool $with_details Output route details (optional)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function optimizeRoutesRequest($id, $global = null, $synchronous = 'true', $active_only = 'true', $ignore_overload_multipliers = null, $with_details = null, $with_geojson = 'false')
    {
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling optimizeRoutes'
            );
        }

        $resourcePath = '/0.1/plannings/{id}/optimize';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // query params
        if ($global !== null) {
            $queryParams['global'] = ObjectSerializer::toQueryValue($global);
        }
        // query params
        if ($synchronous !== null) {
            $queryParams['synchronous'] = ObjectSerializer::toQueryValue($synchronous);
        }
        // query params
        if ($active_only !== null) {
            $queryParams['active_only'] = ObjectSerializer::toQueryValue($active_only);
        }
        // query params
        if ($ignore_overload_multipliers !== null) {
            $queryParams['ignore_overload_multipliers'] = ObjectSerializer::toQueryValue($ignore_overload_multipliers);
        }
        // query params
        if ($with_details !== null) {
            $queryParams['with_details'] = ObjectSerializer::toQueryValue($with_details);
        }
        // query params
        if ($with_geojson !== null) {
            $queryParams['with_geojson'] = ObjectSerializer::toQueryValue($with_geojson);
        }

        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                []
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                [],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation refreshPlanning
     *
     * Recompute the planning after parameter update.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \Mapotempo\Model\V01Planning
     */
    public function refreshPlanning($id, $with_geojson = 'false')
    {
        list($response) = $this->refreshPlanningWithHttpInfo($id, $with_geojson);
        return $response;
    }

    /**
     * Operation refreshPlanningWithHttpInfo
     *
     * Recompute the planning after parameter update.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \Mapotempo\Model\V01Planning, HTTP status code, HTTP response headers (array of strings)
     */
    public function refreshPlanningWithHttpInfo($id, $with_geojson = 'false')
    {
        $returnType = '\Mapotempo\Model\V01Planning';
        $request = $this->refreshPlanningRequest($id, $with_geojson);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string','integer','bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Planning',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status400',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 401:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status401',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 402:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status402',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status403',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 404:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status404',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 405:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status405',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 409:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status409',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 500:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status500',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation refreshPlanningAsync
     *
     * Recompute the planning after parameter update.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function refreshPlanningAsync($id, $with_geojson = 'false')
    {
        return $this->refreshPlanningAsyncWithHttpInfo($id, $with_geojson)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation refreshPlanningAsyncWithHttpInfo
     *
     * Recompute the planning after parameter update.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function refreshPlanningAsyncWithHttpInfo($id, $with_geojson = 'false')
    {
        $returnType = '\Mapotempo\Model\V01Planning';
        $request = $this->refreshPlanningRequest($id, $with_geojson);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'refreshPlanning'
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $with_geojson Fill in response the geojson field with route geometry: &#x60;point&#x60; to return only points, &#x60;polyline&#x60; to return with encoded linestring, &#x60;true&#x60; to return both. (optional, default to false)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function refreshPlanningRequest($id, $with_geojson = 'false')
    {
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling refreshPlanning'
            );
        }

        $resourcePath = '/0.1/plannings/{id}/refresh';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // query params
        if ($with_geojson !== null) {
            $queryParams['with_geojson'] = ObjectSerializer::toQueryValue($with_geojson);
        }

        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml'],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation reverseStopsOrder
     *
     * Reverse stops order.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \Mapotempo\Model\V01Route
     */
    public function reverseStopsOrder($planning_id, $id)
    {
        list($response) = $this->reverseStopsOrderWithHttpInfo($planning_id, $id);
        return $response;
    }

    /**
     * Operation reverseStopsOrderWithHttpInfo
     *
     * Reverse stops order.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \Mapotempo\Model\V01Route, HTTP status code, HTTP response headers (array of strings)
     */
    public function reverseStopsOrderWithHttpInfo($planning_id, $id)
    {
        $returnType = '\Mapotempo\Model\V01Route';
        $request = $this->reverseStopsOrderRequest($planning_id, $id);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string','integer','bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Route',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status400',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 401:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status401',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 402:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status402',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status403',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 404:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status404',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 405:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status405',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 409:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status409',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 500:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status500',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation reverseStopsOrderAsync
     *
     * Reverse stops order.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function reverseStopsOrderAsync($planning_id, $id)
    {
        return $this->reverseStopsOrderAsyncWithHttpInfo($planning_id, $id)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation reverseStopsOrderAsyncWithHttpInfo
     *
     * Reverse stops order.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function reverseStopsOrderAsyncWithHttpInfo($planning_id, $id)
    {
        $returnType = '\Mapotempo\Model\V01Route';
        $request = $this->reverseStopsOrderRequest($planning_id, $id);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'reverseStopsOrder'
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function reverseStopsOrderRequest($planning_id, $id)
    {
        // verify the required parameter 'planning_id' is set
        if ($planning_id === null || (is_array($planning_id) && count($planning_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $planning_id when calling reverseStopsOrder'
            );
        }
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling reverseStopsOrder'
            );
        }

        $resourcePath = '/0.1/plannings/{planning_id}/routes/{id}/reverse_order';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;


        // path params
        if ($planning_id !== null) {
            $resourcePath = str_replace(
                '{' . 'planning_id' . '}',
                ObjectSerializer::toPathValue($planning_id),
                $resourcePath
            );
        }
        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml'],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'PATCH',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation sendSMS
     *
     * Send SMS for each stop visit.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return void
     */
    public function sendSMS($id)
    {
        $this->sendSMSWithHttpInfo($id);
    }

    /**
     * Operation sendSMSWithHttpInfo
     *
     * Send SMS for each stop visit.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of null, HTTP status code, HTTP response headers (array of strings)
     */
    public function sendSMSWithHttpInfo($id)
    {
        $returnType = '';
        $request = $this->sendSMSRequest($id);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            return [null, $statusCode, $response->getHeaders()];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status400',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 401:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status401',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 402:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status402',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status403',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 404:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status404',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 405:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status405',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 409:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status409',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 500:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status500',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation sendSMSAsync
     *
     * Send SMS for each stop visit.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function sendSMSAsync($id)
    {
        return $this->sendSMSAsyncWithHttpInfo($id)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation sendSMSAsyncWithHttpInfo
     *
     * Send SMS for each stop visit.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function sendSMSAsyncWithHttpInfo($id)
    {
        $returnType = '';
        $request = $this->sendSMSRequest($id);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    return [null, $response->getStatusCode(), $response->getHeaders()];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'sendSMS'
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function sendSMSRequest($id)
    {
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling sendSMS'
            );
        }

        $resourcePath = '/0.1/plannings/{id}/send_sms';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;


        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml'],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation sendSMS_0
     *
     * Send SMS for each stop visit.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return void
     */
    public function sendSMS_0($planning_id, $id)
    {
        $this->sendSMS_0WithHttpInfo($planning_id, $id);
    }

    /**
     * Operation sendSMS_0WithHttpInfo
     *
     * Send SMS for each stop visit.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of null, HTTP status code, HTTP response headers (array of strings)
     */
    public function sendSMS_0WithHttpInfo($planning_id, $id)
    {
        $returnType = '';
        $request = $this->sendSMS_0Request($planning_id, $id);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            return [null, $statusCode, $response->getHeaders()];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status400',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 401:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status401',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 402:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status402',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status403',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 404:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status404',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 405:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status405',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 409:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status409',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 500:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status500',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation sendSMS_0Async
     *
     * Send SMS for each stop visit.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function sendSMS_0Async($planning_id, $id)
    {
        return $this->sendSMS_0AsyncWithHttpInfo($planning_id, $id)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation sendSMS_0AsyncWithHttpInfo
     *
     * Send SMS for each stop visit.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function sendSMS_0AsyncWithHttpInfo($planning_id, $id)
    {
        $returnType = '';
        $request = $this->sendSMS_0Request($planning_id, $id);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    return [null, $response->getStatusCode(), $response->getHeaders()];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'sendSMS_0'
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function sendSMS_0Request($planning_id, $id)
    {
        // verify the required parameter 'planning_id' is set
        if ($planning_id === null || (is_array($planning_id) && count($planning_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $planning_id when calling sendSMS_0'
            );
        }
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling sendSMS_0'
            );
        }

        $resourcePath = '/0.1/plannings/{planning_id}/routes/{id}/send_sms';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;


        // path params
        if ($planning_id !== null) {
            $resourcePath = str_replace(
                '{' . 'planning_id' . '}',
                ObjectSerializer::toPathValue($planning_id),
                $resourcePath
            );
        }
        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml'],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation switchVehicles
     *
     * Switch two vehicles.
     *
     * @param  int $route_id route_id (required)
     * @param  int $vehicle_usage_id vehicle_usage_id (required)
     * @param  bool $with_details with_details (required)
     * @param  string $with_geojson with_geojson (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return void
     */
    public function switchVehicles($route_id, $vehicle_usage_id, $with_details, $with_geojson, $id)
    {
        $this->switchVehiclesWithHttpInfo($route_id, $vehicle_usage_id, $with_details, $with_geojson, $id);
    }

    /**
     * Operation switchVehiclesWithHttpInfo
     *
     * Switch two vehicles.
     *
     * @param  int $route_id (required)
     * @param  int $vehicle_usage_id (required)
     * @param  bool $with_details (required)
     * @param  string $with_geojson (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of null, HTTP status code, HTTP response headers (array of strings)
     */
    public function switchVehiclesWithHttpInfo($route_id, $vehicle_usage_id, $with_details, $with_geojson, $id)
    {
        $returnType = '';
        $request = $this->switchVehiclesRequest($route_id, $vehicle_usage_id, $with_details, $with_geojson, $id);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            return [null, $statusCode, $response->getHeaders()];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
            }
            throw $e;
        }
    }

    /**
     * Operation switchVehiclesAsync
     *
     * Switch two vehicles.
     *
     * @param  int $route_id (required)
     * @param  int $vehicle_usage_id (required)
     * @param  bool $with_details (required)
     * @param  string $with_geojson (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function switchVehiclesAsync($route_id, $vehicle_usage_id, $with_details, $with_geojson, $id)
    {
        return $this->switchVehiclesAsyncWithHttpInfo($route_id, $vehicle_usage_id, $with_details, $with_geojson, $id)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation switchVehiclesAsyncWithHttpInfo
     *
     * Switch two vehicles.
     *
     * @param  int $route_id (required)
     * @param  int $vehicle_usage_id (required)
     * @param  bool $with_details (required)
     * @param  string $with_geojson (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function switchVehiclesAsyncWithHttpInfo($route_id, $vehicle_usage_id, $with_details, $with_geojson, $id)
    {
        $returnType = '';
        $request = $this->switchVehiclesRequest($route_id, $vehicle_usage_id, $with_details, $with_geojson, $id);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    return [null, $response->getStatusCode(), $response->getHeaders()];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'switchVehicles'
     *
     * @param  int $route_id (required)
     * @param  int $vehicle_usage_id (required)
     * @param  bool $with_details (required)
     * @param  string $with_geojson (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function switchVehiclesRequest($route_id, $vehicle_usage_id, $with_details, $with_geojson, $id)
    {
        // verify the required parameter 'route_id' is set
        if ($route_id === null || (is_array($route_id) && count($route_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $route_id when calling switchVehicles'
            );
        }
        // verify the required parameter 'vehicle_usage_id' is set
        if ($vehicle_usage_id === null || (is_array($vehicle_usage_id) && count($vehicle_usage_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $vehicle_usage_id when calling switchVehicles'
            );
        }
        // verify the required parameter 'with_details' is set
        if ($with_details === null || (is_array($with_details) && count($with_details) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $with_details when calling switchVehicles'
            );
        }
        // verify the required parameter 'with_geojson' is set
        if ($with_geojson === null || (is_array($with_geojson) && count($with_geojson) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $with_geojson when calling switchVehicles'
            );
        }
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling switchVehicles'
            );
        }

        $resourcePath = '/0.1/plannings/{id}/switch';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;


        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // form params
        if ($route_id !== null) {
            $formParams['route_id'] = ObjectSerializer::toFormValue($route_id);
        }
        // form params
        if ($vehicle_usage_id !== null) {
            $formParams['vehicle_usage_id'] = ObjectSerializer::toFormValue($vehicle_usage_id);
        }
        // form params
        if ($with_details !== null) {
            $formParams['with_details'] = ObjectSerializer::toFormValue($with_details);
        }
        // form params
        if ($with_geojson !== null) {
            $formParams['with_geojson'] = ObjectSerializer::toFormValue($with_geojson);
        }
        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                []
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                [],
                ['multipart/form-data']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'PATCH',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation updatePlanning
     *
     * Update planning.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  \Mapotempo\Model\Body16 $body body (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \Mapotempo\Model\V01Planning
     */
    public function updatePlanning($id, $body = null)
    {
        list($response) = $this->updatePlanningWithHttpInfo($id, $body);
        return $response;
    }

    /**
     * Operation updatePlanningWithHttpInfo
     *
     * Update planning.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  \Mapotempo\Model\Body16 $body (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \Mapotempo\Model\V01Planning, HTTP status code, HTTP response headers (array of strings)
     */
    public function updatePlanningWithHttpInfo($id, $body = null)
    {
        $returnType = '\Mapotempo\Model\V01Planning';
        $request = $this->updatePlanningRequest($id, $body);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string','integer','bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Planning',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status400',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 401:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status401',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 402:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status402',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status403',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 404:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status404',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 405:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status405',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 409:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status409',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 500:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status500',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation updatePlanningAsync
     *
     * Update planning.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  \Mapotempo\Model\Body16 $body (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function updatePlanningAsync($id, $body = null)
    {
        return $this->updatePlanningAsyncWithHttpInfo($id, $body)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation updatePlanningAsyncWithHttpInfo
     *
     * Update planning.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  \Mapotempo\Model\Body16 $body (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function updatePlanningAsyncWithHttpInfo($id, $body = null)
    {
        $returnType = '\Mapotempo\Model\V01Planning';
        $request = $this->updatePlanningRequest($id, $body);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'updatePlanning'
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  \Mapotempo\Model\Body16 $body (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function updatePlanningRequest($id, $body = null)
    {
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling updatePlanning'
            );
        }

        $resourcePath = '/0.1/plannings/{id}';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;


        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml'],
                ['application/json']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'PUT',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation updateRoute
     *
     * Update route attributes.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  \Mapotempo\Model\Body23 $body body (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \Mapotempo\Model\V01RouteProperties
     */
    public function updateRoute($planning_id, $id, $body = null)
    {
        list($response) = $this->updateRouteWithHttpInfo($planning_id, $id, $body);
        return $response;
    }

    /**
     * Operation updateRouteWithHttpInfo
     *
     * Update route attributes.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  \Mapotempo\Model\Body23 $body (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \Mapotempo\Model\V01RouteProperties, HTTP status code, HTTP response headers (array of strings)
     */
    public function updateRouteWithHttpInfo($planning_id, $id, $body = null)
    {
        $returnType = '\Mapotempo\Model\V01RouteProperties';
        $request = $this->updateRouteRequest($planning_id, $id, $body);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string','integer','bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01RouteProperties',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status400',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 401:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status401',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 402:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status402',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status403',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 404:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status404',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 405:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status405',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 409:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status409',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 500:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status500',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation updateRouteAsync
     *
     * Update route attributes.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  \Mapotempo\Model\Body23 $body (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function updateRouteAsync($planning_id, $id, $body = null)
    {
        return $this->updateRouteAsyncWithHttpInfo($planning_id, $id, $body)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation updateRouteAsyncWithHttpInfo
     *
     * Update route attributes.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  \Mapotempo\Model\Body23 $body (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function updateRouteAsyncWithHttpInfo($planning_id, $id, $body = null)
    {
        $returnType = '\Mapotempo\Model\V01RouteProperties';
        $request = $this->updateRouteRequest($planning_id, $id, $body);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'updateRoute'
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  \Mapotempo\Model\Body23 $body (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function updateRouteRequest($planning_id, $id, $body = null)
    {
        // verify the required parameter 'planning_id' is set
        if ($planning_id === null || (is_array($planning_id) && count($planning_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $planning_id when calling updateRoute'
            );
        }
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling updateRoute'
            );
        }

        $resourcePath = '/0.1/plannings/{planning_id}/routes/{id}';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;


        // path params
        if ($planning_id !== null) {
            $resourcePath = str_replace(
                '{' . 'planning_id' . '}',
                ObjectSerializer::toPathValue($planning_id),
                $resourcePath
            );
        }
        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml'],
                ['application/json']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'PUT',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation updateRoutes
     *
     * Update routes visibility and lock. (DEPRECATED. For more information, please use Update Planning operation instead)
     *
     * @param  string $action action (required)
     * @param  string $selection selection (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return void
     */
    public function updateRoutes($action, $selection, $id)
    {
        $this->updateRoutesWithHttpInfo($action, $selection, $id);
    }

    /**
     * Operation updateRoutesWithHttpInfo
     *
     * Update routes visibility and lock. (DEPRECATED. For more information, please use Update Planning operation instead)
     *
     * @param  string $action (required)
     * @param  string $selection (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of null, HTTP status code, HTTP response headers (array of strings)
     */
    public function updateRoutesWithHttpInfo($action, $selection, $id)
    {
        $returnType = '';
        $request = $this->updateRoutesRequest($action, $selection, $id);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            return [null, $statusCode, $response->getHeaders()];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
            }
            throw $e;
        }
    }

    /**
     * Operation updateRoutesAsync
     *
     * Update routes visibility and lock. (DEPRECATED. For more information, please use Update Planning operation instead)
     *
     * @param  string $action (required)
     * @param  string $selection (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function updateRoutesAsync($action, $selection, $id)
    {
        return $this->updateRoutesAsyncWithHttpInfo($action, $selection, $id)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation updateRoutesAsyncWithHttpInfo
     *
     * Update routes visibility and lock. (DEPRECATED. For more information, please use Update Planning operation instead)
     *
     * @param  string $action (required)
     * @param  string $selection (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function updateRoutesAsyncWithHttpInfo($action, $selection, $id)
    {
        $returnType = '';
        $request = $this->updateRoutesRequest($action, $selection, $id);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    return [null, $response->getStatusCode(), $response->getHeaders()];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'updateRoutes'
     *
     * @param  string $action (required)
     * @param  string $selection (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function updateRoutesRequest($action, $selection, $id)
    {
        // verify the required parameter 'action' is set
        if ($action === null || (is_array($action) && count($action) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $action when calling updateRoutes'
            );
        }
        // verify the required parameter 'selection' is set
        if ($selection === null || (is_array($selection) && count($selection) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $selection when calling updateRoutes'
            );
        }
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling updateRoutes'
            );
        }

        $resourcePath = '/0.1/plannings/{id}/update_routes';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;


        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // form params
        if ($action !== null) {
            $formParams['action'] = ObjectSerializer::toFormValue($action);
        }
        // form params
        if ($selection !== null) {
            $formParams['selection'] = ObjectSerializer::toFormValue($selection);
        }
        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                []
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                [],
                ['multipart/form-data']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'PATCH',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation updateStop
     *
     * Update stop.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $route_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  int $id Stop id to update (required)
     * @param  \Mapotempo\Model\Body27 $body body (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return void
     */
    public function updateStop($planning_id, $route_id, $id, $body = null)
    {
        $this->updateStopWithHttpInfo($planning_id, $route_id, $id, $body);
    }

    /**
     * Operation updateStopWithHttpInfo
     *
     * Update stop.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $route_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  int $id Stop id to update (required)
     * @param  \Mapotempo\Model\Body27 $body (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of null, HTTP status code, HTTP response headers (array of strings)
     */
    public function updateStopWithHttpInfo($planning_id, $route_id, $id, $body = null)
    {
        $returnType = '';
        $request = $this->updateStopRequest($planning_id, $route_id, $id, $body);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            return [null, $statusCode, $response->getHeaders()];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
            }
            throw $e;
        }
    }

    /**
     * Operation updateStopAsync
     *
     * Update stop.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $route_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  int $id Stop id to update (required)
     * @param  \Mapotempo\Model\Body27 $body (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function updateStopAsync($planning_id, $route_id, $id, $body = null)
    {
        return $this->updateStopAsyncWithHttpInfo($planning_id, $route_id, $id, $body)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation updateStopAsyncWithHttpInfo
     *
     * Update stop.
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $route_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  int $id Stop id to update (required)
     * @param  \Mapotempo\Model\Body27 $body (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function updateStopAsyncWithHttpInfo($planning_id, $route_id, $id, $body = null)
    {
        $returnType = '';
        $request = $this->updateStopRequest($planning_id, $route_id, $id, $body);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    return [null, $response->getStatusCode(), $response->getHeaders()];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'updateStop'
     *
     * @param  string $planning_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  string $route_id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  int $id Stop id to update (required)
     * @param  \Mapotempo\Model\Body27 $body (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function updateStopRequest($planning_id, $route_id, $id, $body = null)
    {
        // verify the required parameter 'planning_id' is set
        if ($planning_id === null || (is_array($planning_id) && count($planning_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $planning_id when calling updateStop'
            );
        }
        // verify the required parameter 'route_id' is set
        if ($route_id === null || (is_array($route_id) && count($route_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $route_id when calling updateStop'
            );
        }
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling updateStop'
            );
        }

        $resourcePath = '/0.1/plannings/{planning_id}/routes/{route_id}/stops/{id}';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;


        // path params
        if ($planning_id !== null) {
            $resourcePath = str_replace(
                '{' . 'planning_id' . '}',
                ObjectSerializer::toPathValue($planning_id),
                $resourcePath
            );
        }
        // path params
        if ($route_id !== null) {
            $resourcePath = str_replace(
                '{' . 'route_id' . '}',
                ObjectSerializer::toPathValue($route_id),
                $resourcePath
            );
        }
        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                []
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                [],
                ['application/json']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'PUT',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation updateStopsStatus
     *
     * Update stops live status.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $with_details with_details (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return void
     */
    public function updateStopsStatus($id, $with_details = null)
    {
        $this->updateStopsStatusWithHttpInfo($id, $with_details);
    }

    /**
     * Operation updateStopsStatusWithHttpInfo
     *
     * Update stops live status.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $with_details (optional)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of null, HTTP status code, HTTP response headers (array of strings)
     */
    public function updateStopsStatusWithHttpInfo($id, $with_details = null)
    {
        $returnType = '';
        $request = $this->updateStopsStatusRequest($id, $with_details);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            return [null, $statusCode, $response->getHeaders()];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
            }
            throw $e;
        }
    }

    /**
     * Operation updateStopsStatusAsync
     *
     * Update stops live status.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $with_details (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function updateStopsStatusAsync($id, $with_details = null)
    {
        return $this->updateStopsStatusAsyncWithHttpInfo($id, $with_details)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation updateStopsStatusAsyncWithHttpInfo
     *
     * Update stops live status.
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $with_details (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function updateStopsStatusAsyncWithHttpInfo($id, $with_details = null)
    {
        $returnType = '';
        $request = $this->updateStopsStatusRequest($id, $with_details);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    return [null, $response->getStatusCode(), $response->getHeaders()];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'updateStopsStatus'
     *
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     * @param  bool $with_details (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function updateStopsStatusRequest($id, $with_details = null)
    {
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling updateStopsStatus'
            );
        }

        $resourcePath = '/0.1/plannings/{id}/update_stops_status';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;


        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // form params
        if ($with_details !== null) {
            $formParams['with_details'] = ObjectSerializer::toFormValue($with_details);
        }
        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                []
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                [],
                ['multipart/form-data']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'PATCH',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation useOrderArray
     *
     * Use order_array in the planning.
     *
     * @param  int $order_array_id order_array_id (required)
     * @param  int $shift shift (required)
     * @param  string $with_geojson with_geojson (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \Mapotempo\Model\V01Planning
     */
    public function useOrderArray($order_array_id, $shift, $with_geojson, $id)
    {
        list($response) = $this->useOrderArrayWithHttpInfo($order_array_id, $shift, $with_geojson, $id);
        return $response;
    }

    /**
     * Operation useOrderArrayWithHttpInfo
     *
     * Use order_array in the planning.
     *
     * @param  int $order_array_id (required)
     * @param  int $shift (required)
     * @param  string $with_geojson (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \Mapotempo\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \Mapotempo\Model\V01Planning, HTTP status code, HTTP response headers (array of strings)
     */
    public function useOrderArrayWithHttpInfo($order_array_id, $shift, $with_geojson, $id)
    {
        $returnType = '\Mapotempo\Model\V01Planning';
        $request = $this->useOrderArrayRequest($order_array_id, $shift, $with_geojson, $id);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string','integer','bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Planning',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status400',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 401:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status401',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 402:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status402',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status403',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 404:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status404',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 405:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status405',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 409:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status409',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 500:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Mapotempo\Model\V01Status500',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation useOrderArrayAsync
     *
     * Use order_array in the planning.
     *
     * @param  int $order_array_id (required)
     * @param  int $shift (required)
     * @param  string $with_geojson (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function useOrderArrayAsync($order_array_id, $shift, $with_geojson, $id)
    {
        return $this->useOrderArrayAsyncWithHttpInfo($order_array_id, $shift, $with_geojson, $id)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation useOrderArrayAsyncWithHttpInfo
     *
     * Use order_array in the planning.
     *
     * @param  int $order_array_id (required)
     * @param  int $shift (required)
     * @param  string $with_geojson (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function useOrderArrayAsyncWithHttpInfo($order_array_id, $shift, $with_geojson, $id)
    {
        $returnType = '\Mapotempo\Model\V01Planning';
        $request = $this->useOrderArrayRequest($order_array_id, $shift, $with_geojson, $id);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'useOrderArray'
     *
     * @param  int $order_array_id (required)
     * @param  int $shift (required)
     * @param  string $with_geojson (required)
     * @param  string $id Id or the ref field value, then use &#x60;ref:[value]&#x60;. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function useOrderArrayRequest($order_array_id, $shift, $with_geojson, $id)
    {
        // verify the required parameter 'order_array_id' is set
        if ($order_array_id === null || (is_array($order_array_id) && count($order_array_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $order_array_id when calling useOrderArray'
            );
        }
        // verify the required parameter 'shift' is set
        if ($shift === null || (is_array($shift) && count($shift) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $shift when calling useOrderArray'
            );
        }
        // verify the required parameter 'with_geojson' is set
        if ($with_geojson === null || (is_array($with_geojson) && count($with_geojson) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $with_geojson when calling useOrderArray'
            );
        }
        // verify the required parameter 'id' is set
        if ($id === null || (is_array($id) && count($id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $id when calling useOrderArray'
            );
        }

        $resourcePath = '/0.1/plannings/{id}/order_array';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;


        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                '{' . 'id' . '}',
                ObjectSerializer::toPathValue($id),
                $resourcePath
            );
        }

        // form params
        if ($order_array_id !== null) {
            $formParams['order_array_id'] = ObjectSerializer::toFormValue($order_array_id);
        }
        // form params
        if ($shift !== null) {
            $formParams['shift'] = ObjectSerializer::toFormValue($shift);
        }
        // form params
        if ($with_geojson !== null) {
            $formParams['with_geojson'] = ObjectSerializer::toFormValue($with_geojson);
        }
        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json; charset=UTF-8', 'application/vnd.geo+json; charset=UTF-8', 'application/xml'],
                ['multipart/form-data']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'PATCH',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Create http client option
     *
     * @throws \RuntimeException on file opening failure
     * @return array of http client options
     */
    protected function createHttpClientOption()
    {
        $options = [];
        if ($this->config->getDebug()) {
            $options[RequestOptions::DEBUG] = fopen($this->config->getDebugFile(), 'a');
            if (!$options[RequestOptions::DEBUG]) {
                throw new \RuntimeException('Failed to open the debug file: ' . $this->config->getDebugFile());
            }
        }

        return $options;
    }
}
