<?php
/**
 * Body26
 *
 * PHP version 5
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * API
 *
 * [Simplified view of domain model](https://app.mapotempo.com//api/0.1/Model-simpel.svg). ## Model Model is structured around four majors concepts: the Customer account, Destinations, Vehicles and Plannings. * `Customers`: many of objects are linked to a customer account (relating to the user calling API). The customer has many `Users`, each user has his own `api_key`. Be carefull not to confuse with following model `Destination`, `Customer`can only be created by a admin `User`. * `Destinations`: location points to visit with constraints. The same `Destination` can be visited several times : in this case several `Visits` are associated to one `Destination`. * `Vehicles`: vehicles definition are splited in two parts:  * the structural definition named `Vehicle` (car, truck, bike, consumption, etc.)  * and the vehicle usage `VehicleUsage`, a specific usage of a physical vehicle in a specific context. Vehicles can be used in many contexts called `VehicleUsageSet` (set of all vehicles usages under a context). Multiple values are only available if dedicated option for customer is active. For instance, if customer needs to use its vehicle 2 times per day (morning and evening), he needs 2 `VehicleUsageSet` called \"Morning\" and \"Evening\" : each can have different values defined for stores, rest, etc... `VehicleUsageSet` defines default values for vehicle usage. * `Plannings`: `Planning` is a set of `Routes` to `Visit` `Destinations` with `Vehicle` within a `VehicleUsageSet` context. A route is a track between all destinations reached by a vehicle (a new route is created for each customer's vehicle and a route without vehicle is created for all out-of-route destinations). By default all customer's visites are used in a planning.  ## Technical access ### Swagger descriptor This REST API is described with Swagger. The Swagger descriptor defines the request end-points, the parameters and the return values. The API can be addressed by HTTP request or with a generated client using the Swagger descriptor. ### API key All access to the API are subject to an `api_key` parameter in order to authenticate the user. This parameter can be sent with a query string on each available operation: `https://app.mapotempo.com/api/0.1/{objects}?api_key={your_personal_api_key}` ### Return The API supports several return formats: `json` and `xml` which depend of the requested extension used in url. ### I18n Functionnal textual returns are subject to translation and depend of HTTP header `Accept-Language`. HTTP error codes are not translated. ## Admin acces Using an admin `api_key` switches to advanced opperations (on `Customer`, `User`, `Vehicle`, `Profile`). Most of operations from the current api are usable either for normal user `api_key` or admin user `api_key` (not both). ## More concepts When a customer is created some objects are created by default with this new customer: * `Vehicle`: multiple, depending of the `max_vehicles` defined for customer * `DeliverableUnit`: one default * `VehicleUsageSet`: one default * `VehicleUsage`: multiple, depending of the vehicles number * `Store`: one default  ### Profiles, Layers, Routers `Profile` is a concept which allows to set several parameters for the customer: * `Layer`: which allows to choose the background map * `Router`: which allows to build route's information.  Several default profiles are available and can be listed with an admin `api_key`.  ### Tags `Tag` is a concept to filter visits and create planning only for a subset of visits. For instance, if some visits are tagged \"Monday\", it allows to create a new planning for \"Monday\" tag and use only dedicated visits. ### Zonings `Zoning` is a concept which allows to define multiple `Zones` (areas) around destinatons. A `Zone` can be affected to a `Vehicle` and if it is used into a `Planning`, all `Destinations` inside areas will be affected to the zone's vehicle (or `Route`). A polygon defining a `Zone` can be created outside the application or can be automatically generated from a planning.  ## Code samples * Create and display destinations or visits. Here some samples for these operations: [using PHP](https://app.mapotempo.com//api/0.1/examples/php/example.php), [using Ruby](https://app.mapotempo.com//api/0.1/examples/ruby/example.rb). Note you can import destinations/visits and create a planning at the same time if you know beforehand the route for each destination/visit. See the details of importDestinations operation to import your data and create a planning in only one call. * Same operations are available for stores (note you have an existing default store). * With created destinations/visits, you can create a planning (routes and stops are automatically created depending of yours vehicles and destinations/visits) * In existing planning, you have availability to move stops (which represent visits) on a dedicated route (which represent a dedicated vehicle). * With many unaffected (out-of-route) stops in a planning, you may create a zoning to move many stops in several routes. Create a zoning (you can generate zones in this zoning automatically from automatic clustering), if you apply zoning (containing zones linked to a vehicle) on your planning, all stops contained in different zones will be moved in dedicated routes.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: tech@mapotempo.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.20
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Mapotempo\Model;

use \ArrayAccess;
use \Mapotempo\ObjectSerializer;

/**
 * Body26 Class Doc Comment
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class Body26 implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'body_26';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'details' => 'bool',
'synchronous' => 'bool',
'active_only' => 'bool',
'ignore_overload_multipliers' => 'string',
'with_details' => 'bool',
'with_geojson' => 'string'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'details' => null,
'synchronous' => null,
'active_only' => null,
'ignore_overload_multipliers' => null,
'with_details' => null,
'with_geojson' => null    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'details' => 'details',
'synchronous' => 'synchronous',
'active_only' => 'active_only',
'ignore_overload_multipliers' => 'ignore_overload_multipliers',
'with_details' => 'with_details',
'with_geojson' => 'with_geojson'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'details' => 'setDetails',
'synchronous' => 'setSynchronous',
'active_only' => 'setActiveOnly',
'ignore_overload_multipliers' => 'setIgnoreOverloadMultipliers',
'with_details' => 'setWithDetails',
'with_geojson' => 'setWithGeojson'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'details' => 'getDetails',
'synchronous' => 'getSynchronous',
'active_only' => 'getActiveOnly',
'ignore_overload_multipliers' => 'getIgnoreOverloadMultipliers',
'with_details' => 'getWithDetails',
'with_geojson' => 'getWithGeojson'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    const WITH_GEOJSON_TRUE = 'true';
const WITH_GEOJSON_FALSE = 'false';
const WITH_GEOJSON_POINT = 'point';
const WITH_GEOJSON_POLYLINE = 'polyline';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getWithGeojsonAllowableValues()
    {
        return [
            self::WITH_GEOJSON_TRUE,
self::WITH_GEOJSON_FALSE,
self::WITH_GEOJSON_POINT,
self::WITH_GEOJSON_POLYLINE,        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['details'] = isset($data['details']) ? $data['details'] : null;
        $this->container['synchronous'] = isset($data['synchronous']) ? $data['synchronous'] : true;
        $this->container['active_only'] = isset($data['active_only']) ? $data['active_only'] : true;
        $this->container['ignore_overload_multipliers'] = isset($data['ignore_overload_multipliers']) ? $data['ignore_overload_multipliers'] : null;
        $this->container['with_details'] = isset($data['with_details']) ? $data['with_details'] : null;
        $this->container['with_geojson'] = isset($data['with_geojson']) ? $data['with_geojson'] : 'false';
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        $allowedValues = $this->getWithGeojsonAllowableValues();
        if (!is_null($this->container['with_geojson']) && !in_array($this->container['with_geojson'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'with_geojson', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets details
     *
     * @return bool
     */
    public function getDetails()
    {
        return $this->container['details'];
    }

    /**
     * Sets details
     *
     * @param bool $details Output Route Details
     *
     * @return $this
     */
    public function setDetails($details)
    {
        $this->container['details'] = $details;

        return $this;
    }

    /**
     * Gets synchronous
     *
     * @return bool
     */
    public function getSynchronous()
    {
        return $this->container['synchronous'];
    }

    /**
     * Sets synchronous
     *
     * @param bool $synchronous Synchronous processing or not. If not, operation will return nothing and the optimum result will be set on planning as soon `Customer.job_optimizer_id` will be null.
     *
     * @return $this
     */
    public function setSynchronous($synchronous)
    {
        $this->container['synchronous'] = $synchronous;

        return $this;
    }

    /**
     * Gets active_only
     *
     * @return bool
     */
    public function getActiveOnly()
    {
        return $this->container['active_only'];
    }

    /**
     * Sets active_only
     *
     * @param bool $active_only If true only active stops are taken into account by optimization, else inactive stops are also taken into account but are not activated in result route.
     *
     * @return $this
     */
    public function setActiveOnly($active_only)
    {
        $this->container['active_only'] = $active_only;

        return $this;
    }

    /**
     * Gets ignore_overload_multipliers
     *
     * @return string
     */
    public function getIgnoreOverloadMultipliers()
    {
        return $this->container['ignore_overload_multipliers'];
    }

    /**
     * Sets ignore_overload_multipliers
     *
     * @param string $ignore_overload_multipliers Deliverable Unit id and whether or not it should be ignored : {'0'=>{'unit_id'=>'7', 'ignore'=>'true'}}
     *
     * @return $this
     */
    public function setIgnoreOverloadMultipliers($ignore_overload_multipliers)
    {
        $this->container['ignore_overload_multipliers'] = $ignore_overload_multipliers;

        return $this;
    }

    /**
     * Gets with_details
     *
     * @return bool
     */
    public function getWithDetails()
    {
        return $this->container['with_details'];
    }

    /**
     * Sets with_details
     *
     * @param bool $with_details Output route details
     *
     * @return $this
     */
    public function setWithDetails($with_details)
    {
        $this->container['with_details'] = $with_details;

        return $this;
    }

    /**
     * Gets with_geojson
     *
     * @return string
     */
    public function getWithGeojson()
    {
        return $this->container['with_geojson'];
    }

    /**
     * Sets with_geojson
     *
     * @param string $with_geojson Fill in response the geojson field with route geometry: `point` to return only points, `polyline` to return with encoded linestring, `true` to return both.
     *
     * @return $this
     */
    public function setWithGeojson($with_geojson)
    {
        $allowedValues = $this->getWithGeojsonAllowableValues();
        if (!is_null($with_geojson) && !in_array($with_geojson, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'with_geojson', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['with_geojson'] = $with_geojson;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
