<?php
/**
 * Body6
 *
 * PHP version 5
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * API
 *
 * [Simplified view of domain model](https://app.mapotempo.com//api/0.1/Model-simpel.svg). ## Model Model is structured around four majors concepts: the Customer account, Destinations, Vehicles and Plannings. * `Customers`: many of objects are linked to a customer account (relating to the user calling API). The customer has many `Users`, each user has his own `api_key`. Be carefull not to confuse with following model `Destination`, `Customer`can only be created by a admin `User`. * `Destinations`: location points to visit with constraints. The same `Destination` can be visited several times : in this case several `Visits` are associated to one `Destination`. * `Vehicles`: vehicles definition are splited in two parts:  * the structural definition named `Vehicle` (car, truck, bike, consumption, etc.)  * and the vehicle usage `VehicleUsage`, a specific usage of a physical vehicle in a specific context. Vehicles can be used in many contexts called `VehicleUsageSet` (set of all vehicles usages under a context). Multiple values are only available if dedicated option for customer is active. For instance, if customer needs to use its vehicle 2 times per day (morning and evening), he needs 2 `VehicleUsageSet` called \"Morning\" and \"Evening\" : each can have different values defined for stores, rest, etc... `VehicleUsageSet` defines default values for vehicle usage. * `Plannings`: `Planning` is a set of `Routes` to `Visit` `Destinations` with `Vehicle` within a `VehicleUsageSet` context. A route is a track between all destinations reached by a vehicle (a new route is created for each customer's vehicle and a route without vehicle is created for all out-of-route destinations). By default all customer's visites are used in a planning.  ## Technical access ### Swagger descriptor This REST API is described with Swagger. The Swagger descriptor defines the request end-points, the parameters and the return values. The API can be addressed by HTTP request or with a generated client using the Swagger descriptor. ### API key All access to the API are subject to an `api_key` parameter in order to authenticate the user. This parameter can be sent with a query string on each available operation: `https://app.mapotempo.com/api/0.1/{objects}?api_key={your_personal_api_key}` ### Return The API supports several return formats: `json` and `xml` which depend of the requested extension used in url. ### I18n Functionnal textual returns are subject to translation and depend of HTTP header `Accept-Language`. HTTP error codes are not translated. ## Admin acces Using an admin `api_key` switches to advanced opperations (on `Customer`, `User`, `Vehicle`, `Profile`). Most of operations from the current api are usable either for normal user `api_key` or admin user `api_key` (not both). ## More concepts When a customer is created some objects are created by default with this new customer: * `Vehicle`: multiple, depending of the `max_vehicles` defined for customer * `DeliverableUnit`: one default * `VehicleUsageSet`: one default * `VehicleUsage`: multiple, depending of the vehicles number * `Store`: one default  ### Profiles, Layers, Routers `Profile` is a concept which allows to set several parameters for the customer: * `Layer`: which allows to choose the background map * `Router`: which allows to build route's information.  Several default profiles are available and can be listed with an admin `api_key`.  ### Tags `Tag` is a concept to filter visits and create planning only for a subset of visits. For instance, if some visits are tagged \"Monday\", it allows to create a new planning for \"Monday\" tag and use only dedicated visits. ### Zonings `Zoning` is a concept which allows to define multiple `Zones` (areas) around destinatons. A `Zone` can be affected to a `Vehicle` and if it is used into a `Planning`, all `Destinations` inside areas will be affected to the zone's vehicle (or `Route`). A polygon defining a `Zone` can be created outside the application or can be automatically generated from a planning.  ## Code samples * Create and display destinations or visits. Here some samples for these operations: [using PHP](https://app.mapotempo.com//api/0.1/examples/php/example.php), [using Ruby](https://app.mapotempo.com//api/0.1/examples/ruby/example.rb). Note you can import destinations/visits and create a planning at the same time if you know beforehand the route for each destination/visit. See the details of importDestinations operation to import your data and create a planning in only one call. * Same operations are available for stores (note you have an existing default store). * With created destinations/visits, you can create a planning (routes and stops are automatically created depending of yours vehicles and destinations/visits) * In existing planning, you have availability to move stops (which represent visits) on a dedicated route (which represent a dedicated vehicle). * With many unaffected (out-of-route) stops in a planning, you may create a zoning to move many stops in several routes. Create a zoning (you can generate zones in this zoning automatically from automatic clustering), if you apply zoning (containing zones linked to a vehicle) on your planning, all stops contained in different zones will be moved in dedicated routes.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: tech@mapotempo.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.20
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Mapotempo\Model;

use \ArrayAccess;
use \Mapotempo\ObjectSerializer;

/**
 * Body6 Class Doc Comment
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class Body6 implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'body_6';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'ref' => 'string',
'name' => 'string',
'street' => 'string',
'detail' => 'string',
'postalcode' => 'string',
'city' => 'string',
'state' => 'string',
'country' => 'string',
'lat' => 'float',
'lng' => 'float',
'comment' => 'string',
'phone_number' => 'string',
'visits' => '\Mapotempo\Model\V01Visit[]',
'setup_duration' => 'string'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'ref' => null,
'name' => null,
'street' => null,
'detail' => null,
'postalcode' => null,
'city' => null,
'state' => null,
'country' => null,
'lat' => 'float',
'lng' => 'float',
'comment' => null,
'phone_number' => null,
'visits' => null,
'setup_duration' => null    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'ref' => 'ref',
'name' => 'name',
'street' => 'street',
'detail' => 'detail',
'postalcode' => 'postalcode',
'city' => 'city',
'state' => 'state',
'country' => 'country',
'lat' => 'lat',
'lng' => 'lng',
'comment' => 'comment',
'phone_number' => 'phone_number',
'visits' => 'visits',
'setup_duration' => 'setup_duration'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'ref' => 'setRef',
'name' => 'setName',
'street' => 'setStreet',
'detail' => 'setDetail',
'postalcode' => 'setPostalcode',
'city' => 'setCity',
'state' => 'setState',
'country' => 'setCountry',
'lat' => 'setLat',
'lng' => 'setLng',
'comment' => 'setComment',
'phone_number' => 'setPhoneNumber',
'visits' => 'setVisits',
'setup_duration' => 'setSetupDuration'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'ref' => 'getRef',
'name' => 'getName',
'street' => 'getStreet',
'detail' => 'getDetail',
'postalcode' => 'getPostalcode',
'city' => 'getCity',
'state' => 'getState',
'country' => 'getCountry',
'lat' => 'getLat',
'lng' => 'getLng',
'comment' => 'getComment',
'phone_number' => 'getPhoneNumber',
'visits' => 'getVisits',
'setup_duration' => 'getSetupDuration'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['ref'] = isset($data['ref']) ? $data['ref'] : null;
        $this->container['name'] = isset($data['name']) ? $data['name'] : null;
        $this->container['street'] = isset($data['street']) ? $data['street'] : null;
        $this->container['detail'] = isset($data['detail']) ? $data['detail'] : null;
        $this->container['postalcode'] = isset($data['postalcode']) ? $data['postalcode'] : null;
        $this->container['city'] = isset($data['city']) ? $data['city'] : null;
        $this->container['state'] = isset($data['state']) ? $data['state'] : null;
        $this->container['country'] = isset($data['country']) ? $data['country'] : null;
        $this->container['lat'] = isset($data['lat']) ? $data['lat'] : null;
        $this->container['lng'] = isset($data['lng']) ? $data['lng'] : null;
        $this->container['comment'] = isset($data['comment']) ? $data['comment'] : null;
        $this->container['phone_number'] = isset($data['phone_number']) ? $data['phone_number'] : null;
        $this->container['visits'] = isset($data['visits']) ? $data['visits'] : null;
        $this->container['setup_duration'] = isset($data['setup_duration']) ? $data['setup_duration'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['name'] === null) {
            $invalidProperties[] = "'name' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets ref
     *
     * @return string
     */
    public function getRef()
    {
        return $this->container['ref'];
    }

    /**
     * Sets ref
     *
     * @param string $ref A free reference, like an external ID from other information system (forbidden characters are ./\\)
     *
     * @return $this
     */
    public function setRef($ref)
    {
        $this->container['ref'] = $ref;

        return $this;
    }

    /**
     * Gets name
     *
     * @return string
     */
    public function getName()
    {
        return $this->container['name'];
    }

    /**
     * Sets name
     *
     * @param string $name name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->container['name'] = $name;

        return $this;
    }

    /**
     * Gets street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->container['street'];
    }

    /**
     * Sets street
     *
     * @param string $street Content from address only useful for geocoding.
     *
     * @return $this
     */
    public function setStreet($street)
    {
        $this->container['street'] = $street;

        return $this;
    }

    /**
     * Gets detail
     *
     * @return string
     */
    public function getDetail()
    {
        return $this->container['detail'];
    }

    /**
     * Sets detail
     *
     * @param string $detail Content from address useless for geocoding (flat number).
     *
     * @return $this
     */
    public function setDetail($detail)
    {
        $this->container['detail'] = $detail;

        return $this;
    }

    /**
     * Gets postalcode
     *
     * @return string
     */
    public function getPostalcode()
    {
        return $this->container['postalcode'];
    }

    /**
     * Sets postalcode
     *
     * @param string $postalcode postalcode
     *
     * @return $this
     */
    public function setPostalcode($postalcode)
    {
        $this->container['postalcode'] = $postalcode;

        return $this;
    }

    /**
     * Gets city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->container['city'];
    }

    /**
     * Sets city
     *
     * @param string $city city
     *
     * @return $this
     */
    public function setCity($city)
    {
        $this->container['city'] = $city;

        return $this;
    }

    /**
     * Gets state
     *
     * @return string
     */
    public function getState()
    {
        return $this->container['state'];
    }

    /**
     * Sets state
     *
     * @param string $state state
     *
     * @return $this
     */
    public function setState($state)
    {
        $this->container['state'] = $state;

        return $this;
    }

    /**
     * Gets country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->container['country'];
    }

    /**
     * Sets country
     *
     * @param string $country country
     *
     * @return $this
     */
    public function setCountry($country)
    {
        $this->container['country'] = $country;

        return $this;
    }

    /**
     * Gets lat
     *
     * @return float
     */
    public function getLat()
    {
        return $this->container['lat'];
    }

    /**
     * Sets lat
     *
     * @param float $lat Decimal value between -90 and 90. If no geolocation is provided, point will be automatically geocoded.
     *
     * @return $this
     */
    public function setLat($lat)
    {
        $this->container['lat'] = $lat;

        return $this;
    }

    /**
     * Gets lng
     *
     * @return float
     */
    public function getLng()
    {
        return $this->container['lng'];
    }

    /**
     * Sets lng
     *
     * @param float $lng Decimal value between -180 and 180. If no geolocation is provided, point will be automatically geocoded.
     *
     * @return $this
     */
    public function setLng($lng)
    {
        $this->container['lng'] = $lng;

        return $this;
    }

    /**
     * Gets comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->container['comment'];
    }

    /**
     * Sets comment
     *
     * @param string $comment comment
     *
     * @return $this
     */
    public function setComment($comment)
    {
        $this->container['comment'] = $comment;

        return $this;
    }

    /**
     * Gets phone_number
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->container['phone_number'];
    }

    /**
     * Sets phone_number
     *
     * @param string $phone_number phone_number
     *
     * @return $this
     */
    public function setPhoneNumber($phone_number)
    {
        $this->container['phone_number'] = $phone_number;

        return $this;
    }

    /**
     * Gets visits
     *
     * @return \Mapotempo\Model\V01Visit[]
     */
    public function getVisits()
    {
        return $this->container['visits'];
    }

    /**
     * Sets visits
     *
     * @param \Mapotempo\Model\V01Visit[] $visits visits
     *
     * @return $this
     */
    public function setVisits($visits)
    {
        $this->container['visits'] = $visits;

        return $this;
    }

    /**
     * Gets setup_duration
     *
     * @return string
     */
    public function getSetupDuration()
    {
        return $this->container['setup_duration'];
    }

    /**
     * Sets setup_duration
     *
     * @param string $setup_duration Unique preparation duration however number of visits (In case of multiple visits at same time for one destination, this duration will be taken into account only one time). Format is HH:MM:SS or number of seconds.
     *
     * @return $this
     */
    public function setSetupDuration($setup_duration)
    {
        $this->container['setup_duration'] = $setup_duration;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
