<?php
/**
 * V01Planning
 *
 * PHP version 5
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * API
 *
 * [Simplified view of domain model](https://app.mapotempo.com//api/0.1/Model-simpel.svg). ## Model Model is structured around four majors concepts: the Customer account, Destinations, Vehicles and Plannings. * `Customers`: many of objects are linked to a customer account (relating to the user calling API). The customer has many `Users`, each user has his own `api_key`. Be carefull not to confuse with following model `Destination`, `Customer`can only be created by a admin `User`. * `Destinations`: location points to visit with constraints. The same `Destination` can be visited several times : in this case several `Visits` are associated to one `Destination`. * `Vehicles`: vehicles definition are splited in two parts:  * the structural definition named `Vehicle` (car, truck, bike, consumption, etc.)  * and the vehicle usage `VehicleUsage`, a specific usage of a physical vehicle in a specific context. Vehicles can be used in many contexts called `VehicleUsageSet` (set of all vehicles usages under a context). Multiple values are only available if dedicated option for customer is active. For instance, if customer needs to use its vehicle 2 times per day (morning and evening), he needs 2 `VehicleUsageSet` called \"Morning\" and \"Evening\" : each can have different values defined for stores, rest, etc... `VehicleUsageSet` defines default values for vehicle usage. * `Plannings`: `Planning` is a set of `Routes` to `Visit` `Destinations` with `Vehicle` within a `VehicleUsageSet` context. A route is a track between all destinations reached by a vehicle (a new route is created for each customer's vehicle and a route without vehicle is created for all out-of-route destinations). By default all customer's visites are used in a planning.  ## Technical access ### Swagger descriptor This REST API is described with Swagger. The Swagger descriptor defines the request end-points, the parameters and the return values. The API can be addressed by HTTP request or with a generated client using the Swagger descriptor. ### API key All access to the API are subject to an `api_key` parameter in order to authenticate the user. This parameter can be sent with a query string on each available operation: `https://app.mapotempo.com/api/0.1/{objects}?api_key={your_personal_api_key}` ### Return The API supports several return formats: `json` and `xml` which depend of the requested extension used in url. ### I18n Functionnal textual returns are subject to translation and depend of HTTP header `Accept-Language`. HTTP error codes are not translated. ## Admin acces Using an admin `api_key` switches to advanced opperations (on `Customer`, `User`, `Vehicle`, `Profile`). Most of operations from the current api are usable either for normal user `api_key` or admin user `api_key` (not both). ## More concepts When a customer is created some objects are created by default with this new customer: * `Vehicle`: multiple, depending of the `max_vehicles` defined for customer * `DeliverableUnit`: one default * `VehicleUsageSet`: one default * `VehicleUsage`: multiple, depending of the vehicles number * `Store`: one default  ### Profiles, Layers, Routers `Profile` is a concept which allows to set several parameters for the customer: * `Layer`: which allows to choose the background map * `Router`: which allows to build route's information.  Several default profiles are available and can be listed with an admin `api_key`.  ### Tags `Tag` is a concept to filter visits and create planning only for a subset of visits. For instance, if some visits are tagged \"Monday\", it allows to create a new planning for \"Monday\" tag and use only dedicated visits. ### Zonings `Zoning` is a concept which allows to define multiple `Zones` (areas) around destinatons. A `Zone` can be affected to a `Vehicle` and if it is used into a `Planning`, all `Destinations` inside areas will be affected to the zone's vehicle (or `Route`). A polygon defining a `Zone` can be created outside the application or can be automatically generated from a planning.  ## Code samples * Create and display destinations or visits. Here some samples for these operations: [using PHP](https://app.mapotempo.com//api/0.1/examples/php/example.php), [using Ruby](https://app.mapotempo.com//api/0.1/examples/ruby/example.rb). Note you can import destinations/visits and create a planning at the same time if you know beforehand the route for each destination/visit. See the details of importDestinations operation to import your data and create a planning in only one call. * Same operations are available for stores (note you have an existing default store). * With created destinations/visits, you can create a planning (routes and stops are automatically created depending of yours vehicles and destinations/visits) * In existing planning, you have availability to move stops (which represent visits) on a dedicated route (which represent a dedicated vehicle). * With many unaffected (out-of-route) stops in a planning, you may create a zoning to move many stops in several routes. Create a zoning (you can generate zones in this zoning automatically from automatic clustering), if you apply zoning (containing zones linked to a vehicle) on your planning, all stops contained in different zones will be moved in dedicated routes.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: tech@mapotempo.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.20
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Mapotempo\Model;

use \ArrayAccess;
use \Mapotempo\ObjectSerializer;

/**
 * V01Planning Class Doc Comment
 *
 * @category Class
 * @description Fetch planning.
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class V01Planning implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'V01_Planning';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'id' => 'int',
'ref' => 'string',
'name' => 'string',
'outdated' => 'bool',
'zoning_id' => 'int',
'zoning_outdated' => 'bool',
'date' => '\DateTime',
'begin_date' => '\DateTime',
'end_date' => '\DateTime',
'active' => 'bool',
'vehicle_usage_set_id' => 'int',
'zoning_ids' => 'int[]',
'route_ids' => 'int[]',
'tag_ids' => 'int[]',
'tag_operation' => 'string',
'updated_at' => '\DateTime',
'geojson' => 'string',
'routes' => '\Mapotempo\Model\V01RouteProperties[]'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'id' => 'int32',
'ref' => null,
'name' => null,
'outdated' => null,
'zoning_id' => 'int32',
'zoning_outdated' => null,
'date' => 'date',
'begin_date' => 'date',
'end_date' => 'date',
'active' => null,
'vehicle_usage_set_id' => 'int32',
'zoning_ids' => 'int32',
'route_ids' => 'int32',
'tag_ids' => 'int32',
'tag_operation' => null,
'updated_at' => 'date-time',
'geojson' => null,
'routes' => null    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
'ref' => 'ref',
'name' => 'name',
'outdated' => 'outdated',
'zoning_id' => 'zoning_id',
'zoning_outdated' => 'zoning_outdated',
'date' => 'date',
'begin_date' => 'begin_date',
'end_date' => 'end_date',
'active' => 'active',
'vehicle_usage_set_id' => 'vehicle_usage_set_id',
'zoning_ids' => 'zoning_ids',
'route_ids' => 'route_ids',
'tag_ids' => 'tag_ids',
'tag_operation' => 'tag_operation',
'updated_at' => 'updated_at',
'geojson' => 'geojson',
'routes' => 'routes'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
'ref' => 'setRef',
'name' => 'setName',
'outdated' => 'setOutdated',
'zoning_id' => 'setZoningId',
'zoning_outdated' => 'setZoningOutdated',
'date' => 'setDate',
'begin_date' => 'setBeginDate',
'end_date' => 'setEndDate',
'active' => 'setActive',
'vehicle_usage_set_id' => 'setVehicleUsageSetId',
'zoning_ids' => 'setZoningIds',
'route_ids' => 'setRouteIds',
'tag_ids' => 'setTagIds',
'tag_operation' => 'setTagOperation',
'updated_at' => 'setUpdatedAt',
'geojson' => 'setGeojson',
'routes' => 'setRoutes'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
'ref' => 'getRef',
'name' => 'getName',
'outdated' => 'getOutdated',
'zoning_id' => 'getZoningId',
'zoning_outdated' => 'getZoningOutdated',
'date' => 'getDate',
'begin_date' => 'getBeginDate',
'end_date' => 'getEndDate',
'active' => 'getActive',
'vehicle_usage_set_id' => 'getVehicleUsageSetId',
'zoning_ids' => 'getZoningIds',
'route_ids' => 'getRouteIds',
'tag_ids' => 'getTagIds',
'tag_operation' => 'getTagOperation',
'updated_at' => 'getUpdatedAt',
'geojson' => 'getGeojson',
'routes' => 'getRoutes'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    const TAG_OPERATION__AND = 'and';
const TAG_OPERATION__OR = 'or';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getTagOperationAllowableValues()
    {
        return [
            self::TAG_OPERATION__AND,
self::TAG_OPERATION__OR,        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['ref'] = isset($data['ref']) ? $data['ref'] : null;
        $this->container['name'] = isset($data['name']) ? $data['name'] : null;
        $this->container['outdated'] = isset($data['outdated']) ? $data['outdated'] : null;
        $this->container['zoning_id'] = isset($data['zoning_id']) ? $data['zoning_id'] : null;
        $this->container['zoning_outdated'] = isset($data['zoning_outdated']) ? $data['zoning_outdated'] : null;
        $this->container['date'] = isset($data['date']) ? $data['date'] : null;
        $this->container['begin_date'] = isset($data['begin_date']) ? $data['begin_date'] : null;
        $this->container['end_date'] = isset($data['end_date']) ? $data['end_date'] : null;
        $this->container['active'] = isset($data['active']) ? $data['active'] : true;
        $this->container['vehicle_usage_set_id'] = isset($data['vehicle_usage_set_id']) ? $data['vehicle_usage_set_id'] : null;
        $this->container['zoning_ids'] = isset($data['zoning_ids']) ? $data['zoning_ids'] : null;
        $this->container['route_ids'] = isset($data['route_ids']) ? $data['route_ids'] : null;
        $this->container['tag_ids'] = isset($data['tag_ids']) ? $data['tag_ids'] : null;
        $this->container['tag_operation'] = isset($data['tag_operation']) ? $data['tag_operation'] : 'and';
        $this->container['updated_at'] = isset($data['updated_at']) ? $data['updated_at'] : null;
        $this->container['geojson'] = isset($data['geojson']) ? $data['geojson'] : null;
        $this->container['routes'] = isset($data['routes']) ? $data['routes'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        $allowedValues = $this->getTagOperationAllowableValues();
        if (!is_null($this->container['tag_operation']) && !in_array($this->container['tag_operation'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'tag_operation', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id
     *
     * @return int
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param int $id id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets ref
     *
     * @return string
     */
    public function getRef()
    {
        return $this->container['ref'];
    }

    /**
     * Sets ref
     *
     * @param string $ref A free reference, like an external ID from other information system (forbidden characters are ./\\)
     *
     * @return $this
     */
    public function setRef($ref)
    {
        $this->container['ref'] = $ref;

        return $this;
    }

    /**
     * Gets name
     *
     * @return string
     */
    public function getName()
    {
        return $this->container['name'];
    }

    /**
     * Sets name
     *
     * @param string $name name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->container['name'] = $name;

        return $this;
    }

    /**
     * Gets outdated
     *
     * @return bool
     */
    public function getOutdated()
    {
        return $this->container['outdated'];
    }

    /**
     * Sets outdated
     *
     * @param bool $outdated outdated
     *
     * @return $this
     */
    public function setOutdated($outdated)
    {
        $this->container['outdated'] = $outdated;

        return $this;
    }

    /**
     * Gets zoning_id
     *
     * @return int
     */
    public function getZoningId()
    {
        return $this->container['zoning_id'];
    }

    /**
     * Sets zoning_id
     *
     * @param int $zoning_id DEPRECATED. Use zoning_ids instead.
     *
     * @return $this
     */
    public function setZoningId($zoning_id)
    {
        $this->container['zoning_id'] = $zoning_id;

        return $this;
    }

    /**
     * Gets zoning_outdated
     *
     * @return bool
     */
    public function getZoningOutdated()
    {
        return $this->container['zoning_outdated'];
    }

    /**
     * Sets zoning_outdated
     *
     * @param bool $zoning_outdated zoning_outdated
     *
     * @return $this
     */
    public function setZoningOutdated($zoning_outdated)
    {
        $this->container['zoning_outdated'] = $zoning_outdated;

        return $this;
    }

    /**
     * Gets date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->container['date'];
    }

    /**
     * Sets date
     *
     * @param \DateTime $date date
     *
     * @return $this
     */
    public function setDate($date)
    {
        $this->container['date'] = $date;

        return $this;
    }

    /**
     * Gets begin_date
     *
     * @return \DateTime
     */
    public function getBeginDate()
    {
        return $this->container['begin_date'];
    }

    /**
     * Sets begin_date
     *
     * @param \DateTime $begin_date Begin validity period
     *
     * @return $this
     */
    public function setBeginDate($begin_date)
    {
        $this->container['begin_date'] = $begin_date;

        return $this;
    }

    /**
     * Gets end_date
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->container['end_date'];
    }

    /**
     * Sets end_date
     *
     * @param \DateTime $end_date End validity period
     *
     * @return $this
     */
    public function setEndDate($end_date)
    {
        $this->container['end_date'] = $end_date;

        return $this;
    }

    /**
     * Gets active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->container['active'];
    }

    /**
     * Sets active
     *
     * @param bool $active active
     *
     * @return $this
     */
    public function setActive($active)
    {
        $this->container['active'] = $active;

        return $this;
    }

    /**
     * Gets vehicle_usage_set_id
     *
     * @return int
     */
    public function getVehicleUsageSetId()
    {
        return $this->container['vehicle_usage_set_id'];
    }

    /**
     * Sets vehicle_usage_set_id
     *
     * @param int $vehicle_usage_set_id vehicle_usage_set_id
     *
     * @return $this
     */
    public function setVehicleUsageSetId($vehicle_usage_set_id)
    {
        $this->container['vehicle_usage_set_id'] = $vehicle_usage_set_id;

        return $this;
    }

    /**
     * Gets zoning_ids
     *
     * @return int[]
     */
    public function getZoningIds()
    {
        return $this->container['zoning_ids'];
    }

    /**
     * Sets zoning_ids
     *
     * @param int[] $zoning_ids If a new zoning is specified before planning save, all visits will be affected to vehicles specified in zones.
     *
     * @return $this
     */
    public function setZoningIds($zoning_ids)
    {
        $this->container['zoning_ids'] = $zoning_ids;

        return $this;
    }

    /**
     * Gets route_ids
     *
     * @return int[]
     */
    public function getRouteIds()
    {
        return $this->container['route_ids'];
    }

    /**
     * Sets route_ids
     *
     * @param int[] $route_ids route_ids
     *
     * @return $this
     */
    public function setRouteIds($route_ids)
    {
        $this->container['route_ids'] = $route_ids;

        return $this;
    }

    /**
     * Gets tag_ids
     *
     * @return int[]
     */
    public function getTagIds()
    {
        return $this->container['tag_ids'];
    }

    /**
     * Sets tag_ids
     *
     * @param int[] $tag_ids Restrict visits/destinations in the plan (visits/destinations should have all of these tags to be present in the plan).
     *
     * @return $this
     */
    public function setTagIds($tag_ids)
    {
        $this->container['tag_ids'] = $tag_ids;

        return $this;
    }

    /**
     * Gets tag_operation
     *
     * @return string
     */
    public function getTagOperation()
    {
        return $this->container['tag_operation'];
    }

    /**
     * Sets tag_operation
     *
     * @param string $tag_operation Choose how to use selected tags: `and` (for visits with all tags, by default) / `or` (for visits with at least one tag).
     *
     * @return $this
     */
    public function setTagOperation($tag_operation)
    {
        $allowedValues = $this->getTagOperationAllowableValues();
        if (!is_null($tag_operation) && !in_array($tag_operation, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'tag_operation', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['tag_operation'] = $tag_operation;

        return $this;
    }

    /**
     * Gets updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->container['updated_at'];
    }

    /**
     * Sets updated_at
     *
     * @param \DateTime $updated_at Last Updated At
     *
     * @return $this
     */
    public function setUpdatedAt($updated_at)
    {
        $this->container['updated_at'] = $updated_at;

        return $this;
    }

    /**
     * Gets geojson
     *
     * @return string
     */
    public function getGeojson()
    {
        return $this->container['geojson'];
    }

    /**
     * Sets geojson
     *
     * @param string $geojson Geojson string of track and stops of the route. Default empty, set parameter `with_geojson=true|point|polyline` to get this extra content.
     *
     * @return $this
     */
    public function setGeojson($geojson)
    {
        $this->container['geojson'] = $geojson;

        return $this;
    }

    /**
     * Gets routes
     *
     * @return \Mapotempo\Model\V01RouteProperties[]
     */
    public function getRoutes()
    {
        return $this->container['routes'];
    }

    /**
     * Sets routes
     *
     * @param \Mapotempo\Model\V01RouteProperties[] $routes routes
     *
     * @return $this
     */
    public function setRoutes($routes)
    {
        $this->container['routes'] = $routes;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
