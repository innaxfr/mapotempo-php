<?php
/**
 * Body1
 *
 * PHP version 5
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * API
 *
 * [Simplified view of domain model](https://app.mapotempo.com//api/0.1/Model-simpel.svg). ## Model Model is structured around four majors concepts: the Customer account, Destinations, Vehicles and Plannings. * `Customers`: many of objects are linked to a customer account (relating to the user calling API). The customer has many `Users`, each user has his own `api_key`. Be carefull not to confuse with following model `Destination`, `Customer`can only be created by a admin `User`. * `Destinations`: location points to visit with constraints. The same `Destination` can be visited several times : in this case several `Visits` are associated to one `Destination`. * `Vehicles`: vehicles definition are splited in two parts:  * the structural definition named `Vehicle` (car, truck, bike, consumption, etc.)  * and the vehicle usage `VehicleUsage`, a specific usage of a physical vehicle in a specific context. Vehicles can be used in many contexts called `VehicleUsageSet` (set of all vehicles usages under a context). Multiple values are only available if dedicated option for customer is active. For instance, if customer needs to use its vehicle 2 times per day (morning and evening), he needs 2 `VehicleUsageSet` called \"Morning\" and \"Evening\" : each can have different values defined for stores, rest, etc... `VehicleUsageSet` defines default values for vehicle usage. * `Plannings`: `Planning` is a set of `Routes` to `Visit` `Destinations` with `Vehicle` within a `VehicleUsageSet` context. A route is a track between all destinations reached by a vehicle (a new route is created for each customer's vehicle and a route without vehicle is created for all out-of-route destinations). By default all customer's visites are used in a planning.  ## Technical access ### Swagger descriptor This REST API is described with Swagger. The Swagger descriptor defines the request end-points, the parameters and the return values. The API can be addressed by HTTP request or with a generated client using the Swagger descriptor. ### API key All access to the API are subject to an `api_key` parameter in order to authenticate the user. This parameter can be sent with a query string on each available operation: `https://app.mapotempo.com/api/0.1/{objects}?api_key={your_personal_api_key}` ### Return The API supports several return formats: `json` and `xml` which depend of the requested extension used in url. ### I18n Functionnal textual returns are subject to translation and depend of HTTP header `Accept-Language`. HTTP error codes are not translated. ## Admin acces Using an admin `api_key` switches to advanced opperations (on `Customer`, `User`, `Vehicle`, `Profile`). Most of operations from the current api are usable either for normal user `api_key` or admin user `api_key` (not both). ## More concepts When a customer is created some objects are created by default with this new customer: * `Vehicle`: multiple, depending of the `max_vehicles` defined for customer * `DeliverableUnit`: one default * `VehicleUsageSet`: one default * `VehicleUsage`: multiple, depending of the vehicles number * `Store`: one default  ### Profiles, Layers, Routers `Profile` is a concept which allows to set several parameters for the customer: * `Layer`: which allows to choose the background map * `Router`: which allows to build route's information.  Several default profiles are available and can be listed with an admin `api_key`.  ### Tags `Tag` is a concept to filter visits and create planning only for a subset of visits. For instance, if some visits are tagged \"Monday\", it allows to create a new planning for \"Monday\" tag and use only dedicated visits. ### Zonings `Zoning` is a concept which allows to define multiple `Zones` (areas) around destinatons. A `Zone` can be affected to a `Vehicle` and if it is used into a `Planning`, all `Destinations` inside areas will be affected to the zone's vehicle (or `Route`). A polygon defining a `Zone` can be created outside the application or can be automatically generated from a planning.  ## Code samples * Create and display destinations or visits. Here some samples for these operations: [using PHP](https://app.mapotempo.com//api/0.1/examples/php/example.php), [using Ruby](https://app.mapotempo.com//api/0.1/examples/ruby/example.rb). Note you can import destinations/visits and create a planning at the same time if you know beforehand the route for each destination/visit. See the details of importDestinations operation to import your data and create a planning in only one call. * Same operations are available for stores (note you have an existing default store). * With created destinations/visits, you can create a planning (routes and stops are automatically created depending of yours vehicles and destinations/visits) * In existing planning, you have availability to move stops (which represent visits) on a dedicated route (which represent a dedicated vehicle). * With many unaffected (out-of-route) stops in a planning, you may create a zoning to move many stops in several routes. Create a zoning (you can generate zones in this zoning automatically from automatic clustering), if you apply zoning (containing zones linked to a vehicle) on your planning, all stops contained in different zones will be moved in dedicated routes.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: tech@mapotempo.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.20
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Mapotempo\Model;

use \ArrayAccess;
use \Mapotempo\ObjectSerializer;

/**
 * Body1 Class Doc Comment
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class Body1 implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'body_1';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'ref' => 'string',
'name' => 'string',
'end_subscription' => '\DateTime',
'max_vehicles' => 'int',
'visit_duration' => 'string',
'setup_duration' => 'string',
'default_country' => 'string',
'router_id' => 'int',
'router_dimension' => 'string',
'speed_multiplier' => 'float',
'optimization_max_split_size' => 'int',
'optimization_cluster_size' => 'int',
'optimization_time' => 'float',
'optimization_minimal_time' => 'float',
'optimization_stop_soft_upper_bound' => 'float',
'optimization_vehicle_soft_upper_bound' => 'float',
'optimization_cost_waiting_time' => 'float',
'optimization_force_start' => 'bool',
'print_planning_annotating' => 'bool',
'print_header' => 'string',
'print_stop_time' => 'bool',
'print_map' => 'bool',
'print_barcode' => 'string',
'advanced_options' => 'string',
'devices' => 'string',
'router_options_traffic' => 'bool',
'router_options_track' => 'bool',
'router_options_motorway' => 'bool',
'router_options_toll' => 'bool',
'router_options_trailers' => 'int',
'router_options_weight' => 'float',
'router_options_weight_per_axle' => 'float',
'router_options_height' => 'float',
'router_options_width' => 'float',
'router_options_length' => 'float',
'router_options_hazardous_goods' => 'string',
'router_options_max_walk_distance' => 'float',
'router_options_approach' => 'string',
'router_options_snap' => 'float',
'router_options_strict_restriction' => 'bool'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'ref' => null,
'name' => null,
'end_subscription' => 'date',
'max_vehicles' => 'int32',
'visit_duration' => null,
'setup_duration' => null,
'default_country' => null,
'router_id' => 'int32',
'router_dimension' => null,
'speed_multiplier' => 'float',
'optimization_max_split_size' => 'int32',
'optimization_cluster_size' => 'int32',
'optimization_time' => 'float',
'optimization_minimal_time' => 'float',
'optimization_stop_soft_upper_bound' => 'float',
'optimization_vehicle_soft_upper_bound' => 'float',
'optimization_cost_waiting_time' => 'float',
'optimization_force_start' => null,
'print_planning_annotating' => null,
'print_header' => null,
'print_stop_time' => null,
'print_map' => null,
'print_barcode' => null,
'advanced_options' => null,
'devices' => null,
'router_options_traffic' => null,
'router_options_track' => null,
'router_options_motorway' => null,
'router_options_toll' => null,
'router_options_trailers' => 'int32',
'router_options_weight' => 'float',
'router_options_weight_per_axle' => 'float',
'router_options_height' => 'float',
'router_options_width' => 'float',
'router_options_length' => 'float',
'router_options_hazardous_goods' => null,
'router_options_max_walk_distance' => 'float',
'router_options_approach' => null,
'router_options_snap' => 'float',
'router_options_strict_restriction' => null    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'ref' => 'ref',
'name' => 'name',
'end_subscription' => 'end_subscription',
'max_vehicles' => 'max_vehicles',
'visit_duration' => 'visit_duration',
'setup_duration' => 'setup_duration',
'default_country' => 'default_country',
'router_id' => 'router_id',
'router_dimension' => 'router_dimension',
'speed_multiplier' => 'speed_multiplier',
'optimization_max_split_size' => 'optimization_max_split_size',
'optimization_cluster_size' => 'optimization_cluster_size',
'optimization_time' => 'optimization_time',
'optimization_minimal_time' => 'optimization_minimal_time',
'optimization_stop_soft_upper_bound' => 'optimization_stop_soft_upper_bound',
'optimization_vehicle_soft_upper_bound' => 'optimization_vehicle_soft_upper_bound',
'optimization_cost_waiting_time' => 'optimization_cost_waiting_time',
'optimization_force_start' => 'optimization_force_start',
'print_planning_annotating' => 'print_planning_annotating',
'print_header' => 'print_header',
'print_stop_time' => 'print_stop_time',
'print_map' => 'print_map',
'print_barcode' => 'print_barcode',
'advanced_options' => 'advanced_options',
'devices' => 'devices',
'router_options_traffic' => 'router_options[traffic]',
'router_options_track' => 'router_options[track]',
'router_options_motorway' => 'router_options[motorway]',
'router_options_toll' => 'router_options[toll]',
'router_options_trailers' => 'router_options[trailers]',
'router_options_weight' => 'router_options[weight]',
'router_options_weight_per_axle' => 'router_options[weight_per_axle]',
'router_options_height' => 'router_options[height]',
'router_options_width' => 'router_options[width]',
'router_options_length' => 'router_options[length]',
'router_options_hazardous_goods' => 'router_options[hazardous_goods]',
'router_options_max_walk_distance' => 'router_options[max_walk_distance]',
'router_options_approach' => 'router_options[approach]',
'router_options_snap' => 'router_options[snap]',
'router_options_strict_restriction' => 'router_options[strict_restriction]'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'ref' => 'setRef',
'name' => 'setName',
'end_subscription' => 'setEndSubscription',
'max_vehicles' => 'setMaxVehicles',
'visit_duration' => 'setVisitDuration',
'setup_duration' => 'setSetupDuration',
'default_country' => 'setDefaultCountry',
'router_id' => 'setRouterId',
'router_dimension' => 'setRouterDimension',
'speed_multiplier' => 'setSpeedMultiplier',
'optimization_max_split_size' => 'setOptimizationMaxSplitSize',
'optimization_cluster_size' => 'setOptimizationClusterSize',
'optimization_time' => 'setOptimizationTime',
'optimization_minimal_time' => 'setOptimizationMinimalTime',
'optimization_stop_soft_upper_bound' => 'setOptimizationStopSoftUpperBound',
'optimization_vehicle_soft_upper_bound' => 'setOptimizationVehicleSoftUpperBound',
'optimization_cost_waiting_time' => 'setOptimizationCostWaitingTime',
'optimization_force_start' => 'setOptimizationForceStart',
'print_planning_annotating' => 'setPrintPlanningAnnotating',
'print_header' => 'setPrintHeader',
'print_stop_time' => 'setPrintStopTime',
'print_map' => 'setPrintMap',
'print_barcode' => 'setPrintBarcode',
'advanced_options' => 'setAdvancedOptions',
'devices' => 'setDevices',
'router_options_traffic' => 'setRouterOptionsTraffic',
'router_options_track' => 'setRouterOptionsTrack',
'router_options_motorway' => 'setRouterOptionsMotorway',
'router_options_toll' => 'setRouterOptionsToll',
'router_options_trailers' => 'setRouterOptionsTrailers',
'router_options_weight' => 'setRouterOptionsWeight',
'router_options_weight_per_axle' => 'setRouterOptionsWeightPerAxle',
'router_options_height' => 'setRouterOptionsHeight',
'router_options_width' => 'setRouterOptionsWidth',
'router_options_length' => 'setRouterOptionsLength',
'router_options_hazardous_goods' => 'setRouterOptionsHazardousGoods',
'router_options_max_walk_distance' => 'setRouterOptionsMaxWalkDistance',
'router_options_approach' => 'setRouterOptionsApproach',
'router_options_snap' => 'setRouterOptionsSnap',
'router_options_strict_restriction' => 'setRouterOptionsStrictRestriction'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'ref' => 'getRef',
'name' => 'getName',
'end_subscription' => 'getEndSubscription',
'max_vehicles' => 'getMaxVehicles',
'visit_duration' => 'getVisitDuration',
'setup_duration' => 'getSetupDuration',
'default_country' => 'getDefaultCountry',
'router_id' => 'getRouterId',
'router_dimension' => 'getRouterDimension',
'speed_multiplier' => 'getSpeedMultiplier',
'optimization_max_split_size' => 'getOptimizationMaxSplitSize',
'optimization_cluster_size' => 'getOptimizationClusterSize',
'optimization_time' => 'getOptimizationTime',
'optimization_minimal_time' => 'getOptimizationMinimalTime',
'optimization_stop_soft_upper_bound' => 'getOptimizationStopSoftUpperBound',
'optimization_vehicle_soft_upper_bound' => 'getOptimizationVehicleSoftUpperBound',
'optimization_cost_waiting_time' => 'getOptimizationCostWaitingTime',
'optimization_force_start' => 'getOptimizationForceStart',
'print_planning_annotating' => 'getPrintPlanningAnnotating',
'print_header' => 'getPrintHeader',
'print_stop_time' => 'getPrintStopTime',
'print_map' => 'getPrintMap',
'print_barcode' => 'getPrintBarcode',
'advanced_options' => 'getAdvancedOptions',
'devices' => 'getDevices',
'router_options_traffic' => 'getRouterOptionsTraffic',
'router_options_track' => 'getRouterOptionsTrack',
'router_options_motorway' => 'getRouterOptionsMotorway',
'router_options_toll' => 'getRouterOptionsToll',
'router_options_trailers' => 'getRouterOptionsTrailers',
'router_options_weight' => 'getRouterOptionsWeight',
'router_options_weight_per_axle' => 'getRouterOptionsWeightPerAxle',
'router_options_height' => 'getRouterOptionsHeight',
'router_options_width' => 'getRouterOptionsWidth',
'router_options_length' => 'getRouterOptionsLength',
'router_options_hazardous_goods' => 'getRouterOptionsHazardousGoods',
'router_options_max_walk_distance' => 'getRouterOptionsMaxWalkDistance',
'router_options_approach' => 'getRouterOptionsApproach',
'router_options_snap' => 'getRouterOptionsSnap',
'router_options_strict_restriction' => 'getRouterOptionsStrictRestriction'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    const ROUTER_DIMENSION_TIME = 'time';
const ROUTER_DIMENSION_DISTANCE = 'distance';
const PRINT_BARCODE_CODE128 = 'code128';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getRouterDimensionAllowableValues()
    {
        return [
            self::ROUTER_DIMENSION_TIME,
self::ROUTER_DIMENSION_DISTANCE,        ];
    }
    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getPrintBarcodeAllowableValues()
    {
        return [
            self::PRINT_BARCODE_CODE128,        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['ref'] = isset($data['ref']) ? $data['ref'] : null;
        $this->container['name'] = isset($data['name']) ? $data['name'] : null;
        $this->container['end_subscription'] = isset($data['end_subscription']) ? $data['end_subscription'] : null;
        $this->container['max_vehicles'] = isset($data['max_vehicles']) ? $data['max_vehicles'] : null;
        $this->container['visit_duration'] = isset($data['visit_duration']) ? $data['visit_duration'] : null;
        $this->container['setup_duration'] = isset($data['setup_duration']) ? $data['setup_duration'] : null;
        $this->container['default_country'] = isset($data['default_country']) ? $data['default_country'] : null;
        $this->container['router_id'] = isset($data['router_id']) ? $data['router_id'] : null;
        $this->container['router_dimension'] = isset($data['router_dimension']) ? $data['router_dimension'] : null;
        $this->container['speed_multiplier'] = isset($data['speed_multiplier']) ? $data['speed_multiplier'] : null;
        $this->container['optimization_max_split_size'] = isset($data['optimization_max_split_size']) ? $data['optimization_max_split_size'] : null;
        $this->container['optimization_cluster_size'] = isset($data['optimization_cluster_size']) ? $data['optimization_cluster_size'] : null;
        $this->container['optimization_time'] = isset($data['optimization_time']) ? $data['optimization_time'] : null;
        $this->container['optimization_minimal_time'] = isset($data['optimization_minimal_time']) ? $data['optimization_minimal_time'] : null;
        $this->container['optimization_stop_soft_upper_bound'] = isset($data['optimization_stop_soft_upper_bound']) ? $data['optimization_stop_soft_upper_bound'] : null;
        $this->container['optimization_vehicle_soft_upper_bound'] = isset($data['optimization_vehicle_soft_upper_bound']) ? $data['optimization_vehicle_soft_upper_bound'] : null;
        $this->container['optimization_cost_waiting_time'] = isset($data['optimization_cost_waiting_time']) ? $data['optimization_cost_waiting_time'] : null;
        $this->container['optimization_force_start'] = isset($data['optimization_force_start']) ? $data['optimization_force_start'] : null;
        $this->container['print_planning_annotating'] = isset($data['print_planning_annotating']) ? $data['print_planning_annotating'] : null;
        $this->container['print_header'] = isset($data['print_header']) ? $data['print_header'] : null;
        $this->container['print_stop_time'] = isset($data['print_stop_time']) ? $data['print_stop_time'] : null;
        $this->container['print_map'] = isset($data['print_map']) ? $data['print_map'] : null;
        $this->container['print_barcode'] = isset($data['print_barcode']) ? $data['print_barcode'] : null;
        $this->container['advanced_options'] = isset($data['advanced_options']) ? $data['advanced_options'] : null;
        $this->container['devices'] = isset($data['devices']) ? $data['devices'] : null;
        $this->container['router_options_traffic'] = isset($data['router_options_traffic']) ? $data['router_options_traffic'] : null;
        $this->container['router_options_track'] = isset($data['router_options_track']) ? $data['router_options_track'] : null;
        $this->container['router_options_motorway'] = isset($data['router_options_motorway']) ? $data['router_options_motorway'] : null;
        $this->container['router_options_toll'] = isset($data['router_options_toll']) ? $data['router_options_toll'] : null;
        $this->container['router_options_trailers'] = isset($data['router_options_trailers']) ? $data['router_options_trailers'] : null;
        $this->container['router_options_weight'] = isset($data['router_options_weight']) ? $data['router_options_weight'] : null;
        $this->container['router_options_weight_per_axle'] = isset($data['router_options_weight_per_axle']) ? $data['router_options_weight_per_axle'] : null;
        $this->container['router_options_height'] = isset($data['router_options_height']) ? $data['router_options_height'] : null;
        $this->container['router_options_width'] = isset($data['router_options_width']) ? $data['router_options_width'] : null;
        $this->container['router_options_length'] = isset($data['router_options_length']) ? $data['router_options_length'] : null;
        $this->container['router_options_hazardous_goods'] = isset($data['router_options_hazardous_goods']) ? $data['router_options_hazardous_goods'] : null;
        $this->container['router_options_max_walk_distance'] = isset($data['router_options_max_walk_distance']) ? $data['router_options_max_walk_distance'] : null;
        $this->container['router_options_approach'] = isset($data['router_options_approach']) ? $data['router_options_approach'] : null;
        $this->container['router_options_snap'] = isset($data['router_options_snap']) ? $data['router_options_snap'] : null;
        $this->container['router_options_strict_restriction'] = isset($data['router_options_strict_restriction']) ? $data['router_options_strict_restriction'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        $allowedValues = $this->getRouterDimensionAllowableValues();
        if (!is_null($this->container['router_dimension']) && !in_array($this->container['router_dimension'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'router_dimension', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        $allowedValues = $this->getPrintBarcodeAllowableValues();
        if (!is_null($this->container['print_barcode']) && !in_array($this->container['print_barcode'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'print_barcode', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets ref
     *
     * @return string
     */
    public function getRef()
    {
        return $this->container['ref'];
    }

    /**
     * Sets ref
     *
     * @param string $ref Only available in admin.
     *
     * @return $this
     */
    public function setRef($ref)
    {
        $this->container['ref'] = $ref;

        return $this;
    }

    /**
     * Gets name
     *
     * @return string
     */
    public function getName()
    {
        return $this->container['name'];
    }

    /**
     * Sets name
     *
     * @param string $name Only available in admin.
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->container['name'] = $name;

        return $this;
    }

    /**
     * Gets end_subscription
     *
     * @return \DateTime
     */
    public function getEndSubscription()
    {
        return $this->container['end_subscription'];
    }

    /**
     * Sets end_subscription
     *
     * @param \DateTime $end_subscription Only available in admin.
     *
     * @return $this
     */
    public function setEndSubscription($end_subscription)
    {
        $this->container['end_subscription'] = $end_subscription;

        return $this;
    }

    /**
     * Gets max_vehicles
     *
     * @return int
     */
    public function getMaxVehicles()
    {
        return $this->container['max_vehicles'];
    }

    /**
     * Sets max_vehicles
     *
     * @param int $max_vehicles Only available in admin.
     *
     * @return $this
     */
    public function setMaxVehicles($max_vehicles)
    {
        $this->container['max_vehicles'] = $max_vehicles;

        return $this;
    }

    /**
     * Gets visit_duration
     *
     * @return string
     */
    public function getVisitDuration()
    {
        return $this->container['visit_duration'];
    }

    /**
     * Sets visit_duration
     *
     * @param string $visit_duration Default visit duration (HH:MM:SS) or number of seconds (can be overidded on each Visit entity)
     *
     * @return $this
     */
    public function setVisitDuration($visit_duration)
    {
        $this->container['visit_duration'] = $visit_duration;

        return $this;
    }

    /**
     * Gets setup_duration
     *
     * @return string
     */
    public function getSetupDuration()
    {
        return $this->container['setup_duration'];
    }

    /**
     * Sets setup_duration
     *
     * @param string $setup_duration Default setup duration (HH:MM:SS) or number of seconds (can be overidded on each Destination entity)
     *
     * @return $this
     */
    public function setSetupDuration($setup_duration)
    {
        $this->container['setup_duration'] = $setup_duration;

        return $this;
    }

    /**
     * Gets default_country
     *
     * @return string
     */
    public function getDefaultCountry()
    {
        return $this->container['default_country'];
    }

    /**
     * Sets default_country
     *
     * @param string $default_country Default country (can be overidded on each Destination entity)
     *
     * @return $this
     */
    public function setDefaultCountry($default_country)
    {
        $this->container['default_country'] = $default_country;

        return $this;
    }

    /**
     * Gets router_id
     *
     * @return int
     */
    public function getRouterId()
    {
        return $this->container['router_id'];
    }

    /**
     * Sets router_id
     *
     * @param int $router_id router_id
     *
     * @return $this
     */
    public function setRouterId($router_id)
    {
        $this->container['router_id'] = $router_id;

        return $this;
    }

    /**
     * Gets router_dimension
     *
     * @return string
     */
    public function getRouterDimension()
    {
        return $this->container['router_dimension'];
    }

    /**
     * Sets router_dimension
     *
     * @param string $router_dimension router_dimension
     *
     * @return $this
     */
    public function setRouterDimension($router_dimension)
    {
        $allowedValues = $this->getRouterDimensionAllowableValues();
        if (!is_null($router_dimension) && !in_array($router_dimension, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'router_dimension', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['router_dimension'] = $router_dimension;

        return $this;
    }

    /**
     * Gets speed_multiplier
     *
     * @return float
     */
    public function getSpeedMultiplier()
    {
        return $this->container['speed_multiplier'];
    }

    /**
     * Sets speed_multiplier
     *
     * @param float $speed_multiplier Default router speed multiplier (can be overidded on each Vehicle entity)
     *
     * @return $this
     */
    public function setSpeedMultiplier($speed_multiplier)
    {
        $this->container['speed_multiplier'] = $speed_multiplier;

        return $this;
    }

    /**
     * Gets optimization_max_split_size
     *
     * @return int
     */
    public function getOptimizationMaxSplitSize()
    {
        return $this->container['optimization_max_split_size'];
    }

    /**
     * Sets optimization_max_split_size
     *
     * @param int $optimization_max_split_size Maximum number of visits to split problem (default is 500)
     *
     * @return $this
     */
    public function setOptimizationMaxSplitSize($optimization_max_split_size)
    {
        $this->container['optimization_max_split_size'] = $optimization_max_split_size;

        return $this;
    }

    /**
     * Gets optimization_cluster_size
     *
     * @return int
     */
    public function getOptimizationClusterSize()
    {
        return $this->container['optimization_cluster_size'];
    }

    /**
     * Sets optimization_cluster_size
     *
     * @param int $optimization_cluster_size Time in seconds to group near visits (default is 0)
     *
     * @return $this
     */
    public function setOptimizationClusterSize($optimization_cluster_size)
    {
        $this->container['optimization_cluster_size'] = $optimization_cluster_size;

        return $this;
    }

    /**
     * Gets optimization_time
     *
     * @return float
     */
    public function getOptimizationTime()
    {
        return $this->container['optimization_time'];
    }

    /**
     * Sets optimization_time
     *
     * @param float $optimization_time Maximum optimization time by vehicle (default is 60)
     *
     * @return $this
     */
    public function setOptimizationTime($optimization_time)
    {
        $this->container['optimization_time'] = $optimization_time;

        return $this;
    }

    /**
     * Gets optimization_minimal_time
     *
     * @return float
     */
    public function getOptimizationMinimalTime()
    {
        return $this->container['optimization_minimal_time'];
    }

    /**
     * Sets optimization_minimal_time
     *
     * @param float $optimization_minimal_time Minimum optimization time by vehicle (default is 3)
     *
     * @return $this
     */
    public function setOptimizationMinimalTime($optimization_minimal_time)
    {
        $this->container['optimization_minimal_time'] = $optimization_minimal_time;

        return $this;
    }

    /**
     * Gets optimization_stop_soft_upper_bound
     *
     * @return float
     */
    public function getOptimizationStopSoftUpperBound()
    {
        return $this->container['optimization_stop_soft_upper_bound'];
    }

    /**
     * Sets optimization_stop_soft_upper_bound
     *
     * @param float $optimization_stop_soft_upper_bound Stops delay coefficient, 0 to avoid delay (default is 0.3)
     *
     * @return $this
     */
    public function setOptimizationStopSoftUpperBound($optimization_stop_soft_upper_bound)
    {
        $this->container['optimization_stop_soft_upper_bound'] = $optimization_stop_soft_upper_bound;

        return $this;
    }

    /**
     * Gets optimization_vehicle_soft_upper_bound
     *
     * @return float
     */
    public function getOptimizationVehicleSoftUpperBound()
    {
        return $this->container['optimization_vehicle_soft_upper_bound'];
    }

    /**
     * Sets optimization_vehicle_soft_upper_bound
     *
     * @param float $optimization_vehicle_soft_upper_bound Vehicles delay coefficient, 0 to avoid delay (default is 0.3)
     *
     * @return $this
     */
    public function setOptimizationVehicleSoftUpperBound($optimization_vehicle_soft_upper_bound)
    {
        $this->container['optimization_vehicle_soft_upper_bound'] = $optimization_vehicle_soft_upper_bound;

        return $this;
    }

    /**
     * Gets optimization_cost_waiting_time
     *
     * @return float
     */
    public function getOptimizationCostWaitingTime()
    {
        return $this->container['optimization_cost_waiting_time'];
    }

    /**
     * Sets optimization_cost_waiting_time
     *
     * @param float $optimization_cost_waiting_time Coefficient to manage waiting time (default is 1)
     *
     * @return $this
     */
    public function setOptimizationCostWaitingTime($optimization_cost_waiting_time)
    {
        $this->container['optimization_cost_waiting_time'] = $optimization_cost_waiting_time;

        return $this;
    }

    /**
     * Gets optimization_force_start
     *
     * @return bool
     */
    public function getOptimizationForceStart()
    {
        return $this->container['optimization_force_start'];
    }

    /**
     * Sets optimization_force_start
     *
     * @param bool $optimization_force_start Force time for departure (default is false)
     *
     * @return $this
     */
    public function setOptimizationForceStart($optimization_force_start)
    {
        $this->container['optimization_force_start'] = $optimization_force_start;

        return $this;
    }

    /**
     * Gets print_planning_annotating
     *
     * @return bool
     */
    public function getPrintPlanningAnnotating()
    {
        return $this->container['print_planning_annotating'];
    }

    /**
     * Sets print_planning_annotating
     *
     * @param bool $print_planning_annotating print_planning_annotating
     *
     * @return $this
     */
    public function setPrintPlanningAnnotating($print_planning_annotating)
    {
        $this->container['print_planning_annotating'] = $print_planning_annotating;

        return $this;
    }

    /**
     * Gets print_header
     *
     * @return string
     */
    public function getPrintHeader()
    {
        return $this->container['print_header'];
    }

    /**
     * Sets print_header
     *
     * @param string $print_header print_header
     *
     * @return $this
     */
    public function setPrintHeader($print_header)
    {
        $this->container['print_header'] = $print_header;

        return $this;
    }

    /**
     * Gets print_stop_time
     *
     * @return bool
     */
    public function getPrintStopTime()
    {
        return $this->container['print_stop_time'];
    }

    /**
     * Sets print_stop_time
     *
     * @param bool $print_stop_time print_stop_time
     *
     * @return $this
     */
    public function setPrintStopTime($print_stop_time)
    {
        $this->container['print_stop_time'] = $print_stop_time;

        return $this;
    }

    /**
     * Gets print_map
     *
     * @return bool
     */
    public function getPrintMap()
    {
        return $this->container['print_map'];
    }

    /**
     * Sets print_map
     *
     * @param bool $print_map print_map
     *
     * @return $this
     */
    public function setPrintMap($print_map)
    {
        $this->container['print_map'] = $print_map;

        return $this;
    }

    /**
     * Gets print_barcode
     *
     * @return string
     */
    public function getPrintBarcode()
    {
        return $this->container['print_barcode'];
    }

    /**
     * Sets print_barcode
     *
     * @param string $print_barcode Print the Reference as Barcode
     *
     * @return $this
     */
    public function setPrintBarcode($print_barcode)
    {
        $allowedValues = $this->getPrintBarcodeAllowableValues();
        if (!is_null($print_barcode) && !in_array($print_barcode, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'print_barcode', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['print_barcode'] = $print_barcode;

        return $this;
    }

    /**
     * Gets advanced_options
     *
     * @return string
     */
    public function getAdvancedOptions()
    {
        return $this->container['advanced_options'];
    }

    /**
     * Sets advanced_options
     *
     * @param string $advanced_options Advanced options in a serialized json format
     *
     * @return $this
     */
    public function setAdvancedOptions($advanced_options)
    {
        $this->container['advanced_options'] = $advanced_options;

        return $this;
    }

    /**
     * Gets devices
     *
     * @return string
     */
    public function getDevices()
    {
        return $this->container['devices'];
    }

    /**
     * Sets devices
     *
     * @param string $devices Only available in admin.
     *
     * @return $this
     */
    public function setDevices($devices)
    {
        $this->container['devices'] = $devices;

        return $this;
    }

    /**
     * Gets router_options_traffic
     *
     * @return bool
     */
    public function getRouterOptionsTraffic()
    {
        return $this->container['router_options_traffic'];
    }

    /**
     * Sets router_options_traffic
     *
     * @param bool $router_options_traffic router_options_traffic
     *
     * @return $this
     */
    public function setRouterOptionsTraffic($router_options_traffic)
    {
        $this->container['router_options_traffic'] = $router_options_traffic;

        return $this;
    }

    /**
     * Gets router_options_track
     *
     * @return bool
     */
    public function getRouterOptionsTrack()
    {
        return $this->container['router_options_track'];
    }

    /**
     * Sets router_options_track
     *
     * @param bool $router_options_track router_options_track
     *
     * @return $this
     */
    public function setRouterOptionsTrack($router_options_track)
    {
        $this->container['router_options_track'] = $router_options_track;

        return $this;
    }

    /**
     * Gets router_options_motorway
     *
     * @return bool
     */
    public function getRouterOptionsMotorway()
    {
        return $this->container['router_options_motorway'];
    }

    /**
     * Sets router_options_motorway
     *
     * @param bool $router_options_motorway router_options_motorway
     *
     * @return $this
     */
    public function setRouterOptionsMotorway($router_options_motorway)
    {
        $this->container['router_options_motorway'] = $router_options_motorway;

        return $this;
    }

    /**
     * Gets router_options_toll
     *
     * @return bool
     */
    public function getRouterOptionsToll()
    {
        return $this->container['router_options_toll'];
    }

    /**
     * Sets router_options_toll
     *
     * @param bool $router_options_toll router_options_toll
     *
     * @return $this
     */
    public function setRouterOptionsToll($router_options_toll)
    {
        $this->container['router_options_toll'] = $router_options_toll;

        return $this;
    }

    /**
     * Gets router_options_trailers
     *
     * @return int
     */
    public function getRouterOptionsTrailers()
    {
        return $this->container['router_options_trailers'];
    }

    /**
     * Sets router_options_trailers
     *
     * @param int $router_options_trailers router_options_trailers
     *
     * @return $this
     */
    public function setRouterOptionsTrailers($router_options_trailers)
    {
        $this->container['router_options_trailers'] = $router_options_trailers;

        return $this;
    }

    /**
     * Gets router_options_weight
     *
     * @return float
     */
    public function getRouterOptionsWeight()
    {
        return $this->container['router_options_weight'];
    }

    /**
     * Sets router_options_weight
     *
     * @param float $router_options_weight router_options_weight
     *
     * @return $this
     */
    public function setRouterOptionsWeight($router_options_weight)
    {
        $this->container['router_options_weight'] = $router_options_weight;

        return $this;
    }

    /**
     * Gets router_options_weight_per_axle
     *
     * @return float
     */
    public function getRouterOptionsWeightPerAxle()
    {
        return $this->container['router_options_weight_per_axle'];
    }

    /**
     * Sets router_options_weight_per_axle
     *
     * @param float $router_options_weight_per_axle router_options_weight_per_axle
     *
     * @return $this
     */
    public function setRouterOptionsWeightPerAxle($router_options_weight_per_axle)
    {
        $this->container['router_options_weight_per_axle'] = $router_options_weight_per_axle;

        return $this;
    }

    /**
     * Gets router_options_height
     *
     * @return float
     */
    public function getRouterOptionsHeight()
    {
        return $this->container['router_options_height'];
    }

    /**
     * Sets router_options_height
     *
     * @param float $router_options_height router_options_height
     *
     * @return $this
     */
    public function setRouterOptionsHeight($router_options_height)
    {
        $this->container['router_options_height'] = $router_options_height;

        return $this;
    }

    /**
     * Gets router_options_width
     *
     * @return float
     */
    public function getRouterOptionsWidth()
    {
        return $this->container['router_options_width'];
    }

    /**
     * Sets router_options_width
     *
     * @param float $router_options_width router_options_width
     *
     * @return $this
     */
    public function setRouterOptionsWidth($router_options_width)
    {
        $this->container['router_options_width'] = $router_options_width;

        return $this;
    }

    /**
     * Gets router_options_length
     *
     * @return float
     */
    public function getRouterOptionsLength()
    {
        return $this->container['router_options_length'];
    }

    /**
     * Sets router_options_length
     *
     * @param float $router_options_length router_options_length
     *
     * @return $this
     */
    public function setRouterOptionsLength($router_options_length)
    {
        $this->container['router_options_length'] = $router_options_length;

        return $this;
    }

    /**
     * Gets router_options_hazardous_goods
     *
     * @return string
     */
    public function getRouterOptionsHazardousGoods()
    {
        return $this->container['router_options_hazardous_goods'];
    }

    /**
     * Sets router_options_hazardous_goods
     *
     * @param string $router_options_hazardous_goods router_options_hazardous_goods
     *
     * @return $this
     */
    public function setRouterOptionsHazardousGoods($router_options_hazardous_goods)
    {
        $this->container['router_options_hazardous_goods'] = $router_options_hazardous_goods;

        return $this;
    }

    /**
     * Gets router_options_max_walk_distance
     *
     * @return float
     */
    public function getRouterOptionsMaxWalkDistance()
    {
        return $this->container['router_options_max_walk_distance'];
    }

    /**
     * Sets router_options_max_walk_distance
     *
     * @param float $router_options_max_walk_distance router_options_max_walk_distance
     *
     * @return $this
     */
    public function setRouterOptionsMaxWalkDistance($router_options_max_walk_distance)
    {
        $this->container['router_options_max_walk_distance'] = $router_options_max_walk_distance;

        return $this;
    }

    /**
     * Gets router_options_approach
     *
     * @return string
     */
    public function getRouterOptionsApproach()
    {
        return $this->container['router_options_approach'];
    }

    /**
     * Sets router_options_approach
     *
     * @param string $router_options_approach router_options_approach
     *
     * @return $this
     */
    public function setRouterOptionsApproach($router_options_approach)
    {
        $this->container['router_options_approach'] = $router_options_approach;

        return $this;
    }

    /**
     * Gets router_options_snap
     *
     * @return float
     */
    public function getRouterOptionsSnap()
    {
        return $this->container['router_options_snap'];
    }

    /**
     * Sets router_options_snap
     *
     * @param float $router_options_snap router_options_snap
     *
     * @return $this
     */
    public function setRouterOptionsSnap($router_options_snap)
    {
        $this->container['router_options_snap'] = $router_options_snap;

        return $this;
    }

    /**
     * Gets router_options_strict_restriction
     *
     * @return bool
     */
    public function getRouterOptionsStrictRestriction()
    {
        return $this->container['router_options_strict_restriction'];
    }

    /**
     * Sets router_options_strict_restriction
     *
     * @param bool $router_options_strict_restriction router_options_strict_restriction
     *
     * @return $this
     */
    public function setRouterOptionsStrictRestriction($router_options_strict_restriction)
    {
        $this->container['router_options_strict_restriction'] = $router_options_strict_restriction;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
