<?php
/**
 * Body5
 *
 * PHP version 5
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * API
 *
 * [Simplified view of domain model](https://app.mapotempo.com//api/0.1/Model-simpel.svg). ## Model Model is structured around four majors concepts: the Customer account, Destinations, Vehicles and Plannings. * `Customers`: many of objects are linked to a customer account (relating to the user calling API). The customer has many `Users`, each user has his own `api_key`. Be carefull not to confuse with following model `Destination`, `Customer`can only be created by a admin `User`. * `Destinations`: location points to visit with constraints. The same `Destination` can be visited several times : in this case several `Visits` are associated to one `Destination`. * `Vehicles`: vehicles definition are splited in two parts:  * the structural definition named `Vehicle` (car, truck, bike, consumption, etc.)  * and the vehicle usage `VehicleUsage`, a specific usage of a physical vehicle in a specific context. Vehicles can be used in many contexts called `VehicleUsageSet` (set of all vehicles usages under a context). Multiple values are only available if dedicated option for customer is active. For instance, if customer needs to use its vehicle 2 times per day (morning and evening), he needs 2 `VehicleUsageSet` called \"Morning\" and \"Evening\" : each can have different values defined for stores, rest, etc... `VehicleUsageSet` defines default values for vehicle usage. * `Plannings`: `Planning` is a set of `Routes` to `Visit` `Destinations` with `Vehicle` within a `VehicleUsageSet` context. A route is a track between all destinations reached by a vehicle (a new route is created for each customer's vehicle and a route without vehicle is created for all out-of-route destinations). By default all customer's visites are used in a planning.  ## Technical access ### Swagger descriptor This REST API is described with Swagger. The Swagger descriptor defines the request end-points, the parameters and the return values. The API can be addressed by HTTP request or with a generated client using the Swagger descriptor. ### API key All access to the API are subject to an `api_key` parameter in order to authenticate the user. This parameter can be sent with a query string on each available operation: `https://app.mapotempo.com/api/0.1/{objects}?api_key={your_personal_api_key}` ### Return The API supports several return formats: `json` and `xml` which depend of the requested extension used in url. ### I18n Functionnal textual returns are subject to translation and depend of HTTP header `Accept-Language`. HTTP error codes are not translated. ## Admin acces Using an admin `api_key` switches to advanced opperations (on `Customer`, `User`, `Vehicle`, `Profile`). Most of operations from the current api are usable either for normal user `api_key` or admin user `api_key` (not both). ## More concepts When a customer is created some objects are created by default with this new customer: * `Vehicle`: multiple, depending of the `max_vehicles` defined for customer * `DeliverableUnit`: one default * `VehicleUsageSet`: one default * `VehicleUsage`: multiple, depending of the vehicles number * `Store`: one default  ### Profiles, Layers, Routers `Profile` is a concept which allows to set several parameters for the customer: * `Layer`: which allows to choose the background map * `Router`: which allows to build route's information.  Several default profiles are available and can be listed with an admin `api_key`.  ### Tags `Tag` is a concept to filter visits and create planning only for a subset of visits. For instance, if some visits are tagged \"Monday\", it allows to create a new planning for \"Monday\" tag and use only dedicated visits. ### Zonings `Zoning` is a concept which allows to define multiple `Zones` (areas) around destinatons. A `Zone` can be affected to a `Vehicle` and if it is used into a `Planning`, all `Destinations` inside areas will be affected to the zone's vehicle (or `Route`). A polygon defining a `Zone` can be created outside the application or can be automatically generated from a planning.  ## Code samples * Create and display destinations or visits. Here some samples for these operations: [using PHP](https://app.mapotempo.com//api/0.1/examples/php/example.php), [using Ruby](https://app.mapotempo.com//api/0.1/examples/ruby/example.rb). Note you can import destinations/visits and create a planning at the same time if you know beforehand the route for each destination/visit. See the details of importDestinations operation to import your data and create a planning in only one call. * Same operations are available for stores (note you have an existing default store). * With created destinations/visits, you can create a planning (routes and stops are automatically created depending of yours vehicles and destinations/visits) * In existing planning, you have availability to move stops (which represent visits) on a dedicated route (which represent a dedicated vehicle). * With many unaffected (out-of-route) stops in a planning, you may create a zoning to move many stops in several routes. Create a zoning (you can generate zones in this zoning automatically from automatic clustering), if you apply zoning (containing zones linked to a vehicle) on your planning, all stops contained in different zones will be moved in dedicated routes.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: tech@mapotempo.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.20
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Mapotempo\Model;

use \ArrayAccess;
use \Mapotempo\ObjectSerializer;

/**
 * Body5 Class Doc Comment
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class Body5 implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'body_5';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'replace' => 'bool',
'planning_name' => 'string',
'planning_ref' => 'string',
'planning_date' => 'string',
'planning_vehicle_usage_set_id' => 'int',
'remote' => 'string'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'replace' => null,
'planning_name' => null,
'planning_ref' => null,
'planning_date' => null,
'planning_vehicle_usage_set_id' => 'int32',
'remote' => null    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'replace' => 'replace',
'planning_name' => 'planning[name]',
'planning_ref' => 'planning[ref]',
'planning_date' => 'planning[date]',
'planning_vehicle_usage_set_id' => 'planning[vehicle_usage_set_id]',
'remote' => 'remote'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'replace' => 'setReplace',
'planning_name' => 'setPlanningName',
'planning_ref' => 'setPlanningRef',
'planning_date' => 'setPlanningDate',
'planning_vehicle_usage_set_id' => 'setPlanningVehicleUsageSetId',
'remote' => 'setRemote'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'replace' => 'getReplace',
'planning_name' => 'getPlanningName',
'planning_ref' => 'getPlanningRef',
'planning_date' => 'getPlanningDate',
'planning_vehicle_usage_set_id' => 'getPlanningVehicleUsageSetId',
'remote' => 'getRemote'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    const REMOTE_TOMTOM = 'tomtom';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getRemoteAllowableValues()
    {
        return [
            self::REMOTE_TOMTOM,        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['replace'] = isset($data['replace']) ? $data['replace'] : null;
        $this->container['planning_name'] = isset($data['planning_name']) ? $data['planning_name'] : null;
        $this->container['planning_ref'] = isset($data['planning_ref']) ? $data['planning_ref'] : null;
        $this->container['planning_date'] = isset($data['planning_date']) ? $data['planning_date'] : null;
        $this->container['planning_vehicle_usage_set_id'] = isset($data['planning_vehicle_usage_set_id']) ? $data['planning_vehicle_usage_set_id'] : null;
        $this->container['remote'] = isset($data['remote']) ? $data['remote'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        $allowedValues = $this->getRemoteAllowableValues();
        if (!is_null($this->container['remote']) && !in_array($this->container['remote'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'remote', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets replace
     *
     * @return bool
     */
    public function getReplace()
    {
        return $this->container['replace'];
    }

    /**
     * Sets replace
     *
     * @param bool $replace replace
     *
     * @return $this
     */
    public function setReplace($replace)
    {
        $this->container['replace'] = $replace;

        return $this;
    }

    /**
     * Gets planning_name
     *
     * @return string
     */
    public function getPlanningName()
    {
        return $this->container['planning_name'];
    }

    /**
     * Sets planning_name
     *
     * @param string $planning_name planning_name
     *
     * @return $this
     */
    public function setPlanningName($planning_name)
    {
        $this->container['planning_name'] = $planning_name;

        return $this;
    }

    /**
     * Gets planning_ref
     *
     * @return string
     */
    public function getPlanningRef()
    {
        return $this->container['planning_ref'];
    }

    /**
     * Sets planning_ref
     *
     * @param string $planning_ref planning_ref
     *
     * @return $this
     */
    public function setPlanningRef($planning_ref)
    {
        $this->container['planning_ref'] = $planning_ref;

        return $this;
    }

    /**
     * Gets planning_date
     *
     * @return string
     */
    public function getPlanningDate()
    {
        return $this->container['planning_date'];
    }

    /**
     * Sets planning_date
     *
     * @param string $planning_date planning_date
     *
     * @return $this
     */
    public function setPlanningDate($planning_date)
    {
        $this->container['planning_date'] = $planning_date;

        return $this;
    }

    /**
     * Gets planning_vehicle_usage_set_id
     *
     * @return int
     */
    public function getPlanningVehicleUsageSetId()
    {
        return $this->container['planning_vehicle_usage_set_id'];
    }

    /**
     * Sets planning_vehicle_usage_set_id
     *
     * @param int $planning_vehicle_usage_set_id planning_vehicle_usage_set_id
     *
     * @return $this
     */
    public function setPlanningVehicleUsageSetId($planning_vehicle_usage_set_id)
    {
        $this->container['planning_vehicle_usage_set_id'] = $planning_vehicle_usage_set_id;

        return $this;
    }

    /**
     * Gets remote
     *
     * @return string
     */
    public function getRemote()
    {
        return $this->container['remote'];
    }

    /**
     * Sets remote
     *
     * @param string $remote remote
     *
     * @return $this
     */
    public function setRemote($remote)
    {
        $allowedValues = $this->getRemoteAllowableValues();
        if (!is_null($remote) && !in_array($remote, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'remote', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['remote'] = $remote;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
