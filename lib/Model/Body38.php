<?php
/**
 * Body38
 *
 * PHP version 5
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * API
 *
 * [Simplified view of domain model](https://app.mapotempo.com//api/0.1/Model-simpel.svg). ## Model Model is structured around four majors concepts: the Customer account, Destinations, Vehicles and Plannings. * `Customers`: many of objects are linked to a customer account (relating to the user calling API). The customer has many `Users`, each user has his own `api_key`. Be carefull not to confuse with following model `Destination`, `Customer`can only be created by a admin `User`. * `Destinations`: location points to visit with constraints. The same `Destination` can be visited several times : in this case several `Visits` are associated to one `Destination`. * `Vehicles`: vehicles definition are splited in two parts:  * the structural definition named `Vehicle` (car, truck, bike, consumption, etc.)  * and the vehicle usage `VehicleUsage`, a specific usage of a physical vehicle in a specific context. Vehicles can be used in many contexts called `VehicleUsageSet` (set of all vehicles usages under a context). Multiple values are only available if dedicated option for customer is active. For instance, if customer needs to use its vehicle 2 times per day (morning and evening), he needs 2 `VehicleUsageSet` called \"Morning\" and \"Evening\" : each can have different values defined for stores, rest, etc... `VehicleUsageSet` defines default values for vehicle usage. * `Plannings`: `Planning` is a set of `Routes` to `Visit` `Destinations` with `Vehicle` within a `VehicleUsageSet` context. A route is a track between all destinations reached by a vehicle (a new route is created for each customer's vehicle and a route without vehicle is created for all out-of-route destinations). By default all customer's visites are used in a planning.  ## Technical access ### Swagger descriptor This REST API is described with Swagger. The Swagger descriptor defines the request end-points, the parameters and the return values. The API can be addressed by HTTP request or with a generated client using the Swagger descriptor. ### API key All access to the API are subject to an `api_key` parameter in order to authenticate the user. This parameter can be sent with a query string on each available operation: `https://app.mapotempo.com/api/0.1/{objects}?api_key={your_personal_api_key}` ### Return The API supports several return formats: `json` and `xml` which depend of the requested extension used in url. ### I18n Functionnal textual returns are subject to translation and depend of HTTP header `Accept-Language`. HTTP error codes are not translated. ## Admin acces Using an admin `api_key` switches to advanced opperations (on `Customer`, `User`, `Vehicle`, `Profile`). Most of operations from the current api are usable either for normal user `api_key` or admin user `api_key` (not both). ## More concepts When a customer is created some objects are created by default with this new customer: * `Vehicle`: multiple, depending of the `max_vehicles` defined for customer * `DeliverableUnit`: one default * `VehicleUsageSet`: one default * `VehicleUsage`: multiple, depending of the vehicles number * `Store`: one default  ### Profiles, Layers, Routers `Profile` is a concept which allows to set several parameters for the customer: * `Layer`: which allows to choose the background map * `Router`: which allows to build route's information.  Several default profiles are available and can be listed with an admin `api_key`.  ### Tags `Tag` is a concept to filter visits and create planning only for a subset of visits. For instance, if some visits are tagged \"Monday\", it allows to create a new planning for \"Monday\" tag and use only dedicated visits. ### Zonings `Zoning` is a concept which allows to define multiple `Zones` (areas) around destinatons. A `Zone` can be affected to a `Vehicle` and if it is used into a `Planning`, all `Destinations` inside areas will be affected to the zone's vehicle (or `Route`). A polygon defining a `Zone` can be created outside the application or can be automatically generated from a planning.  ## Code samples * Create and display destinations or visits. Here some samples for these operations: [using PHP](https://app.mapotempo.com//api/0.1/examples/php/example.php), [using Ruby](https://app.mapotempo.com//api/0.1/examples/ruby/example.rb). Note you can import destinations/visits and create a planning at the same time if you know beforehand the route for each destination/visit. See the details of importDestinations operation to import your data and create a planning in only one call. * Same operations are available for stores (note you have an existing default store). * With created destinations/visits, you can create a planning (routes and stops are automatically created depending of yours vehicles and destinations/visits) * In existing planning, you have availability to move stops (which represent visits) on a dedicated route (which represent a dedicated vehicle). * With many unaffected (out-of-route) stops in a planning, you may create a zoning to move many stops in several routes. Create a zoning (you can generate zones in this zoning automatically from automatic clustering), if you apply zoning (containing zones linked to a vehicle) on your planning, all stops contained in different zones will be moved in dedicated routes.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: tech@mapotempo.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.20
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Mapotempo\Model;

use \ArrayAccess;
use \Mapotempo\ObjectSerializer;

/**
 * Body38 Class Doc Comment
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class Body38 implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'body_38';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'customer_id' => 'int',
'ref' => 'string',
'name' => 'string',
'contact_email' => 'string',
'phone_number' => 'string',
'emission' => 'float',
'consumption' => 'int',
'color' => 'string',
'fuel_type' => 'string',
'router_id' => 'int',
'router_dimension' => 'string',
'speed_multiplier' => 'float',
'max_distance' => 'int',
'time_window_start' => 'int',
'time_window_end' => 'int',
'store_start_id' => 'int',
'store_stop_id' => 'int',
'service_time_start' => 'int',
'service_time_end' => 'int',
'work_time' => 'int',
'rest_start' => 'int',
'rest_stop' => 'int',
'rest_duration' => 'int',
'store_rest_id' => 'int',
'active' => 'bool',
'cost' => 'double',
'cost_time' => 'double',
'cost_distance' => 'double',
'capacities_deliverable_unit_id' => 'int',
'capacities_quantity' => 'float',
'router_options_track' => 'bool',
'router_options_motorway' => 'bool',
'router_options_toll' => 'bool',
'router_options_trailers' => 'int',
'router_options_weight' => 'float',
'router_options_weight_per_axle' => 'float',
'router_options_height' => 'float',
'router_options_width' => 'float',
'router_options_length' => 'float',
'router_options_hazardous_goods' => 'string',
'router_options_max_walk_distance' => 'float'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'customer_id' => 'int32',
'ref' => null,
'name' => null,
'contact_email' => null,
'phone_number' => null,
'emission' => 'float',
'consumption' => 'int32',
'color' => null,
'fuel_type' => null,
'router_id' => 'int32',
'router_dimension' => null,
'speed_multiplier' => 'float',
'max_distance' => 'int32',
'time_window_start' => 'int32',
'time_window_end' => 'int32',
'store_start_id' => 'int32',
'store_stop_id' => 'int32',
'service_time_start' => 'int32',
'service_time_end' => 'int32',
'work_time' => 'int32',
'rest_start' => 'int32',
'rest_stop' => 'int32',
'rest_duration' => 'int32',
'store_rest_id' => 'int32',
'active' => null,
'cost' => 'double',
'cost_time' => 'double',
'cost_distance' => 'double',
'capacities_deliverable_unit_id' => 'int32',
'capacities_quantity' => 'float',
'router_options_track' => null,
'router_options_motorway' => null,
'router_options_toll' => null,
'router_options_trailers' => 'int32',
'router_options_weight' => 'float',
'router_options_weight_per_axle' => 'float',
'router_options_height' => 'float',
'router_options_width' => 'float',
'router_options_length' => 'float',
'router_options_hazardous_goods' => null,
'router_options_max_walk_distance' => 'float'    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'customer_id' => 'customer_id',
'ref' => 'ref',
'name' => 'name',
'contact_email' => 'contact_email',
'phone_number' => 'phone_number',
'emission' => 'emission',
'consumption' => 'consumption',
'color' => 'color',
'fuel_type' => 'fuel_type',
'router_id' => 'router_id',
'router_dimension' => 'router_dimension',
'speed_multiplier' => 'speed_multiplier',
'max_distance' => 'max_distance',
'time_window_start' => 'time_window_start',
'time_window_end' => 'time_window_end',
'store_start_id' => 'store_start_id',
'store_stop_id' => 'store_stop_id',
'service_time_start' => 'service_time_start',
'service_time_end' => 'service_time_end',
'work_time' => 'work_time',
'rest_start' => 'rest_start',
'rest_stop' => 'rest_stop',
'rest_duration' => 'rest_duration',
'store_rest_id' => 'store_rest_id',
'active' => 'active',
'cost' => 'cost',
'cost_time' => 'cost_time',
'cost_distance' => 'cost_distance',
'capacities_deliverable_unit_id' => 'capacities[deliverable_unit_id]',
'capacities_quantity' => 'capacities[quantity]',
'router_options_track' => 'router_options[track]',
'router_options_motorway' => 'router_options[motorway]',
'router_options_toll' => 'router_options[toll]',
'router_options_trailers' => 'router_options[trailers]',
'router_options_weight' => 'router_options[weight]',
'router_options_weight_per_axle' => 'router_options[weight_per_axle]',
'router_options_height' => 'router_options[height]',
'router_options_width' => 'router_options[width]',
'router_options_length' => 'router_options[length]',
'router_options_hazardous_goods' => 'router_options[hazardous_goods]',
'router_options_max_walk_distance' => 'router_options[max_walk_distance]'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'customer_id' => 'setCustomerId',
'ref' => 'setRef',
'name' => 'setName',
'contact_email' => 'setContactEmail',
'phone_number' => 'setPhoneNumber',
'emission' => 'setEmission',
'consumption' => 'setConsumption',
'color' => 'setColor',
'fuel_type' => 'setFuelType',
'router_id' => 'setRouterId',
'router_dimension' => 'setRouterDimension',
'speed_multiplier' => 'setSpeedMultiplier',
'max_distance' => 'setMaxDistance',
'time_window_start' => 'setTimeWindowStart',
'time_window_end' => 'setTimeWindowEnd',
'store_start_id' => 'setStoreStartId',
'store_stop_id' => 'setStoreStopId',
'service_time_start' => 'setServiceTimeStart',
'service_time_end' => 'setServiceTimeEnd',
'work_time' => 'setWorkTime',
'rest_start' => 'setRestStart',
'rest_stop' => 'setRestStop',
'rest_duration' => 'setRestDuration',
'store_rest_id' => 'setStoreRestId',
'active' => 'setActive',
'cost' => 'setCost',
'cost_time' => 'setCostTime',
'cost_distance' => 'setCostDistance',
'capacities_deliverable_unit_id' => 'setCapacitiesDeliverableUnitId',
'capacities_quantity' => 'setCapacitiesQuantity',
'router_options_track' => 'setRouterOptionsTrack',
'router_options_motorway' => 'setRouterOptionsMotorway',
'router_options_toll' => 'setRouterOptionsToll',
'router_options_trailers' => 'setRouterOptionsTrailers',
'router_options_weight' => 'setRouterOptionsWeight',
'router_options_weight_per_axle' => 'setRouterOptionsWeightPerAxle',
'router_options_height' => 'setRouterOptionsHeight',
'router_options_width' => 'setRouterOptionsWidth',
'router_options_length' => 'setRouterOptionsLength',
'router_options_hazardous_goods' => 'setRouterOptionsHazardousGoods',
'router_options_max_walk_distance' => 'setRouterOptionsMaxWalkDistance'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'customer_id' => 'getCustomerId',
'ref' => 'getRef',
'name' => 'getName',
'contact_email' => 'getContactEmail',
'phone_number' => 'getPhoneNumber',
'emission' => 'getEmission',
'consumption' => 'getConsumption',
'color' => 'getColor',
'fuel_type' => 'getFuelType',
'router_id' => 'getRouterId',
'router_dimension' => 'getRouterDimension',
'speed_multiplier' => 'getSpeedMultiplier',
'max_distance' => 'getMaxDistance',
'time_window_start' => 'getTimeWindowStart',
'time_window_end' => 'getTimeWindowEnd',
'store_start_id' => 'getStoreStartId',
'store_stop_id' => 'getStoreStopId',
'service_time_start' => 'getServiceTimeStart',
'service_time_end' => 'getServiceTimeEnd',
'work_time' => 'getWorkTime',
'rest_start' => 'getRestStart',
'rest_stop' => 'getRestStop',
'rest_duration' => 'getRestDuration',
'store_rest_id' => 'getStoreRestId',
'active' => 'getActive',
'cost' => 'getCost',
'cost_time' => 'getCostTime',
'cost_distance' => 'getCostDistance',
'capacities_deliverable_unit_id' => 'getCapacitiesDeliverableUnitId',
'capacities_quantity' => 'getCapacitiesQuantity',
'router_options_track' => 'getRouterOptionsTrack',
'router_options_motorway' => 'getRouterOptionsMotorway',
'router_options_toll' => 'getRouterOptionsToll',
'router_options_trailers' => 'getRouterOptionsTrailers',
'router_options_weight' => 'getRouterOptionsWeight',
'router_options_weight_per_axle' => 'getRouterOptionsWeightPerAxle',
'router_options_height' => 'getRouterOptionsHeight',
'router_options_width' => 'getRouterOptionsWidth',
'router_options_length' => 'getRouterOptionsLength',
'router_options_hazardous_goods' => 'getRouterOptionsHazardousGoods',
'router_options_max_walk_distance' => 'getRouterOptionsMaxWalkDistance'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    const ROUTER_DIMENSION_TIME = 'time';
const ROUTER_DIMENSION_DISTANCE = 'distance';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getRouterDimensionAllowableValues()
    {
        return [
            self::ROUTER_DIMENSION_TIME,
self::ROUTER_DIMENSION_DISTANCE,        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['customer_id'] = isset($data['customer_id']) ? $data['customer_id'] : null;
        $this->container['ref'] = isset($data['ref']) ? $data['ref'] : null;
        $this->container['name'] = isset($data['name']) ? $data['name'] : null;
        $this->container['contact_email'] = isset($data['contact_email']) ? $data['contact_email'] : null;
        $this->container['phone_number'] = isset($data['phone_number']) ? $data['phone_number'] : null;
        $this->container['emission'] = isset($data['emission']) ? $data['emission'] : null;
        $this->container['consumption'] = isset($data['consumption']) ? $data['consumption'] : null;
        $this->container['color'] = isset($data['color']) ? $data['color'] : null;
        $this->container['fuel_type'] = isset($data['fuel_type']) ? $data['fuel_type'] : null;
        $this->container['router_id'] = isset($data['router_id']) ? $data['router_id'] : null;
        $this->container['router_dimension'] = isset($data['router_dimension']) ? $data['router_dimension'] : null;
        $this->container['speed_multiplier'] = isset($data['speed_multiplier']) ? $data['speed_multiplier'] : null;
        $this->container['max_distance'] = isset($data['max_distance']) ? $data['max_distance'] : null;
        $this->container['time_window_start'] = isset($data['time_window_start']) ? $data['time_window_start'] : null;
        $this->container['time_window_end'] = isset($data['time_window_end']) ? $data['time_window_end'] : null;
        $this->container['store_start_id'] = isset($data['store_start_id']) ? $data['store_start_id'] : null;
        $this->container['store_stop_id'] = isset($data['store_stop_id']) ? $data['store_stop_id'] : null;
        $this->container['service_time_start'] = isset($data['service_time_start']) ? $data['service_time_start'] : null;
        $this->container['service_time_end'] = isset($data['service_time_end']) ? $data['service_time_end'] : null;
        $this->container['work_time'] = isset($data['work_time']) ? $data['work_time'] : null;
        $this->container['rest_start'] = isset($data['rest_start']) ? $data['rest_start'] : null;
        $this->container['rest_stop'] = isset($data['rest_stop']) ? $data['rest_stop'] : null;
        $this->container['rest_duration'] = isset($data['rest_duration']) ? $data['rest_duration'] : null;
        $this->container['store_rest_id'] = isset($data['store_rest_id']) ? $data['store_rest_id'] : null;
        $this->container['active'] = isset($data['active']) ? $data['active'] : null;
        $this->container['cost'] = isset($data['cost']) ? $data['cost'] : null;
        $this->container['cost_time'] = isset($data['cost_time']) ? $data['cost_time'] : null;
        $this->container['cost_distance'] = isset($data['cost_distance']) ? $data['cost_distance'] : null;
        $this->container['capacities_deliverable_unit_id'] = isset($data['capacities_deliverable_unit_id']) ? $data['capacities_deliverable_unit_id'] : null;
        $this->container['capacities_quantity'] = isset($data['capacities_quantity']) ? $data['capacities_quantity'] : null;
        $this->container['router_options_track'] = isset($data['router_options_track']) ? $data['router_options_track'] : null;
        $this->container['router_options_motorway'] = isset($data['router_options_motorway']) ? $data['router_options_motorway'] : null;
        $this->container['router_options_toll'] = isset($data['router_options_toll']) ? $data['router_options_toll'] : null;
        $this->container['router_options_trailers'] = isset($data['router_options_trailers']) ? $data['router_options_trailers'] : null;
        $this->container['router_options_weight'] = isset($data['router_options_weight']) ? $data['router_options_weight'] : null;
        $this->container['router_options_weight_per_axle'] = isset($data['router_options_weight_per_axle']) ? $data['router_options_weight_per_axle'] : null;
        $this->container['router_options_height'] = isset($data['router_options_height']) ? $data['router_options_height'] : null;
        $this->container['router_options_width'] = isset($data['router_options_width']) ? $data['router_options_width'] : null;
        $this->container['router_options_length'] = isset($data['router_options_length']) ? $data['router_options_length'] : null;
        $this->container['router_options_hazardous_goods'] = isset($data['router_options_hazardous_goods']) ? $data['router_options_hazardous_goods'] : null;
        $this->container['router_options_max_walk_distance'] = isset($data['router_options_max_walk_distance']) ? $data['router_options_max_walk_distance'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['customer_id'] === null) {
            $invalidProperties[] = "'customer_id' can't be null";
        }
        if ($this->container['name'] === null) {
            $invalidProperties[] = "'name' can't be null";
        }
        $allowedValues = $this->getRouterDimensionAllowableValues();
        if (!is_null($this->container['router_dimension']) && !in_array($this->container['router_dimension'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'router_dimension', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets customer_id
     *
     * @return int
     */
    public function getCustomerId()
    {
        return $this->container['customer_id'];
    }

    /**
     * Sets customer_id
     *
     * @param int $customer_id customer_id
     *
     * @return $this
     */
    public function setCustomerId($customer_id)
    {
        $this->container['customer_id'] = $customer_id;

        return $this;
    }

    /**
     * Gets ref
     *
     * @return string
     */
    public function getRef()
    {
        return $this->container['ref'];
    }

    /**
     * Sets ref
     *
     * @param string $ref A free reference, like an external ID from other information system (forbidden characters are ./\\)
     *
     * @return $this
     */
    public function setRef($ref)
    {
        $this->container['ref'] = $ref;

        return $this;
    }

    /**
     * Gets name
     *
     * @return string
     */
    public function getName()
    {
        return $this->container['name'];
    }

    /**
     * Sets name
     *
     * @param string $name name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->container['name'] = $name;

        return $this;
    }

    /**
     * Gets contact_email
     *
     * @return string
     */
    public function getContactEmail()
    {
        return $this->container['contact_email'];
    }

    /**
     * Sets contact_email
     *
     * @param string $contact_email contact_email
     *
     * @return $this
     */
    public function setContactEmail($contact_email)
    {
        $this->container['contact_email'] = $contact_email;

        return $this;
    }

    /**
     * Gets phone_number
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->container['phone_number'];
    }

    /**
     * Sets phone_number
     *
     * @param string $phone_number phone_number
     *
     * @return $this
     */
    public function setPhoneNumber($phone_number)
    {
        $this->container['phone_number'] = $phone_number;

        return $this;
    }

    /**
     * Gets emission
     *
     * @return float
     */
    public function getEmission()
    {
        return $this->container['emission'];
    }

    /**
     * Sets emission
     *
     * @param float $emission emission
     *
     * @return $this
     */
    public function setEmission($emission)
    {
        $this->container['emission'] = $emission;

        return $this;
    }

    /**
     * Gets consumption
     *
     * @return int
     */
    public function getConsumption()
    {
        return $this->container['consumption'];
    }

    /**
     * Sets consumption
     *
     * @param int $consumption consumption
     *
     * @return $this
     */
    public function setConsumption($consumption)
    {
        $this->container['consumption'] = $consumption;

        return $this;
    }

    /**
     * Gets color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->container['color'];
    }

    /**
     * Sets color
     *
     * @param string $color Color code with #. For instance: #FF0000
     *
     * @return $this
     */
    public function setColor($color)
    {
        $this->container['color'] = $color;

        return $this;
    }

    /**
     * Gets fuel_type
     *
     * @return string
     */
    public function getFuelType()
    {
        return $this->container['fuel_type'];
    }

    /**
     * Sets fuel_type
     *
     * @param string $fuel_type fuel_type
     *
     * @return $this
     */
    public function setFuelType($fuel_type)
    {
        $this->container['fuel_type'] = $fuel_type;

        return $this;
    }

    /**
     * Gets router_id
     *
     * @return int
     */
    public function getRouterId()
    {
        return $this->container['router_id'];
    }

    /**
     * Sets router_id
     *
     * @param int $router_id router_id
     *
     * @return $this
     */
    public function setRouterId($router_id)
    {
        $this->container['router_id'] = $router_id;

        return $this;
    }

    /**
     * Gets router_dimension
     *
     * @return string
     */
    public function getRouterDimension()
    {
        return $this->container['router_dimension'];
    }

    /**
     * Sets router_dimension
     *
     * @param string $router_dimension router_dimension
     *
     * @return $this
     */
    public function setRouterDimension($router_dimension)
    {
        $allowedValues = $this->getRouterDimensionAllowableValues();
        if (!is_null($router_dimension) && !in_array($router_dimension, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'router_dimension', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['router_dimension'] = $router_dimension;

        return $this;
    }

    /**
     * Gets speed_multiplier
     *
     * @return float
     */
    public function getSpeedMultiplier()
    {
        return $this->container['speed_multiplier'];
    }

    /**
     * Sets speed_multiplier
     *
     * @param float $speed_multiplier speed_multiplier
     *
     * @return $this
     */
    public function setSpeedMultiplier($speed_multiplier)
    {
        $this->container['speed_multiplier'] = $speed_multiplier;

        return $this;
    }

    /**
     * Gets max_distance
     *
     * @return int
     */
    public function getMaxDistance()
    {
        return $this->container['max_distance'];
    }

    /**
     * Sets max_distance
     *
     * @param int $max_distance Maximum achievable distance in meters
     *
     * @return $this
     */
    public function setMaxDistance($max_distance)
    {
        $this->container['max_distance'] = $max_distance;

        return $this;
    }

    /**
     * Gets time_window_start
     *
     * @return int
     */
    public function getTimeWindowStart()
    {
        return $this->container['time_window_start'];
    }

    /**
     * Sets time_window_start
     *
     * @param int $time_window_start Schedule time (HH:MM:SS) or number of seconds
     *
     * @return $this
     */
    public function setTimeWindowStart($time_window_start)
    {
        $this->container['time_window_start'] = $time_window_start;

        return $this;
    }

    /**
     * Gets time_window_end
     *
     * @return int
     */
    public function getTimeWindowEnd()
    {
        return $this->container['time_window_end'];
    }

    /**
     * Sets time_window_end
     *
     * @param int $time_window_end Schedule time (HH:MM:SS) or number of seconds
     *
     * @return $this
     */
    public function setTimeWindowEnd($time_window_end)
    {
        $this->container['time_window_end'] = $time_window_end;

        return $this;
    }

    /**
     * Gets store_start_id
     *
     * @return int
     */
    public function getStoreStartId()
    {
        return $this->container['store_start_id'];
    }

    /**
     * Sets store_start_id
     *
     * @param int $store_start_id store_start_id
     *
     * @return $this
     */
    public function setStoreStartId($store_start_id)
    {
        $this->container['store_start_id'] = $store_start_id;

        return $this;
    }

    /**
     * Gets store_stop_id
     *
     * @return int
     */
    public function getStoreStopId()
    {
        return $this->container['store_stop_id'];
    }

    /**
     * Sets store_stop_id
     *
     * @param int $store_stop_id store_stop_id
     *
     * @return $this
     */
    public function setStoreStopId($store_stop_id)
    {
        $this->container['store_stop_id'] = $store_stop_id;

        return $this;
    }

    /**
     * Gets service_time_start
     *
     * @return int
     */
    public function getServiceTimeStart()
    {
        return $this->container['service_time_start'];
    }

    /**
     * Sets service_time_start
     *
     * @param int $service_time_start Schedule time (HH:MM:SS) or number of seconds
     *
     * @return $this
     */
    public function setServiceTimeStart($service_time_start)
    {
        $this->container['service_time_start'] = $service_time_start;

        return $this;
    }

    /**
     * Gets service_time_end
     *
     * @return int
     */
    public function getServiceTimeEnd()
    {
        return $this->container['service_time_end'];
    }

    /**
     * Sets service_time_end
     *
     * @param int $service_time_end Schedule time (HH:MM:SS) or number of seconds
     *
     * @return $this
     */
    public function setServiceTimeEnd($service_time_end)
    {
        $this->container['service_time_end'] = $service_time_end;

        return $this;
    }

    /**
     * Gets work_time
     *
     * @return int
     */
    public function getWorkTime()
    {
        return $this->container['work_time'];
    }

    /**
     * Sets work_time
     *
     * @param int $work_time Work time duration (HH:MM:SS) or number of seconds
     *
     * @return $this
     */
    public function setWorkTime($work_time)
    {
        $this->container['work_time'] = $work_time;

        return $this;
    }

    /**
     * Gets rest_start
     *
     * @return int
     */
    public function getRestStart()
    {
        return $this->container['rest_start'];
    }

    /**
     * Sets rest_start
     *
     * @param int $rest_start Schedule time (HH:MM:SS) or number of seconds
     *
     * @return $this
     */
    public function setRestStart($rest_start)
    {
        $this->container['rest_start'] = $rest_start;

        return $this;
    }

    /**
     * Gets rest_stop
     *
     * @return int
     */
    public function getRestStop()
    {
        return $this->container['rest_stop'];
    }

    /**
     * Sets rest_stop
     *
     * @param int $rest_stop Schedule time (HH:MM:SS) or number of seconds
     *
     * @return $this
     */
    public function setRestStop($rest_stop)
    {
        $this->container['rest_stop'] = $rest_stop;

        return $this;
    }

    /**
     * Gets rest_duration
     *
     * @return int
     */
    public function getRestDuration()
    {
        return $this->container['rest_duration'];
    }

    /**
     * Sets rest_duration
     *
     * @param int $rest_duration Rest duration (HH:MM:SS) or number of seconds
     *
     * @return $this
     */
    public function setRestDuration($rest_duration)
    {
        $this->container['rest_duration'] = $rest_duration;

        return $this;
    }

    /**
     * Gets store_rest_id
     *
     * @return int
     */
    public function getStoreRestId()
    {
        return $this->container['store_rest_id'];
    }

    /**
     * Sets store_rest_id
     *
     * @param int $store_rest_id store_rest_id
     *
     * @return $this
     */
    public function setStoreRestId($store_rest_id)
    {
        $this->container['store_rest_id'] = $store_rest_id;

        return $this;
    }

    /**
     * Gets active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->container['active'];
    }

    /**
     * Sets active
     *
     * @param bool $active active
     *
     * @return $this
     */
    public function setActive($active)
    {
        $this->container['active'] = $active;

        return $this;
    }

    /**
     * Gets cost
     *
     * @return double
     */
    public function getCost()
    {
        return $this->container['cost'];
    }

    /**
     * Sets cost
     *
     * @param double $cost Cost of the vehicle plus the cost of the structure
     *
     * @return $this
     */
    public function setCost($cost)
    {
        $this->container['cost'] = $cost;

        return $this;
    }

    /**
     * Gets cost_time
     *
     * @return double
     */
    public function getCostTime()
    {
        return $this->container['cost_time'];
    }

    /**
     * Sets cost_time
     *
     * @param double $cost_time Cost of service per hour
     *
     * @return $this
     */
    public function setCostTime($cost_time)
    {
        $this->container['cost_time'] = $cost_time;

        return $this;
    }

    /**
     * Gets cost_distance
     *
     * @return double
     */
    public function getCostDistance()
    {
        return $this->container['cost_distance'];
    }

    /**
     * Sets cost_distance
     *
     * @param double $cost_distance Kilometre/Miles cost
     *
     * @return $this
     */
    public function setCostDistance($cost_distance)
    {
        $this->container['cost_distance'] = $cost_distance;

        return $this;
    }

    /**
     * Gets capacities_deliverable_unit_id
     *
     * @return int
     */
    public function getCapacitiesDeliverableUnitId()
    {
        return $this->container['capacities_deliverable_unit_id'];
    }

    /**
     * Sets capacities_deliverable_unit_id
     *
     * @param int $capacities_deliverable_unit_id capacities_deliverable_unit_id
     *
     * @return $this
     */
    public function setCapacitiesDeliverableUnitId($capacities_deliverable_unit_id)
    {
        $this->container['capacities_deliverable_unit_id'] = $capacities_deliverable_unit_id;

        return $this;
    }

    /**
     * Gets capacities_quantity
     *
     * @return float
     */
    public function getCapacitiesQuantity()
    {
        return $this->container['capacities_quantity'];
    }

    /**
     * Sets capacities_quantity
     *
     * @param float $capacities_quantity capacities_quantity
     *
     * @return $this
     */
    public function setCapacitiesQuantity($capacities_quantity)
    {
        $this->container['capacities_quantity'] = $capacities_quantity;

        return $this;
    }

    /**
     * Gets router_options_track
     *
     * @return bool
     */
    public function getRouterOptionsTrack()
    {
        return $this->container['router_options_track'];
    }

    /**
     * Sets router_options_track
     *
     * @param bool $router_options_track router_options_track
     *
     * @return $this
     */
    public function setRouterOptionsTrack($router_options_track)
    {
        $this->container['router_options_track'] = $router_options_track;

        return $this;
    }

    /**
     * Gets router_options_motorway
     *
     * @return bool
     */
    public function getRouterOptionsMotorway()
    {
        return $this->container['router_options_motorway'];
    }

    /**
     * Sets router_options_motorway
     *
     * @param bool $router_options_motorway router_options_motorway
     *
     * @return $this
     */
    public function setRouterOptionsMotorway($router_options_motorway)
    {
        $this->container['router_options_motorway'] = $router_options_motorway;

        return $this;
    }

    /**
     * Gets router_options_toll
     *
     * @return bool
     */
    public function getRouterOptionsToll()
    {
        return $this->container['router_options_toll'];
    }

    /**
     * Sets router_options_toll
     *
     * @param bool $router_options_toll router_options_toll
     *
     * @return $this
     */
    public function setRouterOptionsToll($router_options_toll)
    {
        $this->container['router_options_toll'] = $router_options_toll;

        return $this;
    }

    /**
     * Gets router_options_trailers
     *
     * @return int
     */
    public function getRouterOptionsTrailers()
    {
        return $this->container['router_options_trailers'];
    }

    /**
     * Sets router_options_trailers
     *
     * @param int $router_options_trailers router_options_trailers
     *
     * @return $this
     */
    public function setRouterOptionsTrailers($router_options_trailers)
    {
        $this->container['router_options_trailers'] = $router_options_trailers;

        return $this;
    }

    /**
     * Gets router_options_weight
     *
     * @return float
     */
    public function getRouterOptionsWeight()
    {
        return $this->container['router_options_weight'];
    }

    /**
     * Sets router_options_weight
     *
     * @param float $router_options_weight router_options_weight
     *
     * @return $this
     */
    public function setRouterOptionsWeight($router_options_weight)
    {
        $this->container['router_options_weight'] = $router_options_weight;

        return $this;
    }

    /**
     * Gets router_options_weight_per_axle
     *
     * @return float
     */
    public function getRouterOptionsWeightPerAxle()
    {
        return $this->container['router_options_weight_per_axle'];
    }

    /**
     * Sets router_options_weight_per_axle
     *
     * @param float $router_options_weight_per_axle router_options_weight_per_axle
     *
     * @return $this
     */
    public function setRouterOptionsWeightPerAxle($router_options_weight_per_axle)
    {
        $this->container['router_options_weight_per_axle'] = $router_options_weight_per_axle;

        return $this;
    }

    /**
     * Gets router_options_height
     *
     * @return float
     */
    public function getRouterOptionsHeight()
    {
        return $this->container['router_options_height'];
    }

    /**
     * Sets router_options_height
     *
     * @param float $router_options_height router_options_height
     *
     * @return $this
     */
    public function setRouterOptionsHeight($router_options_height)
    {
        $this->container['router_options_height'] = $router_options_height;

        return $this;
    }

    /**
     * Gets router_options_width
     *
     * @return float
     */
    public function getRouterOptionsWidth()
    {
        return $this->container['router_options_width'];
    }

    /**
     * Sets router_options_width
     *
     * @param float $router_options_width router_options_width
     *
     * @return $this
     */
    public function setRouterOptionsWidth($router_options_width)
    {
        $this->container['router_options_width'] = $router_options_width;

        return $this;
    }

    /**
     * Gets router_options_length
     *
     * @return float
     */
    public function getRouterOptionsLength()
    {
        return $this->container['router_options_length'];
    }

    /**
     * Sets router_options_length
     *
     * @param float $router_options_length router_options_length
     *
     * @return $this
     */
    public function setRouterOptionsLength($router_options_length)
    {
        $this->container['router_options_length'] = $router_options_length;

        return $this;
    }

    /**
     * Gets router_options_hazardous_goods
     *
     * @return string
     */
    public function getRouterOptionsHazardousGoods()
    {
        return $this->container['router_options_hazardous_goods'];
    }

    /**
     * Sets router_options_hazardous_goods
     *
     * @param string $router_options_hazardous_goods router_options_hazardous_goods
     *
     * @return $this
     */
    public function setRouterOptionsHazardousGoods($router_options_hazardous_goods)
    {
        $this->container['router_options_hazardous_goods'] = $router_options_hazardous_goods;

        return $this;
    }

    /**
     * Gets router_options_max_walk_distance
     *
     * @return float
     */
    public function getRouterOptionsMaxWalkDistance()
    {
        return $this->container['router_options_max_walk_distance'];
    }

    /**
     * Sets router_options_max_walk_distance
     *
     * @param float $router_options_max_walk_distance router_options_max_walk_distance
     *
     * @return $this
     */
    public function setRouterOptionsMaxWalkDistance($router_options_max_walk_distance)
    {
        $this->container['router_options_max_walk_distance'] = $router_options_max_walk_distance;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
