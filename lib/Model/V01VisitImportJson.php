<?php
/**
 * V01VisitImportJson
 *
 * PHP version 5
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * API
 *
 * [Simplified view of domain model](https://app.mapotempo.com//api/0.1/Model-simpel.svg). ## Model Model is structured around four majors concepts: the Customer account, Destinations, Vehicles and Plannings. * `Customers`: many of objects are linked to a customer account (relating to the user calling API). The customer has many `Users`, each user has his own `api_key`. Be carefull not to confuse with following model `Destination`, `Customer`can only be created by a admin `User`. * `Destinations`: location points to visit with constraints. The same `Destination` can be visited several times : in this case several `Visits` are associated to one `Destination`. * `Vehicles`: vehicles definition are splited in two parts:  * the structural definition named `Vehicle` (car, truck, bike, consumption, etc.)  * and the vehicle usage `VehicleUsage`, a specific usage of a physical vehicle in a specific context. Vehicles can be used in many contexts called `VehicleUsageSet` (set of all vehicles usages under a context). Multiple values are only available if dedicated option for customer is active. For instance, if customer needs to use its vehicle 2 times per day (morning and evening), he needs 2 `VehicleUsageSet` called \"Morning\" and \"Evening\" : each can have different values defined for stores, rest, etc... `VehicleUsageSet` defines default values for vehicle usage. * `Plannings`: `Planning` is a set of `Routes` to `Visit` `Destinations` with `Vehicle` within a `VehicleUsageSet` context. A route is a track between all destinations reached by a vehicle (a new route is created for each customer's vehicle and a route without vehicle is created for all out-of-route destinations). By default all customer's visites are used in a planning.  ## Technical access ### Swagger descriptor This REST API is described with Swagger. The Swagger descriptor defines the request end-points, the parameters and the return values. The API can be addressed by HTTP request or with a generated client using the Swagger descriptor. ### API key All access to the API are subject to an `api_key` parameter in order to authenticate the user. This parameter can be sent with a query string on each available operation: `https://app.mapotempo.com/api/0.1/{objects}?api_key={your_personal_api_key}` ### Return The API supports several return formats: `json` and `xml` which depend of the requested extension used in url. ### I18n Functionnal textual returns are subject to translation and depend of HTTP header `Accept-Language`. HTTP error codes are not translated. ## Admin acces Using an admin `api_key` switches to advanced opperations (on `Customer`, `User`, `Vehicle`, `Profile`). Most of operations from the current api are usable either for normal user `api_key` or admin user `api_key` (not both). ## More concepts When a customer is created some objects are created by default with this new customer: * `Vehicle`: multiple, depending of the `max_vehicles` defined for customer * `DeliverableUnit`: one default * `VehicleUsageSet`: one default * `VehicleUsage`: multiple, depending of the vehicles number * `Store`: one default  ### Profiles, Layers, Routers `Profile` is a concept which allows to set several parameters for the customer: * `Layer`: which allows to choose the background map * `Router`: which allows to build route's information.  Several default profiles are available and can be listed with an admin `api_key`.  ### Tags `Tag` is a concept to filter visits and create planning only for a subset of visits. For instance, if some visits are tagged \"Monday\", it allows to create a new planning for \"Monday\" tag and use only dedicated visits. ### Zonings `Zoning` is a concept which allows to define multiple `Zones` (areas) around destinatons. A `Zone` can be affected to a `Vehicle` and if it is used into a `Planning`, all `Destinations` inside areas will be affected to the zone's vehicle (or `Route`). A polygon defining a `Zone` can be created outside the application or can be automatically generated from a planning.  ## Code samples * Create and display destinations or visits. Here some samples for these operations: [using PHP](https://app.mapotempo.com//api/0.1/examples/php/example.php), [using Ruby](https://app.mapotempo.com//api/0.1/examples/ruby/example.rb). Note you can import destinations/visits and create a planning at the same time if you know beforehand the route for each destination/visit. See the details of importDestinations operation to import your data and create a planning in only one call. * Same operations are available for stores (note you have an existing default store). * With created destinations/visits, you can create a planning (routes and stops are automatically created depending of yours vehicles and destinations/visits) * In existing planning, you have availability to move stops (which represent visits) on a dedicated route (which represent a dedicated vehicle). * With many unaffected (out-of-route) stops in a planning, you may create a zoning to move many stops in several routes. Create a zoning (you can generate zones in this zoning automatically from automatic clustering), if you apply zoning (containing zones linked to a vehicle) on your planning, all stops contained in different zones will be moved in dedicated routes.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: tech@mapotempo.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.20
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Mapotempo\Model;

use \ArrayAccess;
use \Mapotempo\ObjectSerializer;

/**
 * V01VisitImportJson Class Doc Comment
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class V01VisitImportJson implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'V01_VisitImportJson';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'id' => 'int',
'ref' => 'string',
'quantity_default' => 'int',
'time_window_start_1' => 'string',
'time_window_end_1' => 'string',
'time_window_start_2' => 'string',
'time_window_end_2' => 'string',
'duration' => 'string',
'tag_ids' => 'int[]',
'priority' => 'int',
'route' => 'string',
'ref_vehicle' => 'string',
'active' => 'bool'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'id' => 'int32',
'ref' => null,
'quantity_default' => 'int32',
'time_window_start_1' => null,
'time_window_end_1' => null,
'time_window_start_2' => null,
'time_window_end_2' => null,
'duration' => null,
'tag_ids' => 'int32',
'priority' => 'int32',
'route' => null,
'ref_vehicle' => null,
'active' => null    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
'ref' => 'ref',
'quantity_default' => 'quantity_default',
'time_window_start_1' => 'time_window_start_1',
'time_window_end_1' => 'time_window_end_1',
'time_window_start_2' => 'time_window_start_2',
'time_window_end_2' => 'time_window_end_2',
'duration' => 'duration',
'tag_ids' => 'tag_ids',
'priority' => 'priority',
'route' => 'route',
'ref_vehicle' => 'ref_vehicle',
'active' => 'active'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
'ref' => 'setRef',
'quantity_default' => 'setQuantityDefault',
'time_window_start_1' => 'setTimeWindowStart1',
'time_window_end_1' => 'setTimeWindowEnd1',
'time_window_start_2' => 'setTimeWindowStart2',
'time_window_end_2' => 'setTimeWindowEnd2',
'duration' => 'setDuration',
'tag_ids' => 'setTagIds',
'priority' => 'setPriority',
'route' => 'setRoute',
'ref_vehicle' => 'setRefVehicle',
'active' => 'setActive'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
'ref' => 'getRef',
'quantity_default' => 'getQuantityDefault',
'time_window_start_1' => 'getTimeWindowStart1',
'time_window_end_1' => 'getTimeWindowEnd1',
'time_window_start_2' => 'getTimeWindowStart2',
'time_window_end_2' => 'getTimeWindowEnd2',
'duration' => 'getDuration',
'tag_ids' => 'getTagIds',
'priority' => 'getPriority',
'route' => 'getRoute',
'ref_vehicle' => 'getRefVehicle',
'active' => 'getActive'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['ref'] = isset($data['ref']) ? $data['ref'] : null;
        $this->container['quantity_default'] = isset($data['quantity_default']) ? $data['quantity_default'] : null;
        $this->container['time_window_start_1'] = isset($data['time_window_start_1']) ? $data['time_window_start_1'] : null;
        $this->container['time_window_end_1'] = isset($data['time_window_end_1']) ? $data['time_window_end_1'] : null;
        $this->container['time_window_start_2'] = isset($data['time_window_start_2']) ? $data['time_window_start_2'] : null;
        $this->container['time_window_end_2'] = isset($data['time_window_end_2']) ? $data['time_window_end_2'] : null;
        $this->container['duration'] = isset($data['duration']) ? $data['duration'] : null;
        $this->container['tag_ids'] = isset($data['tag_ids']) ? $data['tag_ids'] : null;
        $this->container['priority'] = isset($data['priority']) ? $data['priority'] : null;
        $this->container['route'] = isset($data['route']) ? $data['route'] : null;
        $this->container['ref_vehicle'] = isset($data['ref_vehicle']) ? $data['ref_vehicle'] : null;
        $this->container['active'] = isset($data['active']) ? $data['active'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id
     *
     * @return int
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param int $id id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets ref
     *
     * @return string
     */
    public function getRef()
    {
        return $this->container['ref'];
    }

    /**
     * Sets ref
     *
     * @param string $ref A free reference, like an external ID from other information system (forbidden characters are ./\\)
     *
     * @return $this
     */
    public function setRef($ref)
    {
        $this->container['ref'] = $ref;

        return $this;
    }

    /**
     * Gets quantity_default
     *
     * @return int
     */
    public function getQuantityDefault()
    {
        return $this->container['quantity_default'];
    }

    /**
     * Sets quantity_default
     *
     * @param int $quantity_default Deprecated, use quantities instead.
     *
     * @return $this
     */
    public function setQuantityDefault($quantity_default)
    {
        $this->container['quantity_default'] = $quantity_default;

        return $this;
    }

    /**
     * Gets time_window_start_1
     *
     * @return string
     */
    public function getTimeWindowStart1()
    {
        return $this->container['time_window_start_1'];
    }

    /**
     * Sets time_window_start_1
     *
     * @param string $time_window_start_1 Schedule time (HH:MM:SS) or number of seconds
     *
     * @return $this
     */
    public function setTimeWindowStart1($time_window_start_1)
    {
        $this->container['time_window_start_1'] = $time_window_start_1;

        return $this;
    }

    /**
     * Gets time_window_end_1
     *
     * @return string
     */
    public function getTimeWindowEnd1()
    {
        return $this->container['time_window_end_1'];
    }

    /**
     * Sets time_window_end_1
     *
     * @param string $time_window_end_1 Schedule time (HH:MM:SS) or number of seconds
     *
     * @return $this
     */
    public function setTimeWindowEnd1($time_window_end_1)
    {
        $this->container['time_window_end_1'] = $time_window_end_1;

        return $this;
    }

    /**
     * Gets time_window_start_2
     *
     * @return string
     */
    public function getTimeWindowStart2()
    {
        return $this->container['time_window_start_2'];
    }

    /**
     * Sets time_window_start_2
     *
     * @param string $time_window_start_2 Schedule time (HH:MM:SS) or number of seconds
     *
     * @return $this
     */
    public function setTimeWindowStart2($time_window_start_2)
    {
        $this->container['time_window_start_2'] = $time_window_start_2;

        return $this;
    }

    /**
     * Gets time_window_end_2
     *
     * @return string
     */
    public function getTimeWindowEnd2()
    {
        return $this->container['time_window_end_2'];
    }

    /**
     * Sets time_window_end_2
     *
     * @param string $time_window_end_2 Schedule time (HH:MM:SS) or number of seconds
     *
     * @return $this
     */
    public function setTimeWindowEnd2($time_window_end_2)
    {
        $this->container['time_window_end_2'] = $time_window_end_2;

        return $this;
    }

    /**
     * Gets duration
     *
     * @return string
     */
    public function getDuration()
    {
        return $this->container['duration'];
    }

    /**
     * Sets duration
     *
     * @param string $duration Visit duration (HH:MM:SS) or number of seconds
     *
     * @return $this
     */
    public function setDuration($duration)
    {
        $this->container['duration'] = $duration;

        return $this;
    }

    /**
     * Gets tag_ids
     *
     * @return int[]
     */
    public function getTagIds()
    {
        return $this->container['tag_ids'];
    }

    /**
     * Sets tag_ids
     *
     * @param int[] $tag_ids tag_ids
     *
     * @return $this
     */
    public function setTagIds($tag_ids)
    {
        $this->container['tag_ids'] = $tag_ids;

        return $this;
    }

    /**
     * Gets priority
     *
     * @return int
     */
    public function getPriority()
    {
        return $this->container['priority'];
    }

    /**
     * Sets priority
     *
     * @param int $priority Insertion priority used during optimization in case all visits cannot be planned (-4 to 4, 0 if not defined).
     *
     * @return $this
     */
    public function setPriority($priority)
    {
        $this->container['priority'] = $priority;

        return $this;
    }

    /**
     * Gets route
     *
     * @return string
     */
    public function getRoute()
    {
        return $this->container['route'];
    }

    /**
     * Sets route
     *
     * @param string $route Route reference. If route reference is specified, a new planning will be created with a route using the specified reference
     *
     * @return $this
     */
    public function setRoute($route)
    {
        $this->container['route'] = $route;

        return $this;
    }

    /**
     * Gets ref_vehicle
     *
     * @return string
     */
    public function getRefVehicle()
    {
        return $this->container['ref_vehicle'];
    }

    /**
     * Sets ref_vehicle
     *
     * @param string $ref_vehicle Vehicle reference. If vehicle reference is specified, a new planning will be created with a route using the vehicle with specified reference
     *
     * @return $this
     */
    public function setRefVehicle($ref_vehicle)
    {
        $this->container['ref_vehicle'] = $ref_vehicle;

        return $this;
    }

    /**
     * Gets active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->container['active'];
    }

    /**
     * Sets active
     *
     * @param bool $active In order to specify is stop is active in planning or not
     *
     * @return $this
     */
    public function setActive($active)
    {
        $this->container['active'] = $active;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
