<?php
/**
 * Body14
 *
 * PHP version 5
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * API
 *
 * [Simplified view of domain model](https://app.mapotempo.com//api/0.1/Model-simpel.svg). ## Model Model is structured around four majors concepts: the Customer account, Destinations, Vehicles and Plannings. * `Customers`: many of objects are linked to a customer account (relating to the user calling API). The customer has many `Users`, each user has his own `api_key`. Be carefull not to confuse with following model `Destination`, `Customer`can only be created by a admin `User`. * `Destinations`: location points to visit with constraints. The same `Destination` can be visited several times : in this case several `Visits` are associated to one `Destination`. * `Vehicles`: vehicles definition are splited in two parts:  * the structural definition named `Vehicle` (car, truck, bike, consumption, etc.)  * and the vehicle usage `VehicleUsage`, a specific usage of a physical vehicle in a specific context. Vehicles can be used in many contexts called `VehicleUsageSet` (set of all vehicles usages under a context). Multiple values are only available if dedicated option for customer is active. For instance, if customer needs to use its vehicle 2 times per day (morning and evening), he needs 2 `VehicleUsageSet` called \"Morning\" and \"Evening\" : each can have different values defined for stores, rest, etc... `VehicleUsageSet` defines default values for vehicle usage. * `Plannings`: `Planning` is a set of `Routes` to `Visit` `Destinations` with `Vehicle` within a `VehicleUsageSet` context. A route is a track between all destinations reached by a vehicle (a new route is created for each customer's vehicle and a route without vehicle is created for all out-of-route destinations). By default all customer's visites are used in a planning.  ## Technical access ### Swagger descriptor This REST API is described with Swagger. The Swagger descriptor defines the request end-points, the parameters and the return values. The API can be addressed by HTTP request or with a generated client using the Swagger descriptor. ### API key All access to the API are subject to an `api_key` parameter in order to authenticate the user. This parameter can be sent with a query string on each available operation: `https://app.mapotempo.com/api/0.1/{objects}?api_key={your_personal_api_key}` ### Return The API supports several return formats: `json` and `xml` which depend of the requested extension used in url. ### I18n Functionnal textual returns are subject to translation and depend of HTTP header `Accept-Language`. HTTP error codes are not translated. ## Admin acces Using an admin `api_key` switches to advanced opperations (on `Customer`, `User`, `Vehicle`, `Profile`). Most of operations from the current api are usable either for normal user `api_key` or admin user `api_key` (not both). ## More concepts When a customer is created some objects are created by default with this new customer: * `Vehicle`: multiple, depending of the `max_vehicles` defined for customer * `DeliverableUnit`: one default * `VehicleUsageSet`: one default * `VehicleUsage`: multiple, depending of the vehicles number * `Store`: one default  ### Profiles, Layers, Routers `Profile` is a concept which allows to set several parameters for the customer: * `Layer`: which allows to choose the background map * `Router`: which allows to build route's information.  Several default profiles are available and can be listed with an admin `api_key`.  ### Tags `Tag` is a concept to filter visits and create planning only for a subset of visits. For instance, if some visits are tagged \"Monday\", it allows to create a new planning for \"Monday\" tag and use only dedicated visits. ### Zonings `Zoning` is a concept which allows to define multiple `Zones` (areas) around destinatons. A `Zone` can be affected to a `Vehicle` and if it is used into a `Planning`, all `Destinations` inside areas will be affected to the zone's vehicle (or `Route`). A polygon defining a `Zone` can be created outside the application or can be automatically generated from a planning.  ## Code samples * Create and display destinations or visits. Here some samples for these operations: [using PHP](https://app.mapotempo.com//api/0.1/examples/php/example.php), [using Ruby](https://app.mapotempo.com//api/0.1/examples/ruby/example.rb). Note you can import destinations/visits and create a planning at the same time if you know beforehand the route for each destination/visit. See the details of importDestinations operation to import your data and create a planning in only one call. * Same operations are available for stores (note you have an existing default store). * With created destinations/visits, you can create a planning (routes and stops are automatically created depending of yours vehicles and destinations/visits) * In existing planning, you have availability to move stops (which represent visits) on a dedicated route (which represent a dedicated vehicle). * With many unaffected (out-of-route) stops in a planning, you may create a zoning to move many stops in several routes. Create a zoning (you can generate zones in this zoning automatically from automatic clustering), if you apply zoning (containing zones linked to a vehicle) on your planning, all stops contained in different zones will be moved in dedicated routes.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: tech@mapotempo.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.20
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Mapotempo\Model;

use \ArrayAccess;
use \Mapotempo\ObjectSerializer;

/**
 * Body14 Class Doc Comment
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class Body14 implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'body_14';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'ref' => 'string',
'name' => 'string',
'date' => '\DateTime',
'begin_date' => '\DateTime',
'end_date' => '\DateTime',
'active' => 'bool',
'vehicle_usage_set_id' => 'int',
'zoning_ids' => 'int[]',
'tag_operation' => 'string',
'with_geojson' => 'string'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'ref' => null,
'name' => null,
'date' => 'date',
'begin_date' => 'date',
'end_date' => 'date',
'active' => null,
'vehicle_usage_set_id' => 'int32',
'zoning_ids' => 'int32',
'tag_operation' => null,
'with_geojson' => null    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'ref' => 'ref',
'name' => 'name',
'date' => 'date',
'begin_date' => 'begin_date',
'end_date' => 'end_date',
'active' => 'active',
'vehicle_usage_set_id' => 'vehicle_usage_set_id',
'zoning_ids' => 'zoning_ids',
'tag_operation' => 'tag_operation',
'with_geojson' => 'with_geojson'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'ref' => 'setRef',
'name' => 'setName',
'date' => 'setDate',
'begin_date' => 'setBeginDate',
'end_date' => 'setEndDate',
'active' => 'setActive',
'vehicle_usage_set_id' => 'setVehicleUsageSetId',
'zoning_ids' => 'setZoningIds',
'tag_operation' => 'setTagOperation',
'with_geojson' => 'setWithGeojson'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'ref' => 'getRef',
'name' => 'getName',
'date' => 'getDate',
'begin_date' => 'getBeginDate',
'end_date' => 'getEndDate',
'active' => 'getActive',
'vehicle_usage_set_id' => 'getVehicleUsageSetId',
'zoning_ids' => 'getZoningIds',
'tag_operation' => 'getTagOperation',
'with_geojson' => 'getWithGeojson'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    const TAG_OPERATION__AND = 'and';
const TAG_OPERATION__OR = 'or';
const WITH_GEOJSON_TRUE = 'true';
const WITH_GEOJSON_FALSE = 'false';
const WITH_GEOJSON_POINT = 'point';
const WITH_GEOJSON_POLYLINE = 'polyline';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getTagOperationAllowableValues()
    {
        return [
            self::TAG_OPERATION__AND,
self::TAG_OPERATION__OR,        ];
    }
    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getWithGeojsonAllowableValues()
    {
        return [
            self::WITH_GEOJSON_TRUE,
self::WITH_GEOJSON_FALSE,
self::WITH_GEOJSON_POINT,
self::WITH_GEOJSON_POLYLINE,        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['ref'] = isset($data['ref']) ? $data['ref'] : null;
        $this->container['name'] = isset($data['name']) ? $data['name'] : null;
        $this->container['date'] = isset($data['date']) ? $data['date'] : null;
        $this->container['begin_date'] = isset($data['begin_date']) ? $data['begin_date'] : null;
        $this->container['end_date'] = isset($data['end_date']) ? $data['end_date'] : null;
        $this->container['active'] = isset($data['active']) ? $data['active'] : true;
        $this->container['vehicle_usage_set_id'] = isset($data['vehicle_usage_set_id']) ? $data['vehicle_usage_set_id'] : null;
        $this->container['zoning_ids'] = isset($data['zoning_ids']) ? $data['zoning_ids'] : null;
        $this->container['tag_operation'] = isset($data['tag_operation']) ? $data['tag_operation'] : 'and';
        $this->container['with_geojson'] = isset($data['with_geojson']) ? $data['with_geojson'] : 'false';
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['name'] === null) {
            $invalidProperties[] = "'name' can't be null";
        }
        if ($this->container['vehicle_usage_set_id'] === null) {
            $invalidProperties[] = "'vehicle_usage_set_id' can't be null";
        }
        $allowedValues = $this->getTagOperationAllowableValues();
        if (!is_null($this->container['tag_operation']) && !in_array($this->container['tag_operation'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'tag_operation', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        $allowedValues = $this->getWithGeojsonAllowableValues();
        if (!is_null($this->container['with_geojson']) && !in_array($this->container['with_geojson'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'with_geojson', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets ref
     *
     * @return string
     */
    public function getRef()
    {
        return $this->container['ref'];
    }

    /**
     * Sets ref
     *
     * @param string $ref A free reference, like an external ID from other information system (forbidden characters are ./\\)
     *
     * @return $this
     */
    public function setRef($ref)
    {
        $this->container['ref'] = $ref;

        return $this;
    }

    /**
     * Gets name
     *
     * @return string
     */
    public function getName()
    {
        return $this->container['name'];
    }

    /**
     * Sets name
     *
     * @param string $name name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->container['name'] = $name;

        return $this;
    }

    /**
     * Gets date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->container['date'];
    }

    /**
     * Sets date
     *
     * @param \DateTime $date date
     *
     * @return $this
     */
    public function setDate($date)
    {
        $this->container['date'] = $date;

        return $this;
    }

    /**
     * Gets begin_date
     *
     * @return \DateTime
     */
    public function getBeginDate()
    {
        return $this->container['begin_date'];
    }

    /**
     * Sets begin_date
     *
     * @param \DateTime $begin_date Begin validity period
     *
     * @return $this
     */
    public function setBeginDate($begin_date)
    {
        $this->container['begin_date'] = $begin_date;

        return $this;
    }

    /**
     * Gets end_date
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->container['end_date'];
    }

    /**
     * Sets end_date
     *
     * @param \DateTime $end_date End validity period
     *
     * @return $this
     */
    public function setEndDate($end_date)
    {
        $this->container['end_date'] = $end_date;

        return $this;
    }

    /**
     * Gets active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->container['active'];
    }

    /**
     * Sets active
     *
     * @param bool $active active
     *
     * @return $this
     */
    public function setActive($active)
    {
        $this->container['active'] = $active;

        return $this;
    }

    /**
     * Gets vehicle_usage_set_id
     *
     * @return int
     */
    public function getVehicleUsageSetId()
    {
        return $this->container['vehicle_usage_set_id'];
    }

    /**
     * Sets vehicle_usage_set_id
     *
     * @param int $vehicle_usage_set_id vehicle_usage_set_id
     *
     * @return $this
     */
    public function setVehicleUsageSetId($vehicle_usage_set_id)
    {
        $this->container['vehicle_usage_set_id'] = $vehicle_usage_set_id;

        return $this;
    }

    /**
     * Gets zoning_ids
     *
     * @return int[]
     */
    public function getZoningIds()
    {
        return $this->container['zoning_ids'];
    }

    /**
     * Sets zoning_ids
     *
     * @param int[] $zoning_ids If a new zoning is specified before planning save, all visits will be affected to vehicles specified in zones.
     *
     * @return $this
     */
    public function setZoningIds($zoning_ids)
    {
        $this->container['zoning_ids'] = $zoning_ids;

        return $this;
    }

    /**
     * Gets tag_operation
     *
     * @return string
     */
    public function getTagOperation()
    {
        return $this->container['tag_operation'];
    }

    /**
     * Sets tag_operation
     *
     * @param string $tag_operation Choose how to use selected tags: `and` (for visits with all tags, by default) / `or` (for visits with at least one tag).
     *
     * @return $this
     */
    public function setTagOperation($tag_operation)
    {
        $allowedValues = $this->getTagOperationAllowableValues();
        if (!is_null($tag_operation) && !in_array($tag_operation, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'tag_operation', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['tag_operation'] = $tag_operation;

        return $this;
    }

    /**
     * Gets with_geojson
     *
     * @return string
     */
    public function getWithGeojson()
    {
        return $this->container['with_geojson'];
    }

    /**
     * Sets with_geojson
     *
     * @param string $with_geojson Fill in response the geojson field with route geometry: `point` to return only points, `polyline` to return with encoded linestring, `true` to return both.
     *
     * @return $this
     */
    public function setWithGeojson($with_geojson)
    {
        $allowedValues = $this->getWithGeojsonAllowableValues();
        if (!is_null($with_geojson) && !in_array($with_geojson, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'with_geojson', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['with_geojson'] = $with_geojson;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
