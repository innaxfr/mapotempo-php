<?php
/**
 * V01RouteStatus
 *
 * PHP version 5
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * API
 *
 * [Simplified view of domain model](https://app.mapotempo.com//api/0.1/Model-simpel.svg). ## Model Model is structured around four majors concepts: the Customer account, Destinations, Vehicles and Plannings. * `Customers`: many of objects are linked to a customer account (relating to the user calling API). The customer has many `Users`, each user has his own `api_key`. Be carefull not to confuse with following model `Destination`, `Customer`can only be created by a admin `User`. * `Destinations`: location points to visit with constraints. The same `Destination` can be visited several times : in this case several `Visits` are associated to one `Destination`. * `Vehicles`: vehicles definition are splited in two parts:  * the structural definition named `Vehicle` (car, truck, bike, consumption, etc.)  * and the vehicle usage `VehicleUsage`, a specific usage of a physical vehicle in a specific context. Vehicles can be used in many contexts called `VehicleUsageSet` (set of all vehicles usages under a context). Multiple values are only available if dedicated option for customer is active. For instance, if customer needs to use its vehicle 2 times per day (morning and evening), he needs 2 `VehicleUsageSet` called \"Morning\" and \"Evening\" : each can have different values defined for stores, rest, etc... `VehicleUsageSet` defines default values for vehicle usage. * `Plannings`: `Planning` is a set of `Routes` to `Visit` `Destinations` with `Vehicle` within a `VehicleUsageSet` context. A route is a track between all destinations reached by a vehicle (a new route is created for each customer's vehicle and a route without vehicle is created for all out-of-route destinations). By default all customer's visites are used in a planning.  ## Technical access ### Swagger descriptor This REST API is described with Swagger. The Swagger descriptor defines the request end-points, the parameters and the return values. The API can be addressed by HTTP request or with a generated client using the Swagger descriptor. ### API key All access to the API are subject to an `api_key` parameter in order to authenticate the user. This parameter can be sent with a query string on each available operation: `https://app.mapotempo.com/api/0.1/{objects}?api_key={your_personal_api_key}` ### Return The API supports several return formats: `json` and `xml` which depend of the requested extension used in url. ### I18n Functionnal textual returns are subject to translation and depend of HTTP header `Accept-Language`. HTTP error codes are not translated. ## Admin acces Using an admin `api_key` switches to advanced opperations (on `Customer`, `User`, `Vehicle`, `Profile`). Most of operations from the current api are usable either for normal user `api_key` or admin user `api_key` (not both). ## More concepts When a customer is created some objects are created by default with this new customer: * `Vehicle`: multiple, depending of the `max_vehicles` defined for customer * `DeliverableUnit`: one default * `VehicleUsageSet`: one default * `VehicleUsage`: multiple, depending of the vehicles number * `Store`: one default  ### Profiles, Layers, Routers `Profile` is a concept which allows to set several parameters for the customer: * `Layer`: which allows to choose the background map * `Router`: which allows to build route's information.  Several default profiles are available and can be listed with an admin `api_key`.  ### Tags `Tag` is a concept to filter visits and create planning only for a subset of visits. For instance, if some visits are tagged \"Monday\", it allows to create a new planning for \"Monday\" tag and use only dedicated visits. ### Zonings `Zoning` is a concept which allows to define multiple `Zones` (areas) around destinatons. A `Zone` can be affected to a `Vehicle` and if it is used into a `Planning`, all `Destinations` inside areas will be affected to the zone's vehicle (or `Route`). A polygon defining a `Zone` can be created outside the application or can be automatically generated from a planning.  ## Code samples * Create and display destinations or visits. Here some samples for these operations: [using PHP](https://app.mapotempo.com//api/0.1/examples/php/example.php), [using Ruby](https://app.mapotempo.com//api/0.1/examples/ruby/example.rb). Note you can import destinations/visits and create a planning at the same time if you know beforehand the route for each destination/visit. See the details of importDestinations operation to import your data and create a planning in only one call. * Same operations are available for stores (note you have an existing default store). * With created destinations/visits, you can create a planning (routes and stops are automatically created depending of yours vehicles and destinations/visits) * In existing planning, you have availability to move stops (which represent visits) on a dedicated route (which represent a dedicated vehicle). * With many unaffected (out-of-route) stops in a planning, you may create a zoning to move many stops in several routes. Create a zoning (you can generate zones in this zoning automatically from automatic clustering), if you apply zoning (containing zones linked to a vehicle) on your planning, all stops contained in different zones will be moved in dedicated routes.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: tech@mapotempo.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.20
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Mapotempo\Model;

use \ArrayAccess;
use \Mapotempo\ObjectSerializer;

/**
 * V01RouteStatus Class Doc Comment
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class V01RouteStatus implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'V01_RouteStatus';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'id' => 'int',
'vehicle_usage_id' => 'int',
'last_sent_to' => 'string',
'last_sent_at' => '\DateTime',
'quantities' => '\Mapotempo\Model\V01DeliverableUnitQuantity[]',
'departure_status' => 'string',
'departure_status_code' => 'string',
'departure_eta' => '\DateTime',
'departure_eta_formated' => '\DateTime',
'arrival_status' => 'string',
'arrival_status_code' => 'string',
'arrival_eta' => '\DateTime',
'arrival_eta_formated' => '\DateTime',
'stops' => '\Mapotempo\Model\V01StopStatus[]'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'id' => 'int32',
'vehicle_usage_id' => 'int32',
'last_sent_to' => null,
'last_sent_at' => 'date-time',
'quantities' => null,
'departure_status' => null,
'departure_status_code' => null,
'departure_eta' => 'date-time',
'departure_eta_formated' => 'date-time',
'arrival_status' => null,
'arrival_status_code' => null,
'arrival_eta' => 'date-time',
'arrival_eta_formated' => 'date-time',
'stops' => null    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
'vehicle_usage_id' => 'vehicle_usage_id',
'last_sent_to' => 'last_sent_to',
'last_sent_at' => 'last_sent_at',
'quantities' => 'quantities',
'departure_status' => 'departure_status',
'departure_status_code' => 'departure_status_code',
'departure_eta' => 'departure_eta',
'departure_eta_formated' => 'departure_eta_formated',
'arrival_status' => 'arrival_status',
'arrival_status_code' => 'arrival_status_code',
'arrival_eta' => 'arrival_eta',
'arrival_eta_formated' => 'arrival_eta_formated',
'stops' => 'stops'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
'vehicle_usage_id' => 'setVehicleUsageId',
'last_sent_to' => 'setLastSentTo',
'last_sent_at' => 'setLastSentAt',
'quantities' => 'setQuantities',
'departure_status' => 'setDepartureStatus',
'departure_status_code' => 'setDepartureStatusCode',
'departure_eta' => 'setDepartureEta',
'departure_eta_formated' => 'setDepartureEtaFormated',
'arrival_status' => 'setArrivalStatus',
'arrival_status_code' => 'setArrivalStatusCode',
'arrival_eta' => 'setArrivalEta',
'arrival_eta_formated' => 'setArrivalEtaFormated',
'stops' => 'setStops'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
'vehicle_usage_id' => 'getVehicleUsageId',
'last_sent_to' => 'getLastSentTo',
'last_sent_at' => 'getLastSentAt',
'quantities' => 'getQuantities',
'departure_status' => 'getDepartureStatus',
'departure_status_code' => 'getDepartureStatusCode',
'departure_eta' => 'getDepartureEta',
'departure_eta_formated' => 'getDepartureEtaFormated',
'arrival_status' => 'getArrivalStatus',
'arrival_status_code' => 'getArrivalStatusCode',
'arrival_eta' => 'getArrivalEta',
'arrival_eta_formated' => 'getArrivalEtaFormated',
'stops' => 'getStops'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['vehicle_usage_id'] = isset($data['vehicle_usage_id']) ? $data['vehicle_usage_id'] : null;
        $this->container['last_sent_to'] = isset($data['last_sent_to']) ? $data['last_sent_to'] : null;
        $this->container['last_sent_at'] = isset($data['last_sent_at']) ? $data['last_sent_at'] : null;
        $this->container['quantities'] = isset($data['quantities']) ? $data['quantities'] : null;
        $this->container['departure_status'] = isset($data['departure_status']) ? $data['departure_status'] : null;
        $this->container['departure_status_code'] = isset($data['departure_status_code']) ? $data['departure_status_code'] : null;
        $this->container['departure_eta'] = isset($data['departure_eta']) ? $data['departure_eta'] : null;
        $this->container['departure_eta_formated'] = isset($data['departure_eta_formated']) ? $data['departure_eta_formated'] : null;
        $this->container['arrival_status'] = isset($data['arrival_status']) ? $data['arrival_status'] : null;
        $this->container['arrival_status_code'] = isset($data['arrival_status_code']) ? $data['arrival_status_code'] : null;
        $this->container['arrival_eta'] = isset($data['arrival_eta']) ? $data['arrival_eta'] : null;
        $this->container['arrival_eta_formated'] = isset($data['arrival_eta_formated']) ? $data['arrival_eta_formated'] : null;
        $this->container['stops'] = isset($data['stops']) ? $data['stops'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id
     *
     * @return int
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param int $id id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets vehicle_usage_id
     *
     * @return int
     */
    public function getVehicleUsageId()
    {
        return $this->container['vehicle_usage_id'];
    }

    /**
     * Sets vehicle_usage_id
     *
     * @param int $vehicle_usage_id vehicle_usage_id
     *
     * @return $this
     */
    public function setVehicleUsageId($vehicle_usage_id)
    {
        $this->container['vehicle_usage_id'] = $vehicle_usage_id;

        return $this;
    }

    /**
     * Gets last_sent_to
     *
     * @return string
     */
    public function getLastSentTo()
    {
        return $this->container['last_sent_to'];
    }

    /**
     * Sets last_sent_to
     *
     * @param string $last_sent_to Type GPS Device of Last Sent.
     *
     * @return $this
     */
    public function setLastSentTo($last_sent_to)
    {
        $this->container['last_sent_to'] = $last_sent_to;

        return $this;
    }

    /**
     * Gets last_sent_at
     *
     * @return \DateTime
     */
    public function getLastSentAt()
    {
        return $this->container['last_sent_at'];
    }

    /**
     * Sets last_sent_at
     *
     * @param \DateTime $last_sent_at Last Time Sent To External GPS Device.
     *
     * @return $this
     */
    public function setLastSentAt($last_sent_at)
    {
        $this->container['last_sent_at'] = $last_sent_at;

        return $this;
    }

    /**
     * Gets quantities
     *
     * @return \Mapotempo\Model\V01DeliverableUnitQuantity[]
     */
    public function getQuantities()
    {
        return $this->container['quantities'];
    }

    /**
     * Sets quantities
     *
     * @param \Mapotempo\Model\V01DeliverableUnitQuantity[] $quantities quantities
     *
     * @return $this
     */
    public function setQuantities($quantities)
    {
        $this->container['quantities'] = $quantities;

        return $this;
    }

    /**
     * Gets departure_status
     *
     * @return string
     */
    public function getDepartureStatus()
    {
        return $this->container['departure_status'];
    }

    /**
     * Sets departure_status
     *
     * @param string $departure_status Departure status of start store.
     *
     * @return $this
     */
    public function setDepartureStatus($departure_status)
    {
        $this->container['departure_status'] = $departure_status;

        return $this;
    }

    /**
     * Gets departure_status_code
     *
     * @return string
     */
    public function getDepartureStatusCode()
    {
        return $this->container['departure_status_code'];
    }

    /**
     * Sets departure_status_code
     *
     * @param string $departure_status_code Status code of start store.
     *
     * @return $this
     */
    public function setDepartureStatusCode($departure_status_code)
    {
        $this->container['departure_status_code'] = $departure_status_code;

        return $this;
    }

    /**
     * Gets departure_eta
     *
     * @return \DateTime
     */
    public function getDepartureEta()
    {
        return $this->container['departure_eta'];
    }

    /**
     * Sets departure_eta
     *
     * @param \DateTime $departure_eta Estimated time of departure from remote device.
     *
     * @return $this
     */
    public function setDepartureEta($departure_eta)
    {
        $this->container['departure_eta'] = $departure_eta;

        return $this;
    }

    /**
     * Gets departure_eta_formated
     *
     * @return \DateTime
     */
    public function getDepartureEtaFormated()
    {
        return $this->container['departure_eta_formated'];
    }

    /**
     * Sets departure_eta_formated
     *
     * @param \DateTime $departure_eta_formated Estimated time of departure from remote device.
     *
     * @return $this
     */
    public function setDepartureEtaFormated($departure_eta_formated)
    {
        $this->container['departure_eta_formated'] = $departure_eta_formated;

        return $this;
    }

    /**
     * Gets arrival_status
     *
     * @return string
     */
    public function getArrivalStatus()
    {
        return $this->container['arrival_status'];
    }

    /**
     * Sets arrival_status
     *
     * @param string $arrival_status Arrival status of stop store.
     *
     * @return $this
     */
    public function setArrivalStatus($arrival_status)
    {
        $this->container['arrival_status'] = $arrival_status;

        return $this;
    }

    /**
     * Gets arrival_status_code
     *
     * @return string
     */
    public function getArrivalStatusCode()
    {
        return $this->container['arrival_status_code'];
    }

    /**
     * Sets arrival_status_code
     *
     * @param string $arrival_status_code Status code of stop store.
     *
     * @return $this
     */
    public function setArrivalStatusCode($arrival_status_code)
    {
        $this->container['arrival_status_code'] = $arrival_status_code;

        return $this;
    }

    /**
     * Gets arrival_eta
     *
     * @return \DateTime
     */
    public function getArrivalEta()
    {
        return $this->container['arrival_eta'];
    }

    /**
     * Sets arrival_eta
     *
     * @param \DateTime $arrival_eta Estimated time of arrival from remote device.
     *
     * @return $this
     */
    public function setArrivalEta($arrival_eta)
    {
        $this->container['arrival_eta'] = $arrival_eta;

        return $this;
    }

    /**
     * Gets arrival_eta_formated
     *
     * @return \DateTime
     */
    public function getArrivalEtaFormated()
    {
        return $this->container['arrival_eta_formated'];
    }

    /**
     * Sets arrival_eta_formated
     *
     * @param \DateTime $arrival_eta_formated Estimated time of arrival from remote device.
     *
     * @return $this
     */
    public function setArrivalEtaFormated($arrival_eta_formated)
    {
        $this->container['arrival_eta_formated'] = $arrival_eta_formated;

        return $this;
    }

    /**
     * Gets stops
     *
     * @return \Mapotempo\Model\V01StopStatus[]
     */
    public function getStops()
    {
        return $this->container['stops'];
    }

    /**
     * Sets stops
     *
     * @param \Mapotempo\Model\V01StopStatus[] $stops stops
     *
     * @return $this
     */
    public function setStops($stops)
    {
        $this->container['stops'] = $stops;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
