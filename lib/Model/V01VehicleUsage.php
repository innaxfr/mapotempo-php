<?php
/**
 * V01VehicleUsage
 *
 * PHP version 5
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * API
 *
 * [Simplified view of domain model](https://app.mapotempo.com//api/0.1/Model-simpel.svg). ## Model Model is structured around four majors concepts: the Customer account, Destinations, Vehicles and Plannings. * `Customers`: many of objects are linked to a customer account (relating to the user calling API). The customer has many `Users`, each user has his own `api_key`. Be carefull not to confuse with following model `Destination`, `Customer`can only be created by a admin `User`. * `Destinations`: location points to visit with constraints. The same `Destination` can be visited several times : in this case several `Visits` are associated to one `Destination`. * `Vehicles`: vehicles definition are splited in two parts:  * the structural definition named `Vehicle` (car, truck, bike, consumption, etc.)  * and the vehicle usage `VehicleUsage`, a specific usage of a physical vehicle in a specific context. Vehicles can be used in many contexts called `VehicleUsageSet` (set of all vehicles usages under a context). Multiple values are only available if dedicated option for customer is active. For instance, if customer needs to use its vehicle 2 times per day (morning and evening), he needs 2 `VehicleUsageSet` called \"Morning\" and \"Evening\" : each can have different values defined for stores, rest, etc... `VehicleUsageSet` defines default values for vehicle usage. * `Plannings`: `Planning` is a set of `Routes` to `Visit` `Destinations` with `Vehicle` within a `VehicleUsageSet` context. A route is a track between all destinations reached by a vehicle (a new route is created for each customer's vehicle and a route without vehicle is created for all out-of-route destinations). By default all customer's visites are used in a planning.  ## Technical access ### Swagger descriptor This REST API is described with Swagger. The Swagger descriptor defines the request end-points, the parameters and the return values. The API can be addressed by HTTP request or with a generated client using the Swagger descriptor. ### API key All access to the API are subject to an `api_key` parameter in order to authenticate the user. This parameter can be sent with a query string on each available operation: `https://app.mapotempo.com/api/0.1/{objects}?api_key={your_personal_api_key}` ### Return The API supports several return formats: `json` and `xml` which depend of the requested extension used in url. ### I18n Functionnal textual returns are subject to translation and depend of HTTP header `Accept-Language`. HTTP error codes are not translated. ## Admin acces Using an admin `api_key` switches to advanced opperations (on `Customer`, `User`, `Vehicle`, `Profile`). Most of operations from the current api are usable either for normal user `api_key` or admin user `api_key` (not both). ## More concepts When a customer is created some objects are created by default with this new customer: * `Vehicle`: multiple, depending of the `max_vehicles` defined for customer * `DeliverableUnit`: one default * `VehicleUsageSet`: one default * `VehicleUsage`: multiple, depending of the vehicles number * `Store`: one default  ### Profiles, Layers, Routers `Profile` is a concept which allows to set several parameters for the customer: * `Layer`: which allows to choose the background map * `Router`: which allows to build route's information.  Several default profiles are available and can be listed with an admin `api_key`.  ### Tags `Tag` is a concept to filter visits and create planning only for a subset of visits. For instance, if some visits are tagged \"Monday\", it allows to create a new planning for \"Monday\" tag and use only dedicated visits. ### Zonings `Zoning` is a concept which allows to define multiple `Zones` (areas) around destinatons. A `Zone` can be affected to a `Vehicle` and if it is used into a `Planning`, all `Destinations` inside areas will be affected to the zone's vehicle (or `Route`). A polygon defining a `Zone` can be created outside the application or can be automatically generated from a planning.  ## Code samples * Create and display destinations or visits. Here some samples for these operations: [using PHP](https://app.mapotempo.com//api/0.1/examples/php/example.php), [using Ruby](https://app.mapotempo.com//api/0.1/examples/ruby/example.rb). Note you can import destinations/visits and create a planning at the same time if you know beforehand the route for each destination/visit. See the details of importDestinations operation to import your data and create a planning in only one call. * Same operations are available for stores (note you have an existing default store). * With created destinations/visits, you can create a planning (routes and stops are automatically created depending of yours vehicles and destinations/visits) * In existing planning, you have availability to move stops (which represent visits) on a dedicated route (which represent a dedicated vehicle). * With many unaffected (out-of-route) stops in a planning, you may create a zoning to move many stops in several routes. Create a zoning (you can generate zones in this zoning automatically from automatic clustering), if you apply zoning (containing zones linked to a vehicle) on your planning, all stops contained in different zones will be moved in dedicated routes.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: tech@mapotempo.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.20
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Mapotempo\Model;

use \ArrayAccess;
use \Mapotempo\ObjectSerializer;

/**
 * V01VehicleUsage Class Doc Comment
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class V01VehicleUsage implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'V01_VehicleUsage';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'id' => 'int',
'vehicle_usage_set_id' => 'int',
'time_window_start' => 'string',
'time_window_end' => 'string',
'store_start_id' => 'int',
'store_stop_id' => 'int',
'service_time_start' => 'string',
'service_time_end' => 'string',
'work_time' => 'string',
'rest_start' => 'string',
'rest_stop' => 'string',
'rest_duration' => 'string',
'store_rest_id' => 'int',
'active' => 'bool',
'tag_ids' => 'int[]',
'cost' => 'double',
'cost_time' => 'double',
'cost_distance' => 'double'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'id' => 'int32',
'vehicle_usage_set_id' => 'int32',
'time_window_start' => null,
'time_window_end' => null,
'store_start_id' => 'int32',
'store_stop_id' => 'int32',
'service_time_start' => null,
'service_time_end' => null,
'work_time' => null,
'rest_start' => null,
'rest_stop' => null,
'rest_duration' => null,
'store_rest_id' => 'int32',
'active' => null,
'tag_ids' => 'int32',
'cost' => 'double',
'cost_time' => 'double',
'cost_distance' => 'double'    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
'vehicle_usage_set_id' => 'vehicle_usage_set_id',
'time_window_start' => 'time_window_start',
'time_window_end' => 'time_window_end',
'store_start_id' => 'store_start_id',
'store_stop_id' => 'store_stop_id',
'service_time_start' => 'service_time_start',
'service_time_end' => 'service_time_end',
'work_time' => 'work_time',
'rest_start' => 'rest_start',
'rest_stop' => 'rest_stop',
'rest_duration' => 'rest_duration',
'store_rest_id' => 'store_rest_id',
'active' => 'active',
'tag_ids' => 'tag_ids',
'cost' => 'cost',
'cost_time' => 'cost_time',
'cost_distance' => 'cost_distance'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
'vehicle_usage_set_id' => 'setVehicleUsageSetId',
'time_window_start' => 'setTimeWindowStart',
'time_window_end' => 'setTimeWindowEnd',
'store_start_id' => 'setStoreStartId',
'store_stop_id' => 'setStoreStopId',
'service_time_start' => 'setServiceTimeStart',
'service_time_end' => 'setServiceTimeEnd',
'work_time' => 'setWorkTime',
'rest_start' => 'setRestStart',
'rest_stop' => 'setRestStop',
'rest_duration' => 'setRestDuration',
'store_rest_id' => 'setStoreRestId',
'active' => 'setActive',
'tag_ids' => 'setTagIds',
'cost' => 'setCost',
'cost_time' => 'setCostTime',
'cost_distance' => 'setCostDistance'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
'vehicle_usage_set_id' => 'getVehicleUsageSetId',
'time_window_start' => 'getTimeWindowStart',
'time_window_end' => 'getTimeWindowEnd',
'store_start_id' => 'getStoreStartId',
'store_stop_id' => 'getStoreStopId',
'service_time_start' => 'getServiceTimeStart',
'service_time_end' => 'getServiceTimeEnd',
'work_time' => 'getWorkTime',
'rest_start' => 'getRestStart',
'rest_stop' => 'getRestStop',
'rest_duration' => 'getRestDuration',
'store_rest_id' => 'getStoreRestId',
'active' => 'getActive',
'tag_ids' => 'getTagIds',
'cost' => 'getCost',
'cost_time' => 'getCostTime',
'cost_distance' => 'getCostDistance'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['vehicle_usage_set_id'] = isset($data['vehicle_usage_set_id']) ? $data['vehicle_usage_set_id'] : null;
        $this->container['time_window_start'] = isset($data['time_window_start']) ? $data['time_window_start'] : null;
        $this->container['time_window_end'] = isset($data['time_window_end']) ? $data['time_window_end'] : null;
        $this->container['store_start_id'] = isset($data['store_start_id']) ? $data['store_start_id'] : null;
        $this->container['store_stop_id'] = isset($data['store_stop_id']) ? $data['store_stop_id'] : null;
        $this->container['service_time_start'] = isset($data['service_time_start']) ? $data['service_time_start'] : null;
        $this->container['service_time_end'] = isset($data['service_time_end']) ? $data['service_time_end'] : null;
        $this->container['work_time'] = isset($data['work_time']) ? $data['work_time'] : null;
        $this->container['rest_start'] = isset($data['rest_start']) ? $data['rest_start'] : null;
        $this->container['rest_stop'] = isset($data['rest_stop']) ? $data['rest_stop'] : null;
        $this->container['rest_duration'] = isset($data['rest_duration']) ? $data['rest_duration'] : null;
        $this->container['store_rest_id'] = isset($data['store_rest_id']) ? $data['store_rest_id'] : null;
        $this->container['active'] = isset($data['active']) ? $data['active'] : null;
        $this->container['tag_ids'] = isset($data['tag_ids']) ? $data['tag_ids'] : null;
        $this->container['cost'] = isset($data['cost']) ? $data['cost'] : null;
        $this->container['cost_time'] = isset($data['cost_time']) ? $data['cost_time'] : null;
        $this->container['cost_distance'] = isset($data['cost_distance']) ? $data['cost_distance'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id
     *
     * @return int
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param int $id id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets vehicle_usage_set_id
     *
     * @return int
     */
    public function getVehicleUsageSetId()
    {
        return $this->container['vehicle_usage_set_id'];
    }

    /**
     * Sets vehicle_usage_set_id
     *
     * @param int $vehicle_usage_set_id vehicle_usage_set_id
     *
     * @return $this
     */
    public function setVehicleUsageSetId($vehicle_usage_set_id)
    {
        $this->container['vehicle_usage_set_id'] = $vehicle_usage_set_id;

        return $this;
    }

    /**
     * Gets time_window_start
     *
     * @return string
     */
    public function getTimeWindowStart()
    {
        return $this->container['time_window_start'];
    }

    /**
     * Sets time_window_start
     *
     * @param string $time_window_start Schedule time (HH:MM:SS) or number of seconds
     *
     * @return $this
     */
    public function setTimeWindowStart($time_window_start)
    {
        $this->container['time_window_start'] = $time_window_start;

        return $this;
    }

    /**
     * Gets time_window_end
     *
     * @return string
     */
    public function getTimeWindowEnd()
    {
        return $this->container['time_window_end'];
    }

    /**
     * Sets time_window_end
     *
     * @param string $time_window_end Schedule time (HH:MM:SS) or number of seconds
     *
     * @return $this
     */
    public function setTimeWindowEnd($time_window_end)
    {
        $this->container['time_window_end'] = $time_window_end;

        return $this;
    }

    /**
     * Gets store_start_id
     *
     * @return int
     */
    public function getStoreStartId()
    {
        return $this->container['store_start_id'];
    }

    /**
     * Sets store_start_id
     *
     * @param int $store_start_id store_start_id
     *
     * @return $this
     */
    public function setStoreStartId($store_start_id)
    {
        $this->container['store_start_id'] = $store_start_id;

        return $this;
    }

    /**
     * Gets store_stop_id
     *
     * @return int
     */
    public function getStoreStopId()
    {
        return $this->container['store_stop_id'];
    }

    /**
     * Sets store_stop_id
     *
     * @param int $store_stop_id store_stop_id
     *
     * @return $this
     */
    public function setStoreStopId($store_stop_id)
    {
        $this->container['store_stop_id'] = $store_stop_id;

        return $this;
    }

    /**
     * Gets service_time_start
     *
     * @return string
     */
    public function getServiceTimeStart()
    {
        return $this->container['service_time_start'];
    }

    /**
     * Sets service_time_start
     *
     * @param string $service_time_start Schedule time (HH:MM:SS) or number of seconds
     *
     * @return $this
     */
    public function setServiceTimeStart($service_time_start)
    {
        $this->container['service_time_start'] = $service_time_start;

        return $this;
    }

    /**
     * Gets service_time_end
     *
     * @return string
     */
    public function getServiceTimeEnd()
    {
        return $this->container['service_time_end'];
    }

    /**
     * Sets service_time_end
     *
     * @param string $service_time_end Schedule time (HH:MM:SS) or number of seconds
     *
     * @return $this
     */
    public function setServiceTimeEnd($service_time_end)
    {
        $this->container['service_time_end'] = $service_time_end;

        return $this;
    }

    /**
     * Gets work_time
     *
     * @return string
     */
    public function getWorkTime()
    {
        return $this->container['work_time'];
    }

    /**
     * Sets work_time
     *
     * @param string $work_time Work time duration (HH:MM:SS) or number of seconds
     *
     * @return $this
     */
    public function setWorkTime($work_time)
    {
        $this->container['work_time'] = $work_time;

        return $this;
    }

    /**
     * Gets rest_start
     *
     * @return string
     */
    public function getRestStart()
    {
        return $this->container['rest_start'];
    }

    /**
     * Sets rest_start
     *
     * @param string $rest_start Schedule time (HH:MM:SS) or number of seconds
     *
     * @return $this
     */
    public function setRestStart($rest_start)
    {
        $this->container['rest_start'] = $rest_start;

        return $this;
    }

    /**
     * Gets rest_stop
     *
     * @return string
     */
    public function getRestStop()
    {
        return $this->container['rest_stop'];
    }

    /**
     * Sets rest_stop
     *
     * @param string $rest_stop Schedule time (HH:MM:SS) or number of seconds
     *
     * @return $this
     */
    public function setRestStop($rest_stop)
    {
        $this->container['rest_stop'] = $rest_stop;

        return $this;
    }

    /**
     * Gets rest_duration
     *
     * @return string
     */
    public function getRestDuration()
    {
        return $this->container['rest_duration'];
    }

    /**
     * Sets rest_duration
     *
     * @param string $rest_duration Rest duration (HH:MM:SS) or number of seconds
     *
     * @return $this
     */
    public function setRestDuration($rest_duration)
    {
        $this->container['rest_duration'] = $rest_duration;

        return $this;
    }

    /**
     * Gets store_rest_id
     *
     * @return int
     */
    public function getStoreRestId()
    {
        return $this->container['store_rest_id'];
    }

    /**
     * Sets store_rest_id
     *
     * @param int $store_rest_id store_rest_id
     *
     * @return $this
     */
    public function setStoreRestId($store_rest_id)
    {
        $this->container['store_rest_id'] = $store_rest_id;

        return $this;
    }

    /**
     * Gets active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->container['active'];
    }

    /**
     * Sets active
     *
     * @param bool $active active
     *
     * @return $this
     */
    public function setActive($active)
    {
        $this->container['active'] = $active;

        return $this;
    }

    /**
     * Gets tag_ids
     *
     * @return int[]
     */
    public function getTagIds()
    {
        return $this->container['tag_ids'];
    }

    /**
     * Sets tag_ids
     *
     * @param int[] $tag_ids tag_ids
     *
     * @return $this
     */
    public function setTagIds($tag_ids)
    {
        $this->container['tag_ids'] = $tag_ids;

        return $this;
    }

    /**
     * Gets cost
     *
     * @return double
     */
    public function getCost()
    {
        return $this->container['cost'];
    }

    /**
     * Sets cost
     *
     * @param double $cost Cost of the vehicle plus the cost of the structure
     *
     * @return $this
     */
    public function setCost($cost)
    {
        $this->container['cost'] = $cost;

        return $this;
    }

    /**
     * Gets cost_time
     *
     * @return double
     */
    public function getCostTime()
    {
        return $this->container['cost_time'];
    }

    /**
     * Sets cost_time
     *
     * @param double $cost_time Cost of service per hour
     *
     * @return $this
     */
    public function setCostTime($cost_time)
    {
        $this->container['cost_time'] = $cost_time;

        return $this;
    }

    /**
     * Gets cost_distance
     *
     * @return double
     */
    public function getCostDistance()
    {
        return $this->container['cost_distance'];
    }

    /**
     * Sets cost_distance
     *
     * @param double $cost_distance Kilometre/Miles cost
     *
     * @return $this
     */
    public function setCostDistance($cost_distance)
    {
        $this->container['cost_distance'] = $cost_distance;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
