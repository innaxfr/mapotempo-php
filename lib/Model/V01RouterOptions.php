<?php
/**
 * V01RouterOptions
 *
 * PHP version 5
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * API
 *
 * [Simplified view of domain model](https://app.mapotempo.com//api/0.1/Model-simpel.svg). ## Model Model is structured around four majors concepts: the Customer account, Destinations, Vehicles and Plannings. * `Customers`: many of objects are linked to a customer account (relating to the user calling API). The customer has many `Users`, each user has his own `api_key`. Be carefull not to confuse with following model `Destination`, `Customer`can only be created by a admin `User`. * `Destinations`: location points to visit with constraints. The same `Destination` can be visited several times : in this case several `Visits` are associated to one `Destination`. * `Vehicles`: vehicles definition are splited in two parts:  * the structural definition named `Vehicle` (car, truck, bike, consumption, etc.)  * and the vehicle usage `VehicleUsage`, a specific usage of a physical vehicle in a specific context. Vehicles can be used in many contexts called `VehicleUsageSet` (set of all vehicles usages under a context). Multiple values are only available if dedicated option for customer is active. For instance, if customer needs to use its vehicle 2 times per day (morning and evening), he needs 2 `VehicleUsageSet` called \"Morning\" and \"Evening\" : each can have different values defined for stores, rest, etc... `VehicleUsageSet` defines default values for vehicle usage. * `Plannings`: `Planning` is a set of `Routes` to `Visit` `Destinations` with `Vehicle` within a `VehicleUsageSet` context. A route is a track between all destinations reached by a vehicle (a new route is created for each customer's vehicle and a route without vehicle is created for all out-of-route destinations). By default all customer's visites are used in a planning.  ## Technical access ### Swagger descriptor This REST API is described with Swagger. The Swagger descriptor defines the request end-points, the parameters and the return values. The API can be addressed by HTTP request or with a generated client using the Swagger descriptor. ### API key All access to the API are subject to an `api_key` parameter in order to authenticate the user. This parameter can be sent with a query string on each available operation: `https://app.mapotempo.com/api/0.1/{objects}?api_key={your_personal_api_key}` ### Return The API supports several return formats: `json` and `xml` which depend of the requested extension used in url. ### I18n Functionnal textual returns are subject to translation and depend of HTTP header `Accept-Language`. HTTP error codes are not translated. ## Admin acces Using an admin `api_key` switches to advanced opperations (on `Customer`, `User`, `Vehicle`, `Profile`). Most of operations from the current api are usable either for normal user `api_key` or admin user `api_key` (not both). ## More concepts When a customer is created some objects are created by default with this new customer: * `Vehicle`: multiple, depending of the `max_vehicles` defined for customer * `DeliverableUnit`: one default * `VehicleUsageSet`: one default * `VehicleUsage`: multiple, depending of the vehicles number * `Store`: one default  ### Profiles, Layers, Routers `Profile` is a concept which allows to set several parameters for the customer: * `Layer`: which allows to choose the background map * `Router`: which allows to build route's information.  Several default profiles are available and can be listed with an admin `api_key`.  ### Tags `Tag` is a concept to filter visits and create planning only for a subset of visits. For instance, if some visits are tagged \"Monday\", it allows to create a new planning for \"Monday\" tag and use only dedicated visits. ### Zonings `Zoning` is a concept which allows to define multiple `Zones` (areas) around destinatons. A `Zone` can be affected to a `Vehicle` and if it is used into a `Planning`, all `Destinations` inside areas will be affected to the zone's vehicle (or `Route`). A polygon defining a `Zone` can be created outside the application or can be automatically generated from a planning.  ## Code samples * Create and display destinations or visits. Here some samples for these operations: [using PHP](https://app.mapotempo.com//api/0.1/examples/php/example.php), [using Ruby](https://app.mapotempo.com//api/0.1/examples/ruby/example.rb). Note you can import destinations/visits and create a planning at the same time if you know beforehand the route for each destination/visit. See the details of importDestinations operation to import your data and create a planning in only one call. * Same operations are available for stores (note you have an existing default store). * With created destinations/visits, you can create a planning (routes and stops are automatically created depending of yours vehicles and destinations/visits) * In existing planning, you have availability to move stops (which represent visits) on a dedicated route (which represent a dedicated vehicle). * With many unaffected (out-of-route) stops in a planning, you may create a zoning to move many stops in several routes. Create a zoning (you can generate zones in this zoning automatically from automatic clustering), if you apply zoning (containing zones linked to a vehicle) on your planning, all stops contained in different zones will be moved in dedicated routes.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: tech@mapotempo.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.20
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Mapotempo\Model;

use \ArrayAccess;
use \Mapotempo\ObjectSerializer;

/**
 * V01RouterOptions Class Doc Comment
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class V01RouterOptions implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'V01_RouterOptions';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'track' => 'bool',
'motorway' => 'bool',
'toll' => 'bool',
'trailers' => 'int',
'weight' => 'float',
'weight_per_axle' => 'float',
'height' => 'float',
'width' => 'float',
'length' => 'float',
'hazardous_goods' => 'string',
'max_walk_distance' => 'float',
'approach' => 'string',
'snap' => 'float',
'strict_restriction' => 'bool'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'track' => null,
'motorway' => null,
'toll' => null,
'trailers' => 'int32',
'weight' => 'float',
'weight_per_axle' => 'float',
'height' => 'float',
'width' => 'float',
'length' => 'float',
'hazardous_goods' => null,
'max_walk_distance' => 'float',
'approach' => null,
'snap' => 'float',
'strict_restriction' => null    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'track' => 'track',
'motorway' => 'motorway',
'toll' => 'toll',
'trailers' => 'trailers',
'weight' => 'weight',
'weight_per_axle' => 'weight_per_axle',
'height' => 'height',
'width' => 'width',
'length' => 'length',
'hazardous_goods' => 'hazardous_goods',
'max_walk_distance' => 'max_walk_distance',
'approach' => 'approach',
'snap' => 'snap',
'strict_restriction' => 'strict_restriction'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'track' => 'setTrack',
'motorway' => 'setMotorway',
'toll' => 'setToll',
'trailers' => 'setTrailers',
'weight' => 'setWeight',
'weight_per_axle' => 'setWeightPerAxle',
'height' => 'setHeight',
'width' => 'setWidth',
'length' => 'setLength',
'hazardous_goods' => 'setHazardousGoods',
'max_walk_distance' => 'setMaxWalkDistance',
'approach' => 'setApproach',
'snap' => 'setSnap',
'strict_restriction' => 'setStrictRestriction'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'track' => 'getTrack',
'motorway' => 'getMotorway',
'toll' => 'getToll',
'trailers' => 'getTrailers',
'weight' => 'getWeight',
'weight_per_axle' => 'getWeightPerAxle',
'height' => 'getHeight',
'width' => 'getWidth',
'length' => 'getLength',
'hazardous_goods' => 'getHazardousGoods',
'max_walk_distance' => 'getMaxWalkDistance',
'approach' => 'getApproach',
'snap' => 'getSnap',
'strict_restriction' => 'getStrictRestriction'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    const HAZARDOUS_GOODS_EXPLOSIVE = 'explosive';
const HAZARDOUS_GOODS_GAS = 'gas';
const HAZARDOUS_GOODS_FLAMMABLE = 'flammable';
const HAZARDOUS_GOODS_COMBUSTIBLE = 'combustible';
const HAZARDOUS_GOODS_ORGANIC = 'organic';
const HAZARDOUS_GOODS_POISON = 'poison';
const HAZARDOUS_GOODS_RADIO_ACTIVE = 'radio_active';
const HAZARDOUS_GOODS_CORROSIVE = 'corrosive';
const HAZARDOUS_GOODS_POISONOUS_INHALATION = 'poisonous_inhalation';
const HAZARDOUS_GOODS_HARMFUL_TO_WATER = 'harmful_to_water';
const HAZARDOUS_GOODS_OTHER = 'other';
const APPROACH_UNRESTRICTED = 'unrestricted';
const APPROACH_CURB = 'curb';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getHazardousGoodsAllowableValues()
    {
        return [
            self::HAZARDOUS_GOODS_EXPLOSIVE,
self::HAZARDOUS_GOODS_GAS,
self::HAZARDOUS_GOODS_FLAMMABLE,
self::HAZARDOUS_GOODS_COMBUSTIBLE,
self::HAZARDOUS_GOODS_ORGANIC,
self::HAZARDOUS_GOODS_POISON,
self::HAZARDOUS_GOODS_RADIO_ACTIVE,
self::HAZARDOUS_GOODS_CORROSIVE,
self::HAZARDOUS_GOODS_POISONOUS_INHALATION,
self::HAZARDOUS_GOODS_HARMFUL_TO_WATER,
self::HAZARDOUS_GOODS_OTHER,        ];
    }
    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getApproachAllowableValues()
    {
        return [
            self::APPROACH_UNRESTRICTED,
self::APPROACH_CURB,        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['track'] = isset($data['track']) ? $data['track'] : null;
        $this->container['motorway'] = isset($data['motorway']) ? $data['motorway'] : null;
        $this->container['toll'] = isset($data['toll']) ? $data['toll'] : null;
        $this->container['trailers'] = isset($data['trailers']) ? $data['trailers'] : null;
        $this->container['weight'] = isset($data['weight']) ? $data['weight'] : null;
        $this->container['weight_per_axle'] = isset($data['weight_per_axle']) ? $data['weight_per_axle'] : null;
        $this->container['height'] = isset($data['height']) ? $data['height'] : null;
        $this->container['width'] = isset($data['width']) ? $data['width'] : null;
        $this->container['length'] = isset($data['length']) ? $data['length'] : null;
        $this->container['hazardous_goods'] = isset($data['hazardous_goods']) ? $data['hazardous_goods'] : null;
        $this->container['max_walk_distance'] = isset($data['max_walk_distance']) ? $data['max_walk_distance'] : null;
        $this->container['approach'] = isset($data['approach']) ? $data['approach'] : null;
        $this->container['snap'] = isset($data['snap']) ? $data['snap'] : null;
        $this->container['strict_restriction'] = isset($data['strict_restriction']) ? $data['strict_restriction'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        $allowedValues = $this->getHazardousGoodsAllowableValues();
        if (!is_null($this->container['hazardous_goods']) && !in_array($this->container['hazardous_goods'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'hazardous_goods', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        $allowedValues = $this->getApproachAllowableValues();
        if (!is_null($this->container['approach']) && !in_array($this->container['approach'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'approach', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets track
     *
     * @return bool
     */
    public function getTrack()
    {
        return $this->container['track'];
    }

    /**
     * Sets track
     *
     * @param bool $track track
     *
     * @return $this
     */
    public function setTrack($track)
    {
        $this->container['track'] = $track;

        return $this;
    }

    /**
     * Gets motorway
     *
     * @return bool
     */
    public function getMotorway()
    {
        return $this->container['motorway'];
    }

    /**
     * Sets motorway
     *
     * @param bool $motorway motorway
     *
     * @return $this
     */
    public function setMotorway($motorway)
    {
        $this->container['motorway'] = $motorway;

        return $this;
    }

    /**
     * Gets toll
     *
     * @return bool
     */
    public function getToll()
    {
        return $this->container['toll'];
    }

    /**
     * Sets toll
     *
     * @param bool $toll toll
     *
     * @return $this
     */
    public function setToll($toll)
    {
        $this->container['toll'] = $toll;

        return $this;
    }

    /**
     * Gets trailers
     *
     * @return int
     */
    public function getTrailers()
    {
        return $this->container['trailers'];
    }

    /**
     * Sets trailers
     *
     * @param int $trailers trailers
     *
     * @return $this
     */
    public function setTrailers($trailers)
    {
        $this->container['trailers'] = $trailers;

        return $this;
    }

    /**
     * Gets weight
     *
     * @return float
     */
    public function getWeight()
    {
        return $this->container['weight'];
    }

    /**
     * Sets weight
     *
     * @param float $weight Total weight with trailers and shipping goods, in tons
     *
     * @return $this
     */
    public function setWeight($weight)
    {
        $this->container['weight'] = $weight;

        return $this;
    }

    /**
     * Gets weight_per_axle
     *
     * @return float
     */
    public function getWeightPerAxle()
    {
        return $this->container['weight_per_axle'];
    }

    /**
     * Sets weight_per_axle
     *
     * @param float $weight_per_axle weight_per_axle
     *
     * @return $this
     */
    public function setWeightPerAxle($weight_per_axle)
    {
        $this->container['weight_per_axle'] = $weight_per_axle;

        return $this;
    }

    /**
     * Gets height
     *
     * @return float
     */
    public function getHeight()
    {
        return $this->container['height'];
    }

    /**
     * Sets height
     *
     * @param float $height height
     *
     * @return $this
     */
    public function setHeight($height)
    {
        $this->container['height'] = $height;

        return $this;
    }

    /**
     * Gets width
     *
     * @return float
     */
    public function getWidth()
    {
        return $this->container['width'];
    }

    /**
     * Sets width
     *
     * @param float $width width
     *
     * @return $this
     */
    public function setWidth($width)
    {
        $this->container['width'] = $width;

        return $this;
    }

    /**
     * Gets length
     *
     * @return float
     */
    public function getLength()
    {
        return $this->container['length'];
    }

    /**
     * Sets length
     *
     * @param float $length length
     *
     * @return $this
     */
    public function setLength($length)
    {
        $this->container['length'] = $length;

        return $this;
    }

    /**
     * Gets hazardous_goods
     *
     * @return string
     */
    public function getHazardousGoods()
    {
        return $this->container['hazardous_goods'];
    }

    /**
     * Sets hazardous_goods
     *
     * @param string $hazardous_goods hazardous_goods
     *
     * @return $this
     */
    public function setHazardousGoods($hazardous_goods)
    {
        $allowedValues = $this->getHazardousGoodsAllowableValues();
        if (!is_null($hazardous_goods) && !in_array($hazardous_goods, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'hazardous_goods', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['hazardous_goods'] = $hazardous_goods;

        return $this;
    }

    /**
     * Gets max_walk_distance
     *
     * @return float
     */
    public function getMaxWalkDistance()
    {
        return $this->container['max_walk_distance'];
    }

    /**
     * Sets max_walk_distance
     *
     * @param float $max_walk_distance max_walk_distance
     *
     * @return $this
     */
    public function setMaxWalkDistance($max_walk_distance)
    {
        $this->container['max_walk_distance'] = $max_walk_distance;

        return $this;
    }

    /**
     * Gets approach
     *
     * @return string
     */
    public function getApproach()
    {
        return $this->container['approach'];
    }

    /**
     * Sets approach
     *
     * @param string $approach approach
     *
     * @return $this
     */
    public function setApproach($approach)
    {
        $allowedValues = $this->getApproachAllowableValues();
        if (!is_null($approach) && !in_array($approach, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'approach', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['approach'] = $approach;

        return $this;
    }

    /**
     * Gets snap
     *
     * @return float
     */
    public function getSnap()
    {
        return $this->container['snap'];
    }

    /**
     * Sets snap
     *
     * @param float $snap snap
     *
     * @return $this
     */
    public function setSnap($snap)
    {
        $this->container['snap'] = $snap;

        return $this;
    }

    /**
     * Gets strict_restriction
     *
     * @return bool
     */
    public function getStrictRestriction()
    {
        return $this->container['strict_restriction'];
    }

    /**
     * Sets strict_restriction
     *
     * @param bool $strict_restriction strict_restriction
     *
     * @return $this
     */
    public function setStrictRestriction($strict_restriction)
    {
        $this->container['strict_restriction'] = $strict_restriction;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
