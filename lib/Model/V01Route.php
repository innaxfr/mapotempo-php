<?php
/**
 * V01Route
 *
 * PHP version 5
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * API
 *
 * [Simplified view of domain model](https://app.mapotempo.com//api/0.1/Model-simpel.svg). ## Model Model is structured around four majors concepts: the Customer account, Destinations, Vehicles and Plannings. * `Customers`: many of objects are linked to a customer account (relating to the user calling API). The customer has many `Users`, each user has his own `api_key`. Be carefull not to confuse with following model `Destination`, `Customer`can only be created by a admin `User`. * `Destinations`: location points to visit with constraints. The same `Destination` can be visited several times : in this case several `Visits` are associated to one `Destination`. * `Vehicles`: vehicles definition are splited in two parts:  * the structural definition named `Vehicle` (car, truck, bike, consumption, etc.)  * and the vehicle usage `VehicleUsage`, a specific usage of a physical vehicle in a specific context. Vehicles can be used in many contexts called `VehicleUsageSet` (set of all vehicles usages under a context). Multiple values are only available if dedicated option for customer is active. For instance, if customer needs to use its vehicle 2 times per day (morning and evening), he needs 2 `VehicleUsageSet` called \"Morning\" and \"Evening\" : each can have different values defined for stores, rest, etc... `VehicleUsageSet` defines default values for vehicle usage. * `Plannings`: `Planning` is a set of `Routes` to `Visit` `Destinations` with `Vehicle` within a `VehicleUsageSet` context. A route is a track between all destinations reached by a vehicle (a new route is created for each customer's vehicle and a route without vehicle is created for all out-of-route destinations). By default all customer's visites are used in a planning.  ## Technical access ### Swagger descriptor This REST API is described with Swagger. The Swagger descriptor defines the request end-points, the parameters and the return values. The API can be addressed by HTTP request or with a generated client using the Swagger descriptor. ### API key All access to the API are subject to an `api_key` parameter in order to authenticate the user. This parameter can be sent with a query string on each available operation: `https://app.mapotempo.com/api/0.1/{objects}?api_key={your_personal_api_key}` ### Return The API supports several return formats: `json` and `xml` which depend of the requested extension used in url. ### I18n Functionnal textual returns are subject to translation and depend of HTTP header `Accept-Language`. HTTP error codes are not translated. ## Admin acces Using an admin `api_key` switches to advanced opperations (on `Customer`, `User`, `Vehicle`, `Profile`). Most of operations from the current api are usable either for normal user `api_key` or admin user `api_key` (not both). ## More concepts When a customer is created some objects are created by default with this new customer: * `Vehicle`: multiple, depending of the `max_vehicles` defined for customer * `DeliverableUnit`: one default * `VehicleUsageSet`: one default * `VehicleUsage`: multiple, depending of the vehicles number * `Store`: one default  ### Profiles, Layers, Routers `Profile` is a concept which allows to set several parameters for the customer: * `Layer`: which allows to choose the background map * `Router`: which allows to build route's information.  Several default profiles are available and can be listed with an admin `api_key`.  ### Tags `Tag` is a concept to filter visits and create planning only for a subset of visits. For instance, if some visits are tagged \"Monday\", it allows to create a new planning for \"Monday\" tag and use only dedicated visits. ### Zonings `Zoning` is a concept which allows to define multiple `Zones` (areas) around destinatons. A `Zone` can be affected to a `Vehicle` and if it is used into a `Planning`, all `Destinations` inside areas will be affected to the zone's vehicle (or `Route`). A polygon defining a `Zone` can be created outside the application or can be automatically generated from a planning.  ## Code samples * Create and display destinations or visits. Here some samples for these operations: [using PHP](https://app.mapotempo.com//api/0.1/examples/php/example.php), [using Ruby](https://app.mapotempo.com//api/0.1/examples/ruby/example.rb). Note you can import destinations/visits and create a planning at the same time if you know beforehand the route for each destination/visit. See the details of importDestinations operation to import your data and create a planning in only one call. * Same operations are available for stores (note you have an existing default store). * With created destinations/visits, you can create a planning (routes and stops are automatically created depending of yours vehicles and destinations/visits) * In existing planning, you have availability to move stops (which represent visits) on a dedicated route (which represent a dedicated vehicle). * With many unaffected (out-of-route) stops in a planning, you may create a zoning to move many stops in several routes. Create a zoning (you can generate zones in this zoning automatically from automatic clustering), if you apply zoning (containing zones linked to a vehicle) on your planning, all stops contained in different zones will be moved in dedicated routes.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: tech@mapotempo.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.20
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Mapotempo\Model;

use \ArrayAccess;
use \Mapotempo\ObjectSerializer;

/**
 * V01Route Class Doc Comment
 *
 * @category Class
 * @description Fetch customer&#x27;s routes.
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class V01Route implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'V01_Route';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'id' => 'int',
'vehicle_usage_id' => 'int',
'hidden' => 'bool',
'locked' => 'bool',
'color' => 'string',
'geojson' => 'string',
'ref' => 'string',
'outdated' => 'bool',
'distance' => 'float',
'drive_time' => 'int',
'visits_duration' => 'int',
'wait_time' => 'int',
'emission' => 'float',
'start' => '\DateTime',
'end' => '\DateTime',
'departure_status' => 'string',
'departure_eta' => '\DateTime',
'arrival_status' => 'string',
'arrival_eta' => '\DateTime',
'stops' => '\Mapotempo\Model\V01Stop[]',
'stop_out_of_drive_time' => 'bool',
'stop_out_of_work_time' => 'bool',
'stop_out_of_max_distance' => 'bool',
'stop_no_path' => 'bool',
'stop_distance' => 'float',
'stop_drive_time' => 'int',
'updated_at' => '\DateTime',
'last_sent_to' => 'string',
'last_sent_at' => '\DateTime',
'optimized_at' => '\DateTime',
'quantities' => '\Mapotempo\Model\V01DeliverableUnitQuantity[]',
'total_cost' => 'string'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'id' => 'int32',
'vehicle_usage_id' => 'int32',
'hidden' => null,
'locked' => null,
'color' => null,
'geojson' => null,
'ref' => null,
'outdated' => null,
'distance' => 'float',
'drive_time' => 'int32',
'visits_duration' => 'int32',
'wait_time' => 'int32',
'emission' => 'float',
'start' => 'date-time',
'end' => 'date-time',
'departure_status' => null,
'departure_eta' => 'date-time',
'arrival_status' => null,
'arrival_eta' => 'date-time',
'stops' => null,
'stop_out_of_drive_time' => null,
'stop_out_of_work_time' => null,
'stop_out_of_max_distance' => null,
'stop_no_path' => null,
'stop_distance' => 'float',
'stop_drive_time' => 'int32',
'updated_at' => 'date-time',
'last_sent_to' => null,
'last_sent_at' => 'date-time',
'optimized_at' => 'date-time',
'quantities' => null,
'total_cost' => null    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
'vehicle_usage_id' => 'vehicle_usage_id',
'hidden' => 'hidden',
'locked' => 'locked',
'color' => 'color',
'geojson' => 'geojson',
'ref' => 'ref',
'outdated' => 'outdated',
'distance' => 'distance',
'drive_time' => 'drive_time',
'visits_duration' => 'visits_duration',
'wait_time' => 'wait_time',
'emission' => 'emission',
'start' => 'start',
'end' => 'end',
'departure_status' => 'departure_status',
'departure_eta' => 'departure_eta',
'arrival_status' => 'arrival_status',
'arrival_eta' => 'arrival_eta',
'stops' => 'stops',
'stop_out_of_drive_time' => 'stop_out_of_drive_time',
'stop_out_of_work_time' => 'stop_out_of_work_time',
'stop_out_of_max_distance' => 'stop_out_of_max_distance',
'stop_no_path' => 'stop_no_path',
'stop_distance' => 'stop_distance',
'stop_drive_time' => 'stop_drive_time',
'updated_at' => 'updated_at',
'last_sent_to' => 'last_sent_to',
'last_sent_at' => 'last_sent_at',
'optimized_at' => 'optimized_at',
'quantities' => 'quantities',
'total_cost' => 'total_cost'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
'vehicle_usage_id' => 'setVehicleUsageId',
'hidden' => 'setHidden',
'locked' => 'setLocked',
'color' => 'setColor',
'geojson' => 'setGeojson',
'ref' => 'setRef',
'outdated' => 'setOutdated',
'distance' => 'setDistance',
'drive_time' => 'setDriveTime',
'visits_duration' => 'setVisitsDuration',
'wait_time' => 'setWaitTime',
'emission' => 'setEmission',
'start' => 'setStart',
'end' => 'setEnd',
'departure_status' => 'setDepartureStatus',
'departure_eta' => 'setDepartureEta',
'arrival_status' => 'setArrivalStatus',
'arrival_eta' => 'setArrivalEta',
'stops' => 'setStops',
'stop_out_of_drive_time' => 'setStopOutOfDriveTime',
'stop_out_of_work_time' => 'setStopOutOfWorkTime',
'stop_out_of_max_distance' => 'setStopOutOfMaxDistance',
'stop_no_path' => 'setStopNoPath',
'stop_distance' => 'setStopDistance',
'stop_drive_time' => 'setStopDriveTime',
'updated_at' => 'setUpdatedAt',
'last_sent_to' => 'setLastSentTo',
'last_sent_at' => 'setLastSentAt',
'optimized_at' => 'setOptimizedAt',
'quantities' => 'setQuantities',
'total_cost' => 'setTotalCost'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
'vehicle_usage_id' => 'getVehicleUsageId',
'hidden' => 'getHidden',
'locked' => 'getLocked',
'color' => 'getColor',
'geojson' => 'getGeojson',
'ref' => 'getRef',
'outdated' => 'getOutdated',
'distance' => 'getDistance',
'drive_time' => 'getDriveTime',
'visits_duration' => 'getVisitsDuration',
'wait_time' => 'getWaitTime',
'emission' => 'getEmission',
'start' => 'getStart',
'end' => 'getEnd',
'departure_status' => 'getDepartureStatus',
'departure_eta' => 'getDepartureEta',
'arrival_status' => 'getArrivalStatus',
'arrival_eta' => 'getArrivalEta',
'stops' => 'getStops',
'stop_out_of_drive_time' => 'getStopOutOfDriveTime',
'stop_out_of_work_time' => 'getStopOutOfWorkTime',
'stop_out_of_max_distance' => 'getStopOutOfMaxDistance',
'stop_no_path' => 'getStopNoPath',
'stop_distance' => 'getStopDistance',
'stop_drive_time' => 'getStopDriveTime',
'updated_at' => 'getUpdatedAt',
'last_sent_to' => 'getLastSentTo',
'last_sent_at' => 'getLastSentAt',
'optimized_at' => 'getOptimizedAt',
'quantities' => 'getQuantities',
'total_cost' => 'getTotalCost'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['vehicle_usage_id'] = isset($data['vehicle_usage_id']) ? $data['vehicle_usage_id'] : null;
        $this->container['hidden'] = isset($data['hidden']) ? $data['hidden'] : null;
        $this->container['locked'] = isset($data['locked']) ? $data['locked'] : null;
        $this->container['color'] = isset($data['color']) ? $data['color'] : null;
        $this->container['geojson'] = isset($data['geojson']) ? $data['geojson'] : null;
        $this->container['ref'] = isset($data['ref']) ? $data['ref'] : null;
        $this->container['outdated'] = isset($data['outdated']) ? $data['outdated'] : null;
        $this->container['distance'] = isset($data['distance']) ? $data['distance'] : null;
        $this->container['drive_time'] = isset($data['drive_time']) ? $data['drive_time'] : null;
        $this->container['visits_duration'] = isset($data['visits_duration']) ? $data['visits_duration'] : null;
        $this->container['wait_time'] = isset($data['wait_time']) ? $data['wait_time'] : null;
        $this->container['emission'] = isset($data['emission']) ? $data['emission'] : null;
        $this->container['start'] = isset($data['start']) ? $data['start'] : null;
        $this->container['end'] = isset($data['end']) ? $data['end'] : null;
        $this->container['departure_status'] = isset($data['departure_status']) ? $data['departure_status'] : null;
        $this->container['departure_eta'] = isset($data['departure_eta']) ? $data['departure_eta'] : null;
        $this->container['arrival_status'] = isset($data['arrival_status']) ? $data['arrival_status'] : null;
        $this->container['arrival_eta'] = isset($data['arrival_eta']) ? $data['arrival_eta'] : null;
        $this->container['stops'] = isset($data['stops']) ? $data['stops'] : null;
        $this->container['stop_out_of_drive_time'] = isset($data['stop_out_of_drive_time']) ? $data['stop_out_of_drive_time'] : null;
        $this->container['stop_out_of_work_time'] = isset($data['stop_out_of_work_time']) ? $data['stop_out_of_work_time'] : null;
        $this->container['stop_out_of_max_distance'] = isset($data['stop_out_of_max_distance']) ? $data['stop_out_of_max_distance'] : null;
        $this->container['stop_no_path'] = isset($data['stop_no_path']) ? $data['stop_no_path'] : null;
        $this->container['stop_distance'] = isset($data['stop_distance']) ? $data['stop_distance'] : null;
        $this->container['stop_drive_time'] = isset($data['stop_drive_time']) ? $data['stop_drive_time'] : null;
        $this->container['updated_at'] = isset($data['updated_at']) ? $data['updated_at'] : null;
        $this->container['last_sent_to'] = isset($data['last_sent_to']) ? $data['last_sent_to'] : null;
        $this->container['last_sent_at'] = isset($data['last_sent_at']) ? $data['last_sent_at'] : null;
        $this->container['optimized_at'] = isset($data['optimized_at']) ? $data['optimized_at'] : null;
        $this->container['quantities'] = isset($data['quantities']) ? $data['quantities'] : null;
        $this->container['total_cost'] = isset($data['total_cost']) ? $data['total_cost'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id
     *
     * @return int
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param int $id id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets vehicle_usage_id
     *
     * @return int
     */
    public function getVehicleUsageId()
    {
        return $this->container['vehicle_usage_id'];
    }

    /**
     * Sets vehicle_usage_id
     *
     * @param int $vehicle_usage_id vehicle_usage_id
     *
     * @return $this
     */
    public function setVehicleUsageId($vehicle_usage_id)
    {
        $this->container['vehicle_usage_id'] = $vehicle_usage_id;

        return $this;
    }

    /**
     * Gets hidden
     *
     * @return bool
     */
    public function getHidden()
    {
        return $this->container['hidden'];
    }

    /**
     * Sets hidden
     *
     * @param bool $hidden hidden
     *
     * @return $this
     */
    public function setHidden($hidden)
    {
        $this->container['hidden'] = $hidden;

        return $this;
    }

    /**
     * Gets locked
     *
     * @return bool
     */
    public function getLocked()
    {
        return $this->container['locked'];
    }

    /**
     * Sets locked
     *
     * @param bool $locked locked
     *
     * @return $this
     */
    public function setLocked($locked)
    {
        $this->container['locked'] = $locked;

        return $this;
    }

    /**
     * Gets color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->container['color'];
    }

    /**
     * Sets color
     *
     * @param string $color Color code with #. For instance: #FF0000.
     *
     * @return $this
     */
    public function setColor($color)
    {
        $this->container['color'] = $color;

        return $this;
    }

    /**
     * Gets geojson
     *
     * @return string
     */
    public function getGeojson()
    {
        return $this->container['geojson'];
    }

    /**
     * Sets geojson
     *
     * @param string $geojson Geojson string of track and stops of the route. Default empty, set parameter `with_geojson=true|point|polyline` to get this extra content.
     *
     * @return $this
     */
    public function setGeojson($geojson)
    {
        $this->container['geojson'] = $geojson;

        return $this;
    }

    /**
     * Gets ref
     *
     * @return string
     */
    public function getRef()
    {
        return $this->container['ref'];
    }

    /**
     * Sets ref
     *
     * @param string $ref ref
     *
     * @return $this
     */
    public function setRef($ref)
    {
        $this->container['ref'] = $ref;

        return $this;
    }

    /**
     * Gets outdated
     *
     * @return bool
     */
    public function getOutdated()
    {
        return $this->container['outdated'];
    }

    /**
     * Sets outdated
     *
     * @param bool $outdated outdated
     *
     * @return $this
     */
    public function setOutdated($outdated)
    {
        $this->container['outdated'] = $outdated;

        return $this;
    }

    /**
     * Gets distance
     *
     * @return float
     */
    public function getDistance()
    {
        return $this->container['distance'];
    }

    /**
     * Sets distance
     *
     * @param float $distance Total route's distance.
     *
     * @return $this
     */
    public function setDistance($distance)
    {
        $this->container['distance'] = $distance;

        return $this;
    }

    /**
     * Gets drive_time
     *
     * @return int
     */
    public function getDriveTime()
    {
        return $this->container['drive_time'];
    }

    /**
     * Sets drive_time
     *
     * @param int $drive_time Total drive time, in seconds
     *
     * @return $this
     */
    public function setDriveTime($drive_time)
    {
        $this->container['drive_time'] = $drive_time;

        return $this;
    }

    /**
     * Gets visits_duration
     *
     * @return int
     */
    public function getVisitsDuration()
    {
        return $this->container['visits_duration'];
    }

    /**
     * Sets visits_duration
     *
     * @param int $visits_duration Total visits duration, in seconds
     *
     * @return $this
     */
    public function setVisitsDuration($visits_duration)
    {
        $this->container['visits_duration'] = $visits_duration;

        return $this;
    }

    /**
     * Gets wait_time
     *
     * @return int
     */
    public function getWaitTime()
    {
        return $this->container['wait_time'];
    }

    /**
     * Sets wait_time
     *
     * @param int $wait_time Total wait time, in seconds
     *
     * @return $this
     */
    public function setWaitTime($wait_time)
    {
        $this->container['wait_time'] = $wait_time;

        return $this;
    }

    /**
     * Gets emission
     *
     * @return float
     */
    public function getEmission()
    {
        return $this->container['emission'];
    }

    /**
     * Sets emission
     *
     * @param float $emission emission
     *
     * @return $this
     */
    public function setEmission($emission)
    {
        $this->container['emission'] = $emission;

        return $this;
    }

    /**
     * Gets start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->container['start'];
    }

    /**
     * Sets start
     *
     * @param \DateTime $start start
     *
     * @return $this
     */
    public function setStart($start)
    {
        $this->container['start'] = $start;

        return $this;
    }

    /**
     * Gets end
     *
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->container['end'];
    }

    /**
     * Sets end
     *
     * @param \DateTime $end end
     *
     * @return $this
     */
    public function setEnd($end)
    {
        $this->container['end'] = $end;

        return $this;
    }

    /**
     * Gets departure_status
     *
     * @return string
     */
    public function getDepartureStatus()
    {
        return $this->container['departure_status'];
    }

    /**
     * Sets departure_status
     *
     * @param string $departure_status Departure status of start store.
     *
     * @return $this
     */
    public function setDepartureStatus($departure_status)
    {
        $this->container['departure_status'] = $departure_status;

        return $this;
    }

    /**
     * Gets departure_eta
     *
     * @return \DateTime
     */
    public function getDepartureEta()
    {
        return $this->container['departure_eta'];
    }

    /**
     * Sets departure_eta
     *
     * @param \DateTime $departure_eta Estimated time of departure from remote device for start store.
     *
     * @return $this
     */
    public function setDepartureEta($departure_eta)
    {
        $this->container['departure_eta'] = $departure_eta;

        return $this;
    }

    /**
     * Gets arrival_status
     *
     * @return string
     */
    public function getArrivalStatus()
    {
        return $this->container['arrival_status'];
    }

    /**
     * Sets arrival_status
     *
     * @param string $arrival_status Arrival status of stop store.
     *
     * @return $this
     */
    public function setArrivalStatus($arrival_status)
    {
        $this->container['arrival_status'] = $arrival_status;

        return $this;
    }

    /**
     * Gets arrival_eta
     *
     * @return \DateTime
     */
    public function getArrivalEta()
    {
        return $this->container['arrival_eta'];
    }

    /**
     * Sets arrival_eta
     *
     * @param \DateTime $arrival_eta Estimated time of arrival from remote device for stop store.
     *
     * @return $this
     */
    public function setArrivalEta($arrival_eta)
    {
        $this->container['arrival_eta'] = $arrival_eta;

        return $this;
    }

    /**
     * Gets stops
     *
     * @return \Mapotempo\Model\V01Stop[]
     */
    public function getStops()
    {
        return $this->container['stops'];
    }

    /**
     * Sets stops
     *
     * @param \Mapotempo\Model\V01Stop[] $stops stops
     *
     * @return $this
     */
    public function setStops($stops)
    {
        $this->container['stops'] = $stops;

        return $this;
    }

    /**
     * Gets stop_out_of_drive_time
     *
     * @return bool
     */
    public function getStopOutOfDriveTime()
    {
        return $this->container['stop_out_of_drive_time'];
    }

    /**
     * Sets stop_out_of_drive_time
     *
     * @param bool $stop_out_of_drive_time Flag is true when returning to vehicle's store_stop makes route time more than vehicle drive time. See also Stop.out_of_drive_time for previous stops in route.
     *
     * @return $this
     */
    public function setStopOutOfDriveTime($stop_out_of_drive_time)
    {
        $this->container['stop_out_of_drive_time'] = $stop_out_of_drive_time;

        return $this;
    }

    /**
     * Gets stop_out_of_work_time
     *
     * @return bool
     */
    public function getStopOutOfWorkTime()
    {
        return $this->container['stop_out_of_work_time'];
    }

    /**
     * Sets stop_out_of_work_time
     *
     * @param bool $stop_out_of_work_time Flag is true when returning to vehicle's store_stop makes route time outside vehicle time window. See also Stop.out_of_work_time for previous stops in route.
     *
     * @return $this
     */
    public function setStopOutOfWorkTime($stop_out_of_work_time)
    {
        $this->container['stop_out_of_work_time'] = $stop_out_of_work_time;

        return $this;
    }

    /**
     * Gets stop_out_of_max_distance
     *
     * @return bool
     */
    public function getStopOutOfMaxDistance()
    {
        return $this->container['stop_out_of_max_distance'];
    }

    /**
     * Sets stop_out_of_max_distance
     *
     * @param bool $stop_out_of_max_distance Flag is true when returning to vehicle's store_stop makes route distance more than vehicle max distance. See also Stop.out_of_max_distance for previous stops in route.
     *
     * @return $this
     */
    public function setStopOutOfMaxDistance($stop_out_of_max_distance)
    {
        $this->container['stop_out_of_max_distance'] = $stop_out_of_max_distance;

        return $this;
    }

    /**
     * Gets stop_no_path
     *
     * @return bool
     */
    public function getStopNoPath()
    {
        return $this->container['stop_no_path'];
    }

    /**
     * Sets stop_no_path
     *
     * @param bool $stop_no_path Flag is true when no route has been found to return to vehicle's store_stop. See also Stop.no_path for previous stops in route.
     *
     * @return $this
     */
    public function setStopNoPath($stop_no_path)
    {
        $this->container['stop_no_path'] = $stop_no_path;

        return $this;
    }

    /**
     * Gets stop_distance
     *
     * @return float
     */
    public function getStopDistance()
    {
        return $this->container['stop_distance'];
    }

    /**
     * Sets stop_distance
     *
     * @param float $stop_distance Distance between the vehicle's store_stop and last stop.
     *
     * @return $this
     */
    public function setStopDistance($stop_distance)
    {
        $this->container['stop_distance'] = $stop_distance;

        return $this;
    }

    /**
     * Gets stop_drive_time
     *
     * @return int
     */
    public function getStopDriveTime()
    {
        return $this->container['stop_drive_time'];
    }

    /**
     * Sets stop_drive_time
     *
     * @param int $stop_drive_time Time in seconds between the vehicle's store_stop and last stop.
     *
     * @return $this
     */
    public function setStopDriveTime($stop_drive_time)
    {
        $this->container['stop_drive_time'] = $stop_drive_time;

        return $this;
    }

    /**
     * Gets updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->container['updated_at'];
    }

    /**
     * Sets updated_at
     *
     * @param \DateTime $updated_at Last Updated At.
     *
     * @return $this
     */
    public function setUpdatedAt($updated_at)
    {
        $this->container['updated_at'] = $updated_at;

        return $this;
    }

    /**
     * Gets last_sent_to
     *
     * @return string
     */
    public function getLastSentTo()
    {
        return $this->container['last_sent_to'];
    }

    /**
     * Sets last_sent_to
     *
     * @param string $last_sent_to Type GPS Device of Last Sent.
     *
     * @return $this
     */
    public function setLastSentTo($last_sent_to)
    {
        $this->container['last_sent_to'] = $last_sent_to;

        return $this;
    }

    /**
     * Gets last_sent_at
     *
     * @return \DateTime
     */
    public function getLastSentAt()
    {
        return $this->container['last_sent_at'];
    }

    /**
     * Sets last_sent_at
     *
     * @param \DateTime $last_sent_at Last Time Sent To External GPS Device.
     *
     * @return $this
     */
    public function setLastSentAt($last_sent_at)
    {
        $this->container['last_sent_at'] = $last_sent_at;

        return $this;
    }

    /**
     * Gets optimized_at
     *
     * @return \DateTime
     */
    public function getOptimizedAt()
    {
        return $this->container['optimized_at'];
    }

    /**
     * Sets optimized_at
     *
     * @param \DateTime $optimized_at Last optimized at.
     *
     * @return $this
     */
    public function setOptimizedAt($optimized_at)
    {
        $this->container['optimized_at'] = $optimized_at;

        return $this;
    }

    /**
     * Gets quantities
     *
     * @return \Mapotempo\Model\V01DeliverableUnitQuantity[]
     */
    public function getQuantities()
    {
        return $this->container['quantities'];
    }

    /**
     * Sets quantities
     *
     * @param \Mapotempo\Model\V01DeliverableUnitQuantity[] $quantities quantities
     *
     * @return $this
     */
    public function setQuantities($quantities)
    {
        $this->container['quantities'] = $quantities;

        return $this;
    }

    /**
     * Gets total_cost
     *
     * @return string
     */
    public function getTotalCost()
    {
        return $this->container['total_cost'];
    }

    /**
     * Sets total_cost
     *
     * @param string $total_cost total_cost
     *
     * @return $this
     */
    public function setTotalCost($total_cost)
    {
        $this->container['total_cost'] = $total_cost;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
