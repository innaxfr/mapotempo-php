<?php
/**
 * V01Stop
 *
 * PHP version 5
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * API
 *
 * [Simplified view of domain model](https://app.mapotempo.com//api/0.1/Model-simpel.svg). ## Model Model is structured around four majors concepts: the Customer account, Destinations, Vehicles and Plannings. * `Customers`: many of objects are linked to a customer account (relating to the user calling API). The customer has many `Users`, each user has his own `api_key`. Be carefull not to confuse with following model `Destination`, `Customer`can only be created by a admin `User`. * `Destinations`: location points to visit with constraints. The same `Destination` can be visited several times : in this case several `Visits` are associated to one `Destination`. * `Vehicles`: vehicles definition are splited in two parts:  * the structural definition named `Vehicle` (car, truck, bike, consumption, etc.)  * and the vehicle usage `VehicleUsage`, a specific usage of a physical vehicle in a specific context. Vehicles can be used in many contexts called `VehicleUsageSet` (set of all vehicles usages under a context). Multiple values are only available if dedicated option for customer is active. For instance, if customer needs to use its vehicle 2 times per day (morning and evening), he needs 2 `VehicleUsageSet` called \"Morning\" and \"Evening\" : each can have different values defined for stores, rest, etc... `VehicleUsageSet` defines default values for vehicle usage. * `Plannings`: `Planning` is a set of `Routes` to `Visit` `Destinations` with `Vehicle` within a `VehicleUsageSet` context. A route is a track between all destinations reached by a vehicle (a new route is created for each customer's vehicle and a route without vehicle is created for all out-of-route destinations). By default all customer's visites are used in a planning.  ## Technical access ### Swagger descriptor This REST API is described with Swagger. The Swagger descriptor defines the request end-points, the parameters and the return values. The API can be addressed by HTTP request or with a generated client using the Swagger descriptor. ### API key All access to the API are subject to an `api_key` parameter in order to authenticate the user. This parameter can be sent with a query string on each available operation: `https://app.mapotempo.com/api/0.1/{objects}?api_key={your_personal_api_key}` ### Return The API supports several return formats: `json` and `xml` which depend of the requested extension used in url. ### I18n Functionnal textual returns are subject to translation and depend of HTTP header `Accept-Language`. HTTP error codes are not translated. ## Admin acces Using an admin `api_key` switches to advanced opperations (on `Customer`, `User`, `Vehicle`, `Profile`). Most of operations from the current api are usable either for normal user `api_key` or admin user `api_key` (not both). ## More concepts When a customer is created some objects are created by default with this new customer: * `Vehicle`: multiple, depending of the `max_vehicles` defined for customer * `DeliverableUnit`: one default * `VehicleUsageSet`: one default * `VehicleUsage`: multiple, depending of the vehicles number * `Store`: one default  ### Profiles, Layers, Routers `Profile` is a concept which allows to set several parameters for the customer: * `Layer`: which allows to choose the background map * `Router`: which allows to build route's information.  Several default profiles are available and can be listed with an admin `api_key`.  ### Tags `Tag` is a concept to filter visits and create planning only for a subset of visits. For instance, if some visits are tagged \"Monday\", it allows to create a new planning for \"Monday\" tag and use only dedicated visits. ### Zonings `Zoning` is a concept which allows to define multiple `Zones` (areas) around destinatons. A `Zone` can be affected to a `Vehicle` and if it is used into a `Planning`, all `Destinations` inside areas will be affected to the zone's vehicle (or `Route`). A polygon defining a `Zone` can be created outside the application or can be automatically generated from a planning.  ## Code samples * Create and display destinations or visits. Here some samples for these operations: [using PHP](https://app.mapotempo.com//api/0.1/examples/php/example.php), [using Ruby](https://app.mapotempo.com//api/0.1/examples/ruby/example.rb). Note you can import destinations/visits and create a planning at the same time if you know beforehand the route for each destination/visit. See the details of importDestinations operation to import your data and create a planning in only one call. * Same operations are available for stores (note you have an existing default store). * With created destinations/visits, you can create a planning (routes and stops are automatically created depending of yours vehicles and destinations/visits) * In existing planning, you have availability to move stops (which represent visits) on a dedicated route (which represent a dedicated vehicle). * With many unaffected (out-of-route) stops in a planning, you may create a zoning to move many stops in several routes. Create a zoning (you can generate zones in this zoning automatically from automatic clustering), if you apply zoning (containing zones linked to a vehicle) on your planning, all stops contained in different zones will be moved in dedicated routes.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: tech@mapotempo.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.20
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Mapotempo\Model;

use \ArrayAccess;
use \Mapotempo\ObjectSerializer;

/**
 * V01Stop Class Doc Comment
 *
 * @category Class
 * @description Fetch stop.
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class V01Stop implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'V01_Stop';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'id' => 'int',
'index' => 'int',
'status' => 'string',
'status_code' => 'string',
'eta' => '\DateTime',
'eta_formated' => '\DateTime',
'visit_ref' => 'string',
'destination_ref' => 'string',
'active' => 'bool',
'distance' => 'float',
'drive_time' => 'int',
'visit_id' => 'int',
'destination_id' => 'int',
'wait_time' => '\DateTime',
'time' => '\DateTime',
'no_path' => 'bool',
'out_of_window' => 'bool',
'out_of_capacity' => 'bool',
'unmanageable_capacity' => 'bool',
'out_of_drive_time' => 'bool',
'out_of_work_time' => 'bool',
'out_of_max_distance' => 'bool'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'id' => 'int32',
'index' => 'int32',
'status' => null,
'status_code' => null,
'eta' => 'date-time',
'eta_formated' => 'date-time',
'visit_ref' => null,
'destination_ref' => null,
'active' => null,
'distance' => 'float',
'drive_time' => 'int32',
'visit_id' => 'int32',
'destination_id' => 'int32',
'wait_time' => 'date-time',
'time' => 'date-time',
'no_path' => null,
'out_of_window' => null,
'out_of_capacity' => null,
'unmanageable_capacity' => null,
'out_of_drive_time' => null,
'out_of_work_time' => null,
'out_of_max_distance' => null    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
'index' => 'index',
'status' => 'status',
'status_code' => 'status_code',
'eta' => 'eta',
'eta_formated' => 'eta_formated',
'visit_ref' => 'visit_ref',
'destination_ref' => 'destination_ref',
'active' => 'active',
'distance' => 'distance',
'drive_time' => 'drive_time',
'visit_id' => 'visit_id',
'destination_id' => 'destination_id',
'wait_time' => 'wait_time',
'time' => 'time',
'no_path' => 'no_path',
'out_of_window' => 'out_of_window',
'out_of_capacity' => 'out_of_capacity',
'unmanageable_capacity' => 'unmanageable_capacity',
'out_of_drive_time' => 'out_of_drive_time',
'out_of_work_time' => 'out_of_work_time',
'out_of_max_distance' => 'out_of_max_distance'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
'index' => 'setIndex',
'status' => 'setStatus',
'status_code' => 'setStatusCode',
'eta' => 'setEta',
'eta_formated' => 'setEtaFormated',
'visit_ref' => 'setVisitRef',
'destination_ref' => 'setDestinationRef',
'active' => 'setActive',
'distance' => 'setDistance',
'drive_time' => 'setDriveTime',
'visit_id' => 'setVisitId',
'destination_id' => 'setDestinationId',
'wait_time' => 'setWaitTime',
'time' => 'setTime',
'no_path' => 'setNoPath',
'out_of_window' => 'setOutOfWindow',
'out_of_capacity' => 'setOutOfCapacity',
'unmanageable_capacity' => 'setUnmanageableCapacity',
'out_of_drive_time' => 'setOutOfDriveTime',
'out_of_work_time' => 'setOutOfWorkTime',
'out_of_max_distance' => 'setOutOfMaxDistance'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
'index' => 'getIndex',
'status' => 'getStatus',
'status_code' => 'getStatusCode',
'eta' => 'getEta',
'eta_formated' => 'getEtaFormated',
'visit_ref' => 'getVisitRef',
'destination_ref' => 'getDestinationRef',
'active' => 'getActive',
'distance' => 'getDistance',
'drive_time' => 'getDriveTime',
'visit_id' => 'getVisitId',
'destination_id' => 'getDestinationId',
'wait_time' => 'getWaitTime',
'time' => 'getTime',
'no_path' => 'getNoPath',
'out_of_window' => 'getOutOfWindow',
'out_of_capacity' => 'getOutOfCapacity',
'unmanageable_capacity' => 'getUnmanageableCapacity',
'out_of_drive_time' => 'getOutOfDriveTime',
'out_of_work_time' => 'getOutOfWorkTime',
'out_of_max_distance' => 'getOutOfMaxDistance'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['index'] = isset($data['index']) ? $data['index'] : null;
        $this->container['status'] = isset($data['status']) ? $data['status'] : null;
        $this->container['status_code'] = isset($data['status_code']) ? $data['status_code'] : null;
        $this->container['eta'] = isset($data['eta']) ? $data['eta'] : null;
        $this->container['eta_formated'] = isset($data['eta_formated']) ? $data['eta_formated'] : null;
        $this->container['visit_ref'] = isset($data['visit_ref']) ? $data['visit_ref'] : null;
        $this->container['destination_ref'] = isset($data['destination_ref']) ? $data['destination_ref'] : null;
        $this->container['active'] = isset($data['active']) ? $data['active'] : null;
        $this->container['distance'] = isset($data['distance']) ? $data['distance'] : null;
        $this->container['drive_time'] = isset($data['drive_time']) ? $data['drive_time'] : null;
        $this->container['visit_id'] = isset($data['visit_id']) ? $data['visit_id'] : null;
        $this->container['destination_id'] = isset($data['destination_id']) ? $data['destination_id'] : null;
        $this->container['wait_time'] = isset($data['wait_time']) ? $data['wait_time'] : null;
        $this->container['time'] = isset($data['time']) ? $data['time'] : null;
        $this->container['no_path'] = isset($data['no_path']) ? $data['no_path'] : null;
        $this->container['out_of_window'] = isset($data['out_of_window']) ? $data['out_of_window'] : null;
        $this->container['out_of_capacity'] = isset($data['out_of_capacity']) ? $data['out_of_capacity'] : null;
        $this->container['unmanageable_capacity'] = isset($data['unmanageable_capacity']) ? $data['unmanageable_capacity'] : null;
        $this->container['out_of_drive_time'] = isset($data['out_of_drive_time']) ? $data['out_of_drive_time'] : null;
        $this->container['out_of_work_time'] = isset($data['out_of_work_time']) ? $data['out_of_work_time'] : null;
        $this->container['out_of_max_distance'] = isset($data['out_of_max_distance']) ? $data['out_of_max_distance'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id
     *
     * @return int
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param int $id id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets index
     *
     * @return int
     */
    public function getIndex()
    {
        return $this->container['index'];
    }

    /**
     * Sets index
     *
     * @param int $index Stop's Index
     *
     * @return $this
     */
    public function setIndex($index)
    {
        $this->container['index'] = $index;

        return $this;
    }

    /**
     * Gets status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->container['status'];
    }

    /**
     * Sets status
     *
     * @param string $status Status of stop.
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->container['status'] = $status;

        return $this;
    }

    /**
     * Gets status_code
     *
     * @return string
     */
    public function getStatusCode()
    {
        return $this->container['status_code'];
    }

    /**
     * Sets status_code
     *
     * @param string $status_code Status code of stop.
     *
     * @return $this
     */
    public function setStatusCode($status_code)
    {
        $this->container['status_code'] = $status_code;

        return $this;
    }

    /**
     * Gets eta
     *
     * @return \DateTime
     */
    public function getEta()
    {
        return $this->container['eta'];
    }

    /**
     * Sets eta
     *
     * @param \DateTime $eta Estimated time of arrival from remote device.
     *
     * @return $this
     */
    public function setEta($eta)
    {
        $this->container['eta'] = $eta;

        return $this;
    }

    /**
     * Gets eta_formated
     *
     * @return \DateTime
     */
    public function getEtaFormated()
    {
        return $this->container['eta_formated'];
    }

    /**
     * Sets eta_formated
     *
     * @param \DateTime $eta_formated Estimated time of arrival from remote device.
     *
     * @return $this
     */
    public function setEtaFormated($eta_formated)
    {
        $this->container['eta_formated'] = $eta_formated;

        return $this;
    }

    /**
     * Gets visit_ref
     *
     * @return string
     */
    public function getVisitRef()
    {
        return $this->container['visit_ref'];
    }

    /**
     * Sets visit_ref
     *
     * @param string $visit_ref visit_ref
     *
     * @return $this
     */
    public function setVisitRef($visit_ref)
    {
        $this->container['visit_ref'] = $visit_ref;

        return $this;
    }

    /**
     * Gets destination_ref
     *
     * @return string
     */
    public function getDestinationRef()
    {
        return $this->container['destination_ref'];
    }

    /**
     * Sets destination_ref
     *
     * @param string $destination_ref destination_ref
     *
     * @return $this
     */
    public function setDestinationRef($destination_ref)
    {
        $this->container['destination_ref'] = $destination_ref;

        return $this;
    }

    /**
     * Gets active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->container['active'];
    }

    /**
     * Sets active
     *
     * @param bool $active Stop taken into account or not in the route.
     *
     * @return $this
     */
    public function setActive($active)
    {
        $this->container['active'] = $active;

        return $this;
    }

    /**
     * Gets distance
     *
     * @return float
     */
    public function getDistance()
    {
        return $this->container['distance'];
    }

    /**
     * Sets distance
     *
     * @param float $distance Distance between the stop and previous one.
     *
     * @return $this
     */
    public function setDistance($distance)
    {
        $this->container['distance'] = $distance;

        return $this;
    }

    /**
     * Gets drive_time
     *
     * @return int
     */
    public function getDriveTime()
    {
        return $this->container['drive_time'];
    }

    /**
     * Sets drive_time
     *
     * @param int $drive_time Time in seconds between the stop and previous one.
     *
     * @return $this
     */
    public function setDriveTime($drive_time)
    {
        $this->container['drive_time'] = $drive_time;

        return $this;
    }

    /**
     * Gets visit_id
     *
     * @return int
     */
    public function getVisitId()
    {
        return $this->container['visit_id'];
    }

    /**
     * Sets visit_id
     *
     * @param int $visit_id visit_id
     *
     * @return $this
     */
    public function setVisitId($visit_id)
    {
        $this->container['visit_id'] = $visit_id;

        return $this;
    }

    /**
     * Gets destination_id
     *
     * @return int
     */
    public function getDestinationId()
    {
        return $this->container['destination_id'];
    }

    /**
     * Sets destination_id
     *
     * @param int $destination_id destination_id
     *
     * @return $this
     */
    public function setDestinationId($destination_id)
    {
        $this->container['destination_id'] = $destination_id;

        return $this;
    }

    /**
     * Gets wait_time
     *
     * @return \DateTime
     */
    public function getWaitTime()
    {
        return $this->container['wait_time'];
    }

    /**
     * Sets wait_time
     *
     * @param \DateTime $wait_time Time before delivery.
     *
     * @return $this
     */
    public function setWaitTime($wait_time)
    {
        $this->container['wait_time'] = $wait_time;

        return $this;
    }

    /**
     * Gets time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->container['time'];
    }

    /**
     * Sets time
     *
     * @param \DateTime $time Arrival planned at.
     *
     * @return $this
     */
    public function setTime($time)
    {
        $this->container['time'] = $time;

        return $this;
    }

    /**
     * Gets no_path
     *
     * @return bool
     */
    public function getNoPath()
    {
        return $this->container['no_path'];
    }

    /**
     * Sets no_path
     *
     * @param bool $no_path no_path
     *
     * @return $this
     */
    public function setNoPath($no_path)
    {
        $this->container['no_path'] = $no_path;

        return $this;
    }

    /**
     * Gets out_of_window
     *
     * @return bool
     */
    public function getOutOfWindow()
    {
        return $this->container['out_of_window'];
    }

    /**
     * Sets out_of_window
     *
     * @param bool $out_of_window out_of_window
     *
     * @return $this
     */
    public function setOutOfWindow($out_of_window)
    {
        $this->container['out_of_window'] = $out_of_window;

        return $this;
    }

    /**
     * Gets out_of_capacity
     *
     * @return bool
     */
    public function getOutOfCapacity()
    {
        return $this->container['out_of_capacity'];
    }

    /**
     * Sets out_of_capacity
     *
     * @param bool $out_of_capacity out_of_capacity
     *
     * @return $this
     */
    public function setOutOfCapacity($out_of_capacity)
    {
        $this->container['out_of_capacity'] = $out_of_capacity;

        return $this;
    }

    /**
     * Gets unmanageable_capacity
     *
     * @return bool
     */
    public function getUnmanageableCapacity()
    {
        return $this->container['unmanageable_capacity'];
    }

    /**
     * Sets unmanageable_capacity
     *
     * @param bool $unmanageable_capacity unmanageable_capacity
     *
     * @return $this
     */
    public function setUnmanageableCapacity($unmanageable_capacity)
    {
        $this->container['unmanageable_capacity'] = $unmanageable_capacity;

        return $this;
    }

    /**
     * Gets out_of_drive_time
     *
     * @return bool
     */
    public function getOutOfDriveTime()
    {
        return $this->container['out_of_drive_time'];
    }

    /**
     * Sets out_of_drive_time
     *
     * @param bool $out_of_drive_time out_of_drive_time
     *
     * @return $this
     */
    public function setOutOfDriveTime($out_of_drive_time)
    {
        $this->container['out_of_drive_time'] = $out_of_drive_time;

        return $this;
    }

    /**
     * Gets out_of_work_time
     *
     * @return bool
     */
    public function getOutOfWorkTime()
    {
        return $this->container['out_of_work_time'];
    }

    /**
     * Sets out_of_work_time
     *
     * @param bool $out_of_work_time out_of_work_time
     *
     * @return $this
     */
    public function setOutOfWorkTime($out_of_work_time)
    {
        $this->container['out_of_work_time'] = $out_of_work_time;

        return $this;
    }

    /**
     * Gets out_of_max_distance
     *
     * @return bool
     */
    public function getOutOfMaxDistance()
    {
        return $this->container['out_of_max_distance'];
    }

    /**
     * Sets out_of_max_distance
     *
     * @param bool $out_of_max_distance out_of_max_distance
     *
     * @return $this
     */
    public function setOutOfMaxDistance($out_of_max_distance)
    {
        $this->container['out_of_max_distance'] = $out_of_max_distance;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
