<?php
/**
 * V01VehicleWithoutVehicleUsage
 *
 * PHP version 5
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * API
 *
 * [Simplified view of domain model](https://app.mapotempo.com//api/0.1/Model-simpel.svg). ## Model Model is structured around four majors concepts: the Customer account, Destinations, Vehicles and Plannings. * `Customers`: many of objects are linked to a customer account (relating to the user calling API). The customer has many `Users`, each user has his own `api_key`. Be carefull not to confuse with following model `Destination`, `Customer`can only be created by a admin `User`. * `Destinations`: location points to visit with constraints. The same `Destination` can be visited several times : in this case several `Visits` are associated to one `Destination`. * `Vehicles`: vehicles definition are splited in two parts:  * the structural definition named `Vehicle` (car, truck, bike, consumption, etc.)  * and the vehicle usage `VehicleUsage`, a specific usage of a physical vehicle in a specific context. Vehicles can be used in many contexts called `VehicleUsageSet` (set of all vehicles usages under a context). Multiple values are only available if dedicated option for customer is active. For instance, if customer needs to use its vehicle 2 times per day (morning and evening), he needs 2 `VehicleUsageSet` called \"Morning\" and \"Evening\" : each can have different values defined for stores, rest, etc... `VehicleUsageSet` defines default values for vehicle usage. * `Plannings`: `Planning` is a set of `Routes` to `Visit` `Destinations` with `Vehicle` within a `VehicleUsageSet` context. A route is a track between all destinations reached by a vehicle (a new route is created for each customer's vehicle and a route without vehicle is created for all out-of-route destinations). By default all customer's visites are used in a planning.  ## Technical access ### Swagger descriptor This REST API is described with Swagger. The Swagger descriptor defines the request end-points, the parameters and the return values. The API can be addressed by HTTP request or with a generated client using the Swagger descriptor. ### API key All access to the API are subject to an `api_key` parameter in order to authenticate the user. This parameter can be sent with a query string on each available operation: `https://app.mapotempo.com/api/0.1/{objects}?api_key={your_personal_api_key}` ### Return The API supports several return formats: `json` and `xml` which depend of the requested extension used in url. ### I18n Functionnal textual returns are subject to translation and depend of HTTP header `Accept-Language`. HTTP error codes are not translated. ## Admin acces Using an admin `api_key` switches to advanced opperations (on `Customer`, `User`, `Vehicle`, `Profile`). Most of operations from the current api are usable either for normal user `api_key` or admin user `api_key` (not both). ## More concepts When a customer is created some objects are created by default with this new customer: * `Vehicle`: multiple, depending of the `max_vehicles` defined for customer * `DeliverableUnit`: one default * `VehicleUsageSet`: one default * `VehicleUsage`: multiple, depending of the vehicles number * `Store`: one default  ### Profiles, Layers, Routers `Profile` is a concept which allows to set several parameters for the customer: * `Layer`: which allows to choose the background map * `Router`: which allows to build route's information.  Several default profiles are available and can be listed with an admin `api_key`.  ### Tags `Tag` is a concept to filter visits and create planning only for a subset of visits. For instance, if some visits are tagged \"Monday\", it allows to create a new planning for \"Monday\" tag and use only dedicated visits. ### Zonings `Zoning` is a concept which allows to define multiple `Zones` (areas) around destinatons. A `Zone` can be affected to a `Vehicle` and if it is used into a `Planning`, all `Destinations` inside areas will be affected to the zone's vehicle (or `Route`). A polygon defining a `Zone` can be created outside the application or can be automatically generated from a planning.  ## Code samples * Create and display destinations or visits. Here some samples for these operations: [using PHP](https://app.mapotempo.com//api/0.1/examples/php/example.php), [using Ruby](https://app.mapotempo.com//api/0.1/examples/ruby/example.rb). Note you can import destinations/visits and create a planning at the same time if you know beforehand the route for each destination/visit. See the details of importDestinations operation to import your data and create a planning in only one call. * Same operations are available for stores (note you have an existing default store). * With created destinations/visits, you can create a planning (routes and stops are automatically created depending of yours vehicles and destinations/visits) * In existing planning, you have availability to move stops (which represent visits) on a dedicated route (which represent a dedicated vehicle). * With many unaffected (out-of-route) stops in a planning, you may create a zoning to move many stops in several routes. Create a zoning (you can generate zones in this zoning automatically from automatic clustering), if you apply zoning (containing zones linked to a vehicle) on your planning, all stops contained in different zones will be moved in dedicated routes.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: tech@mapotempo.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.20
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Mapotempo\Model;

use \ArrayAccess;
use \Mapotempo\ObjectSerializer;

/**
 * V01VehicleWithoutVehicleUsage Class Doc Comment
 *
 * @category Class
 * @package  Mapotempo
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class V01VehicleWithoutVehicleUsage implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'V01_VehicleWithoutVehicleUsage';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'id' => 'int',
'ref' => 'string',
'name' => 'string',
'capacity_unit' => 'string',
'speed_multiplier' => 'float',
'contact_email' => 'string',
'phone_number' => 'string',
'emission' => 'float',
'consumption' => 'int',
'color' => 'string',
'fuel_type' => 'string',
'router_id' => 'int',
'router_dimension' => 'string',
'router_options' => '\Mapotempo\Model\V01RouterOptions',
'max_distance' => 'int',
'tag_ids' => 'int[]',
'devices' => 'object'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'id' => 'int32',
'ref' => null,
'name' => null,
'capacity_unit' => null,
'speed_multiplier' => 'float',
'contact_email' => null,
'phone_number' => null,
'emission' => 'float',
'consumption' => 'int32',
'color' => null,
'fuel_type' => null,
'router_id' => 'int32',
'router_dimension' => null,
'router_options' => null,
'max_distance' => 'int32',
'tag_ids' => 'int32',
'devices' => null    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
'ref' => 'ref',
'name' => 'name',
'capacity_unit' => 'capacity_unit',
'speed_multiplier' => 'speed_multiplier',
'contact_email' => 'contact_email',
'phone_number' => 'phone_number',
'emission' => 'emission',
'consumption' => 'consumption',
'color' => 'color',
'fuel_type' => 'fuel_type',
'router_id' => 'router_id',
'router_dimension' => 'router_dimension',
'router_options' => 'router_options',
'max_distance' => 'max_distance',
'tag_ids' => 'tag_ids',
'devices' => 'devices'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
'ref' => 'setRef',
'name' => 'setName',
'capacity_unit' => 'setCapacityUnit',
'speed_multiplier' => 'setSpeedMultiplier',
'contact_email' => 'setContactEmail',
'phone_number' => 'setPhoneNumber',
'emission' => 'setEmission',
'consumption' => 'setConsumption',
'color' => 'setColor',
'fuel_type' => 'setFuelType',
'router_id' => 'setRouterId',
'router_dimension' => 'setRouterDimension',
'router_options' => 'setRouterOptions',
'max_distance' => 'setMaxDistance',
'tag_ids' => 'setTagIds',
'devices' => 'setDevices'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
'ref' => 'getRef',
'name' => 'getName',
'capacity_unit' => 'getCapacityUnit',
'speed_multiplier' => 'getSpeedMultiplier',
'contact_email' => 'getContactEmail',
'phone_number' => 'getPhoneNumber',
'emission' => 'getEmission',
'consumption' => 'getConsumption',
'color' => 'getColor',
'fuel_type' => 'getFuelType',
'router_id' => 'getRouterId',
'router_dimension' => 'getRouterDimension',
'router_options' => 'getRouterOptions',
'max_distance' => 'getMaxDistance',
'tag_ids' => 'getTagIds',
'devices' => 'getDevices'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    const ROUTER_DIMENSION_TIME = 'time';
const ROUTER_DIMENSION_DISTANCE = 'distance';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getRouterDimensionAllowableValues()
    {
        return [
            self::ROUTER_DIMENSION_TIME,
self::ROUTER_DIMENSION_DISTANCE,        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['ref'] = isset($data['ref']) ? $data['ref'] : null;
        $this->container['name'] = isset($data['name']) ? $data['name'] : null;
        $this->container['capacity_unit'] = isset($data['capacity_unit']) ? $data['capacity_unit'] : null;
        $this->container['speed_multiplier'] = isset($data['speed_multiplier']) ? $data['speed_multiplier'] : null;
        $this->container['contact_email'] = isset($data['contact_email']) ? $data['contact_email'] : null;
        $this->container['phone_number'] = isset($data['phone_number']) ? $data['phone_number'] : null;
        $this->container['emission'] = isset($data['emission']) ? $data['emission'] : null;
        $this->container['consumption'] = isset($data['consumption']) ? $data['consumption'] : null;
        $this->container['color'] = isset($data['color']) ? $data['color'] : null;
        $this->container['fuel_type'] = isset($data['fuel_type']) ? $data['fuel_type'] : null;
        $this->container['router_id'] = isset($data['router_id']) ? $data['router_id'] : null;
        $this->container['router_dimension'] = isset($data['router_dimension']) ? $data['router_dimension'] : null;
        $this->container['router_options'] = isset($data['router_options']) ? $data['router_options'] : null;
        $this->container['max_distance'] = isset($data['max_distance']) ? $data['max_distance'] : null;
        $this->container['tag_ids'] = isset($data['tag_ids']) ? $data['tag_ids'] : null;
        $this->container['devices'] = isset($data['devices']) ? $data['devices'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        $allowedValues = $this->getRouterDimensionAllowableValues();
        if (!is_null($this->container['router_dimension']) && !in_array($this->container['router_dimension'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'router_dimension', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id
     *
     * @return int
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param int $id id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets ref
     *
     * @return string
     */
    public function getRef()
    {
        return $this->container['ref'];
    }

    /**
     * Sets ref
     *
     * @param string $ref A free reference, like an external ID from other information system (forbidden characters are ./\\)
     *
     * @return $this
     */
    public function setRef($ref)
    {
        $this->container['ref'] = $ref;

        return $this;
    }

    /**
     * Gets name
     *
     * @return string
     */
    public function getName()
    {
        return $this->container['name'];
    }

    /**
     * Sets name
     *
     * @param string $name name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->container['name'] = $name;

        return $this;
    }

    /**
     * Gets capacity_unit
     *
     * @return string
     */
    public function getCapacityUnit()
    {
        return $this->container['capacity_unit'];
    }

    /**
     * Sets capacity_unit
     *
     * @param string $capacity_unit Deprecated, use capacities and deliverable_unit entity instead.
     *
     * @return $this
     */
    public function setCapacityUnit($capacity_unit)
    {
        $this->container['capacity_unit'] = $capacity_unit;

        return $this;
    }

    /**
     * Gets speed_multiplier
     *
     * @return float
     */
    public function getSpeedMultiplier()
    {
        return $this->container['speed_multiplier'];
    }

    /**
     * Sets speed_multiplier
     *
     * @param float $speed_multiplier speed_multiplier
     *
     * @return $this
     */
    public function setSpeedMultiplier($speed_multiplier)
    {
        $this->container['speed_multiplier'] = $speed_multiplier;

        return $this;
    }

    /**
     * Gets contact_email
     *
     * @return string
     */
    public function getContactEmail()
    {
        return $this->container['contact_email'];
    }

    /**
     * Sets contact_email
     *
     * @param string $contact_email contact_email
     *
     * @return $this
     */
    public function setContactEmail($contact_email)
    {
        $this->container['contact_email'] = $contact_email;

        return $this;
    }

    /**
     * Gets phone_number
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->container['phone_number'];
    }

    /**
     * Sets phone_number
     *
     * @param string $phone_number phone_number
     *
     * @return $this
     */
    public function setPhoneNumber($phone_number)
    {
        $this->container['phone_number'] = $phone_number;

        return $this;
    }

    /**
     * Gets emission
     *
     * @return float
     */
    public function getEmission()
    {
        return $this->container['emission'];
    }

    /**
     * Sets emission
     *
     * @param float $emission emission
     *
     * @return $this
     */
    public function setEmission($emission)
    {
        $this->container['emission'] = $emission;

        return $this;
    }

    /**
     * Gets consumption
     *
     * @return int
     */
    public function getConsumption()
    {
        return $this->container['consumption'];
    }

    /**
     * Sets consumption
     *
     * @param int $consumption consumption
     *
     * @return $this
     */
    public function setConsumption($consumption)
    {
        $this->container['consumption'] = $consumption;

        return $this;
    }

    /**
     * Gets color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->container['color'];
    }

    /**
     * Sets color
     *
     * @param string $color Color code with #. For instance: #FF0000
     *
     * @return $this
     */
    public function setColor($color)
    {
        $this->container['color'] = $color;

        return $this;
    }

    /**
     * Gets fuel_type
     *
     * @return string
     */
    public function getFuelType()
    {
        return $this->container['fuel_type'];
    }

    /**
     * Sets fuel_type
     *
     * @param string $fuel_type fuel_type
     *
     * @return $this
     */
    public function setFuelType($fuel_type)
    {
        $this->container['fuel_type'] = $fuel_type;

        return $this;
    }

    /**
     * Gets router_id
     *
     * @return int
     */
    public function getRouterId()
    {
        return $this->container['router_id'];
    }

    /**
     * Sets router_id
     *
     * @param int $router_id router_id
     *
     * @return $this
     */
    public function setRouterId($router_id)
    {
        $this->container['router_id'] = $router_id;

        return $this;
    }

    /**
     * Gets router_dimension
     *
     * @return string
     */
    public function getRouterDimension()
    {
        return $this->container['router_dimension'];
    }

    /**
     * Sets router_dimension
     *
     * @param string $router_dimension router_dimension
     *
     * @return $this
     */
    public function setRouterDimension($router_dimension)
    {
        $allowedValues = $this->getRouterDimensionAllowableValues();
        if (!is_null($router_dimension) && !in_array($router_dimension, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'router_dimension', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['router_dimension'] = $router_dimension;

        return $this;
    }

    /**
     * Gets router_options
     *
     * @return \Mapotempo\Model\V01RouterOptions
     */
    public function getRouterOptions()
    {
        return $this->container['router_options'];
    }

    /**
     * Sets router_options
     *
     * @param \Mapotempo\Model\V01RouterOptions $router_options router_options
     *
     * @return $this
     */
    public function setRouterOptions($router_options)
    {
        $this->container['router_options'] = $router_options;

        return $this;
    }

    /**
     * Gets max_distance
     *
     * @return int
     */
    public function getMaxDistance()
    {
        return $this->container['max_distance'];
    }

    /**
     * Sets max_distance
     *
     * @param int $max_distance Maximum achievable distance in meters
     *
     * @return $this
     */
    public function setMaxDistance($max_distance)
    {
        $this->container['max_distance'] = $max_distance;

        return $this;
    }

    /**
     * Gets tag_ids
     *
     * @return int[]
     */
    public function getTagIds()
    {
        return $this->container['tag_ids'];
    }

    /**
     * Sets tag_ids
     *
     * @param int[] $tag_ids tag_ids
     *
     * @return $this
     */
    public function setTagIds($tag_ids)
    {
        $this->container['tag_ids'] = $tag_ids;

        return $this;
    }

    /**
     * Gets devices
     *
     * @return object
     */
    public function getDevices()
    {
        return $this->container['devices'];
    }

    /**
     * Sets devices
     *
     * @param object $devices devices
     *
     * @return $this
     */
    public function setDevices($devices)
    {
        $this->container['devices'] = $devices;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
